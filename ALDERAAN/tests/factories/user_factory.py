import factory
from factory import lazy_attribute, lazy_attribute_sequence, BUILD_STRATEGY
from faker import Factory
from  django.contrib.auth import get_user_model

User = get_user_model()
faker = Factory.create()

class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User
        # django_get_or_create = ('email',) # Returns existing user if email exists
        strategy = BUILD_STRATEGY # UserFactory() Defaults to UserFactory.build(), therefore not saving the object to the db
    
    first_name = factory.Sequence(lambda _:faker.first_name()) # Generates a random first name
    last_name = lazy_attribute(lambda _:faker.last_name())
    
    # email is determined by the values of first_name and is_staff
    # Therefore, we can predict the email when providing a first_name/is_staff argument
    is_staff = False
    email = lazy_attribute(lambda o: o.first_name + "@echo-markets.com" if o.is_staff else o.first_name + "@test.com")
    date_joined = faker.date_between(start_date='-1m')
    password = factory.PostGenerationMethodCall('set_password', 'defaultpassword')