from rest_framework.permissions import BasePermission

class IsOwnerToEdit(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        elif obj == request.user:
            return True
        else:
            return False