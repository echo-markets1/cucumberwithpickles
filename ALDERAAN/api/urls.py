from django.urls import path
from ALDERAAN.api import views

urlpatterns = [
    path('signup/', views.SignupView.as_view()),
    path('verify-email/<uidb64>/<token>/', views.VerifyEmailView.as_view()),
    path('current/', views.UserDetail.as_view()),
    path('', views.UserList.as_view()),
    path('<uuid:id>/', views.UserDetail.as_view()),
    path('<str:handle>/', views.UserDetail.as_view()),
]
