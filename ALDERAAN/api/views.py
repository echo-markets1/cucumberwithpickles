from django.contrib.auth import get_user_model
from rest_framework import generics, status, views
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from ALDERAAN import serializers
from ALDERAAN.api import permissions
from ALDERAAN.core import account_management
from CORUSCANT.extensions import data_extensions as CORUSCANT_data_extension


User = get_user_model()


class SignupView(generics.CreateAPIView):
    '''
    API endpoint for user registration

    Acceptable request urls:
    GET:<base url>/signup/                            Signup endpoint
    '''
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.UserCreationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        # Views: take care of validations, permissions, everything to do with the Request and Response
        # core: everything else, endpoint/interface/event agnostic. Also triggers production of related events
        serializer.is_valid(raise_exception=True)
        success = account_management.create_new_account(
            serializer, request.data)

        headers = self.get_success_headers(serializer.data)
        if success:
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer.data, status=status.HTTP_500_INTERNAL_SERVER_ERROR, headers=headers)


class VerifyEmailView(views.APIView):
    '''
    API endpoint for email verification as a final step to confirm and activate account from email link, as well as change email

    Acceptable request urls:
    GET:<base url>/verify-email/<uidb64>/<token>/                           Email Confirmation Endpoint
    '''
    authentication_classes = []
    permission_classes = []

    def get(self, request, uidb64, token, *args, **kwargs):
        success = account_management.confirm_email(uidb64, token)
        if success:
            data = {'success': 'true'}
            return Response(data, status=status.HTTP_200_OK)
        else:
            data = {'success': 'false'}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(generics.RetrieveUpdateAPIView):
    '''
    API endpoint for 

    Acceptable request urls:
    GET, POST:<base url>/user/current/                          Endpoint for getting and updating current user info 
    GET:<base url>/user/<uuid:id>/                              Endpoint for getting other user info based on user.id
    GET:<base url>/user/<str:handle>/                           Endpoint for getting other user info based on user.handle
    '''
    permission_classes = [IsAuthenticated, permissions.IsOwnerToEdit]

    def get_queryset(self):
        return User.objects.filter(is_active=True)

    def get_serializer_class(self):
        if self.request.user == self.get_object():
            return serializers.UserReadSerializer
        else:
            return serializers.UserPublicSerializer

    def get_object(self):
        id = self.kwargs.get('id')
        if id:
            self.lookup_field = 'id'
        handle = self.kwargs.get('handle')
        if handle:
            self.lookup_field = 'handle'
        if not (id or handle) or (id or handle) == "current":
            return self.request.user
        return super(UserDetail, self).get_object()

    def _update_user(self, request):
        (user, detail) = account_management.update_user(
            request.user, request.data)
        if user is not None:
            if detail is None:
                serializer = self.get_serializer(user)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(detail, status=status.HTTP_200_OK)
        else:
            return Response(detail, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, *args, **kwargs):
        return self._update_user(request)

    def put(self, request, *args, **kwargs):
        return self._update_user(request)


class UserPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class UserList(generics.ListAPIView):
    '''
    API endpoint for Listing Users. Only publically accessible data is retrievable
    from this endpoint.

    **GET**
        query_params:
            - group (str): The group_id of the group whose members you wish to
                retrieve data for.

        Returns:
            - A serialized representation of all the Users from the query
    '''
    pagination_class = UserPagination
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.UserPublicSerializer

    def get_queryset(self):
        users = User.objects.filter(is_active=True)
        group = self.request.query_params.get('group', None)
        if not group is None:
            group_data = CORUSCANT_data_extension.get_group(
                group, self.request.user.id)
            if not group_data is None:
                member_user_ids = list(
                    map(lambda member: member["user_id"], group_data["members"]))
                users = users.filter(id__in=member_user_ids)
        return users
