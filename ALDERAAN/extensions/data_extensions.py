""" Defines interfaces for external services to query ALDERAAN data. """
from typing import Dict, Union
import uuid
from ALDERAAN.models import User
from ALDERAAN.serializers import UserReadSerializer


def get_name_and_handle_for_user_id(user_id: uuid.UUID) -> Union[Dict, None]:
    try:
        user = User.objects.get(id=user_id)
        return {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'handle': user.handle,
        }
    except:
        return None


def get_user_by_email(email: str) -> Union[Dict, None]:
    try:
        user = User.objects.get(email=email)
        return UserReadSerializer(user).data
    except:
        return None


def get_user_by_id(user_id: uuid.UUID) -> Union[Dict, None]:
    try:
        user = User.objects.get(id=user_id)
        return UserReadSerializer(user).data
    except:
        return None
