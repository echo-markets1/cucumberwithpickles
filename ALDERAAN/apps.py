from django.apps import AppConfig


class AlderaanConfig(AppConfig):
    name = 'ALDERAAN'
