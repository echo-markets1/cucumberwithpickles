import json
import requests
from django.conf import settings


def _send_email(template_number, reciever_name, reciever_email, sender_name="Echo Markets Team", sender_email="accounts@echo-markets.com", link="", **kwargs):
    python_values = {
        "sender": {
            "name": sender_name,
            "email": sender_email
        },
        "to": [{
            "name": reciever_name,
            "email": reciever_email
        }],
        "replyTo": {
            "email": sender_email,
            "name": sender_name
        },
        "templateId": template_number,
        # (int) find template_number to the right of the template name at sendinblue.com under transactional/templates
        "params": {
            "LINK": link,
            "NAME": reciever_name
        }
    }

    json_values = json.dumps(python_values)
    url = "https://api.sendinblue.com/v3/smtp/email"
    headers = {
        'accept': "application/json",
        'content-type': "application/json",
        'api-key': settings.SENDINBLUE_API_KEY
    }
    response = requests.request("POST", url, data=json_values, headers=headers)
    if response.status_code == 201:
        return True
    else:
        return False


def account_creation_send_email(reciever_name, reciever_email, link):
    template_number = 5
    return _send_email(template_number=template_number, reciever_name=reciever_name, reciever_email=reciever_email, link=link)


def email_change_send_email(reciever_name, reciever_email, link):
    template_number = 3
    return _send_email(template_number=template_number, reciever_name=reciever_name, reciever_email=reciever_email, link=link)
