from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    """
    This migration is very important - it create case-insensitive uniqueness handle field
    """

    dependencies = [
        ('ALDERAAN', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            sql=r'CREATE UNIQUE INDEX handle_upper_idx ON alderaan_user(UPPER(handle));',
            reverse_sql=r'DROP INDEX handle_upper_idx;'
        ),
    ]
