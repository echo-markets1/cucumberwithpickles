from typing import Dict
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from ALDERAAN import serializers
from ALDERAAN.external_integrations import emailer
from ALDERAAN.core.tokens import account_activation_token

User = get_user_model()


def _generate_confirmation_link(user: User):
    # TODO config for prod
    domain = 'localhost:8080'
    protocol = 'https://'
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    link = protocol + domain + '/verify-email/' + uid + '/' + token
    return link


def _generate_user_handle(first_name: str, last_name: str):
    handle_base = f"{first_name.replace(' ', '_')}{last_name.replace(' ', '_')}"
    # max handle length = 75, + max 9999 trailing int (4 char) and _ (1 char) = 70
    handle_base = handle_base[:70]
    for index in range(9999):
        handle = f"{handle_base}_{index}"
        if not User.objects.filter(handle=handle).exists():
            return handle
    # If no success creating handle
    return None


def create_new_account(serializer, data):
    if not data['password1'] == data['password2']:
        return False
    try:
        validate_password(password=data['password1'])
    except ValidationError as ve:
        return (None, {"password1": ve})

    handle = _generate_user_handle(data['first_name'], data['last_name'])
    if not handle is None:
        return False
    try:
        user = serializer.save(handle=handle)
        user.set_password(data['password1'])
        user.is_active = False
        user.save()
        link = _generate_confirmation_link(user)
        if emailer.account_creation_send_email(reciever_name=user.first_name, reciever_email=user.email, link=link):
            # PRODUCE EVENT: Account Creation
            return True
        else:
            user.delete()
            # PRODUCE EVENT: Account Creation Failed (Emailer)
            return False
    except:
        return False


def confirm_email(uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        if not user.is_active:
            user.is_active = True
            user.save()
            # PRODUCE EVENT: Account Activation
            return True
        else:
            user.email = user.next_new_email
            user.save()
            # PRODUCE EVENT: Change email confirmed
            return True
    else:
        return False


def update_user(user: User, data: Dict):
    # Update email or password. Should not be updated in combination with other
    # fields, and will return immediately if so.
    if ('new_email_1' or 'new_password_1') and 'current_password' in data:
        if not authenticate(username=user.email, password=data['current_password']):
            return (None, {"current_password": "Your password was entered incorrectly"})
        if 'new_email_1' in data:
            new_email_1 = data['new_email_1']
            if new_email_1 == user.email:
                return (None, {"new_email_1": "The new email provided must be different than the current email"})
            new_email_2 = data['new_email_2']
            if new_email_1 != new_email_2:
                return (None, {"new_email_2": "Your new emails did not match"})
            if User.objects.filter(email=new_email_1).exists():
                return (None, {"new_email_1": "The email provided is already registered to an account"})
            link = _generate_confirmation_link(user)
            if emailer.email_change_send_email(reciever_name=user.first_name, reciever_email=new_email_1, link=link):
                # PRODUCE EVENT: Update User email, confirmation sent
                user.next_new_email = new_email_1
                user.save()
                return (user, {"success": "New email added, please confirm with the link in your inbox"})
            return (None, {"failure": "An issue occurred sending the confirmation email"})
        elif 'new_password_1' in data:
            if data['new_password_1'] != data['new_password_2']:
                return (None, {"new_password_2": "Your new passwords did not match"})
            try:
                validate_password(password=data['new_password_1'], user=user)
            except ValidationError as ve:
                return (None, {"new_password_1": ve})
            user.set_password(data['new_password_1'])
            user.save()
            # PRODUCE EVENT: Update User password
            return (user, {"success": "Your password has been changed successfully"})

    user_was_updated = False
    # Update fields on the User model
    if ('first_name' in data) or ('last_name' in data) or ('handle' in data) or ('description' in data):
        serializer = serializers.UserWriteSerializer(
            user, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user.refresh_from_db()
        user_was_updated = True
        # PRODUCE EVENT: Update User fields

    # Update fields on the Profile model
    if 'profile' in data:
        serializer = serializers.UserProfileSerializer(
            user.profile, data=data['profile'], partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user.refresh_from_db()
        user_was_updated = True
        # PRODUCE EVENT: Update User profile

    # Update fields on the Settings model
    if 'settings' in data:
        serializer = serializers.UserSettingsSerializer(
            user.settings, data=data['settings'], partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user.refresh_from_db()
        user_was_updated = True
        # PRODUCE EVENT: Update User profile

    return (user, None) if user_was_updated else (None, {"failure": "Insufficient data to update the User."})
