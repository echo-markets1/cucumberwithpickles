from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.urls import include, path

urlpatterns = [
    # Browseable API and v1, uses sessions
    path("gui-auth/", include("rest_framework.urls")),
    # Token login and refresh for JWT
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]