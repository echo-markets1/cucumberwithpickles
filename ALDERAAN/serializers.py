from rest_framework import serializers
from django.contrib.auth import get_user_model
from ALDERAAN.models import Profile, Settings


class UserCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'email',
            'first_name',
            'last_name',
        ]


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'user',
            'date_of_birth',
            'gender',
            'gender_options',
            'avatar_bg_hex',
            'avatar_bg_hex_options',
            'avatar_img',
            'avatar_img_options',
            'reason_for_investing',
            'marital_status',
            'marital_status_options',
            'disney_princess',
            'disney_princess_options',
            'education_level',
            'education_level_options',
            'household_income_level',
            'household_income_level_options',
            'residential_status',
            'residential_status_options',
            'preferred_olympics',
            'preferred_olympics_options',
            'number_of_household_adults',  # positive integer
            'number_of_household_children',  # positive integer
            'occupation',
        ]

    def _get_field_options(self, old_choices):
        choices = []
        for choice in old_choices:
            choices.append(
                {'value': choice[0],  'text': choice[1]}
            )
        return choices

    user = serializers.PrimaryKeyRelatedField(read_only=True)

    gender = serializers.ChoiceField(
        default=Profile.Gender.NONE_SELECTED, choices=Profile.Gender.choices)
    gender_options = serializers.SerializerMethodField(read_only=True)

    def get_gender_options(self, obj):
        return self._get_field_options(Profile.Gender.choices)

    avatar_bg_hex_options = serializers.SerializerMethodField(read_only=True)

    def get_avatar_bg_hex_options(self, obj):
        return self._get_field_options(Profile.AvatarBgHex.choices)

    avatar_img_options = serializers.SerializerMethodField(read_only=True)

    def get_avatar_img_options(self, obj):
        return self._get_field_options(obj.avatar_img_choices)

    marital_status = serializers.ChoiceField(
        default=Profile.MaritalStatus.NONE_SELECTED, choices=Profile.MaritalStatus.choices)
    marital_status_options = serializers.SerializerMethodField(read_only=True)

    def get_marital_status_options(self, obj):
        return self._get_field_options(Profile.MaritalStatus.choices)

    disney_princess = serializers.ChoiceField(
        default=Profile.DisneyPrincess.NONE_SELECTED, choices=Profile.DisneyPrincess.choices)
    disney_princess_options = serializers.SerializerMethodField(read_only=True)

    def get_disney_princess_options(self, obj):
        return self._get_field_options(Profile.DisneyPrincess.choices)

    education_level = serializers.ChoiceField(
        default=Profile.EducationLevel.NONE_SELECTED, choices=Profile.EducationLevel.choices)
    education_level_options = serializers.SerializerMethodField(read_only=True)

    def get_education_level_options(self, obj):
        return self._get_field_options(Profile.EducationLevel.choices)

    household_income_level = serializers.ChoiceField(
        default=Profile.HouseholdIncomeLevel.NONE_SELECTED, choices=Profile.HouseholdIncomeLevel.choices)
    household_income_level_options = serializers.SerializerMethodField(
        read_only=True)

    def get_household_income_level_options(self, obj):
        return self._get_field_options(Profile.HouseholdIncomeLevel.choices)

    residential_status = serializers.ChoiceField(
        default=Profile.ResidentialStatus.NONE_SELECTED, choices=Profile.ResidentialStatus.choices)
    residential_status_options = serializers.SerializerMethodField(
        read_only=True)

    def get_residential_status_options(self, obj):
        return self._get_field_options(Profile.ResidentialStatus.choices)

    preferred_olympics = serializers.ChoiceField(
        default=Profile.PreferredOlympics.NONE_SELECTED, choices=Profile.PreferredOlympics.choices)
    preferred_olympics_options = serializers.SerializerMethodField(
        read_only=True)

    def get_preferred_olympics_options(self, obj):
        return self._get_field_options(Profile.PreferredOlympics.choices)


class UserSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = '__all__'

    user = serializers.PrimaryKeyRelatedField(read_only=True)


class UserReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'id',
            'email',
            'handle',
            'first_name',
            'last_name',
            'short_name',
            'full_name',
            'date_joined',
            'has_completed_onboarding',
            'has_completed_user_agreement',
            'description',
            'can_publish',
            'is_staff',
            'last_login',

            'settings',
            'profile'
        ]

    settings = UserSettingsSerializer()
    profile = UserProfileSerializer()
    short_name = serializers.SerializerMethodField(read_only=True)
    full_name = serializers.SerializerMethodField(read_only=True)

    def get_short_name(self, instance):
        return instance.get_short_name()

    def get_full_name(self, instance):
        return instance.get_full_name()


class UserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'id',
            'email',
            'handle',
            'first_name',
            'last_name',
            'has_completed_onboarding',
            'has_completed_user_agreement',
            'description',
            'can_publish',
            'is_staff',
        ]


class UserPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'id',
            'email',
            'handle',
            'first_name',
            'last_name',
            'short_name',
            'full_name',
            'description',
            'avatar_bg_hex',
            'avatar_img'
        ]

    short_name = serializers.SerializerMethodField(read_only=True)
    full_name = serializers.SerializerMethodField(read_only=True)
    avatar_bg_hex = serializers.SerializerMethodField(read_only=True)
    avatar_img = serializers.SerializerMethodField(read_only=True)

    def get_short_name(self, instance):
        return instance.get_short_name()

    def get_full_name(self, instance):
        return instance.get_full_name()

    def get_avatar_bg_hex(self, instance):
        return instance.profile.avatar_bg_hex

    def get_avatar_img(self, instance):
        return instance.profile.avatar_img
