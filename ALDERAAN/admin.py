from django.contrib import admin
from ALDERAAN.models import User, Profile, Settings

admin.site.register(User)
admin.site.register(Profile)
admin.site.register(Settings)