from __future__ import unicode_literals

import uuid

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from ALDERAAN.managers import UserManager
from ALDERAAN.validators import validate_handle
from ALDERAAN.fields import CICharField


class User(AbstractBaseUser, PermissionsMixin):
    """
    Foundational user object, containing essential account information.

    Foreign Key Relationships: 'path.to.Model' - Relationship Type - "Related Name"
        :model:'ALDERAAN.Settings' - One User to One Settings - "settings"
        :model:'ALDERAAN.Profile' - One User to One Profile - "profile"
    """
    # Overrides Django's default username field to be equal to email. This implementation also has an additional, secondary username field
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    # Defines the Manager class used to perform operations on this data model
    objects = UserManager()

    # FIELDS
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(
        _('email address'),
        unique=True,
        help_text=_('Required. Please use a valid email address.'),
        max_length=254
    )
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    # Upon account activation this is set to the user first and last name plus several random numbers, but the user can change it
    handle = CICharField(
        _('handle'),
        unique=True,
        max_length=75,
        validators=[validate_handle]
    )
    # Automatically set when user object created
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    # To reference new email after existing users hit the update email link
    next_new_email = models.EmailField(
        _('new email address'), max_length=254, default='empty@empty.com', blank=True)
    # See if the user completed the onboarding guide. Can be set to false to rerun onboareding if the onboarding needs to change/be added to
    has_completed_onboarding = models.BooleanField(
        _('onboarding'), default=False)
    # Use to prevent user from continuing if false. Can be set to false if the user agreement needs to changed.
    has_completed_user_agreement = models.BooleanField(
        _('user_agreement'), default=False)
    # Description of the user can set, public to other users
    description = models.CharField(
        _('description'),
        max_length=4000,
        default='',
        blank=True,
        help_text=_(
            'Public bio that will be visible to other users'
        ),
    )
    # Has bucket publishing privileges when true
    can_publish = models.BooleanField(default=True)
    # is_staff defines if the user has admin priviledges
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    # Instead of deleting users, set is_active to false
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    # password, last_login are in the superclass AbstractBaseUser. is_superuser is in the PermissionsMixin

    # METHODS

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        short_name = '%s %s' % (self.first_name, self.last_name[0])
        return short_name.strip()


class Settings(models.Model):
    """
    Stores data about the user's settings

    Foreign Key Relationships: 'path.to.Model' - Relationship Type - "Related Name"
        :model:'accounts.User' - One User to One Settings - "user"

    """
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='settings',
    )

    @receiver(post_save, sender=User)
    def create_user_settings(sender, instance, created, **kwargs):
        if created:
            Settings.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_settings(sender, instance, **kwargs):
        instance.settings.save()

    # FIELDS
    # Designates whether users want to recieve notifications
    alerts_on = models.BooleanField(default=True)

    # METHODS
    def turn_on_alerts(self):
        self.alerts_on = True

    def turn_off_alerts(self):
        self.alerts_on = False


class Profile(models.Model):
    """
    Stores data about the user's settings

    """
    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    # FIELDS
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='profile',
    )
    date_of_birth = models.DateField(_('date of birth'), null=True)

    class Gender(models.TextChoices):
        """ User's gender """
        NONE_SELECTED = '', _('Select a gender')
        MALE = 'M', _('Male')
        FEMALE = 'F', _('Female')
        OTHER = 'O', _('Other')
    gender = models.CharField(
        max_length=1,
        choices=Gender.choices,
        default=Gender.NONE_SELECTED,
    )

    class AvatarBgHex(models.TextChoices):
        """ Hex code for the Background color of the user avatar """
        GREEN = "#93C3B3", _('Green')
        BLUE = "#91BBDF", _('Blue')
        PURPLE = "#AAA0C8", _('Purple')
        PINK = "#CF9CBB", _('Pink')
        ORANGE = "#E49081", _('Orange')
        YELLOW = "#EBBC70", _('Yellow')
        WHITE = "#FFFFFF", _('White')

    # The hex code for the background color of the user's profile img
    avatar_bg_hex = models.CharField(
        max_length=7,
        choices=AvatarBgHex.choices,
        default=AvatarBgHex.WHITE
    )

    __avatar_img_choices = [(str(x), f"avatar_{x}") for x in range(1, 51)]

    # The number corresponding to the avatar icon for the user's profile img.
    # Their are 50 icons that can currently be selected by the user, indexed
    # starting at 1.
    #
    # Using a charfield so that in the future, if/when user's can upload their
    # own image, this can correspond to the file name/path to identify the image
    # (probably in S3).
    avatar_img = models.CharField(
        max_length=2,
        choices=__avatar_img_choices,
        blank=True,
    )

    @property
    def avatar_img_choices(self):
        return self.__avatar_img_choices

    reason_for_investing = models.CharField(
        max_length=200,
        blank=True,
    )

    class MaritalStatus(models.TextChoices):
        """ User's marital status """
        NONE_SELECTED = '', _('Select your marital status')
        SINGLE = 'S', _('Single')
        MARRIED = 'M', _('Married')
        LIVING_TOGETHER = 'L', _('Living together')
        NO_LONGER_MARRIED = 'N', _('No longer married')
        WIDOWED = 'W', _("Widowed")
    marital_status = models.CharField(
        max_length=1,
        choices=MaritalStatus.choices,
        default=MaritalStatus.NONE_SELECTED,
    )

    class DisneyPrincess(models.TextChoices):
        """ User's favorite disney princess """
        NONE_SELECTED = '', _('Select a disney princess')
        SNOW_WHITE = 'S', _('Snow White (Snow White and the Seven Dwarfs)')
        CINDERELLA = 'C', _('Cinderella (Cinderella)')
        AURORA = 'AU', _('Aurora (Sleeping Beauty)')
        ARIEL = 'AR', _('Ariel (The Little Mermaid)')
        BELLE = 'B', _('Belle (Beauty and the Beast)')
        JASMINE = 'J', _('Jasmine (Aladdin)')
        POCAHONTAS = 'P', _('Pocahontas (Pocahontas)')
        MULAN = 'M', _('Mulan (Mulan)')
        TIANA = 'T', _('Tiana (The Princess and the Frog)')
        RAPUNZEL = 'R', _('Rapunzel (Tangled)')
        MERIDA = 'ME', _('Merida (Brave)')
        MOANA = 'MO', _('Moana (Moana)')
    disney_princess = models.CharField(
        max_length=2,
        choices=DisneyPrincess.choices,
        default=DisneyPrincess.NONE_SELECTED,
    )

    class EducationLevel(models.TextChoices):
        """ User's education level """
        NONE_SELECTED = '', _('Select an education level')
        SOME_HIGH_SCHOOL = 'HS', _('Some high school or less')
        HIGH_SCHOOL_GRADUATE = 'HG', _(
            'High school graduate or equivalent (GED)')
        SOME_COLLEGE_NO_DEGREE = 'CN', _('Some college, no degree')
        SOME_COLLEGE_IN_PROGRESS = 'CI', _('Some college, in progress')
        BACHELORS = 'B', _('College graduate, (Bachelors degree)')
        GRADUATE_SCHOOL_NO_DEGREE = 'GN', _('Graduate school, no degree')
        GRADUATE_SCHOOL_IN_PROGRESS = 'GI', _('Graduate school, in progress')
        MASTERS = 'M', _('Masters degree')
        PROFESSIONAL_DEGREE = 'P', _('Professional degree')
        DOCTORATE = 'D', _('Doctorate')
    education_level = models.CharField(
        max_length=2,
        choices=EducationLevel.choices,
        default=EducationLevel.NONE_SELECTED,
    )

    class HouseholdIncomeLevel(models.TextChoices):
        """ User's household income level, multiply numbers by 25k to get lower bound of the range """
        NONE_SELECTED = '', _('Select a household income level')
        ZERO = '0', _('Less than $25,000 per year')
        ONE = '1', _('$25,000 - $49,999 per year')
        TWO = '2', _('$50,000 - $74,999 per year')
        THREE = '3', _('$75,000 - $99,999 per year')
        FOUR = '4', _('$100,000 - $124,999 per year')
        FIVE = '5', _('$125,000 - $149,999 per year')
        SIX = '6', _('$150,000 - $174,999 per year')
        SEVEN = '7', _('$175,000 - $199,999 per year')
        EIGHT = '8', _('$200,000 or more per year')
    household_income_level = models.CharField(
        max_length=1,
        choices=HouseholdIncomeLevel.choices,
        default=HouseholdIncomeLevel.NONE_SELECTED,
    )

    class ResidentialStatus(models.TextChoices):
        """ User's residential status"""
        NONE_SELECTED = '', _('Select a residential status')
        PARENTS_OR_RELATVIES = 'PR', _('Living with parents or relatives')
        COUCH = 'CS', _('Couch surfing')
        SPLITTING_RENT = 'SR', _('Renting, splitting with others')
        RENTING = 'RE', _('Renting my own place')
        CAMPUS = 'CH', _('Campus housing')
        CONDO = 'CO', _('I own an condo')
        HOUSE = 'HO', _('I own a house')
    residential_status = models.CharField(
        max_length=2,
        choices=ResidentialStatus.choices,
        default=ResidentialStatus.NONE_SELECTED,
    )

    class PreferredOlympics(models.TextChoices):
        """ User's residential status"""
        NONE_SELECTED = '', _('Select your preferred olympic games')
        SUMMER = 'S', _('Summer olympic games')
        WINTER = 'W', _('Winter olympic games')
        ROMAN = 'R', _(
            'The olympics are soft, we should bring back the Roman Gladiator Games')
    preferred_olympics = models.CharField(
        max_length=1,
        choices=PreferredOlympics.choices,
        default=PreferredOlympics.NONE_SELECTED,
    )

    number_of_household_adults = models.PositiveIntegerField(default=0)
    number_of_household_children = models.PositiveIntegerField(default=0)

    occupation = models.CharField(
        max_length=200,
        blank=True,
    )
