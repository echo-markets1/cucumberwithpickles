from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_handle(handle):
    for substring in handle.split('_'):
        if not substring.isalnum():
            raise ValidationError(
                _('User handles must only consist of letters, numbers, or underscores'),
                params={'handle': handle},
            )
