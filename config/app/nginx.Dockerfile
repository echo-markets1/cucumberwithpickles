# Inspired by https://github.com/kokospapa8/ecs-fargate-sample-app/blob/master/config/app/Dockerfile_nginx
FROM nginx

COPY ./config/nginx/app.conf /etc/nginx/conf.d/app.conf

EXPOSE 80

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]