# Creating image based on official python3 image
FROM python:3.8.3

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# Creating and putting configurations
RUN mkdir /config /config/app
COPY config/app* /config/app/
ENV CONFIG_ROOT=/config
ENV APP_CONFIG_ROOT=$CONFIG_ROOT/app
RUN chmod +x $APP_CONFIG_ROOT/app_entrypoint.sh

# Installing all python dependencies
COPY requirements.txt /config
RUN pip install -r /config/requirements.txt

# Open port 8000 to outside world
EXPOSE 8000

# When container starts, this script will be executed.
# Note that it is NOT executed during building
CMD ["sh", "/config/app/app_entrypoint.sh"]

# Creating and putting application inside container
# and setting it to working directory (meaning it is going to be default)
# Copy project files and folders to the current working directory (i.e. 'app' folder)
ENV SRC_ROOT=/src
ENV APP_SRC_ROOT=$SRC_ROOT/app
RUN mkdir /src $APP_SRC_ROOT
COPY ALDERAAN $APP_SRC_ROOT/ALDERAAN
COPY CORELLIA $APP_SRC_ROOT/CORELLIA
COPY CORUSCANT $APP_SRC_ROOT/CORUSCANT
COPY ENDOR $APP_SRC_ROOT/ENDOR
COPY HOTH $APP_SRC_ROOT/HOTH
COPY ILUM $APP_SRC_ROOT/ILUM
COPY SCARIF $APP_SRC_ROOT/SCARIF
COPY cucumberwithpickles $APP_SRC_ROOT/cucumberwithpickles
COPY manage.py $APP_SRC_ROOT

WORKDIR $APP_SRC_ROOT