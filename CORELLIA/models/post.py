from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _


class ReactingUser(models.Model):
    # Proxy User model
    user_id = models.UUIDField(primary_key=True, editable=False)


class Group(models.Model):
    # Proxy for the CORUSCANT.Group model
    # Enables easier querying of group / post relationships
    id = models.UUIDField(primary_key=True, editable=False)


class Transaction(models.Model):
    # Proxy for the ENDOR.Transaction model
    # Enables easier querying of group / post relationships
    id = models.CharField(max_length=40, primary_key=True, editable=False)


class Post(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    # Content of the post. For now, arbitrarily set at 1000 chars
    message = models.TextField(max_length=1000)

    # This is the user.id of the author
    author_id = models.UUIDField(editable=False)

    # If a post was made within a group, this field tracks which group the post
    # was made in.
    group = models.ForeignKey(
        Group, null=True, on_delete=models.PROTECT, related_name="posts")

    # If a post is in response to a transaction, tracks the transaction the post
    # is in response to.
    transaction = models.ForeignKey(
        Transaction, null=True, on_delete=models.PROTECT, related_name="replies")

    # Timestamp of when the post was made
    created_at = models.DateTimeField(auto_now_add=True)

    # Recursive relationship for subposts. This should be only allowed one level down
    superpost = models.ForeignKey(
        'self', on_delete=models.PROTECT, null=True, related_name='replies')

    # Tracks users that like the post
    likes = models.ManyToManyField(ReactingUser, related_name='post_like')

    # Allows us to pin a post to a top of a thread. Only admins or owners can pin posts
    pinned = models.BooleanField(default=False)

    # Posts should never be deleted, but rather archived
    class Status(models.TextChoices):
        PUBLIC = 'P', _('Post is active and shown in threads')
        ARCHIVED = 'A', _('Post has been removed from view')
    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        default=Status.PUBLIC
    )

    class Meta:
        ordering = ['-created_at']

# Coming soon:

# Formated message field (allow for italics etc)

# Integrate trade notifications into posts/discussion thread?

# Reactions other than likes

# add inline content: graphs, photos, videos, buckets, stocks, users, trades, block quotes/posts
