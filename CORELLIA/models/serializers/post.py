from django.db.models.expressions import F
from rest_framework import serializers
from CORELLIA.models import Post
from django.db.models import Count
from ALDERAAN.extensions import data_extensions as alderaan_data_extension


class AnnotatedListSerializer(serializers.ListSerializer):
    '''
    This class, when set as another serializer's list_serializer_class, will
    add counts as additional columns on the queryset returned. Having this happen here
    (as opposed to as a SerializerMethodField) allows us to order the results returned
    by, for example, likes_count. In this case, due to pagination issues, this class is
    only applied to replies

    In future, we may want to limit the number of subposts, and then additional posts must
    be accessed through the superpost's detail endpoint
    '''

    def to_representation(self, data):
        data = data.annotate(
            likes_count=Count('likes'),
            replies_count=Count('replies')
        ).annotate(total_activity=F('likes_count') + F('replies_count'))
        return super(AnnotatedListSerializer, self).to_representation(data)


class ReplyPostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        list_serializer_class = AnnotatedListSerializer
        fields = [
            'id',
            'message',
            'author_id',
            'author_info',
            'group',
            'created_at',
            'superpost',
            'likes',
            'likes_count',
        ]
        ordering = ['created_at']

    group = serializers.PrimaryKeyRelatedField(read_only=True)
    author_info = serializers.SerializerMethodField(read_only=True)
    superpost = serializers.PrimaryKeyRelatedField(read_only=True)
    likes = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)

    def get_author_info(self, obj):
        # Uses data extension to basic author info (name etc)
        return alderaan_data_extension.get_name_and_handle_for_user_id(user_id=obj.author_id)

    def get_likes(self, obj):
        # Extracts user_ids from ReactingUsers in the many-to-many relationship
        user_ids = []
        for reacting_user in obj.likes.all():
            user_ids.append(reacting_user.user_id)
        return user_ids

    # Allows us to serialize a single reply instance with the likes count when
    # the list serializer is not invoked
    def get_likes_count(self, obj):
        return len(self.get_likes(obj))


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = [
            'id',
            'url',
            'pinned',
            'message',

            'author_id',
            'author_info',

            'group',
            'transaction',
            'created_at',
            'superpost',

            'replies',
            'replies_count',

            'likes',
            'likes_count',

            'total_activity'
        ]
        read_only_fields = ['id', 'group', 'author_id', 'pinned']

    group = serializers.PrimaryKeyRelatedField(allow_null=True, read_only=True)
    transaction = serializers.PrimaryKeyRelatedField(
        allow_null=True, read_only=True)

    url = serializers.HyperlinkedIdentityField(
        view_name='post-detail',
        lookup_field='id'
    )
    superpost = serializers.PrimaryKeyRelatedField(
        read_only=True, allow_null=True)

    replies = serializers.SerializerMethodField(read_only=True)
    replies_count = serializers.SerializerMethodField(read_only=True)

    likes = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)

    total_activity = serializers.SerializerMethodField(read_only=True)

    author_info = serializers.SerializerMethodField(read_only=True)

    def get_likes(self, obj):
        # Extracts user_ids from ReactingUsers in the many-to-many relationship
        user_ids = []
        for reacting_user in obj.likes.all():
            user_ids.append(reacting_user.user_id)
        return user_ids

    # Allows us to serialize a single post instance with the likes count when
    # the list serializer is not invoked
    def get_likes_count(self, obj):
        return len(self.get_likes(obj))

    # Allows us to ensure the proper ordering of the serialized replies
    def get_replies(self, obj):
        replies = obj.replies.order_by('created_at')
        return ReplyPostSerializer(replies, many=True).data

    # Allows us to serialize a single reply instance with the likes count when
    # the list serializer is not invoked
    def get_replies_count(self, obj):
        return len(self.get_replies(obj))

    def get_total_activity(self, obj):
        return self.get_likes_count(obj) + self.get_replies_count(obj)

    def get_author_info(self, obj):
        # Uses data extension to basic author info (name etc)
        return alderaan_data_extension.get_name_and_handle_for_user_id(user_id=obj.author_id)
