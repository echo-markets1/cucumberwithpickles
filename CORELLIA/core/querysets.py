import uuid
from django.db.models.expressions import F
from django.db.models import Q, QuerySet, Count

from CORELLIA.models import Post
from CORUSCANT.extensions import (data_extensions as CORUSCANT_data_ext,
                                  logic_extensions as CORUSCANT_logic_ext)
from ENDOR.extensions import logic_extensions as ENDOR_logic_ext


def get_public_post_queryset(user_id: uuid.UUID, transaction_id: str = None) -> QuerySet:
    """
    Retrieves the queryset of posts which are publicly visible to the user, based
    on the users membership in groups, and if they own the transaction a post
    is related too.

    Parameters:
        - user_id (uuid.UUID): The id of the user to retrieve publically visible
            posts for
        - transaction_id (str): The id of a specific transaction to retrieve posts for

    Returns:
        - QuerySet: The QuerySet of visible posts
    """
    # Only display the posts that are in groups visible to the user, or
    # unassociated with a group
    visible_group_ids = CORUSCANT_data_ext.get_all_visible_group_ids_for(
        user_id)
    qs = Post.objects.filter(
        Q(
            Q(status='P', superpost__isnull=True) &
            Q(
                Q(group__isnull=True) | Q(group__id__in=visible_group_ids)
            )
        )
    )
    if not transaction_id is None:
        # Only return transaction responses when specifically queried for.
        # The transaction is akin to the superpost, and retrieving a response
        # without the context of the transaction can be confusing
        qs = qs.filter(transaction=transaction_id)
    else:
        qs = qs.filter(transaction__isnull=True)
    return qs.annotate(
        likes_count=Count('likes'),
        replies_count=Count('replies')
    ).annotate(total_activity=F('likes_count') + F('replies_count'))


def get_group_post_queryset(group_id: uuid.UUID, transaction_id: str = None) -> QuerySet:
    """
    Retrieves the queryset of posts which are visible to a particular group.
    If a transaction_id is provided, only return the posts from that group or members
    of the group related to the transaction.

    Parameters:
        - group_id (uuid.UUID): The id of the group to retrieve posts for
        - transaction_id (str): The id of a specific transaction to retrieve posts for

    Returns:
        - QuerySet: The QuerySet of visible posts
    """
    qs = Post.objects.filter(
        status='P',
        superpost__isnull=True,
    )
    if ENDOR_logic_ext.is_transaction_visible_to_group(transaction_id, group_id):
        # Only return transaction responses when specifically queried for, and
        # if the transaction is from a portfolio in the group.
        # The transaction is akin to the superpost, and retrieving a response
        # without the context of the transaction can be confusing.
        # Return public responses to transactions as well, provided the author
        # is a member of the group
        public_transaction_responses = qs.filter(
            group__isnull=True, transaction__id=transaction_id)
        public_transaction_response_authors_with_group_membership = []
        for post in public_transaction_responses:
            if CORUSCANT_logic_ext.validate_user_is_group_member(
                group_id,
                post.author_id
            ):
                public_transaction_response_authors_with_group_membership.append(
                    post.author_id)

        public_transaction_responses_visible_to_group = Q(
            group__isnull=True,
            author_id__in=public_transaction_response_authors_with_group_membership,
            transaction__id=transaction_id
        )

        transaction_responses_from_group = Q(
            group__id=group_id,
            transaction__id=transaction_id
        )

        qs = qs.filter(public_transaction_responses_visible_to_group |
                       transaction_responses_from_group)
    else:
        qs = qs.filter(transaction__isnull=True, group__id=group_id)
    return qs.annotate(
        likes_count=Count('likes'),
        replies_count=Count('replies')
    ).annotate(total_activity=F('likes_count') + F('replies_count'))
