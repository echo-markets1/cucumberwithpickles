from typing import Tuple, Union
import uuid
from CORELLIA.models.post import Group, Post, ReactingUser, Transaction


def create_post(message: str, author_id: uuid.UUID, group_id: uuid.UUID = None,
                superpost_id: str = None, transaction_id: str = None) -> Tuple[bool, Union[str, Post]]:
    """
    Core function to manage the creation of new posts
    """
    superpost = None
    has_transaction_id = not transaction_id is None
    if not superpost_id is None:
        if has_transaction_id:
            return (False, 'You can\'t reply to a comment on a transaction')
        try:
            superpost = Post.objects.get(id=superpost_id)
            if superpost.status == 'A':
                return (False, 'No post found for the superpost_id provided')
            if superpost.superpost is not None:
                return (False, 'You can\'t reply to replies')
        except:
            return (False, 'No post found for the superpost_id provided')

    transaction = None
    if has_transaction_id:
        transaction = Transaction.objects.get_or_create(id=transaction_id)[0]

    group = None
    if not group_id is None:
        group = Group.objects.get_or_create(id=group_id)[0]

    new_post = Post.objects.create(
        message=message,
        author_id=author_id,
        group=group,
        transaction=transaction
    )
    if superpost:
        new_post.superpost = superpost
        new_post.save()
        return (True, superpost)
        # PRODUCE EVENT: Post reply created
    else:
        return (True, new_post)
        # PRODUCE EVENT: Post created


def like_post(post_id: uuid.UUID, user_id: uuid.UUID) -> Tuple[bool, str]:
    """
    Add or remove a user's like of a post
    """
    post = None
    try:
        post = Post.objects.get(id=post_id)
        if post.status == 'A':
            return (False, 'This post was archived and cannot be liked')
    except:
        return (False, 'Post was not found')
    reacting_user = ReactingUser.objects.get_or_create(user_id=user_id)[0]
    if(post.likes.filter(user_id=reacting_user.user_id).exists()):
        post.likes.remove(reacting_user)
    else:
        post.likes.add(reacting_user)
    post.save()
    # PRODUCE EVENT: Post liked
    return (True, '')


def pin_post(post_id: uuid.UUID) -> Tuple[bool, str]:
    """
    Pin or unpin post
    """
    post = None
    try:
        post = Post.objects.get(id=post_id)
        if post.status == 'A':
            return (False, 'This post was archived and cannot be pinned')
    except:
        return (False, 'Post was not found')
    post.pinned = not post.pinned
    post.save()
    # PRODUCE EVENT: Post pinned
    return (True, '')
