# Defines functions for other services/apps to request data from CORELLIA
import uuid
from typing import Dict, List, Union
from django.db.models import Count
from django.db.models.expressions import F
from CORELLIA.core.querysets import get_group_post_queryset, get_public_post_queryset
from CORELLIA.models import Group, Post, serializers


def get_group_ids_by_activity_level(descending: bool = True) -> List[uuid.UUID]:
    """
    Activity level is an aggregation of posts, replies, and likes within a group.
    This metric is used to infer engagement within the group.

    Parameters:
        - descending (bool): Determines the ordering of the groups

    Returns:
        - List[uuid.UUID]: A list of group_ids for group look up
    """
    ordering = '-total_activity' if descending else 'total_activity'
    # See https://stackoverflow.com/a/43607512 for more details on annotating
    # based on other annotated aggregates
    groups = Group.objects.all().\
        prefetch_related('posts').\
        prefetch_related('posts__likes').\
        prefetch_related('posts__replies').\
        annotate(
            num_posts=Count('posts'),
            num_likes=Count('posts__likes'),
            # replies are double counted, once as a post and once as reply, as
            # they imply conversation/dialogue in the group
            num_replies=Count('posts__replies')).\
        annotate(total_activity=F('num_posts') + F('num_likes') + F('num_replies')).\
        order_by(ordering)
    return list(map(lambda group: group.id, groups))


def get_transaction_replies(transaction_id: str, user_id: uuid.UUID, group_id: Union[str, uuid.UUID]) -> List[Dict]:
    """
    Retrieve the replies for a given transaction, based on their visibility to the
    user or group provided.  

    Parameters:
        - transaction_id (str): The transaction to retrieve replies for
        - user_id (uuid.UUID): The id of the user requesting, used to determine
            which transaction posts are visible
        - group_id (Union[str, uuid.UUID]): The id of the group a transaction is related to,
            used to determine which transaction posts are visible

    Returns:
        - List[Dict]: A list of serialized posts
    """
    replies = Post.objects.none()
    if group_id is None:
        replies = get_public_post_queryset(user_id, transaction_id)
    else:
        _group_id = group_id
        if type(group_id) is str:
            _group_id = uuid.UUID(group_id)
        replies = get_group_post_queryset(_group_id, transaction_id)
    return serializers.PostSerializer(data=replies, many=True).data


def get_num_transaction_replies(transaction_id: str, user_id: uuid.UUID, group_id: Union[str, uuid.UUID]) -> int:
    """
    Retrieve the number replies for a given transaction, based on their visibility
    to the user or group provided. Used when only the number of replies is needed
    to reduce database hits

    Parameters:
        - transaction_id (str): The transaction to retrieve replies for
        - user_id (uuid.UUID): The id of the user requesting, used to determine
            which transaction posts are visible
        - group_id (Union[str, uuid.UUID]): The id of the group a transaction is related to,
            used to determine which transaction posts are visible

    Returns:
        - int: The number of transaction posts
    """
    replies = Post.objects.none()
    if group_id is None:
        replies = get_public_post_queryset(user_id, transaction_id)
    else:
        _group_id = group_id
        if type(group_id) is str:
            _group_id = uuid.UUID(group_id)
        replies = get_group_post_queryset(_group_id, transaction_id)
    return replies.count()
