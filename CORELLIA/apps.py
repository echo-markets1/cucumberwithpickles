from django.apps import AppConfig


class CorelliaConfig(AppConfig):
    name = 'CORELLIA'
