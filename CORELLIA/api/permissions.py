from rest_framework.permissions import BasePermission
from CORUSCANT import extensions as CORUSCANT_EXTENSTIONS
from CORELLIA.models import Post


class IsMemberOfGroupToPostAndViewOrGroupIsVisibleToView(BasePermission):
    def has_permission(self, request, view):
        # get group_id url kwarg
        # ask CORUSCANT extension if group_id is valid, and user_id is a member of group_id
        if request.method == "GET":
            return CORUSCANT_EXTENSTIONS.validate_user_is_group_member_or_visible_to_view_post(
                user_id=request.user.id,
                group_id=view.kwargs['group_id']
            )
        if request.method == "POST":
            return CORUSCANT_EXTENSTIONS.validate_user_is_group_member(
                user_id=request.user.id,
                group_id=view.kwargs['group_id']
            )


class IsMemberOfGroupOrGroupIsVisibleToViewPostDetail(BasePermission):
    def has_object_permission(self, request, view, obj):
        return CORUSCANT_EXTENSTIONS.validate_user_is_group_member_or_visible_to_view_post(
            user_id=request.user.id,
            group_id=obj.group.id
        )


class IsAdminOrOwnerOfGroup(BasePermission):
    def has_permission(self, request, view):
        # get group_id url kwarg
        # ask CORUSCANT extension if group_id is valid, and user_id is and an admin or owner of group_id
        group_id = Post.objects.get(id=view.kwargs['id']).group.id
        return CORUSCANT_EXTENSTIONS.validate_user_is_group_admin_or_owner(
            user_id=request.user.id,
            group_id=group_id
        )
