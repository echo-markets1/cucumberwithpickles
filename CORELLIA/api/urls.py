from django.urls import path
from CORELLIA.api import views

urlpatterns = [
    path('group/<uuid:group_id>/',
         views.GroupPostList.as_view(), name='group-post-list'),
    path('', views.PostList.as_view(), name='post-list'),
    path('<uuid:id>/', views.PostDetail.as_view(), name='post-detail'),
    path('<uuid:id>/like/', views.LikePost.as_view(), name='like-post'),
    path('<uuid:id>/pin/', views.PinPost.as_view(), name='pin-post'),
    # posts need not be group-specific, however our current use cases are
    # new urls and view sets modeled on the above to apply posts to other domains of the platform
]
