from django.db.models import Count
from rest_framework import filters, generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from CORELLIA.api.permissions import (IsAdminOrOwnerOfGroup,
                                      IsMemberOfGroupToPostAndViewOrGroupIsVisibleToView,
                                      IsMemberOfGroupOrGroupIsVisibleToViewPostDetail)
from CORELLIA.core import post_management
from CORELLIA.core.querysets import get_group_post_queryset, get_public_post_queryset
from CORELLIA.models import Post, serializers


class PostResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    # page_size_query_param allows us to set the number of results returned on one page from client side
    # This will allow us to paginate while making it easier to not just show the user one page at a time
    max_page_size = 1000


class PostList(generics.ListCreateAPIView):
    '''
    API endpoint for viewing top level Posts visible to the user (replies are nested)
    **GET**
        query_params:
            - ordering (str): Order ascending or descending by 'created_at',
                'likes_count', 'replies_count', & 'total_activity'
            - transaction_id (str): Used to query all the comments on a transaction

        Returns:
            - A serialized representation of all the Posts from the query

    **POST**
        kwargs:
            - group_id (uuid.UUID): The id of the Group the post is in

        request.data:
            - message (str): The content of the post
            - superpost_id (str): The id of the post this post is in response to
            - transaction_id (str): The id of the transacton this post in response to

        Returns:
            - Serialized representation of the created Post

    '''
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = [
        'created_at',
        'likes_count',
        'replies_count',
        'total_activity'
    ]
    ordering = ['-pinned', '-created_at']
    serializer_class = serializers.PostSerializer
    pagination_class = PostResultsSetPagination

    def get_queryset(self):
        return get_public_post_queryset(
            user_id=self.request.user.id,
            transaction_id=self.request.query_params.get(
                'transaction_id', None)
        )

    def create(self, request, *args, **kwargs):
        (success, response) = post_management.create_post(
            message=request.data['message'],
            author_id=request.user.id,
            superpost_id=request.data.get('superpost_id', None),
            transaction_id=request.data.get('transaction_id', None),
        )
        if success:
            context = self.get_serializer_context()
            serialized_post = self.serializer_class(response, context=context)
            headers = self.get_success_headers(serialized_post.data)
            return Response(serialized_post.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({'detail': response}, status=status.HTTP_400_BAD_REQUEST)


class GroupPostList(generics.ListCreateAPIView):
    '''
    API endpoint for viewing top level posts in a group (replies are nested),
    and creating or responding to posts in a group.
    **GET**
        query_params:
            - ordering (str): An alternative field to order by. Supported options
                include: 'created_at', 'likes_count', 'replies_count', & 'total_activity'
            - transaction_id (str): Used to query all the comments on a transaction

        Returns:
            - A serialized representation of all the Posts from the query

    **POST**
        kwargs:
            - group_id (uuid.UUID): The id of the Group the post is in

        request.data:
            - message (str): The content of the post
            - superpost_id (str): The id of the post this post is in response to
            - transaction_id (str): The id of the transacton this post in response to

        Returns:
            - Serialized representation of the created Post
    '''
    permission_classes = [IsAuthenticated,
                          IsMemberOfGroupToPostAndViewOrGroupIsVisibleToView]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = [
        'created_at',
        'likes_count',
        'replies_count',
        'total_activity'
    ]
    ordering = ['-pinned', '-created_at']
    serializer_class = serializers.PostSerializer
    pagination_class = PostResultsSetPagination

    def get_queryset(self):
        return get_group_post_queryset(
            group_id=self.kwargs['group_id'],
            transaction_id=self.request.query_params.get(
                'transaction_id', None)
        )

    def create(self, request, *args, **kwargs):
        (success, response) = post_management.create_post(
            message=request.data['message'],
            group_id=kwargs['group_id'],
            author_id=request.user.id,
            superpost_id=request.data.get('superpost_id', None),
            transaction_id=request.data.get('transaction_id', None),
        )
        if success:
            context = self.get_serializer_context()
            serialized_post = self.serializer_class(response, context=context)
            headers = self.get_success_headers(serialized_post.data)
            return Response(serialized_post.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({'detail': response}, status=status.HTTP_400_BAD_REQUEST)


class PostDetail(generics.RetrieveAPIView):
    '''
    API endpoint for viewing details about a specific post.
    '''
    permission_classes = [IsAuthenticated,
                          IsMemberOfGroupOrGroupIsVisibleToViewPostDetail]
    serializer_class = serializers.PostSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Post.objects.filter(
            status='P',
        ).annotate(
            likes_count=Count('likes'),
            replies_count=Count('replies')
        )


class LikePost(PostDetail):
    '''
    API endpoint for liking a post. Prefer a GET to a PATCH because no additional
    data is required to fulfill this request.

    **GET**
        kwargs: 
            - id (str): The id of the post being liked

        Returns:
            - A serialized representation of the liked Post
    '''
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        (success, message) = post_management.like_post(
            kwargs['id'], request.user.id)
        if not success:
            return Response({'detail': message}, status=status.HTTP_400_BAD_REQUEST)
        return self.retrieve(request, *args, **kwargs)


class PinPost(PostDetail):
    '''
    API endpoint for pinning and unpinning posts to top of the results. 
    Prefer a GET to a PATCH because no additional data is required to fulfill
    this request.

     **GET**
        kwargs: 
            - id (str): The id of the post being pinned

        Returns:
            - A serialized representation of the pinned Post
    '''
    permission_classes = [IsAuthenticated, IsAdminOrOwnerOfGroup]

    def get(self, request, *args, **kwargs):
        (success, message) = post_management.pin_post(kwargs['id'])
        if not success:
            return Response({'detail': message}, status=status.HTTP_400_BAD_REQUEST)
        return self.retrieve(request, *args, **kwargs)
