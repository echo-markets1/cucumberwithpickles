import uuid
from django.test import TestCase
from CORELLIA.tests.factories.post_factory import PostFactory
from ALDERAAN.tests.factories.user_factory import UserFactory


class PostManagementTestBase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.author = UserFactory.create()
        cls.group_id = uuid.uuid4()

    def setUp(self):
        self.superpost = PostFactory.create(
            group__id=self.group_id, author_id=self.author.id)
        self.simple_post = PostFactory.create(
            group__id=self.group_id, author_id=self.author.id)


class HTTPCallMixin(TestCase):

    def get(self, expected_status_code=200):
        self.response = self.client.get(self.url)
        self.assertEquals(self.response.status_code, expected_status_code)

    def post(self, expected_status_code=201):
        self.response = self.client.post(self.url, self.request_data)
        self.assertEquals(self.response.status_code, expected_status_code)
