import uuid
from django.test import TestCase
from CORELLIA.extensions.data_extensions import get_group_ids_by_activity_level
from CORELLIA.models.post import ReactingUser
from CORELLIA.tests.factories.post_factory import PostFactory, GroupFactory


class GetGroupIdsByActivityLevelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.group_1 = GroupFactory.create()
        cls.group_2 = GroupFactory.create()
        cls.group_3 = GroupFactory.create()
        PostFactory.create_batch(size=10, group=cls.group_1)
        PostFactory.create_batch(size=9, group=cls.group_2)
        PostFactory.create_batch(size=8, group=cls.group_3)

    def test_extension_orders_by_activity_level_descending_by_default(self):
        # Arrange
        expected_group_order = [
            self.group_1.id,  # total_activity = 10 posts = 10
            self.group_2.id,  # total_activity = 9 posts = 9
            self.group_3.id  # total_activity = 8 posts = 8
        ]

        # Act
        actual_group_order = get_group_ids_by_activity_level()

        # Assert
        self.assertListEqual(
            expected_group_order,
            actual_group_order,
            "The groups should be ordered by activity level, descending."
        )

    def test_extension_orders_by_activity_level_ascending_when_descending_false(self):
        # Arrange
        expected_group_order = [
            self.group_3.id,  # total_activity = 8 posts = 8
            self.group_2.id,  # total_activity = 9 posts = 9
            self.group_1.id  # total_activity = 10 posts = 10
        ]

        # Act
        actual_group_order = get_group_ids_by_activity_level(descending=False)

        # Assert
        self.assertListEqual(
            expected_group_order,
            actual_group_order,
            "The groups should be ordered by activity level, ascending."
        )

    def test_extension_orders_by_activity_level_includes_number_of_likes(self):
        # Arrange
        post_to_like = PostFactory.create(group=self.group_2)
        post_to_like.likes.add(
            ReactingUser.objects.get_or_create(user_id=uuid.uuid4())[0]
        )
        post_to_like.save()

        expected_group_order = [
            self.group_2.id,  # total_activity = 10 posts + 1 like = 11
            self.group_1.id,  # total_activity = 10 posts = 10
            self.group_3.id  # total_activity = 8 posts = 8
        ]

        # Act
        actual_group_order = get_group_ids_by_activity_level()

        # Assert
        self.assertListEqual(
            expected_group_order,
            actual_group_order,
            "The groups should be ordered by activity level, descending."
        )

    def test_extension_orders_by_activity_level_includes_number_of_replies(self):
        # Arrange
        post_to_reply = PostFactory.create(group=self.group_3)
        reply = PostFactory.create(group=self.group_3)
        reply.superpost = post_to_reply
        reply.save()

        expected_group_order = [
            self.group_3.id,  # total_activity = 10 posts + 1 reply = 11
            self.group_1.id,  # total_activity = 10 posts = 10
            self.group_2.id  # total_activity = 9 posts = 9
        ]

        # Act
        actual_group_order = get_group_ids_by_activity_level()

        # Assert
        self.assertListEqual(
            expected_group_order,
            actual_group_order,
            "The groups should be ordered by activity level, descending."
        )
