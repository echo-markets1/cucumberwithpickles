import uuid
from CORELLIA.tests.factories.post_factory import PostFactory, TransactionFactory
from ALDERAAN.tests.factories.user_factory import UserFactory

from CORELLIA.core import post_management
from CORELLIA.models import Post

from CORELLIA.tests.base_classes.base_post_management_tests import PostManagementTestBase


class CreatePostTest(PostManagementTestBase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        super().setUp()
        self.data = {
            'message': 'I am prepared to proceed as a cat',
            'group_id': self.group_id,
            'author_id': self.author.id,
        }

    def test_create_post(self):
        (success, post) = post_management.create_post(**self.data)
        self.assertTrue(success)
        self.assertTrue(Post.objects.filter(author_id=self.author.id).exists())

    def test_create_post_reply(self):
        self.data['superpost_id'] = self.superpost.id
        (success, post) = post_management.create_post(**self.data)
        self.assertTrue(success)
        self.assertEqual(post.replies.count(), 1)

    def test_create_post_reply_fails_since_superpost_doesnt_exist(self):
        self.data['superpost_id'] = uuid.uuid4
        (success, message) = post_management.create_post(**self.data)
        self.assertEqual(
            message, 'No post found for the superpost_id provided')
        self.assertFalse(success)
        self.assertEqual(Post.objects.all().exclude(
            id=self.superpost.id).exclude(id=self.simple_post.id).count(), 0)

    def test_create_post_reply_fails_since_superpost_was_archived(self):
        self.data['superpost_id'] = self.superpost.id
        self.superpost.status = 'A'
        self.superpost.save()
        (success, message) = post_management.create_post(**self.data)
        self.assertEqual(
            message, 'No post found for the superpost_id provided')
        self.assertFalse(success)
        self.assertEqual(Post.objects.all().exclude(
            id=self.superpost.id).exclude(id=self.simple_post.id).count(), 0)

    def test_create_post_reply_fails_since_superpost_is_also_a_reply(self):
        self.data['superpost_id'] = self.superpost.id
        supersuperpost = PostFactory.create(
            group__id=self.superpost.group.id, author_id=self.author.id)
        self.superpost.superpost = supersuperpost
        self.superpost.save()
        (success, message) = post_management.create_post(**self.data)
        self.assertEqual(message, 'You can\'t reply to replies')
        self.assertFalse(success)
        self.assertEqual(Post.objects.all().exclude(
            id=self.superpost.id).exclude(id=self.simple_post.id).count(), 1)

    def test_create_public_post(self):
        del self.data['group_id']
        (success, post) = post_management.create_post(**self.data)
        self.assertTrue(success)
        self.assertTrue(Post.objects.filter(id=post.id).exists())

    def test_create_transaction_response_post(self):
        transaction = TransactionFactory.create()
        self.data['transaction_id'] = transaction.id
        (success, post) = post_management.create_post(**self.data)
        self.assertTrue(success)
        self.assertTrue(Post.objects.filter(id=post.id).exists())

    def test_create_transaction_response_fails_when_replying_to_reply(self):
        num_posts_before = Post.objects.all().count()
        transaction = TransactionFactory.create()
        self.data['transaction_id'] = transaction.id
        self.data['superpost_id'] = self.superpost.id
        (success, message) = post_management.create_post(**self.data)
        self.assertEqual(
            message, 'You can\'t reply to a comment on a transaction')
        self.assertFalse(success)
        self.assertEqual(num_posts_before, Post.objects.all().count())


class LikePostTest(PostManagementTestBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.reacting_user = UserFactory.create(
            email='clone1@kamino.com', handle="clone1")

    def setUp(self):
        super().setUp()
        self.data = {
            'post_id': self.simple_post.id,
            'user_id': self.reacting_user.id,
        }

    def test_like_post(self):
        (success, message) = post_management.like_post(**self.data)
        self.assertTrue(success)
        self.assertEqual(self.simple_post.likes.all().count(), 1)

    def test_like_post_fails_when_post_doesnt_exist(self):
        self.data['post_id'] = uuid.uuid4
        (success, message) = post_management.like_post(**self.data)
        self.assertFalse(success)
        self.assertEqual(self.simple_post.likes.all().count(), 0)
        self.assertEqual(message, 'Post was not found')

    def test_like_post_fails_since_post_was_archived(self):
        self.simple_post.status = 'A'
        self.simple_post.save()
        (success, message) = post_management.like_post(**self.data)
        self.assertFalse(success)
        self.assertEqual(self.simple_post.likes.all().count(), 0)
        self.assertEqual(
            message, 'This post was archived and cannot be liked')

    def test_unlike_post(self):
        (success, message) = post_management.like_post(**self.data)
        (success, message) = post_management.like_post(**self.data)
        self.assertTrue(success)
        self.assertEqual(self.simple_post.likes.all().count(), 0)


class PinPostTest(PostManagementTestBase):

    def setUp(self):
        super().setUp()
        self.data = {
            'post_id': self.simple_post.id,
        }

    def test_pin_post(self):
        (success, message) = post_management.pin_post(**self.data)
        self.simple_post.refresh_from_db()
        self.assertTrue(success)
        self.assertTrue(self.simple_post.pinned)

    def test_pin_post_fails_when_post_doesnt_exist(self):
        self.data['post_id'] = uuid.uuid4
        (success, message) = post_management.pin_post(**self.data)
        self.simple_post.refresh_from_db()
        self.assertFalse(success)
        self.assertFalse(self.simple_post.pinned)
        self.assertEqual(message, 'Post was not found')

    def test_pin_post_fails_since_post_was_archived(self):
        self.simple_post.status = 'A'
        self.simple_post.save()
        (success, message) = post_management.pin_post(**self.data)
        self.simple_post.refresh_from_db()
        self.assertFalse(success)
        self.assertFalse(self.simple_post.pinned)
        self.assertEqual(
            message, 'This post was archived and cannot be pinned')

    def test_unpin_post(self):
        (success, message) = post_management.pin_post(**self.data)
        (success, message) = post_management.pin_post(**self.data)
        self.simple_post.refresh_from_db()
        self.assertTrue(success)
        self.assertFalse(self.simple_post.pinned)
