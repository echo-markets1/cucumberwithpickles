import uuid
import factory
from factory import BUILD_STRATEGY
from faker import Factory
from CORELLIA.models import Post, Group, Transaction

faker = Factory.create()


class GroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ('id',)
    id = factory.LazyFunction(uuid.uuid4)


class TransactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Transaction
        django_get_or_create = ('id',)
    id = factory.LazyFunction(faker.uuid4)


class PostFactory(factory.DjangoModelFactory):
    class Meta:
        model = Post
        strategy = BUILD_STRATEGY

    group = factory.SubFactory(GroupFactory)
    author_id = factory.LazyFunction(uuid.uuid4)
    message = factory.LazyFunction(faker.sentence)
