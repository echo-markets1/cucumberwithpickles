from django.urls import reverse
from CORELLIA.tests.factories.post_factory import PostFactory
from ALDERAAN.tests.factories.user_factory import UserFactory
from CORUSCANT.tests.factories.group_factory import GroupFactory

from CORELLIA.models import Post

from rest_framework.test import APIClient
from CORUSCANT.models import Member, Membership

from CORELLIA.tests.base_classes.base_post_management_tests import PostManagementTestBase, HTTPCallMixin

from ENDOR.tests.factories import alphanumeric_id_generator


class BaseApiPostViewsTest(PostManagementTestBase, HTTPCallMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.client = APIClient()

        cls.request_data = {
            'message': 'I am prepared to proceed as a cat',
        }

        cls.private_group = GroupFactory.create(privacy='P')
        cls.private_group_member = Member.objects.get_or_create(user_id=cls.author.id)[
            0]
        cls.private_group_membership = Membership.objects.create(
            member=cls.private_group_member,
            group=cls.private_group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.private_group_post = PostFactory.create(
            group__id=cls.private_group.id, author_id=cls.author.id)

        cls.visible_group = GroupFactory.create(privacy='V')
        cls.non_member_user = UserFactory.create(
            email="clone2@kamino.com", handle="clone2")

        cls.request_data = {
            'message': 'I am prepared to proceed as a cat',
        }

        cls.private_group_post = PostFactory.create(
            group__id=cls.private_group.id, author_id=cls.author.id)
        cls.visible_group_post = PostFactory.create(
            group__id=cls.visible_group.id, author_id=cls.author.id)


class ApiMemberPostViewsTest(BaseApiPostViewsTest):

    def setUp(self):
        super().setUp()
        self.client.logout()
        self.client.login(username=self.author.email,
                          password='defaultpassword')

    def test_member_of_private_group_can_view_post_list(self):
        self.url = reverse('group-post-list',
                           kwargs={'group_id': str(self.private_group.id)})
        self.get()

    def test_member_of_group_can_create_new_post(self):
        self.url = reverse('group-post-list',
                           kwargs={'group_id': str(self.private_group.id)})
        self.post()
        self.assertEquals(Post.objects.filter(
            author_id=self.author.id).count(), 6)

    def test_member_of_private_group_can_view_post_detail(self):
        self.url = reverse(
            'post-detail', kwargs={'id': str(self.private_group_post.id)})
        self.get()

    def test_member_of_group_can_like_post(self):
        self.url = reverse(
            'like-post', kwargs={'id': str(self.simple_post.id)})
        self.get()
        self.assertEquals(self.simple_post.likes.count(), 1)

    def test_group_owner_can_pin_post(self):
        self.private_group_membership.type = 'O'
        self.private_group_membership.save()
        self.url = reverse(
            'pin-post', kwargs={'id': str(self.private_group_post.id)})
        self.get()
        self.private_group_post.refresh_from_db()
        self.assertTrue(self.private_group_post.pinned)

    def test_group_admin_can_pin_post(self):
        self.private_group_membership.type = 'A'
        self.private_group_membership.save()
        self.url = reverse(
            'pin-post', kwargs={'id': str(self.private_group_post.id)})
        self.get()
        self.private_group_post.refresh_from_db()
        self.assertTrue(self.private_group_post.pinned)

    def test_non_owner_or_admin_of_group_cannot_pin_post(self):
        self.private_group_membership.type = 'S'
        self.private_group_membership.save()
        self.url = reverse(
            'pin-post', kwargs={'id': str(self.private_group_post.id)})
        self.get(expected_status_code=403)
        self.private_group_post.refresh_from_db()
        self.assertFalse(self.private_group_post.pinned)


class ApiNonMemberPostViewsTest(BaseApiPostViewsTest):
    def setUp(self):
        super().setUp()
        self.client.logout()
        self.client.login(username=self.non_member_user.email,
                          password='defaultpassword')

    def test_non_member_of_private_group_cannot_view_post_list(self):
        self.url = reverse('group-post-list',
                           kwargs={'group_id': str(self.private_group.id)})
        self.get(expected_status_code=403)

    def test_non_member_of_visible_group_can_view_post_list(self):
        self.url = reverse('group-post-list',
                           kwargs={'group_id': str(self.visible_group.id)})
        self.get(expected_status_code=200)

    def test_non_member_of_group_cannot_create_new_post(self):
        self.url = reverse('group-post-list',
                           kwargs={'group_id': str(self.visible_group.id)})
        self.post(expected_status_code=403)
        self.assertFalse(Post.objects.filter(
            author_id=self.non_member_user.id).exists())

    def test_non_member_of_private_group_cannot_view_post_detail(self):
        self.url = reverse(
            'post-detail', kwargs={'id': str(self.private_group_post.id)})
        self.get(expected_status_code=403)

    def test_non_member_of_visible_group_can_view_post_detail(self):
        self.url = reverse(
            'post-detail', kwargs={'id': str(self.visible_group_post.id)})
        self.get()

    def test_non_member_of_group_can_like_post(self):
        self.url = reverse(
            'like-post', kwargs={'id': str(self.simple_post.id)})
        self.get()
        self.assertEquals(self.simple_post.likes.count(), 1)
