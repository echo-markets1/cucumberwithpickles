from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('CORELLIA', '0001_initial')]
    operations = [
        migrations.CreateModel(name='Transaction',
                               fields=[
                                   (
                                    'id', models.UUIDField(editable=False, primary_key=True, serialize=False))]),
        migrations.AddField(model_name='post',
                            name='transaction',
                            field=models.ForeignKey(null=True, on_delete=(django.db.models.deletion.PROTECT), related_name='replies', to='CORELLIA.Transaction'))]
