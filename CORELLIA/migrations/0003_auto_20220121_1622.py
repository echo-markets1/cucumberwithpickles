from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('CORELLIA', '0002_auto_20220119_1538')]
    operations = [
        migrations.AlterField(model_name='transaction',
                              name='id',
                              field=models.CharField(editable=False, max_length=40, primary_key=True, serialize=False))]
