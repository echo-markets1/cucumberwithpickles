# Multilayer Data Keys
CASH = "cash"
TICKER_SYMBOL = "ticker_symbol"
QUANTITY = "quantity"
SECURITY_ID = "security_id"
SECURITY_TYPE = "security_type"
SECURITY_SUBTYPE = "security_subtype"

# Portfolio Data Keys
INVESTMENTS = "investments"
TRANSACTIONS = "transactions"


# Transaction Data Keys
DATE = "date"
AMOUNT = "amount"
SUBTYPE = "subtype"
BUY = "buy"
SELL = "sell"
DEPOSIT = "deposit"
DIVIDEND = "dividend"
MERGER = "merger"
WITHDRAWAL = "withdrawal"
INTEREST = "interest"

# Investment Data Keys
INSTITUTION_VALUE = "institution_value"
CRYPTO = "crypto"

# Utility Constants
CURRENCY_PREFIX = "CUR:"
