from django.contrib import admin
from HOTH.models import Investor, Portfolio, PortfolioTimeseries, BaseTimeseries, StockTimeseries, TimeseriesDatapoint

admin.site.register(Investor)
admin.site.register(Portfolio)
admin.site.register(BaseTimeseries)
admin.site.register(TimeseriesDatapoint)
admin.site.register(StockTimeseries)
admin.site.register(PortfolioTimeseries)