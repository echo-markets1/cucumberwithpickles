from django.urls import path
from HOTH.api.views import CompositePortfoliosTimeseriesView, \
    PortfolioSuperlativePerformanceMetricsView, PortfolioTimeseriesView, RiskMetricsView, \
    StockTimeseriesView, InvestorPortfoliosTimeseriesView, PortfolioReturnView

urlpatterns = [
    path('portfolio/timeseries/<id>/',
         PortfolioTimeseriesView.as_view(), name='portfolio-timeseries'),
    path('portfolio/return/<id>/',
         PortfolioReturnView.as_view(), name='portfolio-return'),
    path('investor/timeseries/', InvestorPortfoliosTimeseriesView.as_view(),
         name='investor-portfolios-timeseries'),
    path('investor/timeseries/<id>/', InvestorPortfoliosTimeseriesView.as_view(),
         name='investor-portfolios-timeseries'),
    path('composite/timeseries/', CompositePortfoliosTimeseriesView.as_view(),
         name='composite-portfolios-timeseries'),
    path('composite/timeseries/<uuid:investor_id>/', CompositePortfoliosTimeseriesView.as_view(),
         name='composite-portfolios-timeseries'),
    path('stock/timeseries/<symbol>/',
         StockTimeseriesView.as_view(), name='stock-timeseries'),
    path('riskmetrics/<str:type>/<str:id>/',
         RiskMetricsView.as_view(), name='riskmetrics'),
    path('portfolio/superlativeperformancemetrics/<str:type>/<str:portfolio_id>/',
         PortfolioSuperlativePerformanceMetricsView.as_view(), name="portfolio-superlative-performance-metrics")
]
