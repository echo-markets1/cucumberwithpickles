from rest_framework import views
from HOTH.models import Superlative
from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator_factory import SUPERLATIVE_PERFORMANCE_METRICS_CALCULATOR_FACTORY
from HOTH.core.superlative_performance_metrics.retrievers.superlative_performance_metrics_retriever import SuperlativePerformanceMetricsRetriever
from HOTH.models.portfolio_timeseries import PortfolioTimeseries
from HOTH.models.base_timeseries import IntervalGranularity
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from django.db.models import QuerySet
from HOTH.models.serializers import PortfolioTimeseriesSerializer, StockTimeseriesSerializer, TimeseriesDatapointSerializer
from datetime import datetime
from HOTH.core import data_retriever
from HOTH.core.risk_metrics.retrievers.risk_metrics_retriever_factory import RISK_METRICS_RETRIEVER_FACTORY
import datetime


def _get_start_and_end_dates_for_query(query_params):
    """
    returns (start_date, end_date)
    """
    past_days = query_params.get('past-days')
    if past_days:
        past_days = int(past_days)
        delta = datetime.timedelta(days=past_days)
        start_date = (datetime.datetime.now() - delta)
        start_date = start_date.strftime("%Y-%m-%d")
        return (start_date, None)
    start_date = query_params.get('start-date')
    end_date = query_params.get('end-date')
    return (start_date, end_date)


def _filter_for_weekly_data(queryset: QuerySet) -> QuerySet:
    '''
    We currently support daily and weekly timeseries data from the BE. The
    Weekly data can be derived from the default daily data by filtering against
    the return values from every Monday.

    Parameters:
        - queryset: The queryset to filter

    Returns:
        - QuerySet: The queryset filter to only include datapoints from Mondays
    '''
    return queryset.filter(timestamp__week_day=2)


class PortfolioTimeseriesView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = TimeseriesDatapointSerializer
    supported_granularities = [
        IntervalGranularity.ONE_DAY, IntervalGranularity.ONE_WEEK]

    def get_queryset(self):
        query_params = self.request.query_params
        granularity = query_params.get(
            'granularity', IntervalGranularity.ONE_DAY)
        queryset = PortfolioTimeseries.objects.none()
        if (granularity in self.supported_granularities):
            portfolio_id = self.kwargs.get('id')
            (start_date, end_date) = _get_start_and_end_dates_for_query(query_params)
            queryset = data_retriever.get_portfolio_timeseries(
                portfolio_id=portfolio_id, start_date=start_date, end_date=end_date)
            if granularity == IntervalGranularity.ONE_WEEK:
                queryset = _filter_for_weekly_data(queryset)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class InvestorPortfoliosTimeseriesView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PortfolioTimeseriesSerializer
    supported_granularities = [
        IntervalGranularity.ONE_DAY, IntervalGranularity.ONE_WEEK]

    def get_queryset(self):
        query_params = self.request.query_params
        granularity = query_params.get(
            'granularity', IntervalGranularity.ONE_DAY)
        queryset = QuerySet.none()
        if (granularity in self.supported_granularities):
            investor_id = self.kwargs.get('id', self.request.user.id)
            (start_date, end_date) = _get_start_and_end_dates_for_query(query_params)
            queryset = data_retriever.get_user_portfolio_timeseries(
                investor_id=investor_id, start_date=start_date, end_date=end_date)
            if granularity == IntervalGranularity.ONE_WEEK:
                queryset = _filter_for_weekly_data(queryset)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CompositePortfoliosTimeseriesView(views.APIView):
    permission_classes = [IsAuthenticated]
    supported_granularities = [
        IntervalGranularity.ONE_DAY, IntervalGranularity.ONE_WEEK]

    def get(self, request, *args, **kwargs):
        query_params = self.request.query_params
        granularity = query_params.get(
            'granularity', IntervalGranularity.ONE_DAY)
        composite_timeseries = []
        if (granularity in self.supported_granularities):
            investor_id = self.kwargs.get('investor_id', self.request.user.id)
            (start_date, end_date) = _get_start_and_end_dates_for_query(query_params)
            composite_timeseries = data_retriever.get_composite_portfolio_timeseries(
                investor_id=investor_id, start_date=start_date, end_date=end_date)
            if granularity == IntervalGranularity.ONE_WEEK:
                composite_timeseries = filter(
                    lambda datapoint: datapoint.timestamp.weekday() == 0, composite_timeseries)
        serializer = TimeseriesDatapointSerializer(
            instance=composite_timeseries, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class PortfolioReturnView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        portfolio_id = self.kwargs.get('id')
        (start_date, end_date) = _get_start_and_end_dates_for_query(
            self.request.query_params)
        (raw_return, percent_return) = data_retriever.get_portfolio_return(
            portfolio_id, start_date, end_date)
        return Response({
            'raw_return': raw_return,
            'percent_return': percent_return
        })


class StockTimeseriesView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = StockTimeseriesSerializer

    def get_queryset(self):
        symbol = self.kwargs.get('symbol')
        (start_date, end_date) = _get_start_and_end_dates_for_query(
            self.request.query_params)
        queryset = data_retriever.get_stock_timeseries(
            symbol=symbol, start_date=start_date, end_date=end_date)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RiskMetricsView(APIView):
    '''
    Retrieves the risk metrics across all supported periods for a given entity.
    The entity is uniquely processed and identified by the combo of "type" and
    "id" kwargs. For example, when retrieving a portfolio's risk metrics, type = PORTFOLIO
    and id = <portfolio.id>.

    kwargs:
        - type: Uniquely identifies the risk metrics retriever instance to use
        - id: Uniquely identifies the entity to retrieve risk metrics for

    Returns:
        - A JSON response with the id as the key, and the value as the list of
            serialized RiskMetric instances, one for each Period
    '''
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        type = self.kwargs.get('type')
        retriever = RISK_METRICS_RETRIEVER_FACTORY.get_retriever(type)
        id = self.kwargs.get('id')
        risk_metrics = []
        try:
            for period in retriever.supported_periods:
                risk_metrics.append(retriever.retrieve(
                    source_id=id,
                    benchmark_id=self.request.query_params.get(
                        'benchmark', 'SPX'),
                    period=period
                ).serialize())
            return Response(data={id: risk_metrics}, status=status.HTTP_200_OK)
        except (KeyError, ValueError):
            return Response(
                data={
                    'message': f"Invalid type provided: '{type}'. Acceptable types are: 'PORTFOLIO'"
                },
                status=status.HTTP_400_BAD_REQUEST
            )


class PortfolioSuperlativePerformanceMetricsView(APIView):
    '''
    Retrieves the superlative performance metrics across all supported periods
    for a given superlative and portfolio.

    kwargs:
        - type: The variation of metric to retrieve (ie: BEST, WORST)
        - id: Uniquely identifies the portfolio to retrieve metrics for

    Returns:
        - A JSON response with the id as the key, and the value as the key : value
            pair of type: list of PortfolioSuperlativePerformanceMetrics instances,
            one for each period
    '''
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        type = self.kwargs.get('type')
        retriever = SuperlativePerformanceMetricsRetriever(
            calculator_factory=SUPERLATIVE_PERFORMANCE_METRICS_CALCULATOR_FACTORY)
        portfolio_id = self.kwargs.get('portfolio_id')
        superlative_performance_metrics = []
        try:
            for period in retriever.supported_periods:
                superlative_performance_metrics.append(retriever.retrieve(
                    portfolio_id=portfolio_id,
                    period=period,
                    type=Superlative[type]
                ).serialize())
            return Response(
                data={
                    portfolio_id: {
                        type: superlative_performance_metrics
                    }
                },
                status=status.HTTP_200_OK
            )
        except (KeyError, ValueError):
            return Response(
                data={
                    'message': f"Invalid type provided: '{type}'. Acceptable types are: 'BEST' or 'WORST'"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
