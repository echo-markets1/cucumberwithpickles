from HOTH.tests.factories.portfolio_factory import PortfolioFactory
from HOTH.tests.factories.investor_factory import InvestorFactory
from HOTH.models import PortfolioTimeseries
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class PortfolioTimeseriesFactory(factory.DjangoModelFactory):
    class Meta:
        model = PortfolioTimeseries
        strategy = BUILD_STRATEGY

    investor = factory.SubFactory(InvestorFactory)
    portfolio = factory.SubFactory(PortfolioFactory)
