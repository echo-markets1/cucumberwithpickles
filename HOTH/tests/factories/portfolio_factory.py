from HOTH.models import Portfolio
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class PortfolioFactory(factory.DjangoModelFactory):
    class Meta:
        model = Portfolio
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.text(max_nb_chars=40)
