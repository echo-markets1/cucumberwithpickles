from HOTH.models.risk_metrics import PortfolioRiskMetrics
from HOTH.tests.factories.portfolio_factory import PortfolioFactory
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class RiskMetricsFactory(factory.DjangoModelFactory):
    alpha = factory.fuzzy.FuzzyDecimal(0, 100, 2)
    beta = factory.fuzzy.FuzzyDecimal(0, 3, 2)
    standard_deviation = factory.fuzzy.FuzzyDecimal(0, 100, 2)
    sharpe = factory.fuzzy.FuzzyDecimal(0, 100, 2)
    r_squared = factory.fuzzy.FuzzyDecimal(0, 100, 2)
    last_updated = faker.date()


class PortfolioRiskMetricsFactory(RiskMetricsFactory):
    class Meta:
        model = PortfolioRiskMetrics
        strategy = BUILD_STRATEGY

    portfolio = factory.SubFactory(PortfolioFactory)
