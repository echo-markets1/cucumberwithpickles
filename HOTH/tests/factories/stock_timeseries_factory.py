from HOTH.models import StockTimeseries
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class StockTimeseriesFactory(factory.DjangoModelFactory):
    class Meta:
        model = StockTimeseries
        strategy = BUILD_STRATEGY

    symbol = faker.text(max_nb_chars=10)
    data_available = True
