from HOTH.models import PortfolioSuperlativePerformanceMetrics
from HOTH.tests.factories.portfolio_factory import PortfolioFactory
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class SuperlativePerformanceMetricsFactory(factory.DjangoModelFactory):
    percent_return = factory.fuzzy.FuzzyDecimal(-100, 100, 3)
    end_date = faker.date()
    start_date = faker.date()


class PortfolioSuperlativePerformanceMetricsFactory(SuperlativePerformanceMetricsFactory):
    class Meta:
        model = PortfolioSuperlativePerformanceMetrics
        strategy = BUILD_STRATEGY

    portfolio = factory.SubFactory(PortfolioFactory)
