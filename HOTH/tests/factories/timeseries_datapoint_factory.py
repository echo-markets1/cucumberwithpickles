from HOTH.models import TimeseriesDatapoint
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class TimeseriesDatapointFactory(factory.DjangoModelFactory):
    '''
    Factory for creating TimeseriesDatapoints. When using the create() method,
    must pass in a timeseries object or factory via kwarg.
    '''
    class Meta:
        model = TimeseriesDatapoint
        strategy = BUILD_STRATEGY

    timestamp = factory.LazyFunction(faker.date)
    value = factory.fuzzy.FuzzyDecimal(0, 4000, 5)
