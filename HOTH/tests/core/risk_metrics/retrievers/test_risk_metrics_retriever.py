from django.test.testcases import TestCase
from HOTH.core.risk_metrics.retrievers.risk_metrics_retriever import RiskMetricsRetriever
from HOTH.models.risk_metrics import Period, RiskMetrics
from HOTH.tests.test_data.arkk_timeseries_data import arkk_timeseries_data
from HOTH.tests.test_data.spx_timeseries_data import spx_timeseries_data

from datetime import date
from pandas import Series


class TestRiskMetricsRetriever(RiskMetricsRetriever):
    '''
    Dummy Implementation of the abstract RiskMetricsRetriever class
    Allows us to test the common concrete methods of the abstract class
    '''
    __supported_periods = [(Period.ONE_YEAR.value, Period.ONE_YEAR.label)]

    @property
    def supported_periods(self):
        return self.__supported_periods

    def _get_source_timeseries_as_pandas_series(self, source_id: str, start_date: str) -> Series:
        return

    def _get_benchmark_timeseries_as_pandas_series(self, benchmark_id: str, start_date: str) -> Series:
        return

    def retrieve(self, source_id: str, benchmark_id: str, period: Period) -> RiskMetrics:
        return


class RiskMetricsRetrieverTest(TestCase):
    __retriever = TestRiskMetricsRetriever()
    __dates = [date(2021, 6, 9), date(2021, 6, 10), date(2021, 6, 11)]

    def test_compile_metrics_returns_dict_for_all_metrics(self):
        # Arrange
        source_values_series = Series(data=[12, 24, 36], index=self.__dates)
        source_returns_series = Series(
            data=[0.4, 0.1, 0.4], index=self.__dates)
        benchmark_returns_series = Series(
            data=[0.5, 0.9, 0.3], index=self.__dates)
        num_trading_days = 252
        expected_metrics = ['alpha', 'beta',
                            'standard_deviation', 'sharpe', 'r_squared']

        # Act
        metrics = self.__retriever._compile_metrics(
            source_values_series, source_returns_series, benchmark_returns_series, num_trading_days)

        # Assert
        self.assertEqual(expected_metrics, list(metrics.keys()))
        for value in metrics.values():
            self.assertIsInstance(value, float)

    def test_get_returns_series_returns_source_and_benchmark_returns(self):
        # Arrange
        source_values_series = Series(data=[12, 24, 36], index=self.__dates)
        benchmark_values_series = Series(data=[16, 32, 48], index=self.__dates)

        def __assert_returns_series(returns_series: Series):
            for i in range(0, returns_series.index.size):
                self.assertIsInstance(returns_series.iloc[i], float)

        # Act
        (source_returns_series, benchmark_returns_series) = self.__retriever._get_returns_series(
            source_values_series, benchmark_values_series)

        # Assert
        __assert_returns_series(source_returns_series)
        __assert_returns_series(benchmark_returns_series)

    def test_quantstats_provides_expected_values_daily(self):
        # This test uses real market data to validate that quantstats is providing
        # the expected risk metric output given a known input

        # Arrange
        arkk_daily = arkk_timeseries_data["daily"]
        spx_daily = spx_timeseries_data["daily"]
        source_values_series = Series(
            data=arkk_daily["raw_values"], index=arkk_daily["dates"])
        source_returns_series = Series(
            data=arkk_daily["returns"], index=arkk_daily["dates"])

        benchmark_returns_series = Series(
            data=spx_daily["returns"], index=spx_daily["dates"])
        num_trading_days = 252

        expected_metrics = {
            'alpha': 0.005757961307796289,
            'beta': 1.5156370450783634,
            'standard_deviation': 21.422090196148414,
            'sharpe': -1.0676800044187345,
            'r_squared': 0.3348437934173201
        }

        # Act
        metrics = self.__retriever._compile_metrics(
            source_values_series, source_returns_series, benchmark_returns_series, num_trading_days)

        # Assert
        self.assertDictEqual(metrics, expected_metrics)
