from HOTH.tests.factories.portfolio_risk_metrics_factory import PortfolioRiskMetricsFactory
from typing import Dict
from HOTH.models.risk_metrics import Period
from datetime import date, timedelta
from HOTH.tests.factories.stock_timeseries_factory import StockTimeseriesFactory
from HOTH.tests.factories.timeseries_datapoint_factory import TimeseriesDatapointFactory
from HOTH.tests.factories.portfolio_timeseries_factory import PortfolioTimeseriesFactory
from django.test.testcases import TestCase

from HOTH.models import PortfolioRiskMetrics
from HOTH.core.risk_metrics.retrievers.portfolio_risk_metrics_retriever import PortfolioRiskMetricsRetriever

from pandas import Series
from collections import Counter


class PortfolioRiskMetricsRetrieverTest(TestCase):
    __retriever = PortfolioRiskMetricsRetriever()
    __today = date.today()
    __yesterday = __today - timedelta(days=1)
    __two_days_ago = __yesterday - timedelta(days=1)

    @classmethod
    def setUpTestData(cls):
        cls.source_timeseries = PortfolioTimeseriesFactory.create()
        cls.source_datapoint_1 = TimeseriesDatapointFactory.create(
            timestamp=cls.__today,
            timeseries=cls.source_timeseries
        )
        cls.source_datapoint_2 = TimeseriesDatapointFactory.create(
            timestamp=cls.__yesterday,
            timeseries=cls.source_timeseries
        )
        cls.benchmark_timeseries = StockTimeseriesFactory.create()
        cls.benchmark_datapoint_1 = TimeseriesDatapointFactory.create(
            timestamp=cls.__today,
            timeseries=cls.benchmark_timeseries
        )
        cls.benchmark_datapoint_2 = TimeseriesDatapointFactory.create(
            timestamp=cls.__yesterday,
            timeseries=cls.benchmark_timeseries
        )

    def test_get_values_series_returns_expected_source_and_benchmark_series(self):
        self.__test_get_values_series()

    def test_get_values_series_returns_expected_series_when_more_benchmark_data(self):
        # Arrange
        TimeseriesDatapointFactory.create(
            timestamp=(self.__two_days_ago),
            timeseries=self.benchmark_timeseries
        )
        self.assertGreater(self.benchmark_timeseries.data.count(),
                           self.source_timeseries.data.count())
        self.__test_get_values_series()

    def test_get_values_series_returns_expected_series_when_more_source_data(self):
        # Arrange
        TimeseriesDatapointFactory.create(
            timestamp=(self.__two_days_ago),
            timeseries=self.source_timeseries
        )
        self.assertGreater(self.source_timeseries.data.count(),
                           self.benchmark_timeseries.data.count())

        self.__test_get_values_series()

    def test_retrieve_creates_new_risk_metrics(self):
        # Arrange
        self.assertFalse(PortfolioRiskMetrics.objects.exists())
        expected = {
            "last_updated": self.__today,
            "period": Period.ONE_YEAR,
            "portfolio": self.source_timeseries.portfolio
        }

        # Act
        risk_metrics = self.__retriever.retrieve(
            source_id=self.source_timeseries.portfolio.id,
            benchmark_id=self.benchmark_timeseries.symbol,
            period=Period.ONE_YEAR
        )

        # Assert
        self.assertEqual(PortfolioRiskMetrics.objects.count(), 1)
        self.__assert_risk_metrics(risk_metrics, expected)

    def test_retrieve_updates_existing_risk_metrics_when_outdated(self):
        # Arrange
        risk_metrics = PortfolioRiskMetricsFactory.create(
            last_updated=self.__two_days_ago, portfolio=self.source_timeseries.portfolio)
        previous_update = risk_metrics.last_updated

        self.assertTrue(PortfolioRiskMetrics.objects.exists())
        expected = {
            "last_updated": self.__today,
            "period": risk_metrics.period,
            "portfolio": risk_metrics.portfolio
        }

        # Act
        risk_metrics = self.__retriever.retrieve(
            source_id=self.source_timeseries.portfolio.id,
            benchmark_id=self.benchmark_timeseries.symbol,
            period=Period.ONE_YEAR
        )

        # Assert
        self.assertEqual(PortfolioRiskMetrics.objects.count(), 1)
        self.assertNotEqual(previous_update, risk_metrics.last_updated)
        self.__assert_risk_metrics(risk_metrics, expected)

    def test_retrieve_does_not_update_existing_risk_metrics_when_up_to_date(self):
        # Arrange
        risk_metrics = PortfolioRiskMetricsFactory.create(
            last_updated=self.__today, portfolio=self.source_timeseries.portfolio)
        previous_update = risk_metrics.last_updated

        self.assertTrue(PortfolioRiskMetrics.objects.exists())

        # Act
        updated_risk_metrics = self.__retriever.retrieve(
            source_id=self.source_timeseries.portfolio.id,
            benchmark_id=self.benchmark_timeseries.symbol,
            period=Period.ONE_YEAR
        )

        # Assert
        self.assertEqual(PortfolioRiskMetrics.objects.count(), 1)
        self.assertEqual(previous_update, risk_metrics.last_updated)
        self.assertEqual(risk_metrics, updated_risk_metrics)

    def __test_get_values_series(self):
        '''
        Helper method for testing the _get_values_series method, but more importantly
        the underlying, overridden abstract methods:
            - _get_source_timeseries_as_pandas_series()
            - _get_benchmark_timeseries_as_pandas_series()
        '''
        # Arrange
        expected_source_values = [
            float(self.source_datapoint_1.value), float(self.source_datapoint_2.value)]
        expected_benchmark_values = [
            float(self.benchmark_datapoint_1.value), float(self.benchmark_datapoint_2.value)]
        expected_dates = [self.__today, self.__yesterday]

        # Act
        (source_values_series, benchmark_values_series) = self.__retriever._get_values_series(
            source_id=self.source_timeseries.portfolio.id,
            benchmark_id=self.benchmark_timeseries.symbol,
            start_date=self.__yesterday.isoformat()
        )

        # Assert
        self.assertIsInstance(source_values_series, Series)
        self.assertEqual(
            Counter(expected_source_values),
            Counter(list(source_values_series.values))
        )
        self.assertEqual(
            Counter(expected_dates),
            Counter(list(source_values_series.index))
        )
        self.assertEqual(
            Counter(expected_benchmark_values),
            Counter(list(benchmark_values_series.values))
        )
        self.assertEqual(
            Counter(expected_dates),
            Counter(list(benchmark_values_series.index))
        )

    def __assert_risk_metrics(self, risk_metrics: PortfolioRiskMetrics, expected: Dict):
        # Asserting against the risk metrics them selves is not super useful
        # since the calculations are provided by 3p quantstats. Instead, confirm
        # that the risk metrics meet the other expected qualities defined by
        # logic we own
        self.assertEqual(risk_metrics.last_updated, expected["last_updated"])
        self.assertEqual(risk_metrics.period, expected["period"])
        self.assertEqual(risk_metrics.portfolio, expected["portfolio"])
