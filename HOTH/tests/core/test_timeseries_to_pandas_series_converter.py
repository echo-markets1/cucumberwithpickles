from pandas import Series
from HOTH.tests.factories.portfolio_timeseries_factory import PortfolioTimeseriesFactory
from HOTH.tests.factories.timeseries_datapoint_factory import TimeseriesDatapointFactory
from django.test.testcases import TestCase
from HOTH.core import timeseries_to_pandas_series_converter


class TimeSeriesToPandasSeriesConverterTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        timeseries = PortfolioTimeseriesFactory.create()
        cls.datapoint_1 = TimeseriesDatapointFactory.create(
            timeseries=timeseries
        )
        cls.datapoint_2 = TimeseriesDatapointFactory.create(
            timeseries=timeseries
        )

    def test_convert_creates_pandas_series_matching_timeseries_datapoints(self):
        # Arrange
        expected_values = [float(self.datapoint_1.value),
                           float(self.datapoint_2.value)]
        expected_dates = [self.datapoint_1.timestamp,
                          self.datapoint_2.timestamp]

        # Act
        converted_series = timeseries_to_pandas_series_converter.convert(
            timeseries=[self.datapoint_1, self.datapoint_2]
        )

        # Assert
        self.assertIsInstance(converted_series, Series)
        self.assertEqual(expected_values, list(converted_series.values))
        self.assertEqual(expected_dates, list(converted_series.index))
