from HOTH.core import update_checker
from decimal import Decimal
from typing import Dict
from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator import SuperlativePerformanceMetricsCalculator
from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator_factory import SuperlativePerformanceMetricsCalculatorFactory
import datetime
from HOTH.core.superlative_performance_metrics.retrievers.superlative_performance_metrics_retriever import SuperlativePerformanceMetricsRetriever
from unittest.mock import Mock, patch
from HOTH.models.superlative_performance_metrics import PortfolioSuperlativePerformanceMetrics, Superlative
from HOTH.models.period import Period
from HOTH.tests.factories.portfolio_superlative_performance_metrics_factory import PortfolioSuperlativePerformanceMetricsFactory
from django.test.testcases import TestCase


class SuperlativePerformanceMetricsRetrieverTest(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.mock_calculator_factory = Mock(
            spec=SuperlativePerformanceMetricsCalculatorFactory)
        cls.retriever = SuperlativePerformanceMetricsRetriever(
            cls.mock_calculator_factory)
        cls.mock_calculator = Mock(
            spec=SuperlativePerformanceMetricsCalculator)
        cls.mock_calculator_factory.get_calculator.return_value = cls.mock_calculator

    def test_retrieve_returns_metrics_as_is_when_up_to_date(self):
        # Arrange
        self.stubbed_get_current_date_for_timezone_patcher = patch(
            'HOTH.core.update_checker.get_current_date_for_timezone')
        self.stubbed_get_current_date_for_timezone = self.stubbed_get_current_date_for_timezone_patcher.start()

        best_day = PortfolioSuperlativePerformanceMetricsFactory.create(
            period=Period.ONE_DAY, type=Superlative.BEST)
        self.stubbed_get_current_date_for_timezone.return_value = best_day.last_updated

        # Act
        retrieved_metric = self.retriever.retrieve(
            portfolio_id=best_day.portfolio.id,
            period=Period.ONE_DAY,
            type=Superlative.BEST
        )

        # Assert
        self.mock_calculator_factory.get_calculator.assert_called_with(
            Superlative.BEST)
        self.mock_calculator.calculate.assert_not_called()
        self.assertEqual(best_day, retrieved_metric)

        # Clean up
        self.stubbed_get_current_date_for_timezone_patcher.stop()

    def test_retrieve_returns_updated_metrics_when_out_of_date(self):
        # Arrange
        expected_percent_return = Decimal(1.6676)
        expected_start_date = datetime.date(2020, 8, 12)
        expected_end_date = datetime.date(2020, 9, 11)
        expected_values = {
            'percent_return': expected_percent_return,
            'period': Period.ONE_MONTH,
            'type': Superlative.WORST,
            'start_date': expected_start_date,
            'end_date': expected_end_date,
            'last_updated': update_checker.get_current_date_for_timezone()
        }
        self.mock_calculator.__superlative = Superlative.WORST
        self.mock_calculator.calculate.return_value = {
            'percent_return': expected_percent_return,
            'start_date': expected_start_date,
            'end_date': expected_end_date
        }
        last_updated = datetime.date.today() - datetime.timedelta(days=10)
        worst_month = PortfolioSuperlativePerformanceMetricsFactory.create(
            period=Period.ONE_MONTH,
            type=Superlative.WORST,
            last_updated=last_updated
        )

        # Act
        retrieved_metric = self.retriever.retrieve(
            portfolio_id=worst_month.portfolio.id,
            period=Period.ONE_MONTH,
            type=Superlative.WORST
        )

        # Assert
        self.mock_calculator_factory.get_calculator.assert_called_with(
            Superlative.WORST)
        self.mock_calculator.calculate.assert_called_with(
            portfolio_id=worst_month.portfolio.id,
            period=Period.ONE_MONTH,
            last_updated=last_updated,
            previous_metric_value=worst_month.percent_return
        )
        self.assertEqual(worst_month.pk, retrieved_metric.pk,
                         "Should be the same db instances")
        self.__assert_metric_instance(retrieved_metric, expected_values)

    def test_retrieve_returns_new_metrics_when_none_exist(self):
        # Arrange
        expected_percent_return = Decimal(1.6676)
        expected_start_date = datetime.date(2020, 8, 12)
        expected_end_date = datetime.date(2020, 9, 11)
        expected_values = {
            'percent_return': expected_percent_return,
            'period': Period.ONE_WEEK,
            'type': Superlative.BEST,
            'start_date': expected_start_date,
            'end_date': expected_end_date,
            'last_updated': update_checker.get_current_date_for_timezone()
        }
        self.mock_calculator.__superlative = Superlative.BEST
        self.mock_calculator.calculate.return_value = {
            'percent_return': expected_percent_return,
            'start_date': expected_start_date,
            'end_date': expected_end_date
        }

        worst_month = PortfolioSuperlativePerformanceMetricsFactory.create(
            period=Period.ONE_MONTH,
            type=Superlative.WORST,
        )

        # Act
        retrieved_metric = self.retriever.retrieve(
            portfolio_id=worst_month.portfolio.id,
            period=Period.ONE_WEEK,
            type=Superlative.BEST
        )

        # Assert
        self.mock_calculator_factory.get_calculator.assert_called_with(
            Superlative.BEST)
        self.mock_calculator.calculate.assert_called_with(
            portfolio_id=worst_month.portfolio.id,
            period=Period.ONE_WEEK
        )

        self.assertNotEqual(worst_month.pk, retrieved_metric.pk,
                            "Should not be the same db instances")
        self.__assert_metric_instance(retrieved_metric, expected_values)

    def test_retriever_provides_supported_periods(self):
        # Arrange
        expected_periods = [Period.ONE_DAY, Period.ONE_MONTH, Period.ONE_WEEK]
        # Act
        actual_periods = self.retriever.supported_periods

        # Assert
        self.assertListEqual(actual_periods, expected_periods,
                             "The retriever's supported_periods does not provide the expected collection of periods")

    def __assert_metric_instance(self, instance: PortfolioSuperlativePerformanceMetrics, expected_values: Dict):
        self.assertAlmostEqual(instance.percent_return,
                               expected_values["percent_return"], 3)
        self.assertEqual(instance.period, expected_values["period"])
        self.assertEqual(instance.type, expected_values["type"])
        self.assertEqual(instance.start_date, expected_values["start_date"])
        self.assertEqual(instance.end_date, expected_values["end_date"])
        self.assertEqual(instance.last_updated,
                         expected_values["last_updated"])
