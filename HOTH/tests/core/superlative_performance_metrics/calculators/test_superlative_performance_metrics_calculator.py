from decimal import Decimal
from typing import Dict
from HOTH.models.period import Period
from HOTH.models.superlative_performance_metrics import Superlative
from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator import SuperlativePerformanceMetricsCalculator
from HOTH.models.base_timeseries import TimeseriesDatapoint
from HOTH.tests.factories.portfolio_timeseries_factory import PortfolioTimeseriesFactory
import datetime
from django.test.testcases import TestCase
from HOTH.tests.test_data.arkk_timeseries_data import arkk_timeseries_data
from unittest.mock import patch


class SuperlativePerformanceMetricsCalculatorTest(TestCase):
    '''
    "Abstract" test class written in a way that should be completely reusable
    for child components by overriding the calculator and expected results instances,
    since the core logic of calculators are all the same, but the algorithm's and
    expected outcomes are slightly different.
    '''
    calculator = None
    expected_results = {}

    @classmethod
    def setUpTestData(cls) -> None:
        cls.portfolio_timeseries = PortfolioTimeseriesFactory.create()
        cls.portfolio_id = cls.portfolio_timeseries.portfolio.id
        timeseries_datapoints = []
        for i in range(60):
            date_string = arkk_timeseries_data["daily"]["dates"][i]
            month_day_year = date_string.split('/')
            date = datetime.date(
                int(month_day_year[2]),
                int(month_day_year[0]),
                int(month_day_year[1])
            )
            timeseries_datapoints.append(TimeseriesDatapoint(
                timeseries=cls.portfolio_timeseries,
                timestamp=date,
                value=arkk_timeseries_data["daily"]["raw_values"][i]
            ))
        TimeseriesDatapoint.objects.bulk_create(timeseries_datapoints)
        cls.stubbed_get_portfolio_timeseries_patcher = patch(
            'HOTH.core.data_retriever.get_portfolio_timeseries')
        cls.stubbed_get_portfolio_timeseries = cls.stubbed_get_portfolio_timeseries_patcher.start()
        cls.stubbed_get_portfolio_timeseries.return_value = cls.portfolio_timeseries.data.all()

    @classmethod
    def tearDownClass(cls) -> None:
        super().tearDownClass()
        cls.stubbed_get_portfolio_timeseries_patcher.stop()

    def setUp(self) -> None:
        self.skipTest(
            "Base Class not meant to be tested. Override with subclass to run these tests.")

    def test_calculate_returns_correct_results_for_one_day(self):
        # Arrange
        calculator = self._get_calculator()

        # Act
        results = calculator.calculate(
            portfolio_id=self.portfolio_id,
            period=Period.ONE_DAY,
            last_updated=None,
            previous_metric_value=None
        )

        # Assert
        self._assert_results(actual_results=results,
                             expected_results=self._get_expected_results_for(Period.ONE_DAY))

    def test_calculate_returns_correct_results_for_one_week(self):
        # Arrange
        calculator = self._get_calculator()

        # Act
        results = calculator.calculate(
            portfolio_id=self.portfolio_id,
            period=Period.ONE_WEEK,
            last_updated=None,
            previous_metric_value=None
        )

        # Assert
        self._assert_results(actual_results=results,
                             expected_results=self._get_expected_results_for(Period.ONE_WEEK))

    def test_calculate_returns_correct_results_for_one_month(self):
        # Arrange
        calculator = self._get_calculator()

        # Act
        results = calculator.calculate(
            portfolio_id=self.portfolio_id,
            period=Period.ONE_MONTH,
            last_updated=None,
            previous_metric_value=None
        )

        # Assert
        self._assert_results(actual_results=results,
                             expected_results=self._get_expected_results_for(Period.ONE_MONTH))

    def _get_calculator(self) -> SuperlativePerformanceMetricsCalculator:
        return self.calculator

    def _get_expected_results_for(self, period: Period) -> Dict:
        return self.expected_results[period]

    def _assert_results(self, actual_results: Dict, expected_results: Dict) -> None:
        self.assertAlmostEqual(actual_results["percent_return"],
                               expected_results["percent_return"], 3)
        self.assertEqual(actual_results["start_date"], expected_results["start_date"],
                         "The start_date returned was unexpected")
        self.assertEqual(actual_results["end_date"], expected_results["end_date"],
                         "The end_date returned was unexpected")


class BestPerformanceMetricsCalculatorTest(SuperlativePerformanceMetricsCalculatorTest):

    calculator = SuperlativePerformanceMetricsCalculator(Superlative.BEST)
    expected_results = {
        Period.ONE_DAY: {
            'percent_return': Decimal(4.1877),
            'start_date': datetime.date(2020, 9, 8),
            'end_date': datetime.date(2020, 9, 9)
        },
        Period.ONE_MONTH: {
            'percent_return': Decimal(15.8819),
            'start_date': datetime.date(2020, 8, 3),
            'end_date': datetime.date(2020, 9, 2)
        },
        Period.ONE_WEEK: {
            'percent_return': Decimal(12.0963),
            'start_date': datetime.date(2020, 6, 29),
            'end_date': datetime.date(2020, 7, 6)
        }
    }

    def setUp(self) -> None:
        pass


class WorstPerformanceMetricsCalculatorTest(SuperlativePerformanceMetricsCalculatorTest):

    calculator = SuperlativePerformanceMetricsCalculator(Superlative.WORST)
    expected_results = {
        Period.ONE_DAY: {
            'percent_return': Decimal(-7.3837),
            'start_date': datetime.date(2020, 9, 2),
            'end_date': datetime.date(2020, 9, 3)
        },
        Period.ONE_MONTH: {
            'percent_return': Decimal(1.6676),
            'start_date': datetime.date(2020, 8, 12),
            'end_date': datetime.date(2020, 9, 11)
        },
        Period.ONE_WEEK: {
            'percent_return': Decimal(-14.5149),
            'start_date': datetime.date(2020, 9, 1),
            'end_date': datetime.date(2020, 9, 8)
        }
    }

    def setUp(self) -> None:
        pass
