import datetime
from django.test.testcases import TestCase
from HOTH.core import update_checker


class UpdateCheckerTests(TestCase):
    def test_check_up_to_date_returns_false_when_last_updated_none(self):
        # Arrange
        # Does not matter what today is when last_updated == None
        anyday = datetime.date.today()

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=anyday, last_updated=None)

        # Assert
        self.assertFalse(up_to_date)

    def test_check_up_to_date_returns_true_when_today_sunday_last_updated_two_days_ago(self):
        # Arrange
        sunday = datetime.date(2021, 6, 13)
        friday = datetime.date(2021, 6, 11)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=sunday, last_updated=friday)

        # Assert
        self.assertTrue(up_to_date)

    def test_check_up_to_date_returns_true_when_today_sunday_last_updated_lt_two_days_ago(self):
        # Arrange
        sunday = datetime.date(2021, 6, 13)
        saturday = datetime.date(2021, 6, 12)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=sunday, last_updated=saturday)

        # Assert
        self.assertTrue(up_to_date)

    def test_check_up_to_date_returns_true_when_today_monday_last_updated_three_days_ago(self):
        # Arrange
        monday = datetime.date(2021, 6, 14)
        friday = datetime.date(2021, 6, 11)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=monday, last_updated=friday)

        # Assert
        self.assertTrue(up_to_date)

    def test_check_up_to_date_returns_true_when_today_monday_last_updated_lt_three_days_ago(self):
        # Arrange
        monday = datetime.date(2021, 6, 14)
        satuday = datetime.date(2021, 6, 12)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=monday, last_updated=satuday)

        # Assert
        self.assertTrue(up_to_date)

    def test_check_up_to_date_returns_false_when_today_not_sunday_monday_and_last_updated_one_day_ago(self):
        # Arrange
        tuesday = datetime.date(2021, 6, 15)
        monday = datetime.date(2021, 6, 14)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=tuesday, last_updated=monday)

        # Assert
        self.assertFalse(up_to_date)

    def test_check_up_to_date_returns_false_when_today_not_sunday_monday_and_last_updated_gt_one_day_ago(self):
        # Arrange
        tuesday = datetime.date(2021, 6, 15)
        friday = datetime.date(2021, 6, 11)

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=tuesday, last_updated=friday)

        # Assert
        self.assertFalse(up_to_date)

    def test_check_up_to_date_returns_true_when_last_updated_today(self):
        # Arrange
        # Does not matter what today is as long as today == last_updated
        anyday = datetime.date.today()

        # Act
        (up_to_date, _) = update_checker.check_up_to_date(
            today=anyday, last_updated=anyday)

        # Assert
        self.assertTrue(up_to_date)
