from HOTH.models.period import Period
import datetime
from typing import Dict
from HOTH.models.portfolio import Portfolio
from django.db import models


class RiskMetrics(models.Model):
    '''
    Represents a collection of metrics that provide insight as to the risk of
    a given entity. For further reading on meaning of the supported risk metrics
    see: https://www.investopedia.com/ask/answers/041415/what-are-some-common-measures-risk-used-risk-management.asp 
    '''

    # The relative return on investment (ROI) of an asset compared to a benchmark
    # Ex:
    #   - An alpha of 25 means the asset had an ROI 25% greater than the benchmark
    #   - An alpha of -25 means the asset had an ROI 25% less than the benchmark
    #
    alpha = models.DecimalField(max_digits=8, decimal_places=3)

    # An indicator of volatility of the asset relative to the market
    # Ex:
    #   - A beta of 1.5 means the asset's value is 50% more volatile than the market's
    #   - A beta of .5 means the asset's value is 50% less volatile than the market's
    #   - A beta of 1 means the asset's value is exactly as volatile as the market's
    beta = models.DecimalField(max_digits=6, decimal_places=3)

    # An indicator of the asset's volatility, based on the price distribution
    standard_deviation = models.DecimalField(max_digits=8, decimal_places=3)

    # A proxy for the risk adjusted return of a portfolio. Allows an investor to
    # compare the return of an investment to it's risk. Risk is always relative
    # to the expected return of a "risk-free" investment, such as a
    # U.S. Treasury Bond. Generally, the greater the sharpe, the better
    # Ex:
    #   - A sharpe ratio of 52 means the expected return of the investment is
    #     52 % after accounting for it's risk
    #   - A sharpe ratio of -52 means the expected return of the investment is
    #     -52 % after accounting for it's risk
    sharpe = models.DecimalField(max_digits=8, decimal_places=3)

    # A measure of how similarly an asset performs relative to a index
    # Ex:
    #   - An r_squared comparing FB to the S&P 500 of 93% means that the
    #     performance curve of FB is highly (93%) correlated (similar) to
    #     the S&P 500
    #   - An r_squared comparing FB to the S&P 500 of 7% means that the
    #     performance curve of FB is not (7%) correlated (similar) to the S&P 500
    r_squared = models.DecimalField(max_digits=6, decimal_places=3)

    # The date these metrics were last updated
    last_updated = models.DateField(
        default=datetime.date.today, editable=False)

    # The period over which the risk evaluated for
    period = models.CharField(
        max_length=2,
        editable=False,
        choices=Period.choices,
        default=Period.ONE_YEAR
    )

    class Meta:
        abstract = True

    def serialize(self) -> Dict:
        '''
        Serializes the instance by returning it's values as a dictionary.
        Prefer to DRF serializers, since this is a very simple case, and we only
        case about serialization, not deserialization.
        '''
        return {
            "alpha": self.alpha,
            "beta": self.beta,
            "standard_deviation": self.standard_deviation,
            "sharpe": self.sharpe,
            "r_squared": self.r_squared,
            "period": self.period,
        }


class PortfolioRiskMetrics(RiskMetrics):
    portfolio = models.ForeignKey(
        Portfolio, on_delete=models.PROTECT, related_name="risk_metrics")

    class Meta:
        constraints = [models.UniqueConstraint(
            fields=['period', 'portfolio'], name='single_risk_metric_per_period')]
