from django.db import models
from .base_timeseries import BaseTimeseries


class StockTimeseries(BaseTimeseries):
    symbol = models.CharField(primary_key=True, max_length=10)
    data_available = models.BooleanField(default=True)

    def __str__(self):
        return self.symbol
