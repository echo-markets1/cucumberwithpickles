from django.db import models
from django.utils.translation import ugettext_lazy as _
import datetime


class IntervalGranularity(models.TextChoices):
    """
    Interval between two consecutive points in time series, ie the granularity of the data
    • Supports: 1min, 5min, 15min, 30min, 45min, 1h, 2h, 4h, 1day, 1week, 1month
    """
    ONE_MIN = '1min', _("1 minute between consecutive points in time series")
    FIVE_MIN = '5min', _("5 minutes between consecutive points in time series")
    FIFTEEN_MIN = '15min', _(
        "15 minutes between consecutive points in time series")
    THIRTY_MIN = '30min', _(
        "30 minutes between consecutive points in time series")
    FORTYFIVE_MIN = '45min', _(
        "45 minutes between consecutive points in time series")
    ONE_HR = '1h', _("1 hour between consecutive points in time series")
    TWO_HR = '2h', _("2 hours between consecutive points in time series")
    FOUR_HR = '4h', _("4 hours between consecutive points in time series")
    ONE_DAY = '1day', _("1 day between consecutive points in time series")
    ONE_WEEK = '1week', _("1 week between consecutive points in time series")
    ONE_MONTH = '1month', _(
        "1 month between consecutive points in time series")


class BaseTimeseries(models.Model):
    @property
    def last_updated(self):
        data = self.data.order_by('-timestamp')
        if data.exists():
            return data[0].timestamp
        else:
            # If last_updated is None, it has never been updated, so set to default of 4 years ago
            return datetime.date.today() - datetime.timedelta(days=1095)


class TimeseriesDatapoint(models.Model):
    timeseries = models.ForeignKey(
        BaseTimeseries, on_delete=models.PROTECT, related_name="data")
    timestamp = models.DateField()
    value = models.DecimalField(max_digits=19, decimal_places=5, null=True)

    class Meta:
        ordering = ["timestamp"]
        unique_together = ("timeseries", "timestamp")

    def __str__(self):
        return f"{self.timeseries}: {self.value} @ {self.timestamp}"
