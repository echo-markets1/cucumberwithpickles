from rest_framework import serializers
from HOTH.models import PortfolioTimeseries
from HOTH.core import data_retriever
from HOTH.models.serializers.timeseries_datapoint import TimeseriesDatapointSerializer


class PortfolioTimeseriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PortfolioTimeseries
        fields = '__all__'

    percent_return = serializers.SerializerMethodField()
    raw_return = serializers.SerializerMethodField()
    data = TimeseriesDatapointSerializer(many=True, read_only=True)

    def get_percent_return(self, portfolio_timeseries):
        (raw_return, percent_return) = data_retriever.get_portfolio_return(
            portfolio_timeseries.portfolio.id)
        return percent_return

    def get_raw_return(self, portfolio_timeseries):
        (raw_return, percent_return) = data_retriever.get_portfolio_return(
            portfolio_timeseries.portfolio.id)
        return raw_return
