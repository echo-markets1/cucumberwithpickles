from rest_framework import serializers
from HOTH.models import TimeseriesDatapoint
import datetime

EPOCH_START_DATE = datetime.date(1970, 1, 1)


class TimeseriesDatapointSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeseriesDatapoint
        fields = '__all__'

    unix_timestamp = serializers.SerializerMethodField()

    def get_unix_timestamp(self, datapoint):
        # Unix time = seconds since Epoch start (Jan 1, 1970)
        time_since_epoch_start = (datapoint.timestamp - EPOCH_START_DATE)
        return time_since_epoch_start.total_seconds()
