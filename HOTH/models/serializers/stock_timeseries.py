from rest_framework.serializers import ModelSerializer
from HOTH.models import StockTimeseries
from HOTH.models.serializers.timeseries_datapoint import TimeseriesDatapointSerializer

class StockTimeseriesSerializer(ModelSerializer):
    class Meta:
        model = StockTimeseries
        fields = '__all__'
    data = TimeseriesDatapointSerializer(many=True, read_only=True)