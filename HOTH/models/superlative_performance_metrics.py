from typing import Dict
from HOTH.models import Period, Portfolio
from django.db import models
from django.utils.translation import ugettext_lazy as _
import datetime


class Superlative(models.TextChoices):
    BEST = 'B', _("Best")
    WORST = 'W', _("Worst")


class SuperlativePerformanceMetrics(models.Model):
    '''
    Represents the most extreme performances of an entity over a given period of
    time. For example, an instance with:
    {
        "value" = -17.35,
        "period" = ONE_MONTH,
        "type" = WORST
    }
    would represent that the entity's worst performance over the span of a month
    was -17.35%.
    '''
    # The percent return of the entity over the given period. How this is calculated
    # will be determined by the type
    percent_return = models.DecimalField(max_digits=12, decimal_places=3)

    # The period over which the metric was evaluated. Should be equivalent
    # to the delta between start_date and end_date
    period = models.CharField(
        max_length=2,
        editable=False,
        choices=Period.choices
    )

    # Defines what kind of metric this is ("Best", "Worst", etc.)
    type = models.CharField(
        max_length=1,
        choices=Superlative.choices
    )

    # The date at the beginning of the evaluated period
    start_date = models.DateField()

    # The date at the end of the evaluate period
    end_date = models.DateField()

    # The date these metrics were last updated
    last_updated = models.DateField(
        default=datetime.date.today, editable=False)

    class Meta:
        abstract = True

    def serialize(self) -> Dict:
        '''
        Serializes the instance by returning it's values as a dictionary.
        Prefer to DRF serializers, since this is a very simple case, and we only
        case about serialization, not deserialization.
        '''
        return {
            "percent_return": self.percent_return,
            "period": self.period,
            "start_date": self.start_date,
            "end_date": self.end_date,
        }


class PortfolioSuperlativePerformanceMetrics(SuperlativePerformanceMetrics):
    portfolio = models.ForeignKey(
        Portfolio, on_delete=models.PROTECT, related_name="superlative_performance_metrics")

    class Meta:
        constraints = [models.UniqueConstraint(
            fields=['period', 'type', 'portfolio'], name='single_superlative_per_type_and_period')]
