from django.db import models

class Investor(models.Model):
    user_id = models.UUIDField(primary_key=True, editable=False)
