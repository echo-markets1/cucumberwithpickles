from django.db import models
from .investor import Investor
from .portfolio import Portfolio
from .base_timeseries import BaseTimeseries


class PortfolioTimeseries(BaseTimeseries):
    investor = models.ForeignKey(Investor, on_delete=models.PROTECT)
    portfolio = models.OneToOneField(
        Portfolio, on_delete=models.PROTECT, related_name="timeseries")

    def __str__(self):
        return self.portfolio
