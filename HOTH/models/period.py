from django.db import models
from django.utils.translation import ugettext_lazy as _


class Period(models.TextChoices):
    ''' The period over which a metric was evaluated '''
    ONE_DAY = '1d', _('One day')
    ONE_WEEK = '1w', _('One week')
    ONE_MONTH = '1m', _('One month')
    ONE_YEAR = '1y', _('One year')

    @property
    def as_days(self) -> int:
        period_days_map = {
            self.ONE_DAY: 1,
            self.ONE_WEEK: 7,
            self.ONE_MONTH: 30,
            self.ONE_YEAR: 365
        }
        return period_days_map[self]
