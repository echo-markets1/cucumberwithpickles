from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator_factory import SuperlativePerformanceMetricsCalculatorFactory
from typing import List
from HOTH.core import update_checker
from HOTH.models import Period, PortfolioSuperlativePerformanceMetrics, Portfolio, Superlative


class SuperlativePerformanceMetricsRetriever(object):
    '''
    Used to retrieve the most up to date PortfolioSuperlativePerformanceMetrics
    instace for a given portfolio, superlative type, and time period.

    Attributes:
        - __supported_periods (List[Period]): The periods for which the retriever
            is authorized to calculate metrics for
        - __calculator_factory (SuperlativePerformanceMetricsCalculatorFactory):
            Provides the correct SuperlativePerformanceMetricsCalculator instance
            based on the Superlative passed to retrieve
    '''

    def __init__(self, calculator_factory: SuperlativePerformanceMetricsCalculatorFactory):
        self.__calculator_factory = calculator_factory

    __supported_periods = [Period.ONE_DAY, Period.ONE_MONTH, Period.ONE_WEEK]

    @property
    def supported_periods(self) -> List[Period]:
        return self.__supported_periods

    def __logging_prefix(self, portfolio_id: str, period: Period, type: Superlative):
        ''' Helper function to simplify logging'''
        return f"{type.label} Performance Metrics for portfolio: {portfolio_id} and period: {period} "

    def retrieve(self, portfolio_id: str, period: Period, type: Superlative) -> PortfolioSuperlativePerformanceMetrics:
        '''
        Retrieve up to date SuperlativePerformanceMetrics for a portfolio.

        Given a time period, retrieve the most up to date SuperlativePerformanceMetrics
        for a portfolio. If one does not exist or the metric has not been updated
        in the past day, perform the calculations required to create or update
        the metric.

        Parameters:
            - portfolio_id (str): The id of the related portfolio object
            - period (Period): The enumerated period of time for which the metrics
                are calculated against
            - type (Superlative): The variation of metrics to provide (Best, Worst, etc.)

        Returns:
            - PortfolioSuperlativePerformanceMetrics: The up to date instance of
                PortfolioSuperlativePerformanceMetrics
        '''
        today = update_checker.get_current_date_for_timezone()
        performance_metrics = None
        calculator = self.__calculator_factory.get_calculator(type)
        try:
            superlative_performance_metrics = PortfolioSuperlativePerformanceMetrics.objects.get(
                portfolio__id=portfolio_id, period=period, type=type)
            (up_to_date, last_updated) = update_checker.check_up_to_date(
                today=today,
                last_updated=superlative_performance_metrics.last_updated
            )
            if up_to_date:
                print(self.__logging_prefix(portfolio_id, period, type) +
                      "are already up to date. Returning existing metrics.")
                return superlative_performance_metrics
            else:
                print(self.__logging_prefix(portfolio_id, period, type) +
                      "were not up to date. Updating and returning metrics.")
                performance_metrics = calculator.calculate(
                    portfolio_id=portfolio_id,
                    period=period,
                    last_updated=last_updated,
                    previous_metric_value=superlative_performance_metrics.percent_return
                )
        except PortfolioSuperlativePerformanceMetrics.DoesNotExist:
            print(self.__logging_prefix(portfolio_id, period, type) +
                  "were not found. Creating and returning metrics.")
            performance_metrics = calculator.calculate(
                portfolio_id=portfolio_id,
                period=period,
            )
        performance_metrics["last_updated"] = today
        portfolio = Portfolio.objects.get_or_create(id=portfolio_id)[0]
        return PortfolioSuperlativePerformanceMetrics.objects.update_or_create(
            defaults=performance_metrics,
            portfolio=portfolio,
            period=period,
            type=type
        )[0]
