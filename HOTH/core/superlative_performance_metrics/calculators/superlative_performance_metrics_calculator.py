from typing import Dict, Tuple
from decimal import Decimal
from HOTH.core import data_retriever
from HOTH.models import Period, Superlative, TimeseriesDatapoint
import datetime


class SuperlativePerformanceMetricsCalculator(object):
    '''
    Used to calculate the most up to date SuperlativePerformanceMetrics
    for a given portfolio, superlative type, and time period. There is
    slight differences in the logic based on the superlative attribute.

    Attributes:
        - __superlative (Superlative): Determines whether or not the retriever
            should calculate the best or worst value
        - __should_update_value (function): __superlative defined logic to
            determine whether a value should be updated or not
        - __initial_value_to_beat (Decimal): An artificial min/max defined by
            the __superlative
    '''

    def __init__(self, superlative: Superlative):
        if superlative == Superlative.BEST:
            self.__should_update_value = lambda x, y: x > y
            self.__initial_value_to_beat = Decimal(-999999999999)
        elif superlative == Superlative.WORST:
            self.__should_update_value = lambda x, y: x < y
            self.__initial_value_to_beat = Decimal(999999999999)
        else:
            raise ValueError(
                f"{self.__class__.__name__} does not currently support the Superlative: {superlative}")
        self.__superlative = superlative

    @property
    def superlative(self) -> Superlative:
        return self.__superlative

    def __get_initial_value_to_beat(self, previous_metric_value: Decimal = None) -> Decimal:
        return previous_metric_value if previous_metric_value != None else self.__initial_value_to_beat

    def __should_update_value_to_beat(self, current_value: Decimal, value_to_beat: Decimal) -> Decimal:
        return self.__should_update_value(current_value, value_to_beat)

    def __calculate_percent_return(self, open: Decimal, close: Decimal) -> Decimal:
        return Decimal(((close - open) / open) * 100)

    def __get_start_end_dates(
        self,
        last_updated: datetime.date,
        earliest_timeseries_timestamp: datetime.date,
        period: Period
    ) -> Tuple[datetime.date, datetime.date]:
        period_delta = datetime.timedelta(days=period.as_days)
        if last_updated != None:
            end_date = last_updated
            start_date = end_date - period_delta
        else:
            start_date = earliest_timeseries_timestamp
            end_date = start_date + period_delta
        return (start_date, end_date)

    def calculate(
        self,
        portfolio_id: str,
        period: Period,
        last_updated: datetime.date = None,
        previous_metric_value: Decimal = None
    ) -> Dict:
        '''
        Calculates and compiles the superlative performance metrics, returning
        them as a python dictionary.

        Parameters:
            - portfolio_id (str): The id of the related portfolio object
            - period (Period): The enumerated period of time for which the metrics
                are calculated against
            - last_updated (datetime.date): For existing metrics, the date the
                metric was last updated, so we don't waste resources re calculating
            - previous_metric_value (Decimal): For existing metrics, determines
                what the value to beat will be

        Returns:
            - Dict: A dictionary containing the value, start and end dates of
                the SuperlativePerformanceMetrics, keyed by name
        '''
        timeseries = data_retriever.get_portfolio_timeseries(portfolio_id)
        (start_date, end_date) = self.__get_start_end_dates(
            last_updated=last_updated,
            earliest_timeseries_timestamp=timeseries.first().timestamp,
            period=period
        )
        value_to_beat = self.__get_initial_value_to_beat(previous_metric_value)
        results = {
            "percent_return": value_to_beat,
            "start_date": start_date,
            "end_date": end_date,
        }
        one_day_delta = datetime.timedelta(days=1)
        while end_date < timeseries.last().timestamp:
            start_date = start_date + one_day_delta
            end_date = end_date + one_day_delta
            try:
                current_return = self.__calculate_percent_return(
                    open=timeseries.get(timestamp=start_date).value,
                    close=timeseries.get(timestamp=end_date).value
                )
                if self.__should_update_value_to_beat(current_value=current_return, value_to_beat=value_to_beat):
                    value_to_beat = current_return
                    results["percent_return"] = value_to_beat
                    results["start_date"] = start_date
                    results["end_date"] = end_date
            except TimeseriesDatapoint.DoesNotExist:
                # If data is missing or not record for one or both of start_date
                # and end_date, skip the calculation
                pass
        return results
