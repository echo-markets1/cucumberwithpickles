from HOTH.core.superlative_performance_metrics.calculators.superlative_performance_metrics_calculator import SuperlativePerformanceMetricsCalculator
from HOTH.models import Superlative


class SuperlativePerformanceMetricsCalculatorFactory:
    __calculators = {
        Superlative.BEST: SuperlativePerformanceMetricsCalculator(Superlative.BEST),
        Superlative.WORST: SuperlativePerformanceMetricsCalculator(
            Superlative.WORST)
    }

    def get_calculator(self, type: Superlative) -> SuperlativePerformanceMetricsCalculator:
        return self.__calculators[type]


SUPERLATIVE_PERFORMANCE_METRICS_CALCULATOR_FACTORY = SuperlativePerformanceMetricsCalculatorFactory()
