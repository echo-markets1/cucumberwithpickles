import datetime
import functools
import operator
from typing import Dict, List, Tuple
from HOTH.core import update_checker
from HOTH.constants.endor_data_constants import (CASH, CRYPTO, INSTITUTION_VALUE, INTEREST, SECURITY_ID, TICKER_SYMBOL, QUANTITY,
                                                 DATE, AMOUNT, INVESTMENTS, TRANSACTIONS, SUBTYPE, BUY,
                                                 SELL, DEPOSIT, DIVIDEND, MERGER, WITHDRAWAL,
                                                 SECURITY_TYPE, SECURITY_SUBTYPE, CURRENCY_PREFIX)
from HOTH.interfaces import twelvedata_client_interface
from HOTH.models import PortfolioTimeseries, StockTimeseries, TimeseriesDatapoint, Investor, Portfolio
from HOTH.models.base_timeseries import IntervalGranularity


def update_stocks_timeseries(symbols: List, start_date: datetime.date, interval: IntervalGranularity = IntervalGranularity.ONE_DAY) -> None:
    '''
    Initiates a call to TwelveData for a given list of symbols which need updating,
    and updates the datebase based on the response.

    Parameters: 
        - symbols (list): A list of symbols for stocks whose timeseries data needs
            updating
        - start_date (date): The date object representing the earliest data point
            to retrieve.
        - interval (IntervalGranularity): The interval as defined by TwelveData
            representing the distance between two datapoints in the timeseries.
            For more info, see https://twelvedata.com/docs#stocks
    '''
    # Reach out to twelvedata for historical data for those symbols
    # Gets multiple timeseries in a single twelvedata call
    timeseries_data = twelvedata_client_interface.get_time_series(
        symbols, interval, start_date)
    # Save information to database
    for symbol in symbols:
        # It is assumed elsewhere that any security with a ticker_symbol may
        # have a StockTimeseries. Use the data_available field to determine whether or
        # data is present for a given security
        stock_timeseries = StockTimeseries.objects.get_or_create(symbol=symbol)[
            0]
        # If a symbol from the list is not available in twelvedata, it will not be in the response
        series = timeseries_data.get(symbol, None)
        if series is not None and len(series) > 0:
            for day in series:
                timestamp = datetime.date.fromisoformat(day['datetime'])
                (datapoint, _) = TimeseriesDatapoint.objects.get_or_create(
                    timestamp=timestamp, timeseries=stock_timeseries)
                datapoint.value = float(day['close'])
                datapoint.save()
        else:
            stock_timeseries.data_available = False

        stock_timeseries.save()


def update_investments_timeseries(investments_data: Dict, last_updated: datetime.date) -> None:
    """
    Takes serialized investment data, and updates the timeseries data for any underlying
    securities whose timeseries are not up to date locally.

    Parameters:
        - investments_data (Dict): Serialized investments data from ENDOR
        - last_updated (datetime.date): The last time the related portfolio
            timeseries was updated
    """
    # Collect the symbols that need timeseries data.
    # It prepares investments data so that update_stocks_timeseries can be called effectively
    # Meant to be called with portfolio_data[INVESTMENTS] data from ENDOR
    # Meant to be called when an entire portfolio is out of date (not individual stocks)
    symbols = []
    least_recent_stock_update = datetime.date.max
    for investment_data in investments_data:
        symbol = ''
        if investment_data[SECURITY_TYPE] != CASH or investment_data[SECURITY_SUBTYPE] == CRYPTO:
            symbol = investment_data[TICKER_SYMBOL]
            if symbol.strip():
                (stock_timeseries, _) = StockTimeseries.objects.get_or_create(
                    symbol=symbol)
                (stock_is_up_to_date, stock_last_updated) = update_checker.check_up_to_date(
                    today=update_checker.get_current_date_for_timezone(),
                    last_updated=stock_timeseries.last_updated)
                least_recent_stock_update = min(
                    least_recent_stock_update, stock_last_updated)
                if not stock_is_up_to_date:
                    symbols.append(symbol)
    if len(symbols) > 0:
        # We must use the same start date for the entire call to update stock timeseries
        # Take the least recent date to make sure all timeseries are up to date
        update_stocks_timeseries(symbols, min(
            least_recent_stock_update, last_updated))


def get_stock_timeseries(symbol: str) -> StockTimeseries:
    """
    Retrieves a single stock timeseries, updating from twelvedata as necessary,
    by using above function update_stocks_timeseries

    Parameters:
        - symbol (str): The ticker symbol of the stock to retrieve timeseries for

    Returns:
        - (StockTimeseries): The related, updated stock timeseries instance
    """
    (stock_timeseries, _) = StockTimeseries.objects.get_or_create(symbol=symbol)
    if not CURRENCY_PREFIX in symbol and stock_timeseries.data_available:
        (stock_is_up_to_date, stock_last_updated) = update_checker.check_up_to_date(
            today=update_checker.get_current_date_for_timezone(),
            last_updated=stock_timeseries.last_updated)
        if not stock_is_up_to_date:
            update_stocks_timeseries([symbol], stock_last_updated)
    stock_timeseries.refresh_from_db()
    return stock_timeseries


def update_portfolio_timeseries(portfolio_data: Dict, investor: Investor, portfolio: Portfolio) -> PortfolioTimeseries:
    # Brings the timeseries datapoints of a portfolio up to date.
    # Starts with current holdings and works backward, adding up each day in the interval
    # and adjusting the holdings as nessary. Assumes the Timeseries needs updating
    # Where no day-to-day price data is available (ie mergers that happened after user sold a stock)
    # the algorithm simply uses the buy and sell value of that stock, and so there will be some variance
    # of value in the historical day-to-day value of the stock

    (portfolio_timeseries, _) = PortfolioTimeseries.objects.get_or_create(
        portfolio=portfolio, investor=investor)

    last_updated = portfolio_timeseries.last_updated

    holdings_symbol_quantity_map = {
        # ticker_symbol: number_of_shares
    }
    no_timeseries_data_holdings_security_id_value_map = {
        # security_id: value
    }
    symbol_stock_timeseries_map = {
        # ticker_symbol: StockTimeseries
    }

    def _get_timeseries_for(ticker_symbol) -> StockTimeseries:
        _timeseries = symbol_stock_timeseries_map.get(ticker_symbol, None)
        if _timeseries is None:
            _timeseries = StockTimeseries.objects.get(symbol=ticker_symbol)
            symbol_stock_timeseries_map[ticker_symbol] = _timeseries
        return _timeseries

    def _initialize_holdings_symbol_quantity_map_and_symbol_value_map(portfolio_data: Dict) -> Tuple[Dict[str, float]]:
        """
        In order to determine the value of a paticular holding at a given point
        in time, we need to know how many shares of a given security were held
        at the time of interest.
        """
        _holdings_symbol_quantity_map = {}
        _no_timeseries_data_holdings_security_id_value_map = {}
        _holdings_symbol_quantity_map[CASH] = float(portfolio_data[CASH])
        for investment in portfolio_data[INVESTMENTS]:
            if investment[SECURITY_TYPE] != CASH or investment[SECURITY_SUBTYPE] == CRYPTO:
                symbol = investment[TICKER_SYMBOL]
                if symbol.strip():
                    stock_timeseries = _get_timeseries_for(symbol)
                    if stock_timeseries.data_available:
                        _holdings_symbol_quantity_map[symbol] = float(
                            investment[QUANTITY])
                    else:
                        # When there is no timeseries data available for a security
                        # we can only estimate the price over time by taking the current value,
                        # an updating it as we process related transactions.
                        # Use the security_id as the key to be consistent with the no
                        # ticker_symbol case
                        _no_timeseries_data_holdings_security_id_value_map[investment[SECURITY_ID]] = float(
                            investment[INSTITUTION_VALUE])
                else:
                    # When there is no ticker_symbol provided for a security
                    # we can only estimate the price over time by taking the current value,
                    # an updating it as we process related transactions
                    _no_timeseries_data_holdings_security_id_value_map[investment[SECURITY_ID]] = float(
                        investment[INSTITUTION_VALUE])
        return (_holdings_symbol_quantity_map, _no_timeseries_data_holdings_security_id_value_map)

    def _compute_portfolio_value_at(date: datetime.date):
        value = 0.00

        def _compute_value_of_holding_with_timeseries_data_for(symbol: str, quantity: int) -> float:
            """
            Use the value of the stock at a given point in time, and the quantity of shares held
            at that time to determiine the value of the holding at that time.
            """
            if symbol == CASH:
                return quantity
            timeseries = _get_timeseries_for(symbol)
            try:
                stock_value_on_date = TimeseriesDatapoint.objects.get(
                    timeseries=timeseries,
                    timestamp=date
                )
                return float(stock_value_on_date.value) * quantity
            except TimeseriesDatapoint.DoesNotExist:
                print(
                    f"WARN: No TimeseriesDatapoint for stock: {symbol} on date: {date} was found.")
                return 0.00

        def _compute_value_of_holdings_with_no_timeseries_data() -> float:
            """
            When there is no timeseries data available, add the values of the holding,
            estimated by the transaction history
            """
            if len(no_timeseries_data_holdings_security_id_value_map.values()) == 0:
                return 0.00
            return functools.reduce(operator.add, no_timeseries_data_holdings_security_id_value_map.values())

        for symbol, quantity in holdings_symbol_quantity_map.items():
            value = value + \
                _compute_value_of_holding_with_timeseries_data_for(
                    symbol, quantity)

        value = value + _compute_value_of_holdings_with_no_timeseries_data()
        return value

    def _update_holdings_maps_for_transactions_at(date: datetime.date):
        """
        Updates the holdings maps that help track the value or size of a holding
        overtime based on transactions that occurred for a given date. Upon completion,
        the holdings counter should reflect the state of holdings at EOD for the day
        prior to the given date.
        """
        def _get_transactions_for(date) -> List[Dict]:
            transactions = []
            for transaction in portfolio_data[TRANSACTIONS]:
                if datetime.date.fromisoformat(transaction[DATE]) == date:
                    transactions.append(transaction)
            return transactions

        def _update_from_buy(transaction: Dict) -> None:
            symbol = transaction[TICKER_SYMBOL]
            if symbol.strip():
                initial_quantity = holdings_symbol_quantity_map.get(
                    symbol, 0.00)
                updated_quantity = initial_quantity - \
                    float(transaction[QUANTITY])
                if updated_quantity == 0:
                    del holdings_symbol_quantity_map[symbol]
                elif updated_quantity < 0:
                    print("ERROR: holdings_symbol_quantity_map value should not be < zero for symbol " +
                          symbol + " " + str(updated_quantity) + ". This implies the user previously " +
                          "owned negative shares")
                else:
                    holdings_symbol_quantity_map[symbol] = updated_quantity

            else:
                security_id = transaction[SECURITY_ID]
                initial_value = no_timeseries_data_holdings_security_id_value_map.get(
                    security_id, 0.00)
                updated_value = initial_value - float(transaction[AMOUNT])
                no_timeseries_data_holdings_security_id_value_map[security_id] = updated_value

            # Always update the cash value following the transaction
            holdings_symbol_quantity_map[CASH] = holdings_symbol_quantity_map[CASH] + \
                float(transaction[AMOUNT])

        def _update_from_sell(transaction: Dict) -> None:
            symbol = transaction[TICKER_SYMBOL]
            if symbol.strip():
                initial_quantity = holdings_symbol_quantity_map.get(
                    symbol, 0.00)

                # Sell transaction quantities are listed in negative amounts
                updated_quantity = initial_quantity - \
                    float(transaction[QUANTITY])

                holdings_symbol_quantity_map[symbol] = updated_quantity
            else:
                security_id = transaction[SECURITY_ID]
                initial_value = no_timeseries_data_holdings_security_id_value_map.get(
                    security_id, 0.00)
                updated_value = initial_value - float(transaction[AMOUNT])
                no_timeseries_data_holdings_security_id_value_map[security_id] = updated_value

            # Always update the cash value following the transaction
            holdings_symbol_quantity_map[CASH] = holdings_symbol_quantity_map[CASH] + \
                float(transaction[AMOUNT])

        def _update_from_external_flow(transaction):
            # Deposits and withdrawals are not added and subtracted from the account value because this could
            # result in not showing not just the performance of the investments throughout time,
            # and would show external cash flows into the portfolio on the timeseries and return values
            # In other words, without deposits/withdrawals added/subtracted throughout the portfolio's history,
            # we simulate the historical performance with the premise that the current cash in the account has not
            # been altered at all.
            # Competitors make similar assumptions in their calculations.

            # holdings_symbol_quantity_map[CASH] = holdings_symbol_quantity_map[CASH] + float(transaction[AMOUNT])
            pass

        def _update_from_dividend(transaction: Dict) -> None:
            # Dividends represents earnings on investments payed out by the company to
            # the shareholders. Even in situations where the dividend is automatically
            # reinvested, the dividend itself will show up as change to the accounts
            # cash holdings
            holdings_symbol_quantity_map[CASH] = holdings_symbol_quantity_map[CASH] - \
                float(transaction[AMOUNT])

        def _update_from_interest(transaction: Dict):
            # Interest represents earnings on investments payed out to the company via
            # the shareholders. This is typical for managed funds where there may be a
            # management fee to owning shares of the fund
            holdings_symbol_quantity_map[CASH] = holdings_symbol_quantity_map[CASH] - \
                float(transaction[AMOUNT])

        def _update_from_merger(transaction: Dict):
            # TODO
            # To be added because we do not yet have an example of what this plaid investment_transaction looks like
            pass

        def _transaction_type_unknown(transaction: Dict) -> None:
            print("Transaction subtype of " +
                  transaction[SUBTYPE] + " is unaccounted for.")

        def _handle_transaction_by_subtype(transaction: Dict):
            """
            Uniquely processes the transactin based on the subtype to update 
            the holdings maps.

            Parameters:
                - transaction (Dict): The serialized data of a single transaction
            """
            transaction_subtype_handlers = {
                BUY: _update_from_buy,
                SELL: _update_from_sell,
                DEPOSIT: _update_from_external_flow,
                DIVIDEND: _update_from_dividend,
                MERGER: _update_from_merger,
                WITHDRAWAL: _update_from_external_flow,
                INTEREST: _update_from_interest
            }
            update = transaction_subtype_handlers.get(
                transaction[SUBTYPE], None)

            # If the subtype exists, call the corresponding update function, otherwise execute fallback behavior
            update(transaction) if update is not None else _transaction_type_unknown(
                transaction)

        for transaction in _get_transactions_for(date=date):
            _handle_transaction_by_subtype(transaction)

    # First, update the constituent stocks timeseries, belonging to the portfolio's investments
    update_investments_timeseries(
        portfolio_data[INVESTMENTS], last_updated)

    # Then, initialize the current cash and stock holdings in the account from plaid data
    (holdings_symbol_quantity_map, no_timeseries_data_holdings_security_id_value_map) = \
        _initialize_holdings_symbol_quantity_map_and_symbol_value_map(
            portfolio_data)

    # Working backwards starting from today, adding up the investments held each day
    current_date = update_checker.get_current_date_for_timezone()
    date_to_process = current_date
    while(date_to_process != last_updated):
        # Add up the values of each investments at the given date_to_process
        portfolio_value_at_date_to_process = _compute_portfolio_value_at(
            date_to_process)

        # Save the value of portfolio for the date being processed as a datapoint on the timeseries
        TimeseriesDatapoint.objects.update_or_create(
            timeseries=portfolio_timeseries,
            timestamp=date_to_process,
            defaults={"value": portfolio_value_at_date_to_process}
        )

        # Then check to see if there are any transaction that took place for the date
        # being processed. If so, update the trackers holdings_symbol_quantity_map and
        # no_timeseries_data_security_id_value_map, we update these as we work backwards in time
        _update_holdings_maps_for_transactions_at(date_to_process)

        # Decrement the day by one, as we work backwards
        date_to_process = date_to_process - datetime.timedelta(days=1)

    portfolio_timeseries.save()
    return portfolio_timeseries
