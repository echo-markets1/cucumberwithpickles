
from typing import List

from pandas import Series
from HOTH.models.base_timeseries import TimeseriesDatapoint


def convert(timeseries: List[TimeseriesDatapoint]) -> Series:
    '''
    Converts a list of TimeseriesDatapoints into a Pandas Series.

    Creates the Pandas Series using the values and timestamps from each
    TimeseriesDatapoint as the data and indices of the Pandas Series

    Parameters:
        - timeseries (List[TimeseriesDatapoint]): The timeseries info to convert

    Returns:
        - Series: The Pandas Series representation of the timeseries
    '''
    values = []
    dates = []
    for datapoint in timeseries:
        values.append(datapoint.value)
        dates.append(datapoint.timestamp)
    return Series(data=values, index=dates, dtype=float)
