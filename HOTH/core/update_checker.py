import datetime
from typing import Tuple
import pytz


def check_up_to_date(today: datetime.date, last_updated: datetime.date) -> Tuple[bool, datetime.date]:
    '''
    Analyzes the last_update date to evaluate whether or not an update should be triggered

    Parameters:
        - today (date): The current date
        - last_updated (date): The date object representing the last time an update occurred

    Returns:
        - Tuple[bool, date]: Whether or not an update is required, along with the
            date last_updated
    '''
    if not last_updated:
        # If last_updated is None, it has never been updated, so set to default of 4 years ago
        return (False, today - datetime.timedelta(days=1095))

    elif today.weekday() == 6 and last_updated >= (today - datetime.timedelta(days=2)):
        # today is sunday, last_updated was friday (2 days previous), and no new data available
        return (True, last_updated)

    elif today.weekday() == 0 and last_updated >= (today - datetime.timedelta(days=3)):
        # today is monday, last_updated was friday (3 days previous), and no new data available
        return (True, last_updated)

    elif last_updated <= (today - datetime.timedelta(days=1)):
        # last_updated is greater than or equal to a day ago
        return (False, last_updated)
    else:
        # last_updated is within 1 day (so no updating required)
        return (True, last_updated)


def get_current_date_for_timezone(tz: str = 'US/Eastern') -> datetime.date:
    ''' Takes a timezone as string and returns the current date in that timezone '''
    now = datetime.datetime.now(pytz.timezone(tz))
    return now.date()
