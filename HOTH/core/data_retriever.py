import uuid
from datetime import datetime, date
from django.db.models.query import QuerySet
from typing import List
from ENDOR.extensions import data_extensions as endor_data_extension
from HOTH.models import (TimeseriesDatapoint, PortfolioTimeseries, StockTimeseries,
                         Investor, Portfolio)
from HOTH.core import timeseries_updater, update_checker


def _retrieve_update_portfolio_timeseries(portfolio_id: str) -> PortfolioTimeseries:
    try:
        portfolio_timeseries = PortfolioTimeseries.objects.get(
            portfolio__id=portfolio_id)
        (portfolio_is_up_to_date, _) = update_checker.check_up_to_date(
            today=update_checker.get_current_date_for_timezone(),
            last_updated=portfolio_timeseries.last_updated)
        if portfolio_is_up_to_date:
            return portfolio_timeseries

    except PortfolioTimeseries.DoesNotExist:
        pass

    portfolio_data = endor_data_extension.get_portfolio(
        portfolio_id=portfolio_id)
    (investor, _) = Investor.objects.get_or_create(
        user_id=portfolio_data['user_id'])
    (portfolio, _) = Portfolio.objects.get_or_create(id=portfolio_id)
    portfolio_timeseries = timeseries_updater.update_portfolio_timeseries(
        portfolio_data, investor, portfolio)
    return portfolio_timeseries


def _retrieve_update_user_portfolios_timeseries(investor_id: uuid.UUID) -> List[PortfolioTimeseries]:
    portfolios_data = endor_data_extension.get_portfolios(user_id=investor_id)
    user_portfolios_timeseries = []
    for portfolio_data in portfolios_data:
        portfolio_timeseries = _retrieve_update_portfolio_timeseries(
            portfolio_data['id'])
        user_portfolios_timeseries.append(portfolio_timeseries)
    return user_portfolios_timeseries


def _retrieve_update_stock_timeseries(symbol: str) -> StockTimeseries:
    try:
        stock_timeseries = StockTimeseries.objects.get(symbol=symbol)
        (stock_is_up_to_date, _) = update_checker.check_up_to_date(
            today=update_checker.get_current_date_for_timezone(),
            last_updated=stock_timeseries.last_updated)
        if stock_is_up_to_date:
            return stock_timeseries
    except StockTimeseries.DoesNotExist:
        pass
    return timeseries_updater.get_stock_timeseries(symbol)


def get_portfolio_timeseries(portfolio_id: str, start_date: str = None, end_date: str = None) -> QuerySet:
    # basically queryset wrapper for timeseries
    # return ordered list of datapoints

    timeseries = _retrieve_update_portfolio_timeseries(
        portfolio_id=portfolio_id)
    queryset = TimeseriesDatapoint.objects.filter(timeseries=timeseries)

    if start_date is not None:
        start_date_object = datetime.strptime(start_date, '%Y-%m-%d')
        queryset = queryset.filter(timestamp__gte=start_date_object)

    if end_date is not None:
        end_date_object = datetime.strptime(end_date, '%Y-%m-%d')
        queryset = queryset.filter(timestamp__lte=end_date_object)

    return queryset


def get_user_portfolio_timeseries(investor_id: uuid.UUID, start_date: str = None, end_date: str = None) -> List[QuerySet]:
    user_portfolios_timeseries_list = _retrieve_update_user_portfolios_timeseries(
        investor_id=investor_id)
    start_date_object = date.min
    end_date_object = date.max
    if start_date is not None:
        start_date_object = datetime.strptime(start_date, '%Y-%m-%d')
    if end_date is not None:
        end_date_object = datetime.strptime(end_date, '%Y-%m-%d')
    timeseries_list = []
    for timeseries in user_portfolios_timeseries_list:
        timeseries_qs = TimeseriesDatapoint.objects.filter(
            timeseries=timeseries, timestamp__gte=start_date_object, timestamp__lte=end_date_object)
        timeseries_list.append(timeseries_qs)
    return timeseries_list


def get_composite_portfolio_timeseries(investor_id: uuid.UUID, start_date: str = None, end_date: str = None) -> List[TimeseriesDatapoint]:
    user_portfolios_timeseries_list = _retrieve_update_user_portfolios_timeseries(
        investor_id=investor_id)
    start_date_object = date.min
    end_date_object = date.max
    if start_date is not None:
        start_date_object = datetime.strptime(start_date, '%Y-%m-%d')
    if end_date is not None:
        end_date_object = datetime.strptime(end_date, '%Y-%m-%d')
    timestamp_datapoint_map = {}
    for timeseries in user_portfolios_timeseries_list:
        timeseries_qs = TimeseriesDatapoint.objects.filter(
            timeseries=timeseries, timestamp__gte=start_date_object, timestamp__lte=end_date_object)
        for datapoint in timeseries_qs:
            datapoint_to_update = timestamp_datapoint_map.get(
                datapoint.timestamp.isoformat(), None)
            if datapoint_to_update is None:
                datapoint_to_update = datapoint
            else:
                datapoint_to_update = timestamp_datapoint_map[datapoint.timestamp.isoformat(
                )]
                datapoint_to_update.value = datapoint_to_update.value + datapoint.value
            timestamp_datapoint_map[datapoint.timestamp.isoformat(
            )] = datapoint_to_update
    return list(timestamp_datapoint_map.values())


def get_portfolio_return(portfolio_id: str, start_date: str = None, end_date: str = None):
    # Return (percent return, raw value)
    # call timeseries retriever, then use first and last to calculate
    datapoints = get_portfolio_timeseries(
        portfolio_id, start_date, end_date).order_by('-timestamp')
    raw_return = datapoints.first().value - datapoints.last().value
    percent_return = (raw_return / datapoints.last().value) * 100
    return(round(raw_return, 2), round(percent_return, 2))


def get_stock_timeseries(symbol: str, start_date: str = None, end_date: str = None) -> QuerySet:
    timeseries = _retrieve_update_stock_timeseries(symbol=symbol)
    queryset = TimeseriesDatapoint.objects.filter(timeseries=timeseries)

    if start_date is not None:
        start_date_object = datetime.strptime(start_date, '%Y-%m-%d')
        queryset = queryset.filter(timestamp__gte=start_date_object)

    if end_date is not None:
        end_date_object = datetime.strptime(end_date, '%Y-%m-%d')
        queryset = queryset.filter(timestamp__lte=end_date_object)

    return queryset
