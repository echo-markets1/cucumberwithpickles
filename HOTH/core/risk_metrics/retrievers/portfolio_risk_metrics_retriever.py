from typing import List
from HOTH.models.period import Period
from HOTH.core import update_checker
from HOTH.models.portfolio import Portfolio
from HOTH.models.risk_metrics import PortfolioRiskMetrics, RiskMetrics
from HOTH.core.risk_metrics.retrievers.risk_metrics_retriever import RiskMetricsRetriever

from HOTH.core import data_retriever, timeseries_to_pandas_series_converter
from pandas import Series


class PortfolioRiskMetricsRetriever(RiskMetricsRetriever):
    '''
    RiskMetricsRetriever specialized for retrieving risk metrics data for a
    given portfolio.
    '''
    # Represented as an array of value, label tuples, similar to what is provided
    # by TextChoices.choices
    __supported_periods = [Period.ONE_YEAR]

    @property
    def supported_periods(self) -> List[Period]:
        return self.__supported_periods

    def _get_source_timeseries_as_pandas_series(self, source_id: str, start_date: str) -> Series:
        '''
        Retrieves the timeseries data for the portfolio, and returns it as a
        Pandas Series.

        Parameters:
            - source_id(str): The portfolio.id
            - start_date(str): An isoformat string representation of the earliest
            - timeseries data point

        Returns:
            - Series: A Pandas Series showing the values of the portfolio over time,
                created from the values and dates of the retrieved PortfolioTimeseries
        '''
        timeseries = data_retriever.get_portfolio_timeseries(
            source_id, start_date)
        return timeseries_to_pandas_series_converter.convert(list(timeseries))

    def _get_benchmark_timeseries_as_pandas_series(self, benchmark_id: str, start_date: str) -> Series:
        timeseries = data_retriever.get_stock_timeseries(
            benchmark_id, start_date)
        return timeseries_to_pandas_series_converter.convert(list(timeseries))

    def retrieve(self, source_id: str, benchmark_id: str, period: Period) -> RiskMetrics:
        '''
        Retrieve up to date RiskMetrics for a portfolio

        Given the ids for the portfolio and the benchmark, retrieve the most
        up to date risk metrics for the portfolio. If the risk metrics have been
        updated within the past day, return the existing RiskMetric object.
        Otherwise, create the new RiskMetrics object, or update the existing one.

        Parameters:
            - source_id (str): The id of the related Portfolio object
            - benchmark_id (str): The id of the benchmark used in risk metric calculation
            - period (Period): The enumerated period of time for which the risk metrics are calculated against

        Returns:
            - RiskMetrics: The up to date instance of the PortfolioRiskMetrics
        '''
        try:
            risk_metrics = PortfolioRiskMetrics.objects.get(
                portfolio__id=source_id)
            up_to_date = update_checker.check_up_to_date(
                today=update_checker.get_current_date_for_timezone(),
                last_updated=risk_metrics.last_updated)[0]
            if up_to_date:
                print(
                    f"Risk Metrics for portfolio: {source_id} are already up to date. Returning existing risk_metrics.")
                return risk_metrics
        except PortfolioRiskMetrics.DoesNotExist:
            pass
        print(
            f"Risk Metrics for portfolio: {source_id} are not up to date. Updating risk_metrics now.")
        # If the risk_metrics are not up to date or don't exist, run calculate
        # based on the most recent timeseries data.
        fields_to_save = self._calculate(source_id, benchmark_id, period)
        portfolio = Portfolio.objects.get_or_create(id=source_id)[0]
        return PortfolioRiskMetrics.objects.update_or_create(defaults=fields_to_save, portfolio=portfolio, period=period)[0]
