import pytz
from HOTH.models.risk_metrics import Period, RiskMetrics
from datetime import date, timedelta, datetime
from typing import Dict, List, Tuple
from pandas import Series

import abc
import quantstats


class RiskMetricsRetriever(abc.ABC):
    '''
    Abstract class used to retrieve the most up to date RiskMetrics instance for
    a given entity and benchmark over a given period. Includes concrete methods
    for shared common logic, but the subclass must implement the methods specific
    to pulling their required data and saving the output, as these will be unique
    to the type of RiskMetrics being retrieved

    Attributes:
        - __period_start_date_timedelta_map (Dict): Maps a Period to the timedelta
            required for calculating the start date
        - __period_years_map (Dict): Maps a Period to the corresponding number of years
        - __trading_days_per_year (int): The number of trading days in a year
    '''

    __period_years_map = {
        Period.ONE_YEAR: 1
    }
    __trading_days_per_year = 252

    @property
    @abc.abstractmethod
    def supported_periods(self) -> List[Period]:
        pass

    @abc.abstractmethod
    def _get_source_timeseries_as_pandas_series(self, source_id: str, start_date: str) -> Series:
        '''
        Retrieves the timeseries data for the source entity, and returns it as a
        Pandas Series. The source entity will be unique to the implementing subclass.

        Parameters:
            - source_id (str): The unique identifier of the source object
            - start_date (str): An isoformat string representation of the earliest
                timeseries data point

        Returns:
            - Series: A Pandas Series showing the values of the source over time,
                created from the values and dates of the retrieved timeseries
        '''
    @abc.abstractmethod
    def _get_benchmark_timeseries_as_pandas_series(self, benchmark_id: str, start_date: str) -> Series:
        '''
        Retrieves the timeseries data for the benchmark entity, and returns it
        as a Pandas Series. The benchmark entity will be unique to the implementing
        subclass, but will typically be the S&P 500 (SPX).

        Parameters:
            - benchmark_id (str): The unique identifier of the benchmark object
            - start_date (str): An isoformat string representation of the earliest timeseries data point

        Returns:
            - Series: A Pandas Series showing the values of the benchmark over time,
                created from the values and dates of the retrieved timeseries
        '''

    def _get_values_series(self, source_id: str, benchmark_id: str, start_date: str) -> Tuple[Series, Series]:
        ''' Retrieves the source and benchmark value history Series, dropping any extra indices '''
        source_timeseries = self._get_source_timeseries_as_pandas_series(
            source_id, start_date)
        benchmark_timeseries = self._get_benchmark_timeseries_as_pandas_series(
            benchmark_id, start_date)

        # Make sure the series are of equal length. Required for quantstats processing
        source_indices_to_drop = source_timeseries.index.difference(
            benchmark_timeseries.index)
        if(source_indices_to_drop.size != 0):
            source_timeseries.drop(labels=source_indices_to_drop, inplace=True)
        benchmark_indices_to_drop = benchmark_timeseries.index.difference(
            source_timeseries.index)
        if(benchmark_indices_to_drop.size != 0):
            benchmark_timeseries.drop(
                labels=benchmark_indices_to_drop, inplace=True)
        return (source_timeseries, benchmark_timeseries)

    def _get_returns_series(self, source_values_series: Series, benchmark_values_series: Series) -> Tuple[Series, Series]:
        '''
        Retrieves the source and benchmark returns history Series

        The returns history is represented as the day over day change (as a %)

        Parameters:
            - source_values_series (Series): The raw values of the source over time
            - benchmark_values_series (Series): The raw values of the benchmark over time

        Returns:
            - Tuple[Series, Series]: A tuple of Series representing the source's
            - and benchmark's return histories respectively.
        '''
        # The result of the pct_change() function is the the first index is always NaN
        # Replace with 0 to keep length of data consistent with values series
        return (source_values_series.pct_change().fillna(value=0), benchmark_values_series.pct_change().fillna(value=0))

    def _compile_metrics(self, source_values_series: Series, source_returns_series: Series, benchmark_returns_series: Series, num_trading_days: float) -> Dict[str, float]:
        '''
        Calculates and compiles the metrics, returning them as a python dictionary

        Parameters:
            - source_values_series (Series): The raw values of the source over time
            - source_returns_series (Series): The day over day % returns of the source
                over time
            - benchmark_returns_series (Series): The day over day % returns of the
                benchmark over time
            - num_trading_days (float): The number of trading days over the
                calculated time period

        Returns:
            - Dict: A dictionary containing each risk metric keyed by it's name
        '''
        metrics = {}
        greeks = quantstats.stats.greeks(
            returns=source_returns_series,
            benchmark=benchmark_returns_series,
            periods=num_trading_days
        )

        metrics["alpha"] = greeks["alpha"]
        metrics["beta"] = greeks["beta"]
        metrics["standard_deviation"] = source_values_series.std()
        metrics["sharpe"] = quantstats.stats.sharpe(
            returns=source_returns_series,
            periods=num_trading_days,
            trading_year_days=num_trading_days
        )
        metrics["r_squared"] = quantstats.stats.r_squared(
            returns=source_returns_series,
            benchmark=benchmark_returns_series
        )

        return metrics

    def __get_start_date(self, period: Period) -> date:
        ''' Get the date of the earliest datapoint required based on the period '''
        now = datetime.now(pytz.timezone('US/Eastern'))
        today = now.date()
        return today - timedelta(days=period.as_days)

    def __get_period_as_trading_days(self, period: Period) -> float:
        ''' Get number of trading days that occurred over the period '''
        return float(self.__trading_days_per_year * self.__period_years_map[period])

    def _calculate(self, source_id: str, benchmark_id: str, period: Period) -> Dict:
        ''' Calculate all the information to update the RiskMetrics instance '''
        start_date = self.__get_start_date(period).isoformat()
        (source_values, benchmark_values) = self._get_values_series(
            source_id, benchmark_id, start_date)
        (source_returns, benchmark_returns) = self._get_returns_series(
            source_values, benchmark_values)
        num_trading_days = self.__get_period_as_trading_days(period)
        fields_to_save = self._compile_metrics(
            source_values, source_returns, benchmark_returns, num_trading_days)
        fields_to_save["last_updated"] = date.today()
        return fields_to_save

    @abc.abstractmethod
    def retrieve(self, source_id: str, benchmark_id: str, period: Period = Period.ONE_YEAR) -> RiskMetrics:
        '''
        Retrieve up to date RiskMetrics for a portfolio

        Given the ids for the source entity and the benchmark, retrieve the most
        up to date risk metrics.

        Parameters:
            - source_id (str): The id of the source entity, specific to the subclass
            - benchmark_id (str): The id of the benchmark used in risk metric calculation
            - period (Period): The enumerated period of time for which the risk metrics are calculated against

        Returns:
            - RiskMetrics: The up to date instance of the RiskMetrics, type specific
                to the subclass
        '''
