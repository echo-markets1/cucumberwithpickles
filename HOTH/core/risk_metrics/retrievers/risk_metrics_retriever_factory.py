
from HOTH.core.risk_metrics.retrievers.risk_metrics_retriever import RiskMetricsRetriever
from HOTH.core.risk_metrics.retrievers.portfolio_risk_metrics_retriever import PortfolioRiskMetricsRetriever


PORTFOLIO = "PORTFOLIO"


class RiskMetricsRetrieverFactory:
    __retrievers = {
        PORTFOLIO: PortfolioRiskMetricsRetriever()
    }

    def get_retriever(self, type) -> RiskMetricsRetriever:
        return self.__retrievers[type]


RISK_METRICS_RETRIEVER_FACTORY = RiskMetricsRetrieverFactory()
