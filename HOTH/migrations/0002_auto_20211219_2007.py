# Generated by Django 3.0.2 on 2021-12-19 20:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HOTH', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfoliosuperlativeperformancemetrics',
            name='percent_return',
            field=models.DecimalField(decimal_places=3, max_digits=12),
        ),
    ]
