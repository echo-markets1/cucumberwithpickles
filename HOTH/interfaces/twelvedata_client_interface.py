import json
import datetime
import pickle
import os.path
from typing import Dict, List
from twelvedata import TDClient
from twelvedata.exceptions import BadRequestError
from django.conf import settings
from HOTH.core import update_checker
from HOTH.models.base_timeseries import IntervalGranularity


def _pretty_format(response):
    print(json.dumps(response, indent=2, sort_keys=True))


client = TDClient(apikey=settings.TWELVEDATA_API_KEY)
PICKLED_CRYPTOS_LIST_FILE_NAME = ".supported_cryptos.p"
MAX_OUTPUTSIZE = 5000


def get_time_series(symbols: List, interval: IntervalGranularity, start_date: datetime.date, end_date: datetime.date = None) -> Dict:
    '''
    Retrieves timeseries data for a list of stocks via the TwelveData Client for
    a given date range. 

    Parameters: 
        - symbols (list): A list of symbols for stocks whose timeseries data needs
            updating
        - interval (IntervalGranularity): The interval as defined by TwelveData
            representing the distance between two datapoints in the timeseries.
            For more info, see https://twelvedata.com/docs#stocks
        - start_date (date): The date object representing the earliest data point
            to retrieve.
        - end_date (date): The date object representing the most recent data point
            to retrieve.
    '''
    start_date = start_date.strftime("%Y-%m-%d %H:%M:%S")
    if not end_date is None:
        end_date = end_date.strftime("%Y-%m-%d %H:%M:%S")
    timeseries_data = {}
    try:
        ts = client.time_series(
            symbol=symbols,
            interval=interval,
            start_date=start_date,
            end_date=end_date,
            outputsize=MAX_OUTPUTSIZE,
            country="United States",
            timezone="UTC"
        )
        json_data = ts.as_json()
        # The return from twelvedata looks different when one vs multiple symbols' data is requested.
        # This standardizes it
        if len(symbols) == 1:
            timeseries_data[symbols[0]] = json_data
        else:
            timeseries_data = json_data
    except BadRequestError as bre:
        print(f"WARN: Could not update the timeseries data for the following equities: {symbols}" +
              f". Returning: {timeseries_data}. Parameters: interval = {interval}, start_date = " +
              f"{start_date}, end_date = {end_date}. Error: {bre}."
              )

    return timeseries_data


def get_supported_cryptos(currency_for_quote: str = "USD") -> List[str]:
    '''
    Retrieves a list of supported cryptocurrencies from TwelveData.

    Stores a pickled list in memory for faster loading if updated in the previous
    24 hours.

    Parameters:
        - currency_for_quote (str): The currency code the crypto quote would be reported in

    Returns:
        - List[str]: A list of the cryptos supported by twelvedata
    '''
    ### INNER FUNCTIONS ###
    def _update_supported_cryptos_list(date_updated: datetime.date) -> List[str]:
        cryptocurrencies_list_response = client.get_cryptocurrencies_list(
            # Limit to the quotes in terms of a single currency to limit response size
            currency_quote=currency_for_quote,
        )
        cryptocurrencies_list_response_json = cryptocurrencies_list_response.as_json()
        _supported_cryptos = []
        for result in cryptocurrencies_list_response_json:
            # Example symbols: "BTC/USD", "ETH/USD"
            [crypto_symbol, quote_currency_symbol] = result['symbol'].split(
                '/')
            _supported_cryptos.append(crypto_symbol)
        with open(PICKLED_CRYPTOS_LIST_FILE_NAME, 'wb') as supported_cryptos_file:
            pickle.dump(
                {
                    'last_updated': date_updated.isoformat(),
                    'supported_cryptos_list': _supported_cryptos
                },
                supported_cryptos_file
            )
        return _supported_cryptos

    ### MAIN FUNCTION CODE ###
    today = update_checker.get_current_date_for_timezone()

    # If we have the most up to date list of cryptos available, read from file
    if os.path.exists(PICKLED_CRYPTOS_LIST_FILE_NAME):
        with open(PICKLED_CRYPTOS_LIST_FILE_NAME, 'rb') as supported_cryptos_file:
            supported_cryptos_data = pickle.load(supported_cryptos_file)
            if datetime.date.fromisoformat(supported_cryptos_data['last_updated']) == today:
                return supported_cryptos_data['supported_cryptos_list']

    # If file is out of date or does not exist, trigger the api call
    return _update_supported_cryptos_list(date_updated=today)


def get_stock_info(symbol):
    stocks_list = client.get_stocks_list(
        symbol=symbol, country="United States")
    return stocks_list.as_json()
