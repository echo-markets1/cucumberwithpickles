from typing import Dict
from HOTH.models.serializers import (PortfolioTimeseriesSerializer,
                                     TimeseriesDatapointSerializer, StockTimeseriesSerializer)
from HOTH.core import data_retriever


def get_portfolio_timeseries(portfolio_id, start_date=None, end_date=None) -> Dict:

    queryset = data_retriever.get_portfolio_timeseries(
        portfolio_id=portfolio_id, start_date=start_date, end_date=end_date)
    return TimeseriesDatapointSerializer(queryset, many=True).data


def get_investor_portfolios_timeseries(investor_id, start_date=None, end_date=None) -> Dict:
    queryset = data_retriever.get_user_portfolio_timeseries(
        investor_id=investor_id, start_date=start_date, end_date=end_date)
    return PortfolioTimeseriesSerializer(queryset, many=True).data


def get_portfolio_return(portfolio_id, start_date=None, end_date=None) -> Dict:
    (raw_return, percent_return) = data_retriever.get_portfolio_return(
        portfolio_id, start_date, end_date)
    return {'raw_return': raw_return, 'percent_return': percent_return}


def get_stock_timeseries(symbol, start_date=None, end_date=None):
    queryset = data_retriever.get_stock_timeseries(
        symbol=symbol, start_date=start_date, end_date=end_date)
    return StockTimeseriesSerializer(queryset).data
