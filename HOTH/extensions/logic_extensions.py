from typing import List
from HOTH.interfaces import twelvedata_client_interface


def get_supported_cryptos_list(currency_for_quote: str = None) -> List[str]:
    """
    Retrieve the list of crypto currencies supported by twelvedata.

    Parameters:
        - currency_for_quote (str): The currency the price of the crypto would be quoted in
    """
    return twelvedata_client_interface.get_supported_cryptos() if currency_for_quote is None else twelvedata_client_interface.get_supported_cryptos(currency_for_quote)
