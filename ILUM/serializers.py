from rest_framework import serializers

from ILUM.models import EchoInvite, GroupInvite


class PlatformInviteSerializer(serializers.ModelSerializer):
    class Meta:
        model = EchoInvite
        fields = '__all__'
        read_only_fields = ['invitor', 'status', 'last_updated']


class GroupInviteSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupInvite
        fields = '__all__'
        read_only_fields = ['invitor', 'status', 'last_updated']
