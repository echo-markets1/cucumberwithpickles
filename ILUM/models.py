""" Models for storing invite data and inferring user referral data. """
import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class InviteStatus(models.TextChoices):
    """ The state of the invite. """
    PENDING = 'P', _("Invite has been sent, but no action taken")
    ACCEPTED = 'A', _("Invite has been accepted by the recipient")
    DELETED = 'D', _("Invite has been revoked by the invitor")
    REJECTED = 'R', _("Invite has been rejected by the recipient")
    EXPIRED = 'E', _("Invite has expired")


class Invitor(models.Model):
    """
    Representation within ILUM of an existing user of the platform. The invitor
    is the individual sending out an invite.
    """
    # The id of the associated user
    user_id = models.UUIDField(primary_key=True, editable=False)


class BaseInvite(models.Model):
    """ A an abstract base representation of an invite. """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    invitor = models.ForeignKey(
        Invitor, on_delete=models.DO_NOTHING, related_name="%(class)ss")
    status = models.CharField(max_length=1,
                              editable=True,
                              choices=InviteStatus.choices,
                              default=InviteStatus.PENDING)

    # When the invite was created/sent
    created = models.DateTimeField(
        default=timezone.now, editable=False)

    # The last time the status of this field was updated
    last_updated = models.DateTimeField(
        default=timezone.now, editable=True)

    # The email the invite is being extended to
    recipient_email = models.EmailField(max_length=254)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.last_updated = timezone.now()
        super().save(*args, **kwargs)


class EchoInvite(BaseInvite):
    """
    An invite to the echo platform. Separated from BaseInvite to prevent
    unneccessary concrete inheritance of derivative invite models, such as GroupInvite.
    In other words, when creating a GroupInvite, we do not want to also create
    a platform invite.
    """
    class Meta:
        abstract = False
        # Don't duplicate invites (same invitor and recipient)
        unique_together = ("invitor", "recipient_email")


class Group(models.Model):
    """ Representation within ILUM of an existing group on the platform. """
    id = models.UUIDField(primary_key=True, editable=False)


class GroupInvite(BaseInvite):
    """ An invite to a group. """
    group = models.ForeignKey(
        Group, on_delete=models.DO_NOTHING, related_name='invitations')

    class Meta:
        abstract = False
        # Don't duplicate invites (same invitor, recipient and group)
        unique_together = ("invitor", "recipient_email", "group")
