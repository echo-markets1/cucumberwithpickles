""" A data object for storing about the user sending the invite (the invitor). """


class InvitorInfo:
    """
    A data object for storing about the user sending the invite (the invitor).

    Attributes:
        - __first_name (str): The first name of the invitor
        - __last_name (str): The last name of the invitor
        - __handle (str): The handle for the invitor
    """

    def __init__(self, first_name: str, last_name: str, handle: str):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__handle = handle

    @property
    def name(self):
        """ Provides the invitors name in short format, first name + last initial """
        return f"{self.__first_name} {self.__last_name[0]}."

    @property
    def handle(self):
        """ Provides the invitors handle """
        return self.__handle
