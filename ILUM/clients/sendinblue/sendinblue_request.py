""" A data object defining the info required for making requests to SendInBlue """
import json
from typing import List

from ILUM.clients.sendinblue import sendinblue_constants
from ILUM.clients.sendinblue.invitor_info import InvitorInfo


class SendInBlueRequest:
    """
    A data object to store and convert data required for making requests to
    SendInBlue in a format compatible with their API, including serialization
    into a JSON object.
    See the documentation for more details:
    https://developers.sendinblue.com/reference/sendtransacemail.

    Attributes:
        - __to: The 'to' field of the request to SendInBlue. Currently only composed
            of the email address(es) the email is being sent to.
        - __template_id: The template of the email to send, defined in SendInBlue
        - __params: Addtional, user defined (in SendInBlue templates), parameters
            to help create the email.
    """

    def __init__(self, template_id: int, recipient_emails: List[str], link: str,
                 invitor_info: InvitorInfo):
        self.__to = list(
            map(lambda email: {"email": email}, recipient_emails)
        )
        self.__template_id = template_id
        self.__params = {
            sendinblue_constants.LINK: link,
            sendinblue_constants.INVITOR_NAME: invitor_info.name,
            sendinblue_constants.INVITOR_HANDLE: invitor_info.handle
        }

    @property
    def as_json(self):
        """ Output the request object in JSON format"""
        return json.dumps({
            "to": self.__to,
            "templateId": self.__template_id,
            "params": self.__params
        })
