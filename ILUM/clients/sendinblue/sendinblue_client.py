"""
Lightweight client wrapper for calling SendInBlue

Classes:
    SendInBlueClient
"""
import json
from typing import Tuple
import requests
from ILUM.clients.sendinblue.sendinblue_request import SendInBlueRequest
from django.conf import settings


class SendInBlueClient:
    """
    SendInBlue allows programatically creating and sending emails.
    This class is a lightweight handrolled wrapper for making calls directly
    to SendInBlue. Currently, within the ILUM service, this client is used
    to send invite emails. In the future, we may opt to use the SendInBlue sdk,
    or abstract or own client into a separate, reusable package.

    Attributes:
        - __api_key (str): The SendInBlue v3 API Key
        - __api_endpoint (str): The SendInBlue endpoint to make requests against
        - __header_accept (str): The accept value of the request header
        - __header_content (str): The content value of the request header
    """
    __api_key = settings.SENDINBLUE_API_KEY
    __api_endpoint = "https://api.sendinblue.com/v3/smtp/email"

    def __init__(self, header_accept: str, header_content_type: str) -> None:
        self.__header_accept = header_accept
        self.__header_content_type = header_content_type

    def send_email(self, request: SendInBlueRequest) -> Tuple[bool, str]:
        """
        Triggers a request to SendInBlue to send a templated email.
        See the documentation for more details:
        https://developers.sendinblue.com/reference/sendtransacemail.

        Parameters:
            - request (SendInBlueRequest): The request object.

        Returns
            (bool, str): A bool indicating where the request was made successfully
                and a str message describing the outcome.
        """
        request_json = request.as_json
        headers = {
            'accept': self.__header_accept,
            'content-type': self.__header_content_type,
            'api-key': self.__api_key
        }
        response = requests.request(
            method="POST", url=self.__api_endpoint, data=request_json, headers=headers)
        response_text_dict = json.loads(response.text)
        if response.status_code == 201:
            return (True, f"Email sent successfully. messageId: {response_text_dict['messageId']}")
        else:
            return (False, f"Email failed to send. Error code: {response_text_dict['code']}."
                    + f"Error Message: {response_text_dict['message']}."
                    )


sendinblue_client = SendInBlueClient(
    header_accept="application/json", header_content_type="application/json")
