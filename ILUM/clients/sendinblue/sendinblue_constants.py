""" Constants for interacting with SendInBlue """
### Email Template Id Constants ###
# See https://my.sendinblue.com/camp/lists/template
INVITE_TO_GROUP = 1
INVITE_TO_PLATFORM = 2

### Request Params Constants ###
LINK = "LINK"
INVITOR_NAME = "INVITOR_NAME"
INVITOR_HANDLE = "INVITOR_HANDLE"
