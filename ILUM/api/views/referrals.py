from rest_framework import serializers, status, views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from ILUM.core.invite import invite_types
from ILUM.core.referral.referral_link_creator_factory import REFERRAL_LINK_CREATOR_FACTORY
from ILUM.core.referral.referral_link_response_handler_factory import REFERRAL_LINK_RESPONSE_HANDLER_FACTORY
from ILUM.serializers import GroupInviteSerializer, PlatformInviteSerializer


class ReferralLinkRetrieveView(views.APIView):
    """
    Retrieve the referral link for a given user, invite type, and related model.
    For example, if the link was for joining a Group, the related model would be
    the primary key of the Group.

    **GET**
        kwargs:
            - invite_type (str): The type of invite the referral is for. Default
                to PLATFORM if none is provided
            - pk (uuid.UUID): The primary key of the related model. Defaults to
                the user's id if none is provided

        Returns:
            The link that was generated.

    """
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        # Default to the PLATFORM case, where model instance = invitor
        model_instance_pk = kwargs.get("pk", request.user.id)
        invite_type = kwargs.get("invite_type", invite_types.PLATFORM)
        link_creator = REFERRAL_LINK_CREATOR_FACTORY.get_creator(invite_type)
        link = link_creator.create(
            model_instance_pk=model_instance_pk,
            invitor_user_id=request.user.id
        )
        return Response(data={'referral_link': link}, status=status.HTTP_200_OK)


class AcceptReferralLinkView(views.APIView):
    """
    Accept an invite based on a referral link, creating a new invite if neccessary.

    **POST**
        kwargs:
            - invite_type (str): The type of invite the referral is for

        request.data:
            - referring_user_idb64 (str): The uid of the user who provided the
                link, base64 encoded
            - accepting_user_idb64 (str): The uid of the user redeeming the
                link, base64 encoded
            - token (str): The token uniquely validating the link,
            - token_model_pkb64: The uid of the entity the referral relates to,
                base 64 encoded. For example, a referral link for a group would
                require the uid of the group to redeem

        Returns:
            Serialized representation of the resulting Invite
    """
    authentication_classes = []
    permission_classes = []

    @property
    def invite_type_serializer_map(self):
        return {
            invite_types.PLATFORM: PlatformInviteSerializer,
            invite_types.GROUP: GroupInviteSerializer
        }

    @property
    def serializer_class(self) -> serializers.ModelSerializer:
        """
        The serializer needs to be chosen dynamically, but shouldn't define
        the API schema. Use the invite type to determine the appropriate serializer.
        """
        return self.invite_type_serializer_map[self.kwargs.get("invite_type")]

    def post(self, request, *args, **kwargs):
        invite_type = kwargs.get("invite_type")
        handler = REFERRAL_LINK_RESPONSE_HANDLER_FACTORY.get_handler(
            invite_type)
        (success, result) = handler.handle_accept(**request.data)
        if success:
            serializer = self.serializer_class(instance=result)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data={'message': result}, status=status.HTTP_400_BAD_REQUEST)
