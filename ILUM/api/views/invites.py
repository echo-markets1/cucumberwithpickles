from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from ILUM.core.invite import invite_types
from ILUM.core.invite.creators.echo_invite_creator import echo_invite_creator
from ILUM.core.invite.creators.group_invite_creator import group_invite_creator
from ILUM.core.invite.response_handlers.invite_response_handler_factory import \
    INVITE_RESPONSE_HANDLER_FACTORY
from ILUM.models import EchoInvite, GroupInvite
from ILUM.serializers import GroupInviteSerializer, PlatformInviteSerializer


class AcceptInviteView(generics.GenericAPIView):
    '''
    Endpoint for accepting invites, verified via token.

    **GET*
        kwargs:
            - invite_type (str): The type of invite the referral is for. Default
                to PLATFORM if none is provided
            - uidb64 (str): The base 64 encoded id of the invite
            - token (str): The token uniquely validating the invite

        Returns:
            Serialized representation of the resulting invite
    '''
    authentication_classes = []
    permission_classes = []

    @property
    def invite_type_serializer_map(self):
        return {
            invite_types.PLATFORM: PlatformInviteSerializer,
            invite_types.GROUP: GroupInviteSerializer
        }

    def get_serializer_class(self):
        return self.invite_type_serializer_map[self.kwargs.get("invite_type")]

    def get(self, request, *args, **kwargs):
        """
        Extract the invite's type, base64 encoded id, and related token and
        attempt to set the status to ACCEPTED.
        GET method because no additional request data required.
        """
        invite_type = kwargs.get("invite_type")
        invite_uidb64 = kwargs.get("uidb64")
        token = kwargs.get("token")
        response_handler = INVITE_RESPONSE_HANDLER_FACTORY.get_handler(
            invite_type)
        (success, result) = response_handler.handle_accept(invite_uidb64, token)
        if success:
            serializer = self.get_serializer(result)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(data={'message': result}, status=status.HTTP_400_BAD_REQUEST)


class DeleteInviteView(generics.GenericAPIView):
    '''
    Endpoint for deleting invites. Initiates a soft delete, and doesn't remove
    the object from the DB. No additional data is required, so this is performed
    with a GET method.

    **GET*
        kwargs:
            - invite_type (str): The type of invite the referral is for. Default
                to PLATFORM if none is provided
            - id (uuid.UUID): The id of the invite

        Returns:
            Serialized representation of the resulting invite
    '''
    permission_classes = [IsAuthenticated]

    @property
    def invite_type_serializer_map(self):
        """ Mapping of invite_type to serializer class. """
        return {
            invite_types.PLATFORM: PlatformInviteSerializer,
            invite_types.GROUP: GroupInviteSerializer
        }

    def get_serializer_class(self):
        return self.invite_type_serializer_map[self.kwargs.get("invite_type")]

    def get(self, request, *args, **kwargs):
        """
        Extract the invite's type and id, attempt to set the status to DELETED.
        GET method because no additional request data required.
        """
        invite_type = kwargs.get("invite_type")
        invite_id = kwargs.get("id")
        response_handler = INVITE_RESPONSE_HANDLER_FACTORY.get_handler(
            invite_type)
        (success, result) = response_handler.handle_delete(
            invite_id, request.user.id)
        if success:
            serializer = self.get_serializer(result)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(data={'message': result}, status=status.HTTP_400_BAD_REQUEST)


class PlatformInviteListCreateView(generics.ListCreateAPIView):
    """
    Endpoint for creating and listing platform (Echo)Invites.

    **GET**
        Returns:
            A list of all invites created by the requesting user in a serialized
            representation

    **POST**
        request.data:
            - recipient_email (str): The email of the individual to invite to the
                platform. Also used to send the invite via email

        Returns:
            Serialized representation of the resulting invite
    """

    permission_classes = [IsAuthenticated]
    serializer_class = PlatformInviteSerializer

    def get_queryset(self):
        return EchoInvite.objects.filter(invitor__user_id=self.request.user.id)

    def create(self, request, *args, **kwargs):
        (success, result) = echo_invite_creator.create(
            invitor_user_id=request.user.id,
            recipient_email=request.data["recipient_email"]
        )
        if success:
            serializer = self.get_serializer(result)
            return Response(
                data=serializer.data,
                status=status.HTTP_201_CREATED,
                headers=self.get_success_headers(serializer.data)
            )
        else:
            return Response(data={'message': result}, status=status.HTTP_404_NOT_FOUND, headers={})


class GroupInviteListCreateView(generics.ListCreateAPIView):
    """
    Endpoint for creating and listing GroupInvites.
    Providing a group_id as a query parameter will list the invites for a given group.

    **GET**
        query_params:
            - group_id (uuid.UUID): The id of the Group to retrieve invites for

        Returns:
            A list of all invites created by the requesting user in a serialized
            representation

    **POST**
        request.data:
            - recipient_email (str): The email of the individual to invite to the
                platform. Also used to send the invite via email
            - group (uuid): The id of the Group to create an invite for

        Returns:
            Serialized representation of the resulting invite
    """

    permission_classes = [IsAuthenticated]
    serializer_class = GroupInviteSerializer

    def get_queryset(self):
        group_id = self.request.query_params.get('group_id', None)
        if group_id:
            return GroupInvite.objects.filter(group__id=group_id)
        return GroupInvite.objects.filter(invitor__user_id=self.request.user.id)

    def create(self, request, *args, **kwargs):
        (success, result) = group_invite_creator.create(
            invitor_user_id=request.user.id,
            recipient_email=request.data["recipient_email"],
            group_id=request.data["group"]
        )
        if success:
            serializer = self.get_serializer(result)
            return Response(
                data=serializer.data,
                status=status.HTTP_201_CREATED,
                headers=self.get_success_headers(serializer.data)
            )
        else:
            return Response(data={'message': result}, status=status.HTTP_404_NOT_FOUND, headers={})
