from django.urls import path
from ILUM.api.views import invites, referrals
from ILUM.core.invite import invite_types

urlpatterns = [
    path('accept/<str:invite_type>/<str:uidb64>/<str:token>/',
         invites.AcceptInviteView.as_view()),
    path('<str:invite_type>/<uuid:id>/delete/',
         invites.DeleteInviteView.as_view()),
    path(f'{invite_types.PLATFORM}/',
         invites.PlatformInviteListCreateView.as_view()),
    path(f'{invite_types.GROUP}/', invites.GroupInviteListCreateView.as_view()),
    path('referrallink/<str:invite_type>/<uuid:pk>/',
         referrals.ReferralLinkRetrieveView.as_view()),
    path('referrallink/<str:invite_type>/',
         referrals.AcceptReferralLinkView.as_view()),
    # The PLATFORM referral case where no pk is needed
    path('referrallink/',
         referrals.ReferralLinkRetrieveView.as_view()),
]
