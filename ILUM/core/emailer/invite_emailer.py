""" Class for sending invite emails via SendInBlue"""
from typing import List, Tuple
from ALDERAAN.extensions.data_extensions import get_name_and_handle_for_user_id
from ILUM.clients.sendinblue.invitor_info import InvitorInfo
from ILUM.clients.sendinblue.sendinblue_client import SendInBlueClient, sendinblue_client
from ILUM.clients.sendinblue.sendinblue_request import SendInBlueRequest
from ILUM.models import BaseInvite


class InviteEmailer:
    """
    Class for sending invite emails based on Invite logic, and including
    the link uniquely identifying the invite.
    """

    def __init__(self, sendinblue_client: SendInBlueClient):
        self.__sendinblue_client = sendinblue_client

    def email(self, template_id: int, invite: BaseInvite, link: str) -> Tuple[bool, str]:
        """
        Builds and submits requests to the SendInBlue client.

        Parameters:
            - template_id (int): The specific email template to use
            - invite (BaseInvite): An invite instance to build the request against
            - link (str): The link uniquely identifying the invite the end user can
                follow to accept their invite

        Returns:
            - (bool, str):  A bool indicating where the request was made successfully
                and a str message describing the outcome.
        """
        invitor_info_kwargs = get_name_and_handle_for_user_id(
            invite.invitor.user_id)
        request = SendInBlueRequest(
            template_id=template_id,
            recipient_emails=[invite.recipient_email],
            link=link,
            invitor_info=InvitorInfo(**invitor_info_kwargs)
        )
        return self.__sendinblue_client.send_email(request)

    def batch_email(self, template_id: int, invites: List[BaseInvite], link: str):
        """ Not yet implemented """
        raise NotImplementedError(
            "Batch email functionality has not been implemented yet.")


invite_emailer = InviteEmailer(sendinblue_client=sendinblue_client)
