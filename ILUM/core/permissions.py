import uuid
from ILUM.models import BaseInvite


def is_invitor_to_delete_invite(invite: BaseInvite, user_id: uuid.UUID) -> bool:
    return invite.invitor.user_id == user_id
