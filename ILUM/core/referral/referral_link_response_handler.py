""" Class for handling the responses of referral links. """
from typing import Dict, Tuple, Union
import uuid
from django.db import models
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from ALDERAAN.extensions.data_extensions import get_user_by_id
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import BaseInvite, EchoInvite, InviteStatus, Invitor


class ModelBasedReferralLinkResponseHandler:
    """
    Based on a given Invite model, and the model used to generate the unique token,
    determine if the referral is vald, and create or update a corresponding invite.

    Attributes:
        - __token_model (models.Model): The class of model used to generate/validate the token
        - __invite_model (BaseInvite): The class of Invite being handled
        - __token_generator (ModelBasedTokenGenerator): The generator to perform token validation
    """

    def __init__(self, token_model: models.Model, invite_model: BaseInvite,
                 token_generator: ModelBasedTokenGenerator):
        self.__token_model = token_model
        self.__invite_model = invite_model
        self.__token_generator = token_generator

    def __get_token_model_instance(self, pk: uuid.UUID) -> Union[models.Model, None]:
        # Retrieve the model instance related to tokem if exists, else return None
        try:
            return self.__token_model.objects.get(pk=pk)
        except:
            return None

    def __get_invite_model_update_or_create_kwargs(self, invitor: Invitor, recipient_email: str,
                                                   token_model_instance: models.Model) -> Dict:
        # Provide the kwargs necessary to update or create the invite instance
        kwargs = {
            "invitor": invitor,
            "recipient_email": recipient_email,
        }
        if self.__invite_model != EchoInvite:
            # Non PLATFORM invites have an additional arguement required for creation
            # This should be the same name as the class of the model used to make the token
            token_model_key = self.__token_model.__name__.lower()
            kwargs[token_model_key] = token_model_instance
        return kwargs

    def handle_accept(self, referring_user_idb64: str, accepting_user_idb64: str, token: str,
                      token_model_pkb64: str) -> Tuple[bool, Union[BaseInvite, str]]:
        """
        When a user accepts/redeems a valid referral link, update or create an accepted Invite

        Parameters:
            - referring_user_idb64 (str): The uid of the user who provided the
                link, base64 encoded
            - accepting_user_idb64 (str): The uid of the user redeeming the
                link, base64 encoded
            - token (str): The token uniquely validating the link,
            - token_model_pkb64: The uid of the entity the referral relates to,
                base 64 encoded. For example, a referral link for a group would
                require the uid of the group to redeem

        Returns:
            (bool, BaseInvite | str): A tuple indicating whether or not the operation was
                successful, and corresponding invite or error message
        """
        # Validate the token
        token_model_pk = force_str(
            urlsafe_base64_decode(token_model_pkb64))
        token_model_instance = self.__get_token_model_instance(token_model_pk)
        if not self.__token_generator.check_token(model_instance=token_model_instance, token=token):
            error_message = "The token and related model were not valid together"
            return (False, error_message)

        # Get email of accepter from id
        accepting_user_id = force_str(
            urlsafe_base64_decode(accepting_user_idb64))
        accepting_user_dict = get_user_by_id(user_id=accepting_user_id)
        if accepting_user_dict is None:
            error_message = "The accepting user must have completed " \
                + "registration before a referral can be considered accepted"
            return (False, error_message)

        # Create and accept invite
        invitor_user_id = force_str(
            urlsafe_base64_decode(referring_user_idb64))
        invitor = Invitor.objects.get_or_create(user_id=invitor_user_id)[0]
        update_or_create_kwargs = self.__get_invite_model_update_or_create_kwargs(
            invitor=invitor,
            recipient_email=accepting_user_dict["email"],
            token_model_instance=token_model_instance
        )

        # In case the user had sent a direct invite to the referrer before,
        # use update_or_create to ensure we don't create duplicate db entries
        invite = self.__invite_model.objects.update_or_create(
            defaults={"status": InviteStatus.ACCEPTED},
            **update_or_create_kwargs
        )[0]
        return (True, invite)
