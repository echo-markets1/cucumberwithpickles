""" Generator for creating referral links based on Model instances and related to an invitor. """
import uuid
from django.db import models
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from ILUM.core.model_based_link_generator import ModelBasedLinkGenerator
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator


class ModelBasedReferralLinkCreator:
    """
    Generates referral links based on Model instance and tokens

    Attributes:
        -  __link_generator (ModelBasedLinkGenerator): Generates the referral link
            associated with the model and token
        - __token_generator (ModelBasedTokenGenerator): Generates the token uniquely
            identifying the invitor. Used in link creation
        - __referral_path (str): The base url path of the referral
        - __model_class (models.Model): The Model class to base tokens from.
    """

    def __init__(
            self,
            link_generator: ModelBasedLinkGenerator,
            token_generator: ModelBasedTokenGenerator,
            referral_path: str,
            model_class: models.Model
    ):
        self.__link_generator = link_generator
        self.__token_generator = token_generator
        self.__referral_path = referral_path
        self.__model_class = model_class

    def __get_model_instance(self, model_instance_pk: uuid.UUID) -> models.Model:
        return self.__model_class.objects.get_or_create(pk=model_instance_pk)[0]

    def create(self, model_instance_pk: uuid.UUID, invitor_user_id: uuid.UUID) -> str:
        """
        Creates a referral link to uniquely identify the invitor, the subject of the invite
        (Group, Platform, etc.).

        Parameters:
            - model_instance_pk (uuid.UUID): The unique identifier of the model instance,
                representing the subject of the invite. If the subject of the invite is
                the platform, then default to the invitor's pk.
            - invitor_user_id (uuid.UUID): The user_id of the invitor/referrer

        Returns:
            (str): The referral link
        """
        model_instance = self.__get_model_instance(model_instance_pk)
        token = self.__token_generator.make_token(model_instance)
        b64_invitor_id = urlsafe_base64_encode(force_bytes(invitor_user_id))
        link = self.__link_generator.generate(
            model_instance=model_instance,
            path=self.__referral_path,
            token=token
        )
        return link + b64_invitor_id + '/'
