""" Factory for ModelBasedReferralLinkResponseHandler """

from ILUM.core.invite import invite_types
from ILUM.core.referral.referral_link_response_handler import ModelBasedReferralLinkResponseHandler
from ILUM.core.tokens.referral_token_generator import referral_token_generator
from ILUM.models import EchoInvite, Group, GroupInvite, Invitor


class ModelBasedReferralLinkHandlerFactory:
    """
    Provides a ModelBasedReferralLinkResponseHandler specific to the needs of a given invite_type

    Attributes:
        - __handlers (Dict): The mapping of invite_types to ModelBasedReferralLinkResponseHandler instances
    """
    __handlers = {
        invite_types.PLATFORM: ModelBasedReferralLinkResponseHandler(
            token_model=Invitor,
            invite_model=EchoInvite,
            token_generator=referral_token_generator
        ),
        invite_types.GROUP: ModelBasedReferralLinkResponseHandler(
            token_model=Group,
            invite_model=GroupInvite,
            token_generator=referral_token_generator
        )
    }

    def get_handler(self, invite_type: str) -> ModelBasedReferralLinkResponseHandler:
        """ Return the ModelBasedReferralLinkResponseHandler for the given invite_type. """
        return self.__handlers[invite_type]


REFERRAL_LINK_RESPONSE_HANDLER_FACTORY = ModelBasedReferralLinkHandlerFactory()
