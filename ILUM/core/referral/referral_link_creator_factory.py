""" Factory for ModelBasedReferralLinkCreators """
from ILUM.core.invite import invite_types
from ILUM.core.model_based_link_generator import REFERRAL_BASE_URL_PATH, model_based_link_generator
from ILUM.core.referral.referral_link_creator import ModelBasedReferralLinkCreator
from ILUM.core.tokens.referral_token_generator import referral_token_generator
from ILUM.models import Group, Invitor


class ModelBasedReferralLinkCreatorFactory:
    """
    Provides a ModelBasedReferralCreator specific to the needs of a given invite_type

    Attributes:
        - __creators (Dict): The mapping of invite_types to ModelBasedReferralCreator instances
    """
    __creators = {
        invite_types.PLATFORM: ModelBasedReferralLinkCreator(
            link_generator=model_based_link_generator,
            token_generator=referral_token_generator,
            referral_path=f"{REFERRAL_BASE_URL_PATH}/{invite_types.PLATFORM}",
            model_class=Invitor
        ),
        invite_types.GROUP: ModelBasedReferralLinkCreator(
            link_generator=model_based_link_generator,
            token_generator=referral_token_generator,
            referral_path=f"{REFERRAL_BASE_URL_PATH}/{invite_types.GROUP}",
            model_class=Group
        )
    }

    def get_creator(self, invite_type: str) -> ModelBasedReferralLinkCreator:
        """ Return the ModelBasedReferralCreator for the given invite_type. """
        return self.__creators[invite_type]


REFERRAL_LINK_CREATOR_FACTORY = ModelBasedReferralLinkCreatorFactory()
