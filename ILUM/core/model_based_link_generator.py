""" Generator for creating links based on Model instances and tokens. """
from django.db import models
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

### Path Constants ###
ACCEPT_INVITE_BASE_URL_PATH = 'accept-invite'
REFERRAL_BASE_URL_PATH = 'referral'


class ModelBasedLinkGenerator:
    """ Generator for creating links based on Model instances and tokens. """

    # TODO [https://app.clickup.com/t/1kj3kmg]: Set domain & protocol based on env configuration
    def generate(self, model_instance: models.Model, path: str, token: str) -> str:
        """
        Generate a link based on a model instance and token for provided endpoint.

        Parameters:
            - model_instance (models.Model): The model instance related to the link
            - path (str): The path to the endpoint
            - token (str): The token authenticating the link

        Returns:
            str: The related link/url
        """
        domain = 'localhost:8080'
        protocol = 'https://'
        b64_uid = urlsafe_base64_encode(force_bytes(model_instance.pk))
        link = protocol + domain + '/' + path + '/' + b64_uid + '/' + token + '/'
        return link


model_based_link_generator = ModelBasedLinkGenerator()
