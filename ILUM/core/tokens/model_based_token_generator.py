""" Abstract Generator for creating tokens based on Model instances. """

import abc
from django.conf import settings
from django.db import models


class ModelBasedTokenGenerator(abc.ABC):
    """ Abstract Generator for creating tokens based on Model instances. """

    def __init__(self, key_salt: str):
        self.__key_salt = key_salt

    __secret = settings.SECRET_KEY

    @property
    def key_salt(self):
        return self.__key_salt

    @property
    def secret(self):
        return self.__secret

    @abc.abstractmethod
    def make_token(self, model_instance: models.Model):
        """ Return a token that can be used to identify the model instance. """
    @abc.abstractmethod
    def check_token(self,  model_instance: models.Model, token: str):
        """ Check that a referral token is correct for a given model instance. """
