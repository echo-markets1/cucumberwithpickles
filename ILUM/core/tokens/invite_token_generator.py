""" Generator for creating tokens used in invite link mechanisms. """
from django.utils.crypto import constant_time_compare, salted_hmac
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import BaseInvite


class InviteTokenGenerator(ModelBasedTokenGenerator):
    """ Strategy object used to generate and check tokens for the invite link mechanism. """

    def make_token(self, model_instance: BaseInvite) -> str:
        """
        Return a token that can be used to identify the invitor/invite.
        """
        return self._make_token(invite=model_instance)

    def check_token(self, model_instance: BaseInvite, token: str):
        """
        Check that an invite token is correct for a given user.
        """
        if not (model_instance and token):
            return False

        # Check that the invite and invitor uids has not been tampered with
        if not constant_time_compare(self._make_token(invite=model_instance), token):
            return False

        return True

    def _make_token(self, invite: BaseInvite) -> str:
        hash_string = salted_hmac(
            self.key_salt,
            self._make_hash_value(invite),
            secret=self.secret,
        ).hexdigest()[::2]  # Limit to 20 characters to shorten the URL.
        return "%s" % (hash_string)

    def _make_hash_value(self, invite: BaseInvite):
        """
        Hash the invite's primary key and invitor's primary key to generate a
        token value unique to the invitation, and relatable to the invitor
        """
        return str(invite.pk) + str(invite.invitor.pk)


invite_token_generator = InviteTokenGenerator(
    key_salt="ILUM.core.tokens.invite_token_generator.InviteTokenGenerator")
