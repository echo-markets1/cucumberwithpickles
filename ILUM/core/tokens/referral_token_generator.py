""" Generator for creating tokens used in referral link mechanisms. """
from django.db import models
from django.utils.crypto import constant_time_compare, salted_hmac
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator


class ReferralTokenGenerator(ModelBasedTokenGenerator):
    """ Strategy object used to generate and check tokens for the referral link mechanism. """

    def make_token(self, model_instance: models.Model) -> str:
        """
        Return a token that can be used to identify the referring user.
        """
        return self._make_token(model_instance=model_instance)

    def check_token(self, model_instance: models.Model, token: str) -> bool:
        """
        Check that a referral token is correct for a given user.
        """
        if not (model_instance and token):
            return False

        # Check that the model uids has not been tampered with
        if not constant_time_compare(self._make_token(model_instance), token):
            return False

        return True

    def _make_token(self, model_instance: models.Model) -> str:
        hash_string = salted_hmac(
            self.key_salt,
            self._make_hash_value(model_instance),
            secret=self.secret,
        ).hexdigest()[::2]  # Limit to 20 characters to shorten the URL.
        return "%s" % (hash_string)

    def _make_hash_value(self, model_instance: models.Model) -> str:
        """
        Hash the model_instance's primary key to generate a unique to the
        to the model_instance.
        """
        return str(model_instance.pk)


referral_token_generator = ReferralTokenGenerator(
    key_salt="ILUM.core.tokens.referral_token_generator.ReferralTokenGenerator")
