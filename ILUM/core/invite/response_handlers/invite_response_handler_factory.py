from ILUM.core.invite import invite_types
from ILUM.core.invite.response_handlers.echo_invite_response_handler import EchoInviteResponseHandler
from ILUM.core.invite.response_handlers.invite_response_handler import InviteResponseHandler
from ILUM.core.tokens.invite_token_generator import invite_token_generator
from ILUM.core.invite.response_handlers.group_invite_response_handler import GroupInviteResponseHandler


class InviteResponseHandlerFactory:
    __handlers = {
        invite_types.PLATFORM: EchoInviteResponseHandler(
            invite_token_generator),
        invite_types.GROUP: GroupInviteResponseHandler(invite_token_generator)
    }

    def get_handler(self, invite_type: str) -> InviteResponseHandler:
        return self.__handlers[invite_type]


INVITE_RESPONSE_HANDLER_FACTORY = InviteResponseHandlerFactory()
