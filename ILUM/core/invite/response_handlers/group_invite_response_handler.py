"""
Handles a recipients response to a group invite

Classes:
    - GroupInviteResponseHandler
"""
from typing import Union
from ILUM.core.invite.response_handlers.invite_response_handler import InviteResponseHandler
from ILUM.models import BaseInvite, GroupInvite


class GroupInviteResponseHandler(InviteResponseHandler):
    """ Handles a recipients response to a group invite """

    def _get_invite(self, invite_id: str) -> Union[BaseInvite, None]:
        try:
            return GroupInvite.objects.get(id=invite_id)
        except GroupInvite.DoesNotExist:
            return None
