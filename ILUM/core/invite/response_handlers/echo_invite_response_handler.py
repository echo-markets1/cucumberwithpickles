"""
Handles a recipients response to an echo platform invite

Classes:
    - EchoInviteResponseHandler
"""
from typing import Union
from ILUM.core.invite.response_handlers.invite_response_handler import InviteResponseHandler
from ILUM.models import BaseInvite, EchoInvite


class EchoInviteResponseHandler(InviteResponseHandler):
    """ Handles a recipients response to an echo platform invite """

    def _get_invite(self, invite_id: str) -> Union[BaseInvite, None]:
        try:
            return EchoInvite.objects.get(id=invite_id)
        except EchoInvite.DoesNotExist:
            return None
