"""
Handles a recipients response to an invite

Classes:
    - InviteResponseHandler
"""
import abc
from typing import Tuple, Union
import uuid
from django.utils.encoding import force_str

from django.utils.http import urlsafe_base64_decode
from ILUM.core.permissions import is_invitor_to_delete_invite
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import BaseInvite, InviteStatus


class InviteResponseHandler(abc.ABC):
    """
    Handles a recipient's response to an invite. Handling logic across invite
    types (ie: group vs echo) is largely similar, and handled by this base class,
    however, unique subclasses are required in order to query and modify the 
    appropriate Invite model.

    Attributes:
        - __token_generator (ModelBasedTokenGenerator): Used to validate the token
            when handling an invitation 
    """

    def __init__(self, token_generator: ModelBasedTokenGenerator):
        self.__token_generator = token_generator

    @abc.abstractmethod
    def _get_invite(self, invite_id: uuid.UUID) -> BaseInvite:
        """
        Allows subclass to support specific types of invites. Even though
        invites will share the same schema for primary keys, they will be stored
        in different tables, and different managers will have to be referenced
        to query for them.

        Parameters:
            - invite_id(uuid.UUID): The invite.id to retrieve

        Returns:
            - BaseInvite: The invite instance for the given id
        """

    def __get_base64_encoded_invite(self, invite_uidb64: str) -> BaseInvite:
        """ Returns an invite instance based on a base64encoded invite """
        try:
            invite_id = force_str(urlsafe_base64_decode(invite_uidb64))
            return self._get_invite(invite_id)
        except ValueError:
            return None

    def handle_accept(self, invite_uidb64: str, token: str) -> Tuple[bool, Union[BaseInvite, str]]:
        """
        Updates the status of an Invitation to ACCEPTED, provided the token and
        invite are compatible, and the invite has not already been accepted.

        Parameters:
            - invite_uidb64 (str): A base64 encoded version of the invite id
            - token (str): The token associated with the invite

        Returns:
            (bool, EchoInvite | str): A tuple containing the success of the operation
                and the resulting invite instance or error message
        """
        invite = self.__get_base64_encoded_invite(invite_uidb64)
        if invite is None:
            return (False, "Could not find an invite to accept")
        return self.__handle_accept(invite, token)

    def __handle_accept(self, invite: BaseInvite, token: str) -> Tuple[bool, Union[BaseInvite, str]]:
        if not self.__token_generator.check_token(invite, token):
            return (False, f"The token was invalid for the given invite. Invite id: {invite.id}")
        if invite.status == InviteStatus.ACCEPTED:
            return (False, f"The invite has already been accepted. Invite id: {invite.id}")
        else:
            invite.status = InviteStatus.ACCEPTED
            invite.save()
            return (True, invite)

    def handle_delete(self, invite_id: uuid.UUID,
                      requesting_user_id: uuid.UUID) -> Tuple[bool, Union[BaseInvite, str]]:
        """
        Updates the status of an Invitation to DELETED. No token is required, but
        the requesting user must have proper permissions to delete the invite.

        Parameters:
            - invite_id (uuid.UUID): The invite id
            - requesting_user_id (uuid.UUID): The id of the user initiating the request

        Returns:
            (bool, EchoInvite | str): A tuple containing the success of the operation
                and the resulting invite instance or error message
        """
        invite = self._get_invite(invite_id)
        if invite is None:
            return (False, "Could not find an invite to delete")
        if not is_invitor_to_delete_invite(invite, requesting_user_id):
            return (False, f"User with id: {requesting_user_id} does not have " +
                    "permission to delete this request")
        return self.__handle_delete(invite)

    def __handle_delete(self, invite: BaseInvite) -> Tuple[bool, Union[BaseInvite, str]]:
        if invite.status == InviteStatus.DELETED:
            return (False, f"The invite has already been deleted. Invite id: {invite.id}")
        if invite.status == InviteStatus.ACCEPTED:
            return (False, f"Can not delete an accepted invitation. Invite id: {invite.id}")
        invite.status = InviteStatus.DELETED
        invite.save()
        return (True, invite)
