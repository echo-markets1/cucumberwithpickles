"""
Class for creating and sending platform (Echo) invites

Classes:
    - EchoInviteCreator
"""
from typing import Tuple, Union
import uuid
from ALDERAAN.extensions.data_extensions import get_user_by_email
from ILUM.clients.sendinblue import sendinblue_constants
from ILUM.core.emailer.invite_emailer import InviteEmailer, invite_emailer
from ILUM.core.invite import invite_types
from ILUM.core.model_based_link_generator import ACCEPT_INVITE_BASE_URL_PATH, \
    ModelBasedLinkGenerator, model_based_link_generator
from ILUM.core.tokens.invite_token_generator import invite_token_generator
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import EchoInvite, InviteStatus, Invitor


class EchoInviteCreator:
    """
    Class for creating and sending platform (Echo) invites.

    Attributes:
        - __link_generator (ModelBasedLinkGenerator): Generates the invite link
            associated with the invite
        - __token_generator (ModelBasedTokenGenerator): Generates the token uniquely
            identifying the invite. Used in link creation
        - __emailer (InviteEmailer): Sends the invite email based on the invite
            instance and link
    """
    @property
    def __invite_path(self):
        return ACCEPT_INVITE_BASE_URL_PATH + '/' + invite_types.PLATFORM

    def __init__(self, link_generator: ModelBasedLinkGenerator,
                 token_generator: ModelBasedTokenGenerator, emailer: InviteEmailer):
        self.__link_generator = link_generator
        self.__token_generator = token_generator
        self.__emailer = emailer

    def create(self, invitor_user_id: uuid.UUID,
               recipient_email: str) -> Tuple[bool, Union[EchoInvite, str]]:
        """
        Creates an invite to the Echo platform.

        Creates an invite to the Echo platform storing details of who is being
        invited and who is sending the invite. Does not create an invite if the
        intended recipient is already on the platform, or has already accepted
        an invitation from the user. Resends an existing invitation if not
        previously accepted. Sends an invite via email. Issues sending the email
        will result in failure and delete the invite in case of faulty email address.

        Parameters:
            - invitor_user_id (uuid.UUID): The id of the user sending the invite
            - recipient_email (str): The email of the recipient of the invite

        Returns:
           (bool, EchoInvite | str): A tuple containing the success of the operation
                and the resulting invite instance or error message
        """
        if get_user_by_email(recipient_email):
            return (False, "An account with that email address is already registered.")
        invitor = Invitor.objects.get_or_create(user_id=invitor_user_id)[0]
        invite = EchoInvite.objects.get_or_create(
            invitor=invitor, recipient_email=recipient_email)[0]
        if invite.status == InviteStatus.ACCEPTED:
            # The case where a user accepted an invite to the platform via the
            # provided email address, but has since changed the email address
            # associated with their echo account
            return (False, "An account with that email address has already accepted an invitation")
        token = self.__token_generator.make_token(model_instance=invite)
        referral_link = self.__link_generator.generate(
            model_instance=invite, path=self.__invite_path, token=token)
        email_success = self.__emailer.email(template_id=sendinblue_constants.INVITE_TO_PLATFORM,
                                             invite=invite, link=referral_link)
        if not email_success:
            # Create the failure message before deleting the invite.
            failure_message = "An issue occurred while trying to send the " + \
                f"invite to: {invite.recipient_email}."
            invite.delete()
            return (False, failure_message)
        invite.status = InviteStatus.PENDING
        invite.save()
        return (True, invite)


echo_invite_creator = EchoInviteCreator(
    link_generator=model_based_link_generator,
    token_generator=invite_token_generator,
    emailer=invite_emailer
)
