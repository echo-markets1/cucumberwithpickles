"""
Class for creating and sending group invites

Classes:
    - GroupInviteCreator
"""
import uuid
from ALDERAAN.extensions.data_extensions import get_user_by_email
from CORUSCANT.extensions.logic_extensions import validate_user_is_group_member
from ILUM.clients.sendinblue import sendinblue_constants
from ILUM.core.emailer.invite_emailer import InviteEmailer, invite_emailer
from ILUM.core.invite import invite_types
from ILUM.core.model_based_link_generator import ACCEPT_INVITE_BASE_URL_PATH, \
    ModelBasedLinkGenerator, model_based_link_generator
from ILUM.core.tokens.invite_token_generator import invite_token_generator
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import Group, GroupInvite, InviteStatus, Invitor


class GroupInviteCreator(object):
    """
    Class for creating and sending group invites.

    Attributes:
        - __link_generator (ModelBasedLinkGenerator): Generates the invite link
            associated with the invite
        - __token_generator (ModelBasedTokenGenerator): Generates the token uniquely
            identifying the invite. Used in link creation
        - __emailer (InviteEmailer): Sends the invite email based on the invite
            instance and link
    """
    @property
    def __invite_path(self):
        return ACCEPT_INVITE_BASE_URL_PATH + '/' + invite_types.GROUP

    def __init__(self, link_generator: ModelBasedLinkGenerator,
                 token_generator: ModelBasedTokenGenerator, emailer: InviteEmailer):
        self.__link_generator = link_generator
        self.__token_generator = token_generator
        self.__emailer = emailer

    def __check_for_existing_group_member_by_email(self, group_id: uuid.UUID, email: str) -> bool:
        user_data = get_user_by_email(email)
        if not user_data is None:
            return validate_user_is_group_member(
                group_id=group_id,
                user_id=user_data['id']
            )
        else:
            return False

    def create(self, invitor_user_id: uuid.UUID, recipient_email: str, group_id: uuid.UUID):
        """
        Creates an invite to a group.

        Creates an invite to a group storing details of who is being
        invited, who is sending the invite, and the group being invited to.
        Does not create an invite if the intended recipient is already a member,
        or has already accepted an invitation from the user. Resends an existing
        invitation if not previously accepted. Sends an invite via email. Issues sending the email
        will result in failure and delete the invite in case of faulty email address.

        Parameters:
            - invitor_user_id (uuid.UUID): The id of the user sending the invite
            - recipient_email (str): The email of the recipient of the invite
            - group_id (uuid.UUID): The group being invited too
        Returns:
           (bool, EchoInvite | str): A tuple containing the success of the operation
                and the resulting invite instance or error message
        """
        if self.__check_for_existing_group_member_by_email(group_id=group_id,
                                                           email=recipient_email):
            return (False, "An account with that email address is already a member")
        invitor = Invitor.objects.get_or_create(user_id=invitor_user_id)[0]
        group = Group.objects.get_or_create(id=group_id)[0]
        invite = GroupInvite.objects.get_or_create(
            invitor=invitor, recipient_email=recipient_email, group=group)[0]
        if invite.status == InviteStatus.ACCEPTED:
            # The case where a user accepted an invite to the platform via the
            # provided email address, but has since changed the email address
            # associated with their echo account
            return (False, "An account with that email address has already accepted an invitation")
        token = self.__token_generator.make_token(model_instance=invite)
        referral_link = self.__link_generator.generate(
            model_instance=invite, path=self.__invite_path, token=token)
        email_success = self.__emailer.email(template_id=sendinblue_constants.INVITE_TO_GROUP,
                                             invite=invite, link=referral_link)
        if not email_success:
            # Create the failure message before deleting the invite.
            failure_message = "An issue occurred while trying to send the " + \
                f"invite to: {invite.recipient_email}."
            invite.delete()
            return (False, failure_message)
        # Consider blocking invitations if the recipient has rejected one previously
        invite.status = InviteStatus.PENDING
        invite.save()
        return (True, invite)


group_invite_creator = GroupInviteCreator(
    link_generator=model_based_link_generator,
    token_generator=invite_token_generator,
    emailer=invite_emailer
)
