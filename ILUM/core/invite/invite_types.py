"""
Constants for the type of invites supported by ILUM.

Not stored at the model level because different invites types have distinct feature
requirements, such as group invites requiring a group id.
"""
PLATFORM = "PLATFORM"  # EchoInvite
GROUP = "GROUP"  # GroupInvite
