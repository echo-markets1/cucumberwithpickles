from django.contrib import admin
from ILUM.models import EchoInvite, Group, GroupInvite, Invitor

admin.site.register(Invitor)
admin.site.register(Group)
admin.site.register(EchoInvite)
admin.site.register(GroupInvite)
