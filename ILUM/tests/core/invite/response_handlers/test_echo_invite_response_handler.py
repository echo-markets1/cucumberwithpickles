import uuid
from django.test.testcases import TestCase
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.core.invite.response_handlers.echo_invite_response_handler import EchoInviteResponseHandler
from ILUM.models import InviteStatus, Invitor
from ILUM.tests.factories.invite_factory import EchoInviteFactory
from unittest.mock import Mock


class EchoInviteResponseHandlerTest(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.mock_token_generator = Mock(spec=ModelBasedTokenGenerator)
        cls.response_handler = EchoInviteResponseHandler(
            token_generator=cls.mock_token_generator)
        cls.echo_invite = EchoInviteFactory.create()
        other_invitor = Invitor.objects.create(user_id=uuid.uuid4())
        cls.accepted_echo_invite = EchoInviteFactory.create(
            status=InviteStatus.ACCEPTED, invitor=other_invitor)

    def test_handle_accept_sets_invite_status_accepted_when_valid_token(self):
        # Preconditions
        self.assertNotEqual(self.echo_invite.status, InviteStatus.ACCEPTED,
                            "Precondition failed: status must not be ACCEPTED")

        # Arrange
        invite_uidb64 = urlsafe_base64_encode(
            force_bytes(self.echo_invite.pk))
        token = "some_token"
        self.mock_token_generator.check_token.return_value = True

        # Act
        response = self.response_handler.handle_accept(invite_uidb64, token)

        # Assert
        self.assertTrue(
            response[0], "handle_accept should return True when successful")
        self.assertEqual(response[1].id, self.echo_invite.id,
                         "input and output invite should be the same instance")
        self.assertEqual(response[1].status, InviteStatus.ACCEPTED,
                         "handle_accept should set the invite.status to ACCEPTED")

    def test_handle_accept_does_not_update_invite_status_when_invalid_token(self):
        # Preconditions
        self.assertNotEqual(self.echo_invite.status, InviteStatus.ACCEPTED,
                            "Precondition failed: status must not be ACCEPTED")

        # Arrange
        invite_uidb64 = urlsafe_base64_encode(
            force_bytes(self.echo_invite.pk))
        expected_status = self.echo_invite.status
        token = "some_token"
        self.mock_token_generator.check_token.return_value = False

        # Act
        response = self.response_handler.handle_accept(invite_uidb64, token)

        # Assert
        self.assertFalse(
            response[0], "handle_accept should return False when unsuccessful")
        self.assertIn("token was invalid", response[1])
        self.assertNotEqual(self.echo_invite.status, InviteStatus.ACCEPTED)
        self.assertEqual(self.echo_invite.status, expected_status,
                         "handle_accept should not update the invite.status on failure")

    def test_handle_accept_fails_when_invite_already_accepted(self):
        # Precondition
        self.assertEqual(self.accepted_echo_invite.status,
                         InviteStatus.ACCEPTED, "Precondition failed: status must be ACCEPTED")

        # Arrange
        invite_uidb64 = urlsafe_base64_encode(
            force_bytes(self.accepted_echo_invite.pk))
        token = "some_token"
        self.mock_token_generator.check_token.return_value = True

        # Act
        response = self.response_handler.handle_accept(invite_uidb64, token)

        # Assert
        self.assertFalse(
            response[0], "handle_accept should return False when unsuccessful")
        self.assertIn("already been accepted", response[1])
        self.assertEqual(self.accepted_echo_invite.status,
                         InviteStatus.ACCEPTED)

    def test_handle_accept_fails_when_no_invite_found(self):
        # Arrange
        random_uidb64 = urlsafe_base64_encode(force_bytes(uuid.uuid4()))
        token = "some_token"
        self.mock_token_generator.check_token.return_value = True

        # Act
        response = self.response_handler.handle_accept(random_uidb64, token)

        # Assert
        self.assertFalse(
            response[0], "handle_accept should return False when unsuccessful")
        self.assertIn("Could not find", response[1])

    def test_handle_delete_sets_invite_status_deleted_on_success(self):
        # Preconditions
        self.assertNotEqual(self.echo_invite.status, InviteStatus.DELETED,
                            "Precondition failed: status must not be DELETED")

        # Arrange

        # Act
        response = self.response_handler.handle_delete(
            self.echo_invite.id,
            self.echo_invite.invitor.user_id
        )

        # Assert
        self.assertTrue(
            response[0], "handle_delete should return True when successful")
        self.assertEqual(response[1].id, self.echo_invite.id,
                         "input and output invite should be the same instance")
        self.assertEqual(response[1].status, InviteStatus.DELETED,
                         "handle_delete should set the invite.status to DELETED")

    def test_handle_delete_does_not_update_invite_status_when_already_accepted(self):
        # Preconditions
        self.assertEqual(self.accepted_echo_invite.status, InviteStatus.ACCEPTED,
                         "Precondition failed: status must be ACCEPTED")

        # Arrange

        # Act
        response = self.response_handler.handle_delete(
            self.accepted_echo_invite.id,
            self.accepted_echo_invite.invitor.user_id
        )

        # Assert
        self.assertFalse(
            response[0], "handle_delete should return False when unsuccessful")
        self.assertIn("delete an accepted invitation", response[1])
        self.assertEqual(self.accepted_echo_invite.status, InviteStatus.ACCEPTED,
                         "handle_delete should not update the invite.status on failure")

    def test_handle_delete_fails_when_invite_already_deleted(self):
        # Arrange
        other_invitor = Invitor.objects.create(user_id=uuid.uuid4())
        deleted_invite = EchoInviteFactory.create(
            status=InviteStatus.DELETED, invitor=other_invitor)

        # Act
        response = self.response_handler.handle_delete(
            deleted_invite.id,
            deleted_invite.invitor.user_id
        )

        # Assert
        self.assertFalse(
            response[0], "handle_delete should return False when unsuccessful")
        self.assertIn("already been deleted", response[1])
        self.assertEqual(deleted_invite.status, InviteStatus.DELETED)

    def test_handle_delete_fails_when_no_invite_found(self):
        # Arrange

        # Act
        response = self.response_handler.handle_delete(
            uuid.uuid4(),  # Some random nonexistent invite_id
            uuid.uuid4()
        )

        # Assert
        self.assertFalse(
            response[0], "handle_delete should return False when unsuccessful")
        self.assertIn("Could not find", response[1])

    def test_handle_delete_fails_when_requestor_lacks_permissions(self):
        # Arrange

        # Act
        response = self.response_handler.handle_delete(
            self.echo_invite.id,
            uuid.uuid4()  # Some random user_id
        )

        # Assert
        self.assertFalse(
            response[0], "handle_delete should return False when unsuccessful")
        self.assertIn("does not have permission", response[1])
