import uuid
from django.test.testcases import TestCase
from unittest.mock import Mock, patch
from ILUM.core.emailer.invite_emailer import InviteEmailer
from ILUM.core.model_based_link_generator import ModelBasedLinkGenerator
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.core.invite.creators.group_invite_creator import GroupInviteCreator
from ILUM.models import GroupInvite, InviteStatus
from ILUM.tests.factories.invite_factory import GroupInviteFactory


class GroupInviteCreatorTest(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.mock_link_generator = Mock(spec=ModelBasedLinkGenerator)
        cls.mock_token_generator = Mock(spec=ModelBasedTokenGenerator)
        cls.mock_emailer = Mock(spec=InviteEmailer)
        cls.invite_creator = GroupInviteCreator(
            link_generator=cls.mock_link_generator,
            token_generator=cls.mock_token_generator,
            emailer=cls.mock_emailer
        )
        cls.stubbed_get_user_by_email_patcher = patch(
            'ILUM.core.invite.creators.group_invite_creator.get_user_by_email')
        cls.stubbed_validate_user_is_group_member_patcher = patch(
            'ILUM.core.invite.creators.group_invite_creator.validate_user_is_group_member')
        cls.invitor_id = uuid.uuid4()
        cls.group_id = uuid.uuid4()
        cls.recipient_email = "obiwan@thehighground.edu"

    def setUp(self) -> None:
        super().setUp()
        self.__reset_mocks()  # Resets the call counter of the mocks
        self.__setup__check_for_existing_group_member_by_email_stubs()

    def tearDown(self) -> None:
        super().tearDown()
        self.__teardown__check_for_existing_group_member_by_email_stubs()

    def __reset_mocks(self):
        self.mock_token_generator.make_token.reset_mock()
        self.mock_link_generator.generate.reset_mock()
        self.mock_emailer.email.reset_mock()
        self.mock_token_generator.make_token.return_value = "some_token"
        self.mock_link_generator.generate.return_value = "https://echo-market.com/some/endpoint"

    def __assert_mocks_called(self):
        self.mock_token_generator.make_token.assert_called_once()
        self.mock_link_generator.generate.assert_called_once()
        self.mock_emailer.email.assert_called_once()

    def __assert_mocks_not_called(self):
        self.mock_token_generator.make_token.assert_not_called()
        self.mock_link_generator.generate.assert_not_called()
        self.mock_emailer.email.assert_not_called()

    def __setup__check_for_existing_group_member_by_email_stubs(self):
        self.stubbed_get_user_by_email = self.stubbed_get_user_by_email_patcher.start()
        self.stubbed_validate_user_is_group_member = \
            self.stubbed_validate_user_is_group_member_patcher.start()
        self.stubbed_get_user_by_email.return_value = None
        self.stubbed_validate_user_is_group_member.return_value = False

    def __teardown__check_for_existing_group_member_by_email_stubs(self):
        self.stubbed_get_user_by_email_patcher.stop()
        self.stubbed_validate_user_is_group_member_patcher.stop()

    def test_creator_creates_pending_invite_on_success(self):
        # Arrange
        self.mock_emailer.email.return_value = True
        expected_num_invites = GroupInvite.objects.all().count() + 1

        # Act
        response = self.invite_creator.create(
            invitor_user_id=self.invitor_id,
            recipient_email=self.recipient_email,
            group_id=self.group_id
        )

        # Assert
        self.assertTrue(
            response[0], "create should return true when successful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should create a single new invite on success"
        )
        self.assertEqual(response[1].status, InviteStatus.PENDING,
                         "The newly created invite should be set pending")
        self.__assert_mocks_called()

    def test_creator_does_not_recreate_existing_invite(self):
        # Arrange
        self.mock_emailer.email.return_value = True
        invite = GroupInviteFactory.create()
        previous_update_time = invite.last_updated
        expected_num_invites = GroupInvite.objects.all().count()

        # Act
        response = self.invite_creator.create(
            invitor_user_id=invite.invitor.user_id,
            recipient_email=invite.recipient_email,
            group_id=invite.group.id
        )

        # Assert
        self.assertTrue(
            response[0], "create should return true when successful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should not recreate an existing invite"
        )
        self.assertEqual(response[1].status, InviteStatus.PENDING,
                         "The existing invite should still be set pending")
        self.assertGreater(response[1].last_updated, previous_update_time,
                           "The existing invite's last_updated should get updated")
        self.__assert_mocks_called()

    def test_creator_sets_existing_deleted_invite_pending(self):
        # Arrange
        self.mock_emailer.email.return_value = True
        invite = GroupInviteFactory.create(status=InviteStatus.DELETED)
        previous_update_time = invite.last_updated
        expected_num_invites = GroupInvite.objects.all().count()

        # Act
        response = self.invite_creator.create(
            invitor_user_id=invite.invitor.user_id,
            recipient_email=invite.recipient_email,
            group_id=invite.group.id
        )

        # Assert
        self.assertTrue(
            response[0], "create should return true when successful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should not recreate an existing invite"
        )
        self.assertEqual(response[1].status, InviteStatus.PENDING,
                         "The existing deleted invite should still be set pending")
        self.assertGreater(response[1].last_updated, previous_update_time,
                           "The existing invite's last_updated should get updated")
        self.__assert_mocks_called()

    def test_creator_fails_and_deletes_invite_on_email_failure(self):
        # Arrange
        self.mock_emailer.email.return_value = False
        expected_num_invites = GroupInvite.objects.all().count()

        # Act
        response = self.invite_creator.create(
            invitor_user_id=self.invitor_id,
            recipient_email=self.recipient_email,
            group_id=self.group_id
        )

        # Assert
        self.assertFalse(
            response[0], "create should return false when unsuccessful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should not create new invites on failure"
        )
        self.assertIn("trying to send the invite", response[1])
        self.__assert_mocks_called()

    def test_creator_fails_when_invite_already_accepted(self):
        # Arrange
        invite = GroupInviteFactory.create(status=InviteStatus.ACCEPTED)
        expected_num_invites = GroupInvite.objects.all().count()

        # Act
        response = self.invite_creator.create(
            invitor_user_id=invite.invitor.user_id,
            recipient_email=invite.recipient_email,
            group_id=invite.group.id
        )

        # Assert
        self.assertFalse(
            response[0], "create should return false when unsuccessful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should not create new invites on failure"
        )
        self.assertIn("already accepted an invitation", response[1])
        self.__assert_mocks_not_called()

    def test_creator_fails_when_email_belongs_to_existing_group_member(self):
        # Arrange
        self.stubbed_get_user_by_email.return_value = {"id": uuid.uuid4()}
        self.stubbed_validate_user_is_group_member.return_value = True
        expected_num_invites = GroupInvite.objects.all().count()

        # Act
        response = self.invite_creator.create(
            invitor_user_id=self.invitor_id,
            recipient_email=self.recipient_email,
            group_id=self.group_id
        )

        # Assert
        self.assertFalse(
            response[0], "create should return false when unsuccessful")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "create should not create new invites on failure"
        )
        self.assertIn("already a member", response[1])
        self.__assert_mocks_not_called()
