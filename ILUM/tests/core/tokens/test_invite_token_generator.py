from django.test.testcases import TestCase
from ILUM.core.tokens.invite_token_generator import invite_token_generator
from ILUM.tests.factories.invite_factory import EchoInviteFactory, GroupInviteFactory


class InviteTokenGeneratorTest(TestCase):
    __token_generator = invite_token_generator

    @classmethod
    def setUpTestData(cls) -> None:
        cls.echo_invite = EchoInviteFactory.create()
        cls.group_invite = GroupInviteFactory.create()

    def test_token_generator_supports_echo_invites(self):
        # Arrange

        # Act
        echo_invite_token = self.__token_generator.make_token(self.echo_invite)

        # Assert
        self.assertTrue(
            self.__token_generator.check_token(
                model_instance=self.echo_invite,
                token=echo_invite_token
            ),
            "The token should be compatible with the invite it was made from."
        )

    def test_token_generator_supports_group_invites(self):
        # Arrange

        # Act
        group_invite_token = self.__token_generator.make_token(
            self.group_invite)

        # Assert
        self.assertTrue(
            self.__token_generator.check_token(
                model_instance=self.group_invite,
                token=group_invite_token
            ),
            "The token should be compatible with the invite t was made from."
        )

    def test_token_uniquely_identifies_invite(self):
        # Arrange

        # Act
        group_invite_token = self.__token_generator.make_token(
            self.group_invite)

        # Assert
        self.assertFalse(
            self.__token_generator.check_token(
                model_instance=self.echo_invite,
                token=group_invite_token
            ),
            "The token should only be compatible with the invite it was made from."
        )
        self.assertTrue(
            self.__token_generator.check_token(
                model_instance=self.group_invite,
                token=group_invite_token
            ),
            "The token should be compatible with the invite t was made from."
        )

    def test_check_token_returns_false_when_no_model_instance(self):
        # Arrange

        # Act
        echo_invite_token = self.__token_generator.make_token(self.echo_invite)

        # Assert
        self.assertFalse(self.__token_generator.check_token(
            model_instance=None, token=echo_invite_token))

    def test_check_token_returns_false_when_no_token(self):
        # Arrance

        # Act

        # Assert
        self.assertFalse(self.__token_generator.check_token(
            model_instance=self.group_invite, token=None))
