from django.test.testcases import TestCase
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from ILUM.core.model_based_link_generator import model_based_link_generator
from ILUM.tests.factories.invite_factory import EchoInviteFactory


class ModelBasedLinkGeneratorTest(TestCase):
    __link_generator = model_based_link_generator

    @classmethod
    def setUpTestData(cls) -> None:
        cls.echo_invite = EchoInviteFactory.create()

    def test_link_generator_generates_expected_link(self):
        # Arrange
        path = "path/to/endpoint"
        token = "some_token"
        b64_invite_uid = urlsafe_base64_encode(
            force_bytes(self.echo_invite.pk))

        # Act
        link = self.__link_generator.generate(
            model_instance=self.echo_invite, path=path, token=token)

        # Assert
        self.assertIn(path, link)
        self.assertIn(token, link)
        self.assertIn(b64_invite_uid, link)
