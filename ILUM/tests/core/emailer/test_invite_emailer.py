from django.test.testcases import TestCase
from unittest.mock import Mock, patch
from ILUM.core.emailer.invite_emailer import InviteEmailer
from ILUM.clients.sendinblue.invitor_info import InvitorInfo
from ILUM.clients.sendinblue.sendinblue_client import SendInBlueClient
from ILUM.clients.sendinblue.sendinblue_request import SendInBlueRequest
from ILUM.tests.factories.invite_factory import GroupInviteFactory


class InviteEmailerTest(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.mock_sendinblue_client = Mock(spec=SendInBlueClient)
        cls.emailer = InviteEmailer(cls.mock_sendinblue_client)
        cls.stub_get_name_and_handle_for_user_id_patcher = patch(
            "ILUM.core.emailer.invite_emailer.get_name_and_handle_for_user_id")
        cls.stub_get_name_and_handle_for_user_id = cls.stub_get_name_and_handle_for_user_id_patcher.start()
        cls.stub_get_name_and_handle_for_user_id.return_value = {
            "first_name": "Ezra",
            "last_name": "Bridger",
            "handle": "admiralthrawnstinks3"
        }
        cls.template_id = 3
        cls.link = "https://echo-market.com/some/endpoint"
        cls.invite = GroupInviteFactory.create()

    def setUp(self) -> None:
        self.mock_sendinblue_client.reset_mock()  # Reset call counter

    @classmethod
    def tearDownClass(cls) -> None:
        cls.stub_get_name_and_handle_for_user_id_patcher.stop()
        super().tearDownClass()

    def test_email_returns_success_when_send_email_success(self):
        # Arrange
        self.mock_sendinblue_client.send_email.return_value = (True, "Success")
        expected_invitor_info = InvitorInfo(
            first_name="Ezra", last_name="Bridger", handle="admiralthrawnstinks3")
        expected_request = SendInBlueRequest(
            template_id=self.template_id,
            recipient_emails=[self.invite.recipient_email],
            link=self.link,
            invitor_info=expected_invitor_info
        )
        # Act
        response = self.emailer.email(
            template_id=self.template_id,
            invite=self.invite,
            link=self.link
        )

        # Assert
        actual_request = self.mock_sendinblue_client.send_email.call_args.args[0]
        self.assertTrue(
            response[0], "email should return True when successful")
        self.assertIn("Success", response[1])
        self.assertEqual(expected_request.as_json, actual_request.as_json)

    def test_email_returns_failure_when_send_email_fails(self):
        # Arrange
        self.mock_sendinblue_client.send_email.return_value = (
            False, "Failure")
        expected_invitor_info = InvitorInfo(
            first_name="Ezra", last_name="Bridger", handle="admiralthrawnstinks3")
        expected_request = SendInBlueRequest(
            template_id=self.template_id,
            recipient_emails=[self.invite.recipient_email],
            link=self.link,
            invitor_info=expected_invitor_info
        )
        # Act
        response = self.emailer.email(
            template_id=self.template_id,
            invite=self.invite,
            link=self.link
        )

        # Assert
        actual_request = self.mock_sendinblue_client.send_email.call_args.args[0]
        self.assertFalse(
            response[0], "email should return False when unsuccessful")
        self.assertIn("Failure", response[1])
        self.assertEqual(expected_request.as_json, actual_request.as_json)
