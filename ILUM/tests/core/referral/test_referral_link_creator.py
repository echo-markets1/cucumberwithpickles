from unittest.mock import Mock
import uuid
from django.test.testcases import TestCase
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from ILUM.core.model_based_link_generator import model_based_link_generator
from ILUM.core.referral.referral_link_creator import ModelBasedReferralLinkCreator
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import Group


class ModelBasedReferralLinkCreatorTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.mock_token_generator = Mock(spec=ModelBasedTokenGenerator)
        cls.referral_path = "some_referral_path"

    def test_create_returns_expected_link(self):
        # Arrange
        creator = ModelBasedReferralLinkCreator(
            link_generator=model_based_link_generator,
            token_generator=self.mock_token_generator,
            referral_path=self.referral_path,
            model_class=Group
        )
        expected_token = "some_token"
        self.mock_token_generator.make_token.return_value = expected_token

        model_instance_pk = uuid.uuid4()
        invitor_user_id = uuid.uuid4()

        # Act
        link = creator.create(model_instance_pk, invitor_user_id)

        # Assert
        self.assertIn(self.referral_path, link)
        self.assertIn(expected_token, link)
        self.assertIn(urlsafe_base64_encode(
            force_bytes(invitor_user_id)), link)
