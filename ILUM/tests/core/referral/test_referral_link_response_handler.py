import uuid
from django.test.testcases import TestCase
from unittest.mock import Mock, patch
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from ILUM.core.referral.referral_link_response_handler import ModelBasedReferralLinkResponseHandler
from ILUM.core.tokens.model_based_token_generator import ModelBasedTokenGenerator
from ILUM.models import EchoInvite, Group, GroupInvite, InviteStatus, Invitor
from ILUM.tests.factories.invite_factory import GroupInviteFactory


class ModelBasedReferralLinkResponseHandlerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.mock_token_generator = Mock(spec=ModelBasedTokenGenerator)
        cls.response_handler = ModelBasedReferralLinkResponseHandler(
            token_model=Group,
            invite_model=GroupInvite,
            token_generator=cls.mock_token_generator
        )
        cls.stubbed_get_user_by_id_patcher = patch(
            "ILUM.core.referral.referral_link_response_handler.get_user_by_id")
        cls.stubbed_get_user_by_id = cls.stubbed_get_user_by_id_patcher.start()

    def setUp(self):
        self.mock_token_generator.check_token.return_value = True
        self.stubbed_get_user_by_id.return_value = {
            "email": "anakin@ihatesand.com"}

    def __make_b64_encoded_id(self):
        return urlsafe_base64_encode(force_bytes(uuid.uuid4()))

    def test_handle_accept_fails_when_invalid_token(self):
        # Arrange
        expected_num_invites = GroupInvite.objects.all().count()
        referring_user_idb64 = self.__make_b64_encoded_id()
        accepting_user_idb64 = self.__make_b64_encoded_id()
        token = "some_token"
        token_model_pkb64 = self.__make_b64_encoded_id()
        self.mock_token_generator.check_token.return_value = False

        # Act
        (success, result) = self.response_handler.handle_accept(
            referring_user_idb64, accepting_user_idb64, token, token_model_pkb64)

        # Assert
        self.assertFalse(
            success, "handle_accept should return false when unsuccessful")
        self.assertIn("not valid", result)
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "handle_accept should not create new invites on failure"
        )

    def test_handle_accept_fails_when_accepting_user_has_not_registered(self):
        # Arrange
        expected_num_invites = GroupInvite.objects.all().count()
        referring_user_idb64 = self.__make_b64_encoded_id()
        accepting_user_idb64 = self.__make_b64_encoded_id()
        token = "some_token"
        token_model_pkb64 = self.__make_b64_encoded_id()
        self.stubbed_get_user_by_id.return_value = None

        # Act
        (success, result) = self.response_handler.handle_accept(
            referring_user_idb64, accepting_user_idb64, token, token_model_pkb64)

        # Assert
        self.assertFalse(
            success, "handle_accept should return false when unsuccessful")
        self.assertIn("must have completed registration", result)
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "handle_accept should not create new invites on failure"
        )

    def test_handle_accept_creates_accepted_invite_when_successful(self):
        # Arrange
        expected_num_invites = GroupInvite.objects.all().count() + 1
        referring_user_idb64 = self.__make_b64_encoded_id()
        accepting_user_idb64 = self.__make_b64_encoded_id()
        token = "some_token"
        token_model_pk = uuid.uuid4()
        token_model_pkb64 = urlsafe_base64_encode(force_bytes(token_model_pk))
        Group.objects.create(pk=token_model_pk)

        # Act
        (success, result) = self.response_handler.handle_accept(
            referring_user_idb64, accepting_user_idb64, token, token_model_pkb64)

        # Assert
        self.assertTrue(
            success, "handle_accept should return true when successful")
        self.assertEqual(result.status, InviteStatus.ACCEPTED,
                         "handle_accept should create & return and accepted invite")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "handle_accept should create new invites on success"
        )

    def test_handle_accept_updates_existing_invite_when_successful(self):
        # Arrange
        existing_invite = GroupInviteFactory.create()
        expected_num_invites = GroupInvite.objects.all().count()
        referring_user_idb64 = urlsafe_base64_encode(
            force_bytes(existing_invite.invitor.user_id))
        accepting_user_idb64 = self.__make_b64_encoded_id()
        token = "some_token"
        token_model_pkb64 = urlsafe_base64_encode(
            force_bytes(existing_invite.group.id))
        self.stubbed_get_user_by_id.return_value = {
            "email": existing_invite.recipient_email
        }

        # Act
        (success, result) = self.response_handler.handle_accept(
            referring_user_idb64, accepting_user_idb64, token, token_model_pkb64)

        # Assert
        self.assertTrue(
            success, "handle_accept should return true when successful")
        self.assertEqual(result.status, InviteStatus.ACCEPTED,
                         "handle_accept should update & return and accepted invite")
        self.assertEqual(
            expected_num_invites,
            GroupInvite.objects.all().count(),
            "handle_accept should update existing invites on success"
        )

    def test_handle_accept_compatible_with_platform_invites(self):
        # Arrange
        platform_response_handler = ModelBasedReferralLinkResponseHandler(
            token_model=Invitor,
            invite_model=EchoInvite,
            token_generator=self.mock_token_generator
        )
        expected_num_invites = EchoInvite.objects.all().count() + 1
        referring_user_idb64 = self.__make_b64_encoded_id()
        accepting_user_idb64 = self.__make_b64_encoded_id()
        token = "some_token"
        token_model_pkb64 = self.__make_b64_encoded_id()

        # Act
        (success, result) = platform_response_handler.handle_accept(
            referring_user_idb64, accepting_user_idb64, token, token_model_pkb64)

        # Assert
        self.assertTrue(
            success, "handle_accept should return true when successful")
        self.assertEqual(result.status, InviteStatus.ACCEPTED,
                         "handle_accept should create & return and accepted invite")
        self.assertEqual(
            expected_num_invites,
            EchoInvite.objects.all().count(),
            "handle_accept should create new invites on success"
        )
