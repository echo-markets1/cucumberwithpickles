import factory
from factory import BUILD_STRATEGY
from faker import Factory
from ILUM.tests.factories.group_factory import GroupFactory
from ILUM.tests.factories.invitor_factory import InvitorFactory
from ILUM.models import EchoInvite, GroupInvite
faker = Factory.create()


class BaseInviteFactory(factory.DjangoModelFactory):
    invitor = factory.SubFactory(InvitorFactory)
    recipient_email = faker.company_email()


class EchoInviteFactory(BaseInviteFactory):
    class Meta:
        model = EchoInvite
        strategy = BUILD_STRATEGY


class GroupInviteFactory(BaseInviteFactory):
    class Meta:
        model = GroupInvite
        strategy = BUILD_STRATEGY

    group = factory.SubFactory(GroupFactory)
