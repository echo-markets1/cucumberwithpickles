import factory
from factory import BUILD_STRATEGY
from faker import Factory
from ILUM.models import Invitor
faker = Factory.create()


class InvitorFactory(factory.DjangoModelFactory):
    class Meta:
        model = Invitor
        strategy = BUILD_STRATEGY
        django_get_or_create = ('user_id',)

    user_id = faker.uuid4(cast_to=None)
