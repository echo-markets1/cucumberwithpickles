import factory
from factory import BUILD_STRATEGY
from faker import Factory
from ILUM.models import Group
faker = Factory.create()


class GroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = Group
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.uuid4(cast_to=None)
