import json
from unittest.mock import Mock, patch
from django.test.testcases import TestCase
from django.conf import settings
from ILUM.clients.sendinblue.sendinblue_client import SendInBlueClient
from ILUM.clients.sendinblue.sendinblue_request import SendInBlueRequest


class SendInBlueClientTest(TestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.stub_request_module_patcher = patch(
            "ILUM.clients.sendinblue.sendinblue_client.requests")
        cls.stub_request_module = cls.stub_request_module_patcher.start()
        cls.mock_sendinblue_request = Mock(spec=SendInBlueRequest)
        cls.mock_sendinblue_request.as_json = "{'key': 'value'}"
        cls.sendinblue_client = SendInBlueClient(
            header_accept="application/json", header_content_type="application/json")

    def setUp(self) -> None:
        self.stub_request_module.reset_mock()
        super().setUp()

    def __assert_request_kwargs(self, actual_kwargs: dict):
        self.assertEqual(
            actual_kwargs['method'],
            "POST",
            "request method should be a POST"
        )
        self.assertEqual(
            actual_kwargs['url'],
            "https://api.sendinblue.com/v3/smtp/email",
            "request url should be the sendinblue v3 api endpoint"
        )
        self.assertEqual(
            actual_kwargs['data'],
            self.mock_sendinblue_request.as_json,
            "request data should be a json representation of the SendInBlueRequest"
        )
        self.assertDictEqual(
            actual_kwargs['headers'],
            {
                'accept': "application/json",
                'content-type': "application/json",
                'api-key': settings.SENDINBLUE_API_KEY
            }
        )

    def test_send_email_success(self):
        # Arrange
        message_id = "1234"
        mock_response = Mock(spec=object)
        mock_response.status_code = 201
        mock_response.text = json.dumps({
            "messageId": message_id
        })
        self.stub_request_module.request.return_value = mock_response

        # Act
        result = self.sendinblue_client.send_email(
            request=self.mock_sendinblue_request)

        # Assert
        kwargs = self.stub_request_module.request.call_args.kwargs
        self.assertTrue(
            result[0], "send_email should return True when successful")
        self.assertIn(message_id, result[1], "result should contain messageId")
        self.__assert_request_kwargs(actual_kwargs=kwargs)

    def test_send_email_failure(self):
        # Arrange
        code = "method_not_allowed"
        message = "Method is not allowed on this path"
        mock_response = Mock(spec=object)
        mock_response.status_code = 400
        mock_response.text = json.dumps({
            "code": code,
            "message": message
        })
        self.stub_request_module.request.return_value = mock_response

        # Act
        result = self.sendinblue_client.send_email(
            request=self.mock_sendinblue_request)

        # Assert
        kwargs = self.stub_request_module.request.call_args.kwargs
        self.assertFalse(
            result[0], "send_email should return False when unsuccessful")
        self.assertIn(code, result[1], "result should contain code")
        self.assertIn(message, result[1], "result should contain message")
        self.__assert_request_kwargs(actual_kwargs=kwargs)
