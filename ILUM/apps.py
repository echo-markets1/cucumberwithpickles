from django.apps import AppConfig


class IlumConfig(AppConfig):
    name = 'ILUM'
