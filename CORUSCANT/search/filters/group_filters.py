from django.db.models import Q, QuerySet
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector


def sqlite_filter(queryset: QuerySet, query: str) -> QuerySet:
    # This lacks a method of ranking the results (currently random). postgres_filter function on prod will solve this
    # also lacks the more user friendly search vector and ranking of results
    queryset = queryset.filter(
        Q(name__icontains=query) |
        Q(description__icontains=query)
    ).order_by('?')

    return queryset


def postgres_filter(queryset: QuerySet, query: str) -> QuerySet:
    # Use in prod, this uses more advanced filtering techniques
    # The following should work on the live server with PostgreSQL.
    vectors = (
        SearchVector('name')
        + SearchVector('description')
    )
    search_queries = SearchQuery(query)
    queryset = queryset.annotate(
        rank=SearchRank(
            vectors,
            search_queries,
            weights=[1.0, 0.8]
        )
    ).order_by('-rank')

    return queryset
