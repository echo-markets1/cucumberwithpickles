from django.apps import AppConfig


class CoruscantConfig(AppConfig):
    name = 'CORUSCANT'
