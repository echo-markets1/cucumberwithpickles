from django.db import models
from django.utils.translation import ugettext_lazy as _

from .member import Member
from .group import Group


class Type(models.TextChoices):
    OWNER = 'O', _(
        "Owner of the group, can modify groups detail, existence, membership and administration")
    ADMIN = 'A', _(
        "Administrator of the group, can modify status of other memberships")
    STANDARD = 'S', _("Standard membership, no additional privileges")


class Status(models.TextChoices):
    INVITED = 'I', _('Invited but user has not accepted')
    REQUESTED = 'R', _('Requested but admin has not accepted')
    ACTIVE = 'A', _('Active member of group')
    DISABLED = 'D', _(
        'Previously active, no longer participating in the group')


class Membership(models.Model):
    class Meta:
        unique_together = (('member', 'group'))

    member = models.ForeignKey(Member, on_delete=models.PROTECT)
    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    portfolio_id = models.CharField(null=True, max_length=40)

    datetime_joined = models.DateTimeField(auto_now_add=True)

    type = models.CharField(
        max_length=1,
        choices=Type.choices,
        default=Type.STANDARD
    )

    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        default=Status.ACTIVE
    )

    def __str__(self):
        return f'member: {self.member.user_id} | group: {self.group}'
