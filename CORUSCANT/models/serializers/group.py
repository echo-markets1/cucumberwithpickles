from rest_framework import serializers
from CORUSCANT.models import Group
from CORUSCANT.models.serializers.membership import NestedMembershipSerializer


class GroupSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Group
        fields = [
            'id',
            'name',
            'description',
            'privacy',
            'members',
            'requested_members',
            'invited_members',
        ]
        extras_kwargs = {
            'url': {'lookup_field': 'id'}
        }

    members = serializers.SerializerMethodField(read_only=True)
    requested_members = serializers.SerializerMethodField(read_only=True)
    invited_members = serializers.SerializerMethodField(read_only=True)
    id = serializers.UUIDField(read_only=True)

    def get_members(self, obj):
        members = obj.membership_set.filter(status='A')
        return NestedMembershipSerializer(members, many=True, read_only=True).data

    def get_requested_members(self, obj):
        members = obj.membership_set.filter(status='R')
        return NestedMembershipSerializer(members, many=True, read_only=True).data

    def get_invited_members(self, obj):
        members = obj.membership_set.filter(status='I')
        return NestedMembershipSerializer(members, many=True, read_only=True).data


class GroupCreationSerializer(serializers.ModelSerializer):

    portfolio_id = serializers.CharField()

    class Meta:
        model = Group
        fields = [
            'name',
            'description',
            'privacy',
            'portfolio_id'
        ]

    def create(self, validated_data):
        # This should not be called
        raise NotImplementedError(
            'Can not create object. `GroupCreationSerializer` is not a valid ModelSerializer.')

    def update(self, validated_data):
        # This should not be called
        raise NotImplementedError(
            'Can not update object. `GroupCreationSerializer` is not a valid ModelSerializer.')
