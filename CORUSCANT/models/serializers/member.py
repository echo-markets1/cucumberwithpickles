from rest_framework import serializers
from ..member import Member


class MemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ['user_id']