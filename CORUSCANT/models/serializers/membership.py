from rest_framework import serializers
from CORUSCANT.models import Membership


class NestedMembershipSerializer(serializers.ModelSerializer):
    # Intended to be used as a nested serialization within a group serialization
    class Meta:
        model = Membership
        fields = [
            'user_id',
            'portfolio_id',
            'datetime_joined',
            'type',
            'status'
        ]
        read_only_fields = ['user_id', 'portfolio_id',
                            'datetime_joined', 'status']
    user_id = serializers.UUIDField(source='member.user_id', read_only=True)


class CompleteMembershipSerializer(serializers.ModelSerializer):

    class Meta:
        model = Membership
        fields = [
            'user_id',
            'portfolio_id',
            'datetime_joined',
            'type',
            'status',
            'group'
        ]
        read_only_fields = [
            'user_id',
            'portfolio_id',
            'datetime_joined',
            'type',
            'status',
            'group'
        ]

    user_id = serializers.UUIDField(
        source='member.user_id', read_only=True)
