from django.db import models
import uuid
from .member import Member
from django.utils.translation import ugettext_lazy as _


class Privacy(models.TextChoices):
    """ Defines the groups of permissions and accessibililty of a group  """
    INVITE_ONLY = 'I', _('Invite only. Is not searchable')
    PRIVATE = 'P', _(
        'Searchable, requires permission to join and view group details')
    VISIBLE = 'V', _('Details visible to all, requires permission to join')
    NONE = 'N', _('Anyone can view and join')


class Group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=160)
    members = models.ManyToManyField(Member, through='Membership')
    privacy = models.CharField(
        max_length=1,
        choices=Privacy.choices,
        default=Privacy.PRIVATE,
    )

    def __str__(self):
        return f'name: {self.name} | privacy: {self.privacy}'
