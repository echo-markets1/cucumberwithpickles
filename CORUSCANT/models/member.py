from django.db import models


class Member(models.Model):
    """
    Members should be created when User Created Event is consumed
    Until then, a get_or_create method is used where appropriate
    TODO https://app.clickup.com/t/kmxarw
    """
    user_id = models.UUIDField(primary_key=True, editable=False)
