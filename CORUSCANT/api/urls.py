from django.urls import path
from CORUSCANT.api.views import group, member, membership

urlpatterns = [
    path('', group.GroupListCreate.as_view(), name='group_list_create'),
    path('<uuid:id>/', group.GroupDetail.as_view(), name='group_detail'),
    path('search/', group.GroupSearch.as_view(), name='group_search'),
    path('<uuid:group_id>/membership/',
         member.MemberCreate.as_view(), name='member_create'),
    path('<uuid:group_id>/membership/<uuid:member_id>/',
         member.MemberDetail.as_view(), name='member_detail'),
    path('memberships/<uidb64>/',
         membership.MembershipList.as_view(), name='membership_list')
]
