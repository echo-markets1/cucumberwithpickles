from rest_framework.permissions import BasePermission
from CORUSCANT.core import permissions


class IsAdminOrOwnerToEdit(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'PUT' or request.method == 'PATCH':
            return permissions.is_group_admin_or_owner_to_edit_group(obj, request.user.id)
        else:
            return True


class IsOwnerToEdit(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'PUT' or request.method == 'PATCH':
            return permissions.is_group_owner_to_edit_membership(obj, request.user)
        else:
            return True


class IsMemberOrVisibleToView(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return permissions.is_group_member_or_is_visible_to_view_group(obj, request.user.id)
        else:
            return True


class IsUserToDeleteMembership(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'DELETE':
            return permissions.is_user_to_delete_membership(obj, request.user.id)
        else:
            return True
