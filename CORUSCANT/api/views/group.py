from base64 import urlsafe_b64decode
from django.conf import settings
from django.db.models import Count
from django.db.models.expressions import Case, When
from django.db.models.query_utils import Q
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from CORELLIA.extensions.data_extensions import get_group_ids_by_activity_level
from CORUSCANT.models import Group
from CORUSCANT.api import permissions
from CORUSCANT.core import group_management
from CORUSCANT.models.group import Privacy
from CORUSCANT.models.membership import Status
from CORUSCANT.models.serializers.group import GroupCreationSerializer, GroupSerializer
from CORUSCANT.search.filters import group_filters


class GroupsLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 10
    max_limit = 50  # Arbitrary, fine to change in the future if desired


class GroupListCreate(generics.ListCreateAPIView):
    '''
    API endpoint for Listing and Creating Groups.

    **GET**
        query_params:
            - filter (str): Options are "top" or "trending". Filtering for the top
                groups returns the non-invite-only groups the user is not a member
                of, sorted by number of members descending. trending returns the
                non-invite-only groups the user is not a member of, sorted by the
                level of engagement in that group. No filter results in all the groups
                the requester is a member of.

        Returns:
            - A serialized representation of all the Groups from the query

    **POST**
        request.data:
            - name (str): The name of the group
            - description (str): The description of the group
            - privacy (Privacy): The privacy setting of the group
            - portfolio_id (str): The id of the portfolio being used to join the group

        Returns:
            - Serialized representation of the created Group
    '''
    permission_classes = [IsAuthenticated]
    pagination_class = GroupsLimitOffsetPagination

    def get_serializer_class(self):
        if self.request.method == "POST":
            return GroupCreationSerializer
        return GroupSerializer

    def get_queryset(self):
        qs = Group.objects.none()
        query_params = self.request.query_params
        user_id_b64 = query_params.get('user_id', None)
        user_id = urlsafe_b64decode(
            user_id_b64) if user_id_b64 != None else self.request.user.id
        filter_by = query_params.get('filter', None)
        if filter_by == "top":
            qs = Group.objects.\
                exclude(privacy=Privacy.INVITE_ONLY).\
                exclude(membership__member__user_id=user_id, membership__status=Status.ACTIVE).\
                annotate(num_members=Count('members')).\
                order_by('-num_members')
        elif filter_by == "trending":
            group_ids_ordered_by_activity_level = get_group_ids_by_activity_level()

            # Allows us to build a queryset of the Groups preserving the order
            activity_level = Case(*[When(id=id, then=position)
                                    for position, id in enumerate(group_ids_ordered_by_activity_level)])
            qs = Group.objects.\
                exclude(privacy=Privacy.INVITE_ONLY).\
                exclude(membership__member__user_id=user_id, membership__status=Status.ACTIVE).\
                filter(id__in=group_ids_ordered_by_activity_level).\
                order_by(activity_level)
        elif user_id != self.request.user.id:
            # Groups the user is an active member of, but are visible to non members
            qs = Group.objects.\
                filter(membership__member__user_id=user_id, membership__status=Status.ACTIVE).\
                exclude(privacy=Privacy.INVITE_ONLY)
        else:
            # All groups the requesting user is an active member of
            qs = Group.objects.filter(
                membership__member__user_id=user_id,
                membership__status=Status.ACTIVE
            )

        return qs

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        create_group_request_data = serializer.data
        create_group_request_data["user_id"] = request.user.id
        (success, response) = group_management.create_group(
            **create_group_request_data)
        if success:
            serializer = GroupSerializer(instance=response)
            headers = self.get_success_headers(serializer.data)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({'detail': response}, status=status.HTTP_400_BAD_REQUEST, headers={})


class GroupDetail(generics.RetrieveUpdateAPIView):
    '''
    API endpoint for editting an existing Group.

    **GET**
        kwargs:
            - id (uuid.UUID): The id of the group

        Returns:
            - Serialized representation of the Group
    **PUT/PATCH**
        kwargs:
            - id (uuid.UUID): The id of the group

        request.data:
            - name (str): The name of the group
            - description (str): The description of the group

        Returns:
            - Serialized representation of the updated Group
    '''
    permission_classes = [
        IsAuthenticated, permissions.IsMemberOrVisibleToView
        # Since we are preferring to decouple the core logic of editting the
        # Group from the API logic, the underlying methods which call the following
        # permissions classes never get called. However, these permissions calls
        # are baked into the group_management core logic
        # permissions.IsAdminOrOwnerToEdit,
        #
    ]
    serializer_class = GroupSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Group.objects.all()

    def update(self, request, *args, **kwargs):
        (success, result) = group_management.edit_group(
            kwargs.get('id'),
            request.user.id,
            request.data.get("name", None),
            request.data.get("description", None)
        )
        if success:
            serializer = self.get_serializer(result)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            if "does not have permission" in result:
                error_code = status.HTTP_403_FORBIDDEN
            else:
                error_code = status.HTTP_400_BAD_REQUEST
            return Response({'detail': result}, status=error_code)

    def partial_update(self, request, *args, **kwargs):
        self.update(self, request, *args, **kwargs)


class SearchPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class GroupSearch(generics.ListAPIView):
    pagination_class = SearchPagination
    serializer_class = GroupSerializer
    search_fields = ['q']
    permission_classes = [IsAuthenticated, permissions.IsMemberOrVisibleToView]

    def get_queryset(self):
        query = self.request.query_params.get('q', '')
        # First, ensure that the base queryset is either visible groups or groups
        # the user is an active member of
        queryset = Group.objects.filter(
            ~Q(privacy=Privacy.INVITE_ONLY) |
            Q(membership__member__user_id=self.request.user.id,
              membership__status=Status.ACTIVE)
        )
        if ('sqlite' in settings.DATABASES['default']['NAME']):
            return group_filters.sqlite_filter(queryset, query).distinct()
        else:
            return group_filters.postgres_filter(queryset, query).distinct()
