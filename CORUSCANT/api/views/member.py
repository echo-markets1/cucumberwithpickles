import uuid
from django.http.response import Http404
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from CORUSCANT.models.group import Group
from CORUSCANT.models.serializers.group import GroupSerializer
from CORUSCANT.api.permissions import IsOwnerToEdit, IsUserToDeleteMembership
from CORUSCANT.core import membership_management


def _get_group(id: uuid.UUID) -> Group:
    try:
        return Group.objects.get(id=id)
    except Group.DoesNotExist:
        raise Http404


class MemberCreate(views.APIView):
    '''
    API endpoint for creating and accepting group membership
    Note: No get method because membership lists are gotten from the group's endpoint.

    **POST**
        kwargs:
            - group_id (uuid.UUID): The id of the Group for which membership is being modified.

        request.data:
            - portfolio_id (str): The identifier of the portfolio being used to join the group.
            - member_id (uuid.UUID): The id of the member being accepted/created

        Returns:
            - Serialized representation of the Group with the new/updated member
    '''
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        group = _get_group(id=kwargs.get("group_id"))
        portfolio_id = request.data.get('portfolio_id', None)
        member_id = request.data.get('member_id', None)
        (success, result) = membership_management.add_membership(
            group=group,
            request_user_id=request.user.id,
            portfolio_id=portfolio_id,
            member_id=member_id,
        )
        if success:
            group.refresh_from_db()
            serializer = GroupSerializer(instance=group)
            return Response(data=serializer.data, status=status.HTTP_202_ACCEPTED)
        else:
            return Response(data={'detail': result}, status=status.HTTP_400_BAD_REQUEST)


class MemberDetail(views.APIView):
    '''
    API endpoint for deleting and editing group membership. If the member is
    currently Active in the group, the DELETE acts as a soft delete. Otherwise,
    if the membership is in a non Active state, the membership instance is deleted
    from the database.

    **PATCH**
        kwargs:
            - group_id (uuid.UUID): The id of the Group for which membership is being modified.
            - member_id (uuid.UUID): The id of the member being accepted/created

        request.data:
            - type (Type): The new membership type for the given member

        Returns:
            - Serialized representation of the Group with the new/updated member

    **DELETE**
        kwargs:
            - group_id (uuid.UUID): The id of the Group for which membership is being modified.
            - member_id (uuid.UUID): The id of the member being accepted/created

        Returns:
            - If not deleted, a serialized representation of the Group with the new/updated member.
    '''
    permission_classes = [IsAuthenticated,
                          IsOwnerToEdit, IsUserToDeleteMembership]

    def patch(self, request, *args, **kwargs):
        group = _get_group(id=kwargs.get("group_id"))
        member_type = request.data.get('type', None)
        member_id = kwargs.get('member_id')
        (success, result) = membership_management.edit_membership(
            group=group,
            request_user_id=request.user.id,
            member_id=member_id,
            new_type=member_type
        )
        if success:
            group.refresh_from_db()
            serializer = GroupSerializer(instance=group)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(data={'detail': result}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        group = _get_group(id=kwargs.get("group_id"))
        member_id = kwargs.get('member_id')
        (success, result) = membership_management.delete_membership(
            group=group,
            request_user_id=request.user.id,
            member_id=member_id,
        )
        if success:
            group.refresh_from_db()
            serializer = GroupSerializer(instance=group)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(data={'detail': result}, status=status.HTTP_400_BAD_REQUEST)
