from base64 import urlsafe_b64decode
from django.utils.encoding import force_str
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from CORUSCANT.models.group import Privacy
from CORUSCANT.models.membership import Membership

from CORUSCANT.models.serializers.membership import CompleteMembershipSerializer


class MembershipList(generics.ListAPIView):
    '''
    API endpoint for listing out Memberships for a user.

    **GET**
        kwargs: 
            - uidb64 (str): The base64 id of the user whose memberships are
                being retrieved

        Returns:
            - A list of serialized Memberships for the user
    '''
    permission_classes = [IsAuthenticated]
    serializer_class = CompleteMembershipSerializer

    def get_queryset(self):
        user_id_b64 = self.kwargs['uidb64']
        user_id = force_str(urlsafe_b64decode(user_id_b64))
        qs = Membership.objects.filter(member__user_id=user_id)
        if user_id != self.request.user.id:
            # If the request is for another member, only return the memberships
            # that are publically visible
            qs = qs.exclude(group__privacy=Privacy.INVITE_ONLY)
        return qs
