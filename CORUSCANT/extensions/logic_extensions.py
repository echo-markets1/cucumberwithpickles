import uuid
from typing import Union
from CORUSCANT.models import Group
from CORUSCANT.core import permissions


def validate_user_is_group_member_or_visible_to_view_group(user_id: Union[str, uuid.UUID], group_id: uuid.UUID) -> bool:
    try:
        group = Group.objects.get(id=group_id)
        return permissions.is_group_member_or_is_visible_to_view_group(group=group, user_id=user_id)
    except:
        return False


def validate_user_is_group_member_or_visible_to_view_post(user_id: Union[str, uuid.UUID], group_id: uuid.UUID) -> bool:
    try:
        group = Group.objects.get(id=group_id)
        return permissions.is_group_member_or_is_visible_to_view_post(group=group, user_id=user_id)
    except:
        return False


def validate_user_is_group_member(group_id: uuid.UUID, user_id: Union[str, uuid.UUID]) -> bool:
    try:
        group = Group.objects.get(id=group_id)
        return group.members.filter(user_id=user_id).exists()
    except:
        return False


def validate_user_is_group_admin_or_owner(user_id: Union[str, uuid.UUID], group_id: uuid.UUID) -> bool:
    try:
        group = Group.objects.get(id=group_id)
        return permissions.is_group_admin_or_owner_to_edit_group(group, user_id)
    except:
        return False
