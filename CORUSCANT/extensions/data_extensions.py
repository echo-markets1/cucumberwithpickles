""" Defines interfaces for external services to query CORUSCANT data. """
import uuid
from django.db.models import Q
from typing import Dict, List
from CORUSCANT.models import Group, serializers
from CORUSCANT.core import permissions
from CORUSCANT.models.group import Privacy
from CORUSCANT.models.membership import Status


def get_group(group_id: uuid.UUID, request_user_id: uuid.UUID) -> Dict:
    try:
        group = Group.objects.get(id=group_id)
        user_can_view = permissions.is_group_member_or_is_visible_to_view_group(
            group, request_user_id)
        if user_can_view:
            return serializers.group.GroupSerializer(group).data
        return None
    except:
        return None


def get_all_visible_group_ids_for(user_id: uuid.UUID) -> List[uuid.UUID]:
    groups = Group.objects.filter(
        Q(privacy__in=[Privacy.VISIBLE, Privacy.NONE]) |
        Q(membership__member__user_id=user_id, membership__status=Status.ACTIVE)
    )
    return list(map(lambda group: group.id, groups))


def get_all_group_ids_for_portfolio(portfolio_id: str) -> List[uuid.UUID]:
    groups = Group.objects.filter(membership__portfolio_id=portfolio_id)
    return list(map(lambda group: group.id, groups))


def get_all_portfolio_ids_for_group(group_id: uuid.UUID, request_user_id: uuid.UUID) -> List[str]:
    portfolio_ids = []
    group = get_group(group_id, request_user_id)
    if not group is None:
        portfolio_ids = list(
            map(lambda member: member['portfolio_id'], group["members"]))
    return portfolio_ids
