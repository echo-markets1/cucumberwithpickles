from django.test import TestCase
from ALDERAAN.tests.factories.user_factory import UserFactory
from CORUSCANT.tests.factories.group_factory import GroupFactory
from CORUSCANT.core import membership_management
from CORUSCANT.models import Member, Membership

from CORUSCANT.tests import alphanumeric_id_generator


class BaseGroupTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = UserFactory.create()
        cls.request_user = UserFactory.create(
            email="clone1@kamino@wars.com", handle="clone1")
        cls.invite_user = UserFactory.create(
            email="clone2@kamino@wars.com", handle="clone2")
        cls.standard_user = UserFactory.create(
            email="clone3@kamino@wars.com", handle="clone3")


class MembershipMixin(TestCase):
    def assert_membership_exists_with_status_and_type(self, user_id, status='A', type='S'):
        new_membership = Membership.objects.filter(member__user_id=user_id)
        self.assertTrue(new_membership.exists())
        self.assertEqual(new_membership[0].type, type)
        self.assertEqual(new_membership[0].status, status)

    def assert_membership_does_not_exist(self, user_id):
        new_membership = Membership.objects.filter(member__user_id=user_id)
        self.assertFalse(new_membership.exists())

    def invite_membership(self):
        return membership_management.add_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.invite_user.id,
        )

    def request_membership(self):
        return membership_management.add_membership(
            group=self.group,
            request_user_id=self.request_user.id,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
        )


class BaseInviteOnlyGroupTest(BaseGroupTest):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='I')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )


class BasePrivateGroupTest(BaseGroupTest):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='P')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )


class BaseVisibleGroupTest(BaseGroupTest):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='V')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )


class BaseNoPrivacyGroupTest(BaseGroupTest):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='N')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )
