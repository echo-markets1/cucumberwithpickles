from django.urls import reverse
from rest_framework.test import APIClient
from CORUSCANT.models import Member, Membership
from CORUSCANT.tests.base_classes import base_group_test_classes
from CORUSCANT.tests import alphanumeric_id_generator
from CORUSCANT.tests.factories.group_factory import GroupFactory

# TODO Also assert events get emitted properly


class APIGroupListViewTest(base_group_test_classes.BaseGroupTest):

    def setUp(self):
        super().setUp()
        self.request_data = {
            'name': 'I AM GROUP',
            'description': 'Group?',
            'privacy': 'N',
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.client.login(username=self.owner.email,
                          password='defaultpassword')
        self.url = reverse('group_list_create')

    def test_create_group(self):
        self.response = self.client.post(self.url, self.request_data)
        self.assertEquals(self.response.status_code, 201)
        self.assertEquals(
            self.response.data['name'], self.request_data['name'])
        self.assertEquals(
            self.response.data['members'][0]['user_id'], str(self.owner.id))


class APIGroupDetailViewTest(base_group_test_classes.BaseGroupTest):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='I')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )

    def setUp(self):
        self.request_data = {
            'name': 'name or something idk',
            'description': "some description"
        }
        self.client = APIClient()
        self.url = reverse('group_detail', kwargs={'id': self.group.id})

    def test_get_group_detail(self):
        self.client.login(username=self.standard_user.email,
                          password='defaultpassword')
        self.response = self.client.get(self.url)
        self.assertEquals(self.response.status_code, 200)

    def test_edit_group_detail(self):
        self.client.login(username=self.owner.email,
                          password='defaultpassword')
        self.response = self.client.put(self.url, data=self.request_data)
        self.assertEquals(self.response.status_code, 200)
        self.assertEquals(
            self.response.data['name'], self.request_data['name'])
        self.assertEquals(
            self.response.data['description'], self.request_data['description'])

    def test_non_admin_cannot_edit_group(self):
        self.client.login(username=self.standard_user.email,
                          password='defaultpassword')
        self.response = self.client.put(self.url, data=self.request_data)
        self.assertEquals(self.response.status_code, 403)

    def test_non_member_can_view_non_private_group(self):
        self.client.login(username=self.request_user.email,
                          password='defaultpassword')
        non_private_group = GroupFactory.create(privacy='N')
        url = reverse('group_detail', kwargs={'id': non_private_group.id})
        self.response = self.client.get(url)
        self.assertEquals(self.response.status_code, 200)

    def test_non_member_cannot_view_private_group(self):
        self.client.login(username=self.request_user.email,
                          password='defaultpassword')
        self.response = self.client.get(self.url)
        self.assertEquals(self.response.status_code, 403)

    def test_logged_out_user_cannot_view_private_group(self):
        self.client.logout()
        self.response = self.client.get(self.url)
        self.assertEquals(self.response.status_code, 401)

# class APIGroupSearchViewTest(base_group_test_classes.BaseGroupTest):
#     pass
