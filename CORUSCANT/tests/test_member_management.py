from CORUSCANT.core import membership_management
from CORUSCANT.models import Member, Membership
from CORUSCANT.tests.base_classes import base_group_test_classes

from CORUSCANT.tests import alphanumeric_id_generator

# TODO Also assert events get emitted properly


class AddMembershipInviteOnlyTest(base_group_test_classes.BaseInviteOnlyGroupTest, base_group_test_classes.MembershipMixin):

    def test_owner_invites_member(self):
        self.invite_membership()
        self.assert_membership_exists_with_status_and_type(
            user_id=self.invite_user.id, status='I')

    def test_invited_member_accepts(self):
        # Invited user accepts
        self.invite_membership()
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.invite_user.id,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.invite_user.id)

    def test_non_admin_cannot_invite_members(self):
        self.owner_membership.type = 'S'
        self.owner_membership.save()
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.invite_user.id,
        )
        self.assertFalse(success)
        self.assert_membership_does_not_exist(user_id=self.invite_user.id)

    def test_request_membership(self):
        (success, membership) = self.request_membership()
        self.assertFalse(success)
        self.assert_membership_does_not_exist(user_id=self.invite_user.id)


class AddMembershipPrivateGroupTest(base_group_test_classes.BasePrivateGroupTest, AddMembershipInviteOnlyTest):
    def test_request_membership(self):
        (success, membership) = self.request_membership()
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.request_user.id, status='R')

    def test_request_membership_approved(self):
        self.request_membership()
        # Owner approves member
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.request_user.id,
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.request_user.id)

    def test_non_admin_cannot_approve_member_requests(self):
        self.owner_membership.type = 'S'
        self.owner_membership.save()
        self.request_membership()
        # Owner approves member
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.request_user.id,
        )
        self.assertFalse(success)
        self.assert_membership_does_not_exist(user_id=self.invite_user.id)


class AddMembershipVisibleGroupTest(base_group_test_classes.BaseVisibleGroupTest, AddMembershipPrivateGroupTest):
    pass


class AddMembershipNoPrivacyGroupTest(base_group_test_classes.BaseNoPrivacyGroupTest, base_group_test_classes.MembershipMixin):

    def test_member_can_join(self):
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.invite_user.id,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.invite_user.id)

    def test_owner_invites_member(self):
        self.owner_membership.type = 'S'
        self.owner_membership.save()
        self.invite_membership()
        self.assert_membership_exists_with_status_and_type(
            user_id=self.invite_user.id, status='I')

    def test_invited_member_accepts(self):
        # Invited user accepts
        self.invite_membership()
        (success, membership) = membership_management.add_membership(
            group=self.group,
            request_user_id=self.invite_user.id,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.invite_user.id)


class EditMembershipTest(base_group_test_classes.BasePrivateGroupTest, base_group_test_classes.MembershipMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.membership = cls.standard_membership

    def test_owner_edits_membership(self):
        (success, membership) = membership_management.edit_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.standard_user.id,
            new_type='O'
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.standard_user.id, type='O')

    def test_non_owner_cannot_edit_membership(self):
        (success, membership) = membership_management.edit_membership(
            group=self.group,
            request_user_id=self.standard_user.id,
            member_id=self.standard_user.id,
            new_type='O'
        )
        self.assertFalse(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.standard_user.id, type='S')

    def test_group_must_always_have_one_onwer(self):
        (success, membership) = membership_management.edit_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.owner.id,
            new_type='S'
        )
        self.assertFalse(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.owner.id, type='O')


class DeleteMembershipTest(base_group_test_classes.BasePrivateGroupTest, base_group_test_classes.MembershipMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.active_membership = cls.standard_membership
        cls.requested_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.request_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='R'
        )
        cls.invited_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.invite_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='I'
        )

    def test_owner_deletes_active_members(self):
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.standard_user.id,
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.standard_user.id, status='D')

    def test_owner_deletes_requested_members(self):
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.request_user.id,
        )
        self.assertTrue(success)
        self.assert_membership_does_not_exist(user_id=self.request_user.id)

    def test_owner_deletes_invited_members(self):
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.invite_user.id,
        )
        self.assertTrue(success)
        self.assert_membership_does_not_exist(user_id=self.invite_user.id)

    def test_non_owner_can_delete_own_membership(self):
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.standard_user.id,
            member_id=self.standard_user.id,
        )
        self.assertTrue(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.standard_user.id, status='D', type='S')

    def test_non_owner_cannot_delete_other_user_membership(self):
        expected_status = self.requested_membership.status
        expected_type = self.requested_membership.type
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.standard_user.id,
            member_id=self.request_user.id,
        )
        self.assertFalse(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.request_user.id, status=expected_status, type=expected_type)

    def test_group_must_always_have_one_onwer(self):
        (success, membership) = membership_management.delete_membership(
            group=self.group,
            request_user_id=self.owner.id,
            member_id=self.owner.id,
        )
        self.assertFalse(success)
        self.assert_membership_exists_with_status_and_type(
            user_id=self.owner.id, type='O')
