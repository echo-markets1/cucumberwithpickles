from django.test import TestCase
from django.urls import reverse
from CORUSCANT.models import Member, Membership
from CORUSCANT.tests.base_classes import base_group_test_classes
from rest_framework.test import APIClient

from CORUSCANT.tests import alphanumeric_id_generator

# TODO Also assert events get emitted properly


class ApiMemberMixin(TestCase):

    def post(self, expected_status_code=202):
        self.response = self.client.post(self.url, self.request_data)
        self.assertEquals(self.response.status_code, expected_status_code)


class RequestSetupMixin(TestCase):
    def set_up_request_to_invite_member(self, request_user, member_user):
        self.request_data = {
            'member_id': member_user.id,
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.client.login(username=request_user.email,
                          password='defaultpassword')

    def set_up_request_to_request_membership(self):
        self.request_data = {
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.client.login(username=self.request_user.email,
                          password='defaultpassword')

    def set_up_request_to_edit_member(self, request_user, member_user):
        self.request_data = {
            'type': 'A'
        }
        self.url = reverse(
            'member_detail',
            kwargs={
                'group_id': str(self.group.id),
                'member_id': member_user.id,
            }
        )
        self.client.login(username=request_user.email,
                          password='defaultpassword')

    def set_up_request_to_delete_member(self, request_user, member_user):
        self.url = reverse(
            'member_detail',
            kwargs={
                'group_id': str(self.group.id),
                'member_id': member_user.id,
            }
        )
        self.client.login(username=request_user.email,
                          password='defaultpassword')

    def set_up_request_to_approve_member(self, request_user, member_user):
        self.request_data = {
            'member_id': member_user.id
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.client.login(username=request_user.email,
                          password='defaultpassword')


class BaseMemberApiTest(ApiMemberMixin, RequestSetupMixin):
    def setUp(self):
        self.skipTest(
            "Base Class not meant to be tested. Override with subclass to run these tests.")

    def test_owner_can_edit_member(self):
        self.set_up_request_to_edit_member(self.owner, self.standard_user)
        self.response = self.client.patch(self.url, data=self.request_data)
        self.assertEquals(self.response.status_code, 200)
        updated_member = {}
        for member in self.response.data['members']:
            if member["user_id"] == str(self.standard_user.id):
                updated_member = member
        self.assertEquals(updated_member['type'], self.request_data['type'])

    def test_non_owner_cannot_edit_member(self):
        self.set_up_request_to_edit_member(self.standard_user, self.owner)
        self.response = self.client.patch(self.url, data=self.request_data)
        self.assertEquals(self.response.status_code, 400)
        self.assertEquals(Membership.objects.get(
            member__user_id=self.owner.id).type, 'O')

    def test_owner_can_delete_member(self):
        self.set_up_request_to_delete_member(self.owner, self.standard_user)
        self.response = self.client.delete(self.url)
        self.assertEquals(self.response.status_code, 200)
        self.assertEquals(Membership.objects.get(
            member__user_id=self.standard_user.id).status, 'D')

    def test_non_owner_can_delete_own_membership(self):
        self.set_up_request_to_delete_member(
            self.standard_user, self.standard_user)
        self.response = self.client.delete(self.url)
        self.assertEquals(self.response.status_code, 200)
        self.assertEquals(Membership.objects.get(
            member__user_id=self.standard_user.id).status, 'D')

    def test_non_owner_cannot_delete_other_member(self):
        self.set_up_request_to_delete_member(self.standard_user, self.owner)
        self.response = self.client.delete(self.url)
        self.assertEquals(self.response.status_code, 400)
        self.assertEquals(Membership.objects.get(
            member__user_id=self.standard_user.id).status, 'A')


class ApiInviteOnlyGroupViewsTest(base_group_test_classes.BaseInviteOnlyGroupTest, BaseMemberApiTest):
    def setUp(self):
        self.client = APIClient()

    def test_admin_invite_group_member(self):
        self.set_up_request_to_invite_member(self.owner, self.invite_user)
        self.post()
        self.assertEquals(
            self.response.data['invited_members'][0]['user_id'], str(self.invite_user.id))

    def test_non_admin_attempts_member_invite(self):
        # Intended result is failure in this class, since Invite Only, Private, and Visible groups should only allow admins and owners to invite
        self.set_up_request_to_invite_member(
            self.standard_user, self.invite_user)
        self.post(expected_status_code=400)
        self.assertEquals(
            self.response.data['detail'], 'You do not have permission to do that.')

    def test_accept_group_invite(self):
        invited_member = Member.objects.get_or_create(
            user_id=self.invite_user.id)[0]
        self.invited_membership = Membership.objects.create(
            member=invited_member,
            group=self.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='I'
        )
        self.client.login(username=self.invite_user.email,
                          password='defaultpassword')
        self.request_data = {
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.invite_user.id) in user_ids,
                        "The returned Group should contain the new member")

    def test_request_membership(self):
        # In this class, expected behavior is failure - Invite Only does not membership requests. Subclasses (ie Private group tests) will expect a positive response
        self.set_up_request_to_request_membership()
        self.post(expected_status_code=400)
        self.assertEquals(
            self.response.data['detail'], 'Membership requests are not allowed for an invite-only group')


class ApiPrivateGroupViewsTest(base_group_test_classes.BasePrivateGroupTest, BaseMemberApiTest):
    def setUp(self):
        self.client = APIClient()

    def test_request_membership(self):
        self.set_up_request_to_request_membership()
        self.post()
        requested_user_ids = list(
            map(lambda member: member["user_id"], self.response.data['requested_members']))
        self.assertTrue(str(self.request_user.id) in requested_user_ids,
                        "The returned Group should contain the newly requested member")

    def createRequesterMembership(self):
        self.requested_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=self.request_user.id)[0],
            group=self.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='R'
        )

    def test_admin_approve_membership_request(self):
        self.createRequesterMembership()
        self.set_up_request_to_approve_member(self.owner, self.request_user)
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.request_user.id) in user_ids,
                        "The returned Group should contain the new member")

    def test_non_admin_cannot_approve_membership_request(self):
        self.createRequesterMembership()
        self.set_up_request_to_approve_member(
            self.standard_user, self.request_user)
        self.post(expected_status_code=400)
        self.assertEquals(
            self.response.data['detail'], 'You do not have permission to do that.')

    def test_admin_invite_group_member(self):
        self.set_up_request_to_invite_member(self.owner, self.invite_user)
        self.post()
        self.assertEquals(
            self.response.data['invited_members'][0]['user_id'], str(self.invite_user.id))

    def test_non_admin_attempts_member_invite(self):
        # Intended result is failure in this class, since Invite Only, Private, and Visible groups should only allow admins and owners to invite
        self.set_up_request_to_invite_member(
            self.standard_user, self.invite_user)
        self.post(expected_status_code=400)
        self.assertEquals(
            self.response.data['detail'], 'You do not have permission to do that.')

    def test_accept_group_invite(self):
        invited_member = Member.objects.get_or_create(
            user_id=self.invite_user.id)[0]
        self.invited_membership = Membership.objects.create(
            member=invited_member,
            group=self.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='I'
        )
        self.client.login(username=self.invite_user.email,
                          password='defaultpassword')
        self.request_data = {
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.invite_user.id) in user_ids,
                        "The returned Group should contain the new member")


class ApiVisibleGroupViewsTest(base_group_test_classes.BaseVisibleGroupTest, BaseMemberApiTest):
    def setUp(self):
        self.client = APIClient()

    def test_admin_invite_group_member(self):
        self.set_up_request_to_invite_member(self.owner, self.invite_user)
        self.post()
        self.assertEquals(
            self.response.data['invited_members'][0]['user_id'], str(self.invite_user.id))

    def test_non_admin_attempts_member_invite(self):
        # Intended result is failure in this class, since Invite Only, Private, and Visible groups should only allow admins and owners to invite
        self.set_up_request_to_invite_member(
            self.standard_user, self.invite_user)
        self.post(expected_status_code=400)
        self.assertEquals(
            self.response.data['detail'], 'You do not have permission to do that.')

    def test_accept_group_invite(self):
        invited_member = Member.objects.get_or_create(
            user_id=self.invite_user.id)[0]
        self.invited_membership = Membership.objects.create(
            member=invited_member,
            group=self.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='I'
        )
        self.client.login(username=self.invite_user.email,
                          password='defaultpassword')
        self.request_data = {
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.invite_user.id) in user_ids,
                        "The returned Group should contain the new member")

    def test_request_membership(self):
        self.set_up_request_to_request_membership()
        self.post()
        requested_user_ids = list(
            map(lambda member: member["user_id"], self.response.data['requested_members']))
        self.assertTrue(str(self.request_user.id) in requested_user_ids,
                        "The returned Group should contain the newly requested member")


class ApiNoPrivacyGroupViewsTest(base_group_test_classes.BaseNoPrivacyGroupTest, BaseMemberApiTest):
    def setUp(self):
        self.client = APIClient()

    def test_non_member_can_join(self):
        self.set_up_request_to_request_membership()
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.request_user.id) in user_ids,
                        "The returned Group should contain the new member")

    def test_non_admin_attempts_member_invite(self):
        self.set_up_request_to_invite_member(
            self.standard_user, self.invite_user)
        self.post()
        invited_user_ids = list(
            map(lambda member: member["user_id"], self.response.data['invited_members']))
        self.assertTrue(str(self.invite_user.id) in invited_user_ids,
                        "The returned Group should contain the newly invited member")

    def test_accept_group_invite(self):
        invited_member = Member.objects.get_or_create(
            user_id=self.invite_user.id)[0]
        self.invited_membership = Membership.objects.create(
            member=invited_member,
            group=self.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='I'
        )
        self.client.login(username=self.invite_user.email,
                          password='defaultpassword')
        self.request_data = {
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }
        self.url = reverse('member_create', kwargs={
                           'group_id': str(self.group.id)})
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.invite_user.id) in user_ids,
                        "The returned Group should contain the new member")

    def test_request_membership(self):
        self.set_up_request_to_request_membership()
        self.post()
        user_ids = list(
            map(lambda member: member["user_id"], self.response.data['members']))
        self.assertTrue(str(self.request_user.id) in user_ids,
                        "The returned Group should contain the new member")
