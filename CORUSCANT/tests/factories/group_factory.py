import factory
from factory import BUILD_STRATEGY
from faker import Factory
from CORUSCANT.models import Group

faker = Factory.create()


class GroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = Group
        strategy = BUILD_STRATEGY

    name = factory.LazyFunction(faker.name)
    description = factory.LazyFunction(faker.sentence)
