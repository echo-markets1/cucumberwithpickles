from CORUSCANT.core import group_management
from CORUSCANT.models import Group
from CORUSCANT.models.member import Member
from CORUSCANT.models.membership import Membership
from CORUSCANT.tests.base_classes import base_group_test_classes

from CORUSCANT.tests import alphanumeric_id_generator
from CORUSCANT.tests.factories.group_factory import GroupFactory

# TODO Also assert events get emitted properly


class CreateGroupTest(base_group_test_classes.BaseGroupTest):

    def setUp(self):
        super().setUp()
        self.request_data = {
            'name': 'Group name',
            'description': 'Group description',
            'privacy': 'N',
            'user_id': self.owner.id,
            'portfolio_id': str(alphanumeric_id_generator.create_random_id()),
        }

    def test_create_group(self):
        (success, group) = group_management.create_group(**self.request_data)
        self.assertTrue(success)
        self.assertEqual(1, Group.objects.count())
        self.assertTrue(group.members.filter(user_id=self.owner.id).exists())


class EditGroupTest(base_group_test_classes.BaseGroupTest):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.group = GroupFactory.create(privacy='I')
        cls.owner_membership = Membership.objects.create(
            member=Member.objects.get_or_create(user_id=cls.owner.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='O',
            status='A'
        )
        cls.standard_membership = Membership.objects.create(
            member=Member.objects.get_or_create(
                user_id=cls.standard_user.id)[0],
            group=cls.group,
            portfolio_id=str(alphanumeric_id_generator.create_random_id()),
            type='S',
            status='A'
        )

    def setUp(self):
        self.request_data = {
            'name': 'I AM GROUP',
            'description': 'GROUP',
        }

    def test_edit_group_success(self):
        (success, response_data) = group_management.edit_group(
            self.group.id, self.owner.id,  self.request_data["name"], self.request_data["description"])
        self.group.refresh_from_db()
        self.assertTrue(success)
        self.assertEqual(
            self.group.name,
            self.request_data["name"],
            "Edit should update name of Group on success"
        )
        self.assertEqual(
            self.group.description,
            self.request_data["description"],
            "Edit should update description of Group on success"
        )
