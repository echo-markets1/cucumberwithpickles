import uuid
from CORUSCANT.models import Membership


def is_group_admin_or_owner_to_edit_group(group, user_id):
    membership = Membership.objects.get(group=group, member__user_id=user_id)
    if (membership.type == 'O' or membership.type == 'A'):
        return True
    else:
        return False


def is_group_owner_to_edit_membership(group, user):
    membership = Membership.objects.get(group=group, member__user_id=user.id)
    if (membership.type == 'O'):
        return True
    else:
        return False


def is_group_member_or_is_visible_to_view_group(group, user_id):
    if group.privacy != 'I':
        return True
    elif group.members.filter(user_id=user_id).exists():
        return True
    else:
        return False


def is_group_member_or_is_visible_to_view_post(group, user_id):
    if group.privacy in ['N', 'V']:
        return True
    elif group.members.filter(user_id=user_id).exists():
        return True
    else:
        return False


def is_user_to_delete_membership(membership: Membership, user_id: uuid.UUID) -> bool:
    return membership.member.user_id == user_id
