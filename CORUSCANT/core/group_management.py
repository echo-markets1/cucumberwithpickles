
from typing import Tuple, Union
import uuid
from CORUSCANT.models.group import Group, Privacy
from CORUSCANT.core import membership_management, permissions


def create_group(name: str, description: str, privacy: Privacy, portfolio_id: str,
                 user_id: uuid.UUID) -> Tuple[bool, Union[Group, str]]:
    """
    Creates a Group based on the name, description, privacy, and id of the user

    Parameters:
        - name (str): The name of the group
        - description (str): The description of the group
        - privacy (Privacy): The privacy setting of the group
        - portfolio_id (str): The id of the portfolio being used to join the group
        - user_id (uuid.UUID): The id of the user creating the group
    """
    group = Group.objects.create(
        name=name,
        description=description,
        privacy=privacy
    )
    (success, detail) = membership_management.add_membership(
        group=group,
        request_user_id=user_id,
        portfolio_id=portfolio_id,
    )
    if success:
        # PRODUCE EVENT: Group created
        return (True, group)
    else:
        # PRODUCE EVENT: Group creation FAILED
        group.delete()
        return (False, detail)


def edit_group(group_id: uuid.UUID, editting_member_id: uuid.UUID, name: str = None,
               description: str = None) -> Tuple[bool, Union[Group, str]]:
    """
    Edits the name and description of a Group

    Parameters:
        - group_id (uuid.UUID): The id of the group being editted
        - editting_member_id (uuid.UUID): The id of the member attempting to edit the group
        - name (str): The name of the group
        - description (str): The description of the group
    """
    try:
        group = Group.objects.get(id=group_id)
        if not permissions.is_group_admin_or_owner_to_edit_group(
                group, editting_member_id):
            return (False, f"Member with id: '{editting_member_id}' does not " +
                    f"have permission to edit group with id: {group_id}")
        if not name is None:
            group.name = name
        if not description is None:
            group.description = description
        group.save()
        # PRODUCE EVENT: Group edited (included changed data)
        return (True, group)
    except:
        return (False, f"Could not find a group with id: {group_id}")
