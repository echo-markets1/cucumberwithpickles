from typing import Tuple, Union
import uuid
from CORUSCANT.models.group import Group
from CORUSCANT.models.member import Member
from CORUSCANT.models.membership import Membership, Status, Type
from CORUSCANT.core.bucket_compatibility_validator import validate_bucket


def add_membership(group: Group, request_user_id: uuid.UUID, member_id: uuid.UUID = None,
                   portfolio_id: str = None) -> Tuple[bool, Union[Membership, str]]:
    """
    This contains the logic for adding members.
    5 cases:
    On new group creation, adding the first member as the owner
        group, request_user_id, portfolio_id, group_creation
    Group has no privacy and user is joining
        group, request_user_id, portfolio_id
    Group has privacy P or V, and user is requesting to be added
        group, request_user_id, portfolio_id
    Admin approves an request
        group, request_user_id, portfolio_id
    Admin invites another user
        group, request_user_id, portfolio_id
    Invited user accepts invite
        group, request_user_id, portfolio_id

    """
    def _update_or_create_membership(membership_type: Type, status: Status, id: uuid.UUID = request_user_id) -> Membership:
        # TODO https://app.clickup.com/t/kmxarw remove this line when events are configured correctly
        member = Member.objects.get_or_create(user_id=id)[0]
        to_update = {
            'portfolio_id': portfolio_id,
            'type': membership_type,
            'status': status
        }
        return Membership.objects.update_or_create(
            member=member,
            group=group,
            defaults=to_update
        )[0]

    def _add_first_member():
        status = 'A'
        membership_type = 'O'
        (valid, msg) = validate_bucket()
        if valid and membership_type:
            pending_membership = _update_or_create_membership(
                membership_type=membership_type, status=status)
            # PRODUCE EVENT: Membership status changed. Member joined
            return (True, pending_membership)
        else:
            # PRODUCE EVENT: Membership status change FAILED
            return (False, msg)

    def _add_member_to_no_privacy_group():
        status = 'A'
        membership_type = 'S'
        (valid, msg) = validate_bucket()
        if valid and membership_type:
            pending_membership = _update_or_create_membership(
                membership_type=membership_type, status=status)
            # PRODUCE EVENT: Membership status changed. Member joined
            return (True, pending_membership)
        else:
            # PRODUCE EVENT: Membership status change FAILED
            return (False, msg)

    def _add_membership_request():
        status = 'R'
        membership_type = 'S'
        (valid, msg) = validate_bucket()
        if valid:
            pending_membership = _update_or_create_membership(
                membership_type=membership_type, status=status)
            # PRODUCE EVENT: Membership status changed. Membership requested
            return (True, pending_membership)
        else:
            # PRODUCE EVENT: Membership status change FAILED
            return (False, msg)

    def _add_membership_invitation():
        new_membership = _update_or_create_membership(
            membership_type='S', status='I', id=member_id)
        # PRODUCE EVENT: Membership status changed. Member invited
        return (True, new_membership)

    def _approve_membership_request(pending_membership: Membership):
        pending_membership.status = 'A'
        pending_membership.save()
        # PRODUCE EVENT: Membership status changed. Membership request approved
        return (True, pending_membership)

    def _accept_membership_invitation(pending_membership: Membership):
        (valid, msg) = validate_bucket()
        if valid:
            pending_membership.status = 'A'
            pending_membership.save()
            # PRODUCE EVENT: Membership status changed. Member accepted invite
            return (True, pending_membership)
        else:
            # PRODUCE EVENT: Membership status change FAILED
            return (False, msg)

    try:
        membership_of_request_user = Membership.objects.get(
            group=group, member__user_id=request_user_id)
        if member_id and ((membership_of_request_user.type == 'A' or membership_of_request_user.type == 'O') or group.privacy == 'N'):
            memberships = group.membership_set.filter(
                member__user_id=member_id)
            # admin or owner inviting a new member
            if not memberships.exists():
                return _add_membership_invitation()

            # admin or owner approving a requested member
            elif memberships[0].status == 'R':
                return _approve_membership_request(memberships[0])

        # invited member accepting the membership
        elif not member_id:
            if membership_of_request_user.status == 'I':
                return _accept_membership_invitation(membership_of_request_user)
            elif membership_of_request_user.status == 'D':
                # No privacy settings, anyone can (re)add themselves
                if group.privacy == 'N':
                    return _add_member_to_no_privacy_group()

                # deleted members can re-request membership for private and visible groups
                if group.privacy == 'P' or group.privacy == 'V':
                    return _add_membership_request()

    except Membership.DoesNotExist:
        # group is getting created for the first time with owner
        if not group.members.exists():
            return _add_first_member()

        # No privacy settings, anyone can add themselves
        elif group.privacy == 'N':
            return _add_member_to_no_privacy_group()

        # new members can request membership
        elif group.privacy == 'P' or group.privacy == 'V':
            return _add_membership_request()

        elif group.privacy == 'I':
            return (False, "Membership requests are not allowed for an invite-only group")

    return (False, "You do not have permission to do that.")


def edit_membership(group: Group, request_user_id: uuid.UUID, member_id: uuid.UUID,
                    new_type: Type) -> Tuple[bool, Union[Membership, str]]:
    try:
        membership_of_request_user = Membership.objects.get(
            group=group, member__user_id=request_user_id)
        if membership_of_request_user.type != 'O':
            return (False, 'You do not have permission to change this')
        else:
            membership = Membership.objects.get(
                group=group, member__user_id=member_id)
            if membership.type == 'O' and new_type != 'O' and (Membership.objects.filter(group=group, type='O', status='S').count() <= 1):
                return (False, "There must be one owner of the group at all times")
            membership.type = new_type
            membership.save()
            # PRODUCE EVENT: Membership edited
            return (True, membership)
    except Membership.DoesNotExist:
        return (False, 'There is not a member for that id')


def delete_membership(group: Group, request_user_id: uuid.UUID,
                      member_id: uuid.UUID) -> Tuple[bool, Union[Membership, str]]:
    try:
        membership_of_request_user = Membership.objects.get(
            group=group, member__user_id=request_user_id)
        if request_user_id != member_id and membership_of_request_user.type != 'O':
            return (False, 'You do not have permission to remove users')
        else:
            membership = Membership.objects.get(
                group=group, member__user_id=member_id)
            if membership.type == 'O' and Membership.objects.filter(group=group, type='O', status='S').count() <= 1:
                return (False, "There must be one owner of the group at all times")
            if membership.status == 'A':
                membership.status = 'D'
                membership.type = 'S'
                membership.save()
                return (True, membership)
            elif membership.status == 'I' or membership.status == 'R':
                membership.delete()
                # PRODUCE EVENT: Membership deleted
                return (True, 'instance deleted')
    except Membership.DoesNotExist:
        return (False, 'There is not a member for that id')
