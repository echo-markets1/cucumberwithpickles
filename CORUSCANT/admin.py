from django.contrib import admin
from CORUSCANT.models import Member, Membership, Group

admin.site.register(Member)
admin.site.register(Membership)
admin.site.register(Group)