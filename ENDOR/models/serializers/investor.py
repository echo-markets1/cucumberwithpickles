from rest_framework.serializers import ModelSerializer
from ENDOR.models import Investor
from ENDOR.models.serializers.portfolio import PortfolioSerializer

class InvestorSerializer(ModelSerializer):
    class Meta:
        model = Investor
        fields = '__all__'
    portfolios = PortfolioSerializer(many=True)