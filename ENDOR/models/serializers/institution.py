from rest_framework.serializers import ModelSerializer
from ENDOR.models import Institution

class InstitutionSerializer(ModelSerializer):
    class Meta:
        model = Institution
        fields = '__all__'

    def to_internal_value(self, data):
        """ Map scarif data to the model schema. """
        return {
            "id": data["id"],
            "name": data["name"],
            "website_url": data["website_url"],
            "primary_color": data["primary_color"],
            "logo": data["logo"],
        }

