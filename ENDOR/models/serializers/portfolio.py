from datetime import date, timedelta
from django.db.models import Q
from django.db.models.fields import UUIDField
from rest_framework import serializers
from ENDOR.models import Portfolio, Transaction
from ENDOR.models.serializers.transaction import TransactionSerializer
from ENDOR.models.serializers.investment import InvestmentSerializer
from ENDOR.models.serializers.institution import InstitutionSerializer
from decimal import Decimal


class PortfolioEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Portfolio
        fields = ['name', 'description', 'active']


class PortfolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Portfolio
        # fields = '__all__'
        exclude = ['investor']

    transactions = TransactionSerializer(many=True)
    investments = InvestmentSerializer(many=True)
    institution = InstitutionSerializer()

    transactions_count = serializers.SerializerMethodField()
    trades_count = serializers.SerializerMethodField()
    investments_count = serializers.SerializerMethodField()

    # A proxy for guesstimating the age of the account
    earliest_recorded_transaction_date = serializers.SerializerMethodField()

    # A proxy for how actively a user trades. Calculated over the past year.
    trades_per_week = serializers.SerializerMethodField()

    total_cost_basis = serializers.SerializerMethodField()

    # Cash is determined dynamically by any investments that have a security of type = is_cash
    cash = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()
    balance_current = serializers.SerializerMethodField()

    def get_transactions_count(self, portfolio: Portfolio) -> int:
        return portfolio.transactions.all().count()

    def get_trades_count(self, portfolio: Portfolio) -> int:
        """
        The number of transactions filtered to only include trades (buys and sells)
        transaction.type definition can be found in SCARIF
        """
        return portfolio.transactions.filter(type__in=["buy", "sell"]).count()

    def get_investments_count(self, portfolio: Portfolio) -> int:
        return portfolio.investments.all().count()

    def get_earliest_recorded_transaction_date(self, portfolio: Portfolio) -> date:
        try:
            return portfolio.transactions.earliest("date").date
        except Transaction.DoesNotExist:
            return date.today()

    def get_trades_per_week(self, portfolio: Portfolio) -> float:
        one_year_ago = date.today() - timedelta(days=365)
        transactions_per_year = portfolio.transactions.filter(
            Q(date__gte=one_year_ago) & Q(type__in=["buy", "sell"])).count()
        return transactions_per_year / 52

    def get_total_cost_basis(self, portfolio: Portfolio) -> Decimal:
        total_cost_basis = Decimal(0.0000)
        for investment in portfolio.investments.all():
            total_cost_basis = total_cost_basis + investment.cost_basis
        return total_cost_basis

    def get_cash(self, portfolio: Portfolio) -> Decimal:
        cash_total = Decimal(0.0000)
        # for cash_investment in portfolio.investments.filter(security__is_cash=True):
        # Using is_cash (or is_cash_equilvalent) cannot be trusted:
        # In plaid, currency investments and transactions have two different security objects.
        # One has is_cash_equilvalent=True and the other is False
        # On our platform, we correct this, using only one security object per currency.
        # However, is_cash may be falsely set. Safer to rely on the security.type field
        for cash_investment in portfolio.investments.filter(security__type="cash"):
            cash_total = Decimal(cash_investment.quantity) + cash_total
        return cash_total

    def get_user_id(self, portfolio: Portfolio) -> UUIDField:
        return portfolio.investor.user_id

    def get_balance_current(self, portfolio: Portfolio) -> Decimal:
        return portfolio.calculated_balance_current

    def to_internal_value(self, data):
        """ Map SCARIF data to the model schema. """
        return {
            "id": data['id'],
            "investor": data['investor'],  # ENDOR Investor object instance
            # ENDOR Institution object instance
            "institution": data['institution'],
            "name": data['name'],
            "official_name": data["official_name"],
            "type": data["type"],
            "subtype": data["subtype"],
            "balance_current": data["balance_current"],
            "balance_available": data["balance_available"]
        }

    def create(self, validated_data):
        return Portfolio.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.official_name = validated_data.get(
            "official_name", instance.official_name)
        instance.type = validated_data.get("type", instance.type)
        instance.subtype = validated_data.get("subtype", instance.subtype)
        instance.balance_current = validated_data.get(
            "balance_current", instance.balance_current)
        instance.balance_available = validated_data.get(
            "balance_available", instance.balance_available)
        instance.save()
        return instance
