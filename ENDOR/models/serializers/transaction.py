from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ENDOR.models import Transaction
from ENDOR.models.serializers.security import SecuritySerializer, flatten_security_representation
from ALDERAAN.extensions import data_extensions as alderaan_data_extension
from CORELLIA.extensions import data_extensions as corellia_data_extension


class TransactionSerializer(ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'
    security = SecuritySerializer()
    user_id = serializers.UUIDField(
        source='portfolio.investor.user_id', read_only=True)
    user_info = serializers.SerializerMethodField()
    is_trade = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    likes_count = serializers.SerializerMethodField()
    replies_count = serializers.SerializerMethodField()

    def get_user_info(self, obj: Transaction):
        # Uses data extension to get basic user info (name, handle etc)
        return alderaan_data_extension.get_name_and_handle_for_user_id(user_id=obj.portfolio.investor.user_id)

    def get_is_trade(self, obj: Transaction):
        # True if the transaction was a buy or sell
        trade_types = ['buy', 'sell']
        return obj.type in trade_types

    def get_likes(self, obj: Transaction):
        # Extracts user_ids from Investors in the many-to-many relationship
        return list(map(lambda investor: investor.user_id, obj.likes.all()))

    def get_likes_count(self, obj: Transaction):
        return len(self.get_likes(obj))

    def get_replies_count(self, obj: Transaction):
        # If the serializer is used outside the context of an HTTP request,
        # the user_id and group_id must be provided as context to the Serializer
        request = self.context.get('request', None)
        user_id = None
        if request is None:
            user_id = self.context.get('user_id', None)
        else:
            user_id = request.user.id

        kwargs = self.context.get('kwargs', None)
        query_params = self.context.get('query_params', None)
        group_id = None
        if not kwargs is None:
            group_id = kwargs.get('group_id', None)
        elif not query_params is None:
            group_id = query_params.get('group_id', None)
        else:
            group_id = self.context.get('group_id', None)

        return corellia_data_extension.get_num_transaction_replies(
            obj.id,
            user_id,
            group_id
        )

    def to_representation(self, instance):
        """
        Move fields from security to investment representation.
        A clean way of flattening the response given, so no security object is visible
        """
        representation = super().to_representation(instance)
        return flatten_security_representation(representation)

    def to_internal_value(self, data):
        """ Map SCARIF data to the model schema. """
        return {
            "id": data['id'],
            "portfolio": data["portfolio"],  # ENDOR Portfolio object instance
            "security": data["security"],  # ENDOR Security object instance
            "date": data["date_posted"],
            "quantity": data["quantity"],
            "amount": data["amount"],
            "price": data["price"],
            "fees": data["fees"],
            "type": data["type"],
            "subtype": data["subtype"],
        }

    def create(self, validated_data):
        return Transaction.objects.create(**validated_data)

    def update(self, instance, validated_data):
        # Transactions do no change. Deletion due to cancelation happens on the transformer level
        return instance
