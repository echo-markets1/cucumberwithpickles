from rest_framework.serializers import ModelSerializer
from ENDOR.constants.serialization_constants import EQUITY_SYMBOL, ID, IS_CASH, NAME, REPORTED_CURRENCY, SUBTYPE, TICKER_SYMBOL, TYPE
from ENDOR.models import Security


class SecuritySerializer(ModelSerializer):
    class Meta:
        model = Security
        fields = '__all__'

    def to_internal_value(self, data):
        """ Map SCARIF input data to the model schema. """
        return {
            ID: data[ID],
            TICKER_SYMBOL: data[TICKER_SYMBOL],
            EQUITY_SYMBOL: data[EQUITY_SYMBOL],
            TYPE: data[TYPE],
            SUBTYPE: data[SUBTYPE],
            NAME: data[NAME],
            IS_CASH: data["is_cash_equivalent"],
            REPORTED_CURRENCY: data[REPORTED_CURRENCY]
        }


def flatten_security_representation(representation):
    # To be called in another serializer's (investment's and transaction's) to_representation method
    # Takes a representation of an object, from super().to_representation(obj), and flattens security fields
    # Ensures that security model is hidden from frontend and no security object is visible
    # Returns the representation with the new fields added
    security_representation = representation.pop('security')
    representation['security_id'] = security_representation[ID]
    representation[TICKER_SYMBOL] = security_representation[TICKER_SYMBOL]
    representation[EQUITY_SYMBOL] = security_representation[EQUITY_SYMBOL]
    representation['security_type'] = security_representation[TYPE]
    representation['security_subtype'] = security_representation[SUBTYPE]
    representation['security_name'] = security_representation[NAME]
    representation[IS_CASH] = security_representation[IS_CASH]
    representation['security_reported_currency'] = security_representation[REPORTED_CURRENCY]
    return representation
