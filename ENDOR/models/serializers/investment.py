from decimal import Decimal, DecimalException
from rest_framework import serializers
from ENDOR.models import Investment
from ENDOR.models.serializers.security import SecuritySerializer, flatten_security_representation


class InvestmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Investment
        fields = '__all__'
    security = SecuritySerializer()

    average_cost = serializers.SerializerMethodField()

    def get_average_cost(self, investment):
        try:
            return Decimal(investment.cost_basis) / Decimal(investment.quantity)
        except DecimalException:
            return Decimal(0)

    def to_representation(self, instance):
        """
        Move fields from security to investment representation.
        A clean way of flattening the response given, so no security object is visible
        """
        representation = super().to_representation(instance)
        return flatten_security_representation(representation)

    def to_internal_value(self, data):
        """ Map SCARIF data to the model schema. """
        return {
            "id": data["id"],
            "portfolio": data["portfolio"],  # ENDOR Portfolio object instance
            "security": data["security"],  # ENDOR Security object instance
            "quantity": data["quantity"],
            "institution_price": data["institution_price"],
            "institution_price_as_of": data["institution_price_as_of"],
            "institution_value": data["institution_value"],
            "cost_basis": data["cost_basis"]
        }

    def create(self, validated_data):
        return Investment.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.quantity = validated_data.get("quantity", instance.quantity)
        instance.institution_price = validated_data.get(
            "institution_price", instance.institution_price)
        instance.institution_price_as_of = validated_data.get(
            "institution_price_as_of", instance.institution_price_as_of)
        instance.institution_value = validated_data.get(
            "institution_value", instance.institution_value)
        instance.cost_basis = validated_data.get(
            "cost_basis", instance.cost_basis)
        instance.save()
        return instance
