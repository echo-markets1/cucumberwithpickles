import copy
from typing import Dict
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ENDOR.constants.regexs import OPTION_PARSE_REGEX
from ENDOR.constants.serialization_constants import CURRENCY, CURRENCY_PREFIX, ID, REPORTED_CURRENCY, TICKER_SYMBOL, EQUITY_SYMBOL, TYPE, SUBTYPE, UNOFFICIAL_CURRENCY_CODE
from HOTH.extensions import logic_extensions


class Currency(models.Model):
    """
    The code of the currency portfolio and security values are reported in.
    Use blank when no currency provided.
    """
    code = models.CharField(blank=True, max_length=8, primary_key=True)


class SecurityManager(models.Manager):
    def transform(self, data: Dict):
        """
        Transforms SCARIF Security data into ENDOR Security model, creating and updating as appropriate.
        This allows us to add/remove fields in ENDOR, tailoring our representation of their portfolio.
        Takes the raw Security data from SCARIF

        Parameters:
            - data (Dict): Serialized Security data from SCARIF
        """
        ### INNER FUNCTIONS ###
        def _handle_options():
            """
            Determines the equity symbol and subtype based on the ticker symbol
            for a derivative.
            """
            option_match = OPTION_PARSE_REGEX.match(
                data_to_transform[TICKER_SYMBOL])
            if option_match:
                data_to_transform[EQUITY_SYMBOL] = option_match.group(
                    EQUITY_SYMBOL)
                data_to_transform[SUBTYPE] = Security.Subtype.CALL if option_match.group(
                    TYPE).lower() == 'c' else Security.Subtype.PUT

        def _handle_currencies(currency: Currency):
            """
            Determines the ticker symbol, equity symbol and subtype based on the
            ticker_symbol for a cash holding.
            """
            ### INNER FUNCTIONS ###
            def _is_crypto(symbol) -> bool:
                return symbol in logic_extensions.get_supported_cryptos_list()

            ### MAIN FUNCTION CODE ###
            _ticker_symbol = data_to_transform[TICKER_SYMBOL].replace(
                CURRENCY_PREFIX, "")
            data_to_transform[EQUITY_SYMBOL] = _ticker_symbol
            if _is_crypto(_ticker_symbol):
                # Update the ticker_symbol to one that is searchable wth twelvedata
                # Updating the ticker_symbol vs the equity symbol simplies
                # downstream logic such that all consumers can initiate searches
                # against the ticker_symbol, rather than implementing addtional
                # logic to determine what field to search against
                # Example: "BTC/USD", "ETH/USD"
                data_to_transform[TICKER_SYMBOL] = f"{_ticker_symbol}/{currency.code}"
                data_to_transform[SUBTYPE] = Security.Subtype.CRYPTO
            else:
                data_to_transform[SUBTYPE] = Security.Subtype.NONE

        def _handle_equities():
            """
            Determines the equity symbol and subtype based on the ticker symbol
            for an equity.
            """
            data_to_transform[EQUITY_SYMBOL] = data_to_transform[TICKER_SYMBOL]
            data_to_transform[SUBTYPE] = Security.Subtype.NONE

        def _handle_default():
            """
            Determines the equity symbol and subtype based on the ticker symbol
            for a security with no ticker_symbol.
            """
            data_to_transform[EQUITY_SYMBOL] = ''
            data_to_transform[SUBTYPE] = Security.Subtype.NONE

        def _get_currency_code() -> str:
            """
            Get the currency code from the security data. Only one of currency
            or unofficial_currency_code can be non-null, but neither is gauranteed
            to be present.
            """
            # Default to blank to trigger a check of unofficial currency code in case of failure
            currency_code = data_to_transform.get(CURRENCY, '')
            if not currency_code.strip():
                # Default to USD if no code provided
                currency_code = data_to_transform.get(
                    UNOFFICIAL_CURRENCY_CODE, 'USD')
            return currency_code

        def _set_reported_currency():
            """ Set the reported_currency field for deserialization """
            (currency, _) = Currency.objects.get_or_create(
                code=_get_currency_code())
            data_to_transform[REPORTED_CURRENCY] = currency

        def _create_or_update(instance=None):
            """
            Deserialize the Security data, updating the existing security if
            present and creating frm scratch if not.
            """
            from ENDOR.models.serializers import SecuritySerializer
            serializer = SecuritySerializer(instance, data=data_to_transform)
            serializer.is_valid()
            security = serializer.save()
            return security

        ### MAIN FUNCTION CODE ###
        data_to_transform = copy.deepcopy(data)
        _set_reported_currency()
        ticker_symbol = data_to_transform[TICKER_SYMBOL]
        if ticker_symbol.strip():
            if data_to_transform[TYPE] == Security.Type.CASH:
                _handle_currencies(
                    currency=data_to_transform[REPORTED_CURRENCY])
            elif data_to_transform[TYPE] == Security.Type.DERIVATIVE:
                _handle_options()
            else:
                _handle_equities()
        else:
            _handle_default()

        try:
            security = Security.objects.get(id=data_to_transform[ID])
            return _create_or_update(instance=security)
        except Security.DoesNotExist:
            return _create_or_update()


class Security(models.Model):
    """
    A security is the option, derivative, etc from which an investment gets its value, or changes hands with a transaction.
    """
    objects = SecurityManager()

    # Equivalent to the is passed in from SCARIF. Unfortunately, since ticker_symbol
    # can be null, we must rely on a different unique value as the pk
    id = models.CharField(primary_key=True, max_length=40)
    # This is the symbol of the security
    ticker_symbol = models.CharField(max_length=32, blank=True)
    # Symbol of the underlying equity. Will be the same as ticker_symbol if it is an equity
    # For derivatives will be the ticker_symbol of the underlying security
    # For cash investments, this will the code/symbol representing the currency
    equity_symbol = models.CharField(max_length=8, blank=True)
    # Name of equity from SCARIF and plaid
    name = models.CharField(max_length=100, null=True)

    # These options mirror those in SCARIF
    class Type(models.TextChoices):
        CASH = 'cash', _(
            "Cash, currency, and money market funds")
        DERIVATIVE = 'derivative', _(
            "Options, warrants, and other derivative instruments")
        EQUITY = 'equity', _("Domestic and foreign equities")
        ETF = 'etf', _("Multi-asset exchange-traded investment funds")
        FIXED_INCOME = 'fixed income', _(
            "Bonds and certificates of deposit (CDs)")
        LOAN = 'loan', _("Loans and loan receivables")
        MUTUAL_FUND = 'mutual fund', _(
            "Open- and closed-end vehicles pooling funds of multiple investors")
        OTHER = 'other', _("Unknown or other investment types")
    type = models.CharField(
        max_length=12,
        choices=Type.choices
    )

    # Subtype is a further classification, seperate from SCARIF. Can help to distinguish cryptos and options
    class Subtype(models.TextChoices):
        CALL = 'call', _("Call option")
        PUT = 'put', _("Put option")
        CRYPTO = 'crypto', _("Cryptocurrency")
        NONE = 'none', _("No subtype distinct from the type")

    subtype = models.CharField(
        max_length=6,
        choices=Subtype.choices,
        default=Subtype.NONE
    )

    # Used to easily distinguish amount of dollars the user has in their account
    # This can also indicate a highly liquid security, since it is derived from SCARIF.Security.is_cash_equivalent
    is_cash = models.BooleanField(default=False)

    # The currency this security is reported in
    reported_currency = models.ForeignKey(Currency, on_delete=models.PROTECT)
