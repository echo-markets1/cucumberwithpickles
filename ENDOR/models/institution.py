from django.db import models


class InstitutionManager(models.Manager):
    def transform(self, **kwargs):
        data = kwargs.get('data')

        (institution, created) = Institution.objects.get_or_create(
            id=data['id'])

        if created:
            from ENDOR.models.serializers import InstitutionSerializer
            serializer = InstitutionSerializer(institution, data=data)
            serializer.is_valid()
            institution = serializer.save()

        return institution


class Institution(models.Model):
    """
    Represents a single institution which reports data on securities. Ex: Robinhood
    """
    objects = InstitutionManager()

    # Unique identifier for the institution, provided by plaid
    id = models.CharField(max_length=40, primary_key=True)

    # The official name of the institution
    name = models.CharField(max_length=128, null=True,
                            blank=True, default=None)

    # The URL for the institution's website
    primary_color = models.CharField(
        max_length=9, null=True, blank=True, default=None)

    # Hexadecimal representation of the primary color used by the institution
    website_url = models.URLField(null=True, blank=True, default=None)

    # Base64 encoded representation of the institution's logo
    logo = models.TextField(null=True, blank=True, default=None)
