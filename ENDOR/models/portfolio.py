from decimal import Decimal
from typing import Dict
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from ENDOR.models import Investor, Institution


class PortfolioManager(models.Manager):
    def transform(self, data: Dict, investor: Investor, institution: Institution):
        """
        Transforms SCARIF Account data into ENDOR's Portfolio model, creating and updating as appropriate.
        This allows us to add/remove fields in ENDOR, tailoring our representation of their portfolio.
        Takes the raw Account data from SCARIF, as well as the ENDOR models of the investor and institution

        Parameters:
            - data (Dict): Serialized account data from SCARIF to transform
            - investor (Investor): The Investor (User) that owns the portfolio
            - institution (Institution): The investment Institution of the portfolio
        """
        data['investor'] = investor
        data['institution'] = institution

        def _create_or_update(data, instance=None):
            from ENDOR.models.serializers import PortfolioSerializer
            serializer = PortfolioSerializer(instance, data=data)
            serializer.is_valid()
            portfolio = serializer.save()
            return portfolio

        try:
            portfolio = Portfolio.objects.get(
                id=data['id'],
                investor=investor,
                institution=institution
            )
            return _create_or_update(instance=portfolio, data=data)

        except ObjectDoesNotExist:
            return _create_or_update(data=data)


class Portfolio(models.Model):
    """
    Portfolios are a collection of investments belonging to a specific account with a certain institution.
    For example, a user may have 2 different portfolio for 2 different accounts at an instituion, 
    eg a brokerage account and retirement savings account.
    Or a user may have multiple portfolios belonging to different accounts at different institutions,
    eg seperate brokerage accounts at Robinhood and Charles Schwab
    """
    objects = PortfolioManager()
    # id corresponds to account_id on SCARIF.models.Account
    id = models.CharField(primary_key=True, max_length=40)

    investor = models.ForeignKey(Investor, on_delete=models.PROTECT)

    institution = models.ForeignKey(
        Institution, on_delete=models.PROTECT, null=True)

    # The amount of funds available to be withdrawn from the account, as determined by the financial institution.
    # While one might think this should correspond to the cash in the account, discovered this might not be the case.
    # For example, with Robinhood, it seems to be the total value of the portfolio, when last pulled by scarif
    # Cash (or cash equivalent) is stored as investment objects. These are aggregated in field in serializer before serving to client
    balance_available = models.DecimalField(
        max_digits=19, decimal_places=5, null=True)

    # The total value of assets as presented by the institution.
    balance_current = models.DecimalField(
        max_digits=19, decimal_places=5, null=True)

    @property
    def calculated_balance_current(self) -> Decimal:
        """
        Given the possibility of balance_current being null, use this value
        in downstream calculations of balance
        """
        balance_current = self.balance_current
        # If nonnull, return as is
        if balance_current is None:
            # If there are investments in the account, derive the balance from their values
            if self.investments.all().exists():
                sum_of_investments = self.investments.all().aggregate(
                    models.Sum('institution_value')
                )
                balance_current = sum_of_investments["institution_value__sum"]
            # Otherwise, assume equal to balance_available, given that both can not be null
            else:
                balance_current = self.balance_available
        return balance_current

    # Type and subtype set by plaid. See https://plaid.com/docs/api/products/#investments-holdings-get-response-type for more info
    type = models.CharField(
        max_length=10
    )
    subtype = models.CharField(
        max_length=36
    )

    # The name of the account, either assigned by the user or by the financial institution itself. Allow users to change this on Endor
    name = models.CharField(max_length=128)

    # The official name of the account as given by the financial institution. Maybe null
    official_name = models.CharField(max_length=128, null=True)

    # Set by the user, local to Endor
    description = models.TextField(null=True)

    # Whether or not this portfolio and it's performance are shared publicly
    active = models.BooleanField(default=True)
