from typing import Dict
from django.db import models
from ENDOR.constants.serialization_constants import CANCEL_TRANSACTION_ID
from ENDOR.models import Portfolio, Security, Investor


class TransactionManager(models.Manager):
    def transform(self, data: Dict, portfolio: Portfolio, security: Security):
        """
        Transforms SCARIF InvestmentTransactions data into ENDOR's Transaction model, creating and updating as appropriate.
        This allows us to add/remove fields in ENDOR, tailoring our representation of their portfolio.
        Takes the raw InvestmentTransaction data from SCARIF, as well as the ENDOR models of the portfolio and security

        Parameters:
            - data (Dict): Serialized InvestmentTransaction from SCARIF
            - portfolio (Portfolio): The Portfolio the transaction occured in
            - security (Security): The Security that was traded
        """
        data['portfolio'] = portfolio
        data['security'] = security

        def _create_or_update(data, instance=None):
            from ENDOR.models.serializers import TransactionSerializer
            serializer = TransactionSerializer(instance, data=data)
            serializer.is_valid()
            transaction = serializer.save()
            return transaction

        transaction = None
        try:
            transaction = Transaction.objects.get(
                id=data['id'],
                portfolio=portfolio,
                security=security
            )
            transaction = _create_or_update(data, instance=transaction)
        except Transaction.DoesNotExist:
            transaction = _create_or_update(data=data)

        # Remove canceled transactions and the transaction they canceled from ENDOR
        if not data[CANCEL_TRANSACTION_ID] is None:
            canceled_transaction = Transaction.objects.get(
                id=data[CANCEL_TRANSACTION_ID])
            canceled_transaction.delete()
            transaction.delete()

        return transaction


class Transaction(models.Model):
    """
    Represents an investment of a security changing hands
    """
    objects = TransactionManager()
    id = models.CharField(max_length=40, primary_key=True)

    portfolio = models.ForeignKey(
        Portfolio, on_delete=models.PROTECT, related_name="transactions")
    security = models.ForeignKey(Security, on_delete=models.PROTECT)

    date = models.DateField(
        auto_now=False, auto_created=False, auto_now_add=False)
    quantity = models.DecimalField(max_digits=19, decimal_places=5)
    amount = models.DecimalField(max_digits=12, decimal_places=5)
    price = models.DecimalField(max_digits=12, decimal_places=5)
    fees = models.DecimalField(max_digits=12, decimal_places=5)
    type = models.CharField(
        max_length=8
    )
    subtype = models.CharField(
        max_length=36
    )
    likes = models.ManyToManyField(Investor, related_name="liked_transactions")

    class Meta:
        ordering = ["-date"]
