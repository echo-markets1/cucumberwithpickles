from decimal import Decimal
from typing import Dict
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from ENDOR.core.database_management import cost_basis_updater
from ENDOR.models import Portfolio, Security


class InvestmentManager(models.Manager):
    def transform(self, data: Dict, portfolio: Portfolio, security: Security):
        """
        Transforms SCARIF Holdings data into ENDOR Investment model, creating and
        updating as appropriate. This allows us to add/remove fields in ENDOR,
        tailoring our representation of their portfolio. Takes the raw Investment
        data from SCARIF, as well as the ENDOR models of the portfolio and security

        Parameters:
            - data (Dict): Serialized holdings data from SCARIF to transform
            - portfolio (Portfolio): The related Portfolio instance
            - security (Security): The related Security instance
        """
        data['security'] = security
        data['portfolio'] = portfolio

        def _create_or_update(data, instance=None):
            from ENDOR.models.serializers import InvestmentSerializer
            serializer = InvestmentSerializer(instance, data=data)
            serializer.is_valid()
            investment = serializer.save()
            return investment

        def _set_cost_basis(investment):
            """ Update the cost_basis of the investment based on the transaction history """
            new_cost_basis = cost_basis_updater.update(investment)
            provided_cost_basis = data['cost_basis']
            if provided_cost_basis and round(new_cost_basis, 2) != round(Decimal(provided_cost_basis), 2):
                print(f'WARN: Plaid provided cost_basis does not match ENDOR-computed cost_basis with ' +
                      f'investment id: {investment.id}, ' +
                      f'Computed: {new_cost_basis} != Provided: {provided_cost_basis}'
                      )
        investment = None
        try:
            investment = Investment.objects.get(
                portfolio=portfolio,
                security=security
            )
            investment = _create_or_update(data, instance=investment)
        except ObjectDoesNotExist:
            investment = _create_or_update(data=data)

        _set_cost_basis(investment)
        return investment


class Investment(models.Model):
    """
    An investment contains the information specific to the user's position or
    holding of a security, in a specific portfolio.
    """
    objects = InvestmentManager()

    portfolio = models.ForeignKey(
        Portfolio, on_delete=models.PROTECT, related_name="investments")
    security = models.ForeignKey(Security, on_delete=models.PROTECT)

    class Meta:
        unique_together = ('portfolio', 'security')

    quantity = models.DecimalField(max_digits=19, decimal_places=5)

    institution_price = models.DecimalField(max_digits=12, decimal_places=5)
    institution_price_as_of = models.CharField(max_length=32, null=True)
    institution_value = models.DecimalField(max_digits=19, decimal_places=5)
    # This is internally calculated, since it can be
    # Used to then derive average_cost, as a calculated field on the serializer
    # 2 decimal places because only 2 digits of decimal are ever needed to provide to user
    cost_basis = models.DecimalField(max_digits=12, decimal_places=2)
