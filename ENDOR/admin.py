from django.contrib import admin

from ENDOR.models import Investment, Security, Transaction, Portfolio, Institution, Investor

# Register your models here.

admin.site.register(Investment)
admin.site.register(Security)
admin.site.register(Transaction)
admin.site.register(Portfolio)
admin.site.register(Institution)
admin.site.register(Investor)