from typing import Union
import uuid
from CORUSCANT.extensions.data_extensions import get_all_group_ids_for_portfolio

from ENDOR.models import Transaction


def is_transaction_visible_to_group(transaction_id: str, group_id: Union[str, uuid.UUID]) -> bool:
    try:
        transaction = Transaction.objects.get(id=transaction_id)
        portfolio_group_ids = get_all_group_ids_for_portfolio(
            transaction.portfolio.id)
        _group_id = group_id
        if type(group_id) is str:
            _group_id = uuid.UUID(group_id)
        return _group_id in portfolio_group_ids
    except Transaction.DoesNotExist:
        return False
