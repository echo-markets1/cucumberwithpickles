from typing import Dict, List
import uuid
from ENDOR.models import Portfolio
from ENDOR.models.serializers import PortfolioSerializer
from CORUSCANT.extensions import data_extensions as coruscant_data_connection


def get_portfolios(user_id: uuid.UUID) -> List[Dict]:
    portfolios = Portfolio.objects.filter(investor__user_id=user_id)
    return PortfolioSerializer(portfolios, many=True).data


def get_portfolio(portfolio_id: str) -> Dict:
    portfolio = Portfolio.objects.get(id=portfolio_id)
    return PortfolioSerializer(portfolio).data


def get_portfolios_for_group(group_id: uuid.UUID) -> List[Dict]:
    group = coruscant_data_connection.get_group(group_id)
    portfolios = Portfolio.objects.none()
    for member in group['members']:
        portfolio_id = member['portfolio_id']
        portfolio = Portfolio.objects.filter(id=portfolio_id)
        portfolios.union(portfolio)
    return PortfolioSerializer(portfolios, many=True).data
