from django.urls import path
from ENDOR.api.views import portfolio, transaction

urlpatterns = [
    path('', portfolio.PortfolioList.as_view(), name='portfolio-list'),
    path('transactions/', transaction.TransactionList.as_view(),
         name='transaction-list'),
    path('transactions/like/<str:id>/',
         transaction.LikeTransaction.as_view(), name='like-transaction'),
    path('<str:pk>/', portfolio.PortfolioDetail.as_view(), name='portfolio-detail'),
]
