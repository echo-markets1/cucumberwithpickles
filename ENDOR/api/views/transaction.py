from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from CORUSCANT.extensions import data_extensions as CORUSCANT_data_ext
from ENDOR.core.transactions import transaction_reactions
from ENDOR.models import serializers, Transaction


class TransactionResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class TransactionList(generics.ListAPIView):
    '''
    API endpoint for Listing Transactions.

    **GET**
        query_params:
            - portfolio_id (str): Return the list of transactions for a specific portfolio
            - group_id (uuid.UUID): Return the list of transactions visible to a specific
                group

        Returns:
            - A serialized representation of all the Transactions from the query
    '''
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.TransactionSerializer
    pagination_class = TransactionResultsSetPagination

    def get_queryset(self):
        portfolio_id = self.request.query_params.get('portfolio_id', None)
        if not portfolio_id is None:
            return Transaction.objects.filter(portfolio__id=portfolio_id)
        group_id = self.request.query_params.get('group_id', None)
        group_porfolio_ids = []
        if not group_id is None:
            group_porfolio_ids = CORUSCANT_data_ext.get_all_portfolio_ids_for_group(
                group_id, self.request.user.id)
        else:
            user_groups_ids = CORUSCANT_data_ext.get_all_visible_group_ids_for(
                self.request.user.id)
            for group_id in user_groups_ids:
                group_porfolio_ids = list(set(
                    group_porfolio_ids + CORUSCANT_data_ext.get_all_portfolio_ids_for_group(group_id, self.request.user.id)))
        return Transaction.objects.filter(portfolio__id__in=group_porfolio_ids)


class TransactionDetail(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.TransactionSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Transaction.objects.all()


class LikeTransaction(TransactionDetail):
    '''
    API endpoint for liking a Transaction. Prefer a GET to a PATCH because no
    additional data is required to fulfill this request.

    **GET**
        kwargs: 
            - id (str): The id of the Transaction being liked

        Returns:
            - A serialized representation of the liked Transaction
    '''

    def get(self, request, *args, **kwargs):
        (success, message) = transaction_reactions.like(
            kwargs['id'], request.user.id)
        if not success:
            return Response({'detail': message}, status=status.HTTP_400_BAD_REQUEST)
        return self.retrieve(request, *args, **kwargs)
