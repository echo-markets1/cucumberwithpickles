from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from ENDOR.models import serializers
from ENDOR.models import Portfolio
from ENDOR.core.database_management.portfolios_updates_coordinator import update_portfolios
from CORUSCANT.extensions import data_extensions as coruscant_data_connection


class PortfolioList(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.PortfolioSerializer

    def get_queryset(self):
        queryset = Portfolio.objects.none()

        group_id = self.request.query_params.get('group-id')
        # Can add otherways of filtering portfolios by adding other possible query params here.
        # Make sure to enforce privacy & security on reading portfolios
        user_id = self.request.query_params.get('user-id')
        if group_id is not None:
            group = coruscant_data_connection.get_group(
                group_id, self.request.user.id)
            if group:
                for member in group['members']:
                    portfolio_id = member['portfolio_id']
                    portfolio = Portfolio.objects.filter(id=portfolio_id)
                    queryset = queryset | portfolio

        elif user_id is not None:
            show_active = user_id == self.request.user.id
            queryset = Portfolio.objects.filter(
                investor__user_id=user_id, active=show_active)
        else:
            queryset = Portfolio.objects.filter(
                investor__user_id=self.request.user.id)

        return queryset

    def get(self, request, *args, **kwargs):
        # Triggers an update of the portfolio's holdings and transactions info
        update_portfolios(request.user.id)
        return self.list(request, *args, **kwargs)


class PortfolioDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.PortfolioSerializer

    def get_queryset(self):
        queryset = Portfolio.objects.all()
        if self.request.method in ["PUT", "PATCH"]:
            queryset = queryset.filter(investor__user_id=self.request.user.id)
        return queryset

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method in ["PUT", "PATCH"]:
            return serializers.PortfolioEditSerializer
        else:
            return serializers.PortfolioSerializer
