from rest_framework.permissions import BasePermission

from ENDOR.core.permissions import has_portfolio_read_access


class IsOwnerGroupMateOrActivePortfolioToView(BasePermission):
    """ Determines what level of read access is granted to a portfolio """

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return has_portfolio_read_access(portfolio_id=obj.id, user_id=request.user.id)
        return False
