import re

OPTION_PARSE_REGEX = re.compile(r'''(
    (?P<equity_symbol>\w{1,6}) # beginning ticker, 1 to 6 word characters
    (?P<separator>\s+)?        # optional separator
    (?P<date>\d{6})            # 6 digits for yymmdd
    (?P<type>[cp])             # C or P for call or put
    (?P<strike>\d{8})          # 8 digits for strike price
)''', re.VERBOSE | re.IGNORECASE)
