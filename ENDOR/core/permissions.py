import uuid

from CORUSCANT.extensions import data_extensions as CORUSCANT_DATA_EXT
from ENDOR.models import Portfolio


def has_portfolio_read_access(portfolio_id: str, user_id: uuid.UUID) -> bool:
    """
    Determines whether or not a portfolio should be visible to a user based
    on the Portfolio's active status.

    Params:
        - portfolio_id (str): The id of the portfolio to check permissions for
        - user_id (uuid.UUID): The id of the user to check permissions against

    Returns:
        - (bool): Whether or not the user has permission to view the portfolio
    """
    portfolio = None
    try:
        portfolio = Portfolio.objects.get(id=portfolio_id)
    except:
        return False
    if portfolio.active:
        return True

    def _is_portfolio_visible_through_group_membership() -> bool:
        user_group_ids = CORUSCANT_DATA_EXT.get_all_visible_group_ids_for(
            user_id)
        portfolio_group_ids = CORUSCANT_DATA_EXT.get_all_group_ids_for_portfolio(
            portfolio_id)
        for group_id in user_group_ids:
            if group_id in portfolio_group_ids:
                return True
        return False

    return _is_portfolio_visible_through_group_membership()
