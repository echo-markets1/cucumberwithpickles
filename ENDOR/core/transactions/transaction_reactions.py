from typing import Tuple
import uuid
from ENDOR.models import Transaction, Investor


def like(transaction_id: str, reacting_user_id: uuid.UUID) -> Tuple[bool, str]:
    """ Toggle a user's like reaction of a transaction """
    transaction = None
    try:
        transaction = Transaction.objects.get(id=transaction_id)
    except Transaction.DoesNotExist:
        return (False, f'Transaction with id {transaction_id} was not found')
    (user, _) = Investor.objects.get_or_create(user_id=reacting_user_id)
    if transaction.likes.filter(user_id=user.user_id).exists():
        transaction.likes.remove(user)
    else:
        transaction.likes.add(user)
    transaction.save()
    return (True, '')
