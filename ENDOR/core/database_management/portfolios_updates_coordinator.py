from SCARIF.extensions import data_extensions as scarif_data_connection
from ENDOR.core.database_management import scarif_data_handler

def update_portfolios(user_id):
    # If user_id is provided, only that user's portfolios get updated.
    # Otherwise all in DB get handled
    data = scarif_data_connection.get_items(user_id=user_id)
    scarif_data_handler.handle(data)
    