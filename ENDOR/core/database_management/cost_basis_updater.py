
from decimal import Decimal


def update(investment):
    """
    Cost basis is the price paid for the current position of the asset
    Average cost is computed from it (cost_basis/quantity) - average purchase price (ie buys only) of a portfolios position of stock. 
    """
    buy_transactions = investment.portfolio.transactions.filter(
        security=investment.security,
        type="buy"
    ).order_by('-date')
    position = Decimal(investment.quantity)
    counter = 0
    cost_basis = Decimal(0.0)
    while (position > 0) and (counter < len(buy_transactions)):
        cost_basis = cost_basis + \
            Decimal(buy_transactions[counter].amount) + \
            Decimal(buy_transactions[counter].fees)
        quantity = Decimal(buy_transactions[counter].quantity)
        position = position - quantity
        counter = counter + 1
    # The cost_basis should never be 0. Even if the user was gifted the investment
    # (Ex: New account promotions), the cost_basis still must be non-zero to
    # accurately calculate the gains/losses on the position over time.
    #
    # There are several quirky reasons the prior algorithm could output a cost_basis
    # of 0. All of them have to do with the data quality from Plaid and underlying
    # institutions. As a fallback, assume all shares were purchased at the current price
    if cost_basis == Decimal(0.0):
        cost_basis = Decimal(investment.quantity) * \
            Decimal(investment.institution_price)
    investment.cost_basis = cost_basis
    investment.save()
    return cost_basis
