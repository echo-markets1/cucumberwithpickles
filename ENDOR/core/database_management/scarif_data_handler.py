from typing import Dict
from uuid import UUID

from ENDOR.models import Investor, Portfolio, Security, Investment, Transaction, Institution


def handle(data: Dict) -> None:

    def _get_up_to_date_security(security_data: Dict) -> Security:
        """
        Optimize by only transforming security data for securities that have
        not already been created or updated.
        """
        security_id = security_data['id']
        security_instance = updated_securities_id_instance_map.get(
            security_id, None)
        if security_instance is None:
            security_instance = Security.objects.transform(data=security_data)
            updated_securities_id_instance_map[security_id] = security_instance
        return security_instance

    def _get_investor_from(user_id_str: str) -> Investor:
        """ Optimize by minimizing calls to the db for the same investor objects """
        user_id_uuid = UUID(user_id_str, version=4)
        investor_instance = investor_id_instance_map.get(user_id_uuid, None)
        if investor_instance is None:
            (investor_instance, _) = Investor.objects.get_or_create(
                user_id=user_id_uuid
            )
            investor_id_instance_map[user_id_uuid] = investor_instance
        return investor_instance

    def _get_up_to_date_institution(institution_data: Dict) -> Institution:
        """
        Optimize by only transforming institution data for institutions that have
        not already been created or updated.
        """
        institution_id = institution_data['id']
        institution_instance = updated_institutions_id_instance_map.get(
            institution_id, None)
        if institution_instance is None:
            institution_instance = Institution.objects.transform(
                data=institution_data,
            )
            updated_institutions_id_instance_map[institution_id] = institution_instance
        return institution_instance

    def _create_or_update_portfolio(portfolio_data: Dict, investor: Investor, institution: Institution) -> Portfolio:
        return Portfolio.objects.transform(
            data=portfolio_data,
            investor=investor,
            institution=institution
        )

    def _create_or_update_investment(investment_data: Dict, portfolio: Portfolio) -> Investment:
        security = _get_up_to_date_security(investment_data['security'])
        return Investment.objects.transform(
            data=investment_data,
            portfolio=portfolio,
            security=security
        )

    def _create_or_update_transaction(transaction_data: Dict, portfolio: Portfolio) -> Transaction:
        security = _get_up_to_date_security(transaction_data['security'])
        return Transaction.objects.transform(
            data=transaction_data,
            portfolio=portfolio,
            security=security
        )

    # Optimize by minimizing database operations
    updated_securities_id_instance_map = {
        # security.id : Security
    }
    updated_institutions_id_instance_map = {
        # institution.id : Institution
    }
    investor_id_instance_map = {
        # investor.user_id: Investor
    }
    for user_id, items in data.items():
        ### MAIN FUNCTION CODE ###
        investor = _get_investor_from(user_id)
        for item in items:
            # Update or create the institution
            institution = _get_up_to_date_institution(item['institution'])

            for account in item['accounts']:
                # Update or create the portfolio (modeled by an Account object in SCARIF)
                portfolio = _create_or_update_portfolio(
                    account, investor, institution)

                # Update or create the transactions
                for transaction_data in account['investment_transactions']:
                    _create_or_update_transaction(transaction_data, portfolio)

                # Update or create the investments in the account
                updated_investments_id_list = []
                for investment_data in account['holdings']:
                    investment = _create_or_update_investment(
                        investment_data, portfolio)
                    updated_investments_id_list.append(
                        investment.security.id)
                # Remove investment objects that the user does not have any more
                portfolio.investments.exclude(
                    security__id__in=updated_investments_id_list).delete()
