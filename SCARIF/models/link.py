from django.db import models


class Link(models.Model):
    """ Used to link an Echo user to their plaid credentials """
    user_id = models.UUIDField(primary_key=True)
