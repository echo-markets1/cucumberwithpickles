from django.db import models
from django.utils.translation import ugettext_lazy as _
from SCARIF.models import UnofficialCurrencyCode, Currency, Institution


class Security(models.Model):
    """
    Represents a single Security entity, modeled after:
    https://plaid.com/docs/api/products/#investments-holdings-get-response-securities
    Decoupled from any specific user and their holdings of the Security
    """
    # A unique, Plaid-specific identifier for the Security, used in their response
    # to associate Securities with Holdings and InvestmentTransactions
    id = models.CharField(primary_key=True, max_length=40)

    # 12-character globally unique securities identifier.
    isin = models.CharField(null=True, blank=True,
                            unique=True, max_length=12)
    # 9-character identifier assigned to North American securities
    # TODO make cusip unique=True when security_id issue is resolved with Plaid
    cusip = models.CharField(null=True, blank=True,
                             max_length=9)
    # 7-character identifier assigned to securities in the UK.
    sedol = models.CharField(null=True, blank=True,
                             unique=True, max_length=7)

    # In certain cases, Plaid will provide the ID of another security whose performance
    # resembles this security, typically when the original security has low volume, or
    # when a private security can be modeled with a publicly traded security.
    #
    # Prefer to storing as OneToOne field with the proxy Security incase the
    # "original" Security is created before the proxy.
    proxy_security_id = models.CharField(
        blank=True, max_length=40, unique=True, null=True)

    # All the Institutions which have a unique id for the given security
    institutions = models.ManyToManyField(
        Institution, through="InstitutionalSecurityId", blank=True)

    # A descriptive name for the security, suitable for display.
    name = models.CharField(max_length=100, null=True)

    # The same stock/ticker symbol may have a different security_id based on
    # other fields.
    #
    # Ex: BLDP ("Ballard Power Systems")
    #
    # {
    #   'close_price': 28.21, 'close_price_as_of': '2021-03-02', 'cusip': '058586108',
    #   'institution_id': None, 'institution_security_id': None, 'is_cash_equivalent': False,
    #   'isin': None, 'iso_currency_code': 'USD', 'name': 'Ballard Power Systems Inc.',
    #   'proxy_security_id': 'moPE4dE1yMHJXKypJ6pKhvbZ1LZ7NVHdE9q90',
    #   'security_id': '54jm3zmZ7kFLOrmvgR1pC5b4KA4V8atLOm39V', 'sedol': None,
    #   'ticker_symbol': 'BLDP', 'type': 'equity', 'unofficial_currency_code': None
    # }
    #
    # {
    #   'close_price': 35.59, 'close_price_as_of': '2021-03-02', 'cusip': None,
    #   'institution_id': None, 'institution_security_id': None, 'is_cash_equivalent': False,
    #   'isin': None, 'iso_currency_code': 'CAD', 'name': 'Ballard Power Systems Inc.',
    #   'proxy_security_id': None, 'security_id': 'moPE4dE1yMHJXKypJ6pKhvbZ1LZ7NVHdE9q90', 'sedol': None,
    #   'ticker_symbol': 'BLDP', 'type': 'equity', 'unofficial_currency_code': None
    # }
    # These are listed as different securities (i.e. different security_ids)
    # because one is traded in 'CAD', while the other is traded in 'USD'
    ticker_symbol = models.CharField(
        max_length=24, editable=False, unique=False, blank=True)

    # Indicates that a security is a highly liquid asset and can be treated like cash.
    is_cash_equivalent = models.BooleanField()

    class Type(models.TextChoices):
        CASH = 'cash', _(
            "Cash, currency, and money market funds")
        DERIVATIVE = 'derivative', _(
            "Options, warrants, and other derivative instruments")
        EQUITY = 'equity', _("Domestic and foreign equities")
        ETF = 'etf', _("Multi-asset exchange-traded investment funds")
        FIXED_INCOME = 'fixed income', _(
            "Bonds and certificates of deposit (CDs)")
        LOAN = 'loan', _("Loans and loan receivables")
        MUTUAL_FUND = 'mutual fund', _(
            "Open- and closed-end vehicles pooling funds of multiple investors")
        OTHER = 'other', _("Unknown or other investment types")
    type = models.CharField(
        max_length=12,
        choices=Type.choices
    )

    # Null when the security is not publically listed
    #
    # Current most expensive stock is 6 figures, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    close_price = models.DecimalField(
        null=True, max_digits=12, decimal_places=5)

    # Consider converting this DateTime or Timestamp once we know the data shape
    close_price_as_of = models.CharField(max_length=32, null=True)

    # Only one currency code can be nonnull
    currency = models.ForeignKey(
        Currency, on_delete=models.PROTECT, null=True)

    unofficial_currency_code = models.CharField(
        null=True,
        max_length=4,
        editable=False,
        choices=UnofficialCurrencyCode.choices,
        default=None
    )
