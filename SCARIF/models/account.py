from django.db import models
from django.utils.translation import ugettext_lazy as _
from SCARIF.models import UnofficialCurrencyCode, Currency, Item


class Account(models.Model):
    """
    Represents a user's account within a specific institution. A user may have multiple accounts in an institution.
    https://plaid.com/docs/api/products/#investments-holdings-get-response-accounts
    """

    id = models.CharField(primary_key=True, max_length=40)

    # The item associated with the account. This is relationship is implicitly
    # defined by plaid via the data/accounts an item has access to, but this
    # relationship is not explicitly provided in output from plaid
    item = models.ForeignKey(
        Item, on_delete=models.PROTECT, related_name="accounts")

    # The amount of funds available to be withdrawn from the account, as determined by the financial institution
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    balance_available = models.DecimalField(
        max_digits=19, decimal_places=5, null=True)

    # The total amount of funds in or owed by the account.
    # For investment-type accounts, the current balance is the total value of assets as presented by the institution
    # May be cached and we may need to call /accounts/balance/get to get up to date value
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    balance_current = models.DecimalField(
        max_digits=19, decimal_places=5, null=True)

    # In North America, this field is typically only available for credit-type accounts.
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    balance_limit = models.DecimalField(
        null=True, max_digits=19, decimal_places=5)

    # The ISO-4217 currency code of the balance. Always null if unofficial_currency_code is non-null
    balance_currency = models.ForeignKey(
        Currency, on_delete=models.PROTECT, null=True)

    # The unofficial currency code associated with the balance.
    # Always null if iso_currency_code is non-null.
    balance_unofficial_currency_code = models.CharField(
        null=True,
        max_length=4,
        editable=False,
        choices=UnofficialCurrencyCode.choices,
        default=None
    )

    # The last 2-4 alphanumeric characters of an account's official account number.
    # Note that the mask may be non-unique between an Item's accounts,
    # and it may also not match the mask that the bank displays to the user.
    mask = models.CharField(max_length=4)

    # The name of the account, either assigned by the user or by the financial institution itself
    name = models.CharField(max_length=128)

    # The official name of the account as given by the financial institution
    official_name = models.CharField(max_length=128, null=True)

    class Type(models.TextChoices):
        INVESTMENT = 'investment', _("Investment account")
        CREDIT = 'credit', _("Credit card")
        DEPOSITORY = 'depository', _("Depository account")
        LOAN = 'loan', _("Loan account")

        # An investment account is only listed as type BROKERAGE when using the
        # Assets product endpoint, used for underwriting loans.
        # See the documentation for more details:
        # https://plaid.com/docs/api/products/#investments-holdings-get-response-accounts-type
        #
        # As of 12/4/2021, we only provide support against Investment's Product Endpoints
        # https://plaid.com/docs/api/products/#investments
        BROKERAGE = 'brokerage', _(
            "An investment account. Used for /assets/ endpoints only; other endpoints use investment")
        OTHER = 'other', _("Non-specified account type")
    type = models.CharField(
        max_length=10,
        choices=Type.choices
    )

    # Descriptive subtype of the Account.type
    # See https://plaid.com/docs/api/accounts/#account-type-schema for reference
    # List is accurate for API Version 2020-09-14
    class Subtype(models.TextChoices):
        # DEPOSITORY
        CHECKING = 'checking', _("Checking account")
        SAVINGS = 'savings', _("Savings account")
        HSA_CASH = 'hsa cash', _(
            "Health Savings Account (US only) that can only hold cash")
        CD = 'cd', _("Certificate of deposit account")
        MONEY_MARKET = 'money market', _("Money market account")
        PAYPAL = 'paypal', _("PayPal depository account")
        PREPAID = 'prepaid', _("Prepaid debit card")
        CASH_MANAGEMENT = 'cash management', _(
            "A cash management account, typically a cash account at a brokerage")
        EBT = 'ebt', _(
            """An Electronic Benefit Transfer (EBT) account, 
            used by certain public assistance programs to distribute funds (US only)
            """
        )
        # CREDIT
        CREDIT_CARD = 'credit card', _("Bank-issued credit card")
        PAYPAL_CREDIT = 'paypal credit', _("PayPal-issued credit card")
        # LOANS
        AUTO = 'auto loan', _("Auto loan")
        COMMERCIAL = 'commercial loan', _("Commercial loan")
        CONSTRUCTION = 'construction loan', _("Construction loan")
        CONSUMER = 'consumer loan', _("Consumer loan")
        HOME_EQUITY = 'home equity loan', _(
            "Home Equity Line of Credit (HELOC)")
        LOAN = 'general loan', _("General loan")
        MORTGAGE = 'mortgage loan', _("Mortgage loan")
        OVERDRAFT = 'overdraft', _(
            "Pre-approved overdraft account, usually tied to a checking account")
        LINE_OF_CREDIT = 'line of credit', _("Pre-approved line of credit")
        STUDENT = 'student loan', _("Studen loan")
        OTHER_LOAN = 'other loan', _("Other loan type or unknown loan type")
        # INVESTMENT
        FIVE_TWENTY_NINE = '529', _(
            "Tax-advantaged college savings and prepaid tuition 529 plans (US)")
        FOUR_ZERO_ONE_A = '401a', _(
            "Employer-sponsored money-purchase 401(a) retirement plan (US)")
        FOUR_ZERO_ONE_K = '401k', _("Standard 401(k) retirement account (US)")
        FOUR_ZERO_THREE_B = '403b', _(
            "403(b) retirement savings account for non-profits and schools (US)")
        FOUR_FIVE_SEVEN_B = '457b', _(
            "Tax-advantaged deferred-compensation 457(b) retirement plan for governments and non-profits (US)")
        BROKERAGE = 'brokerage', _("Standard brokerage account")
        CASH_ISA = 'cash isa', _(
            "Individual Savings Account (ISA) that pays interest tax-free (UK)")
        EDUCATION_SAVINGS_ACCOUNT = 'education savings account', _(
            "Tax-advantaged Coverdell Education Savings Account (ESA) (US)")
        FIXED_ANNUITY = 'fixed annuity', _("Fixed annuity")
        GIC = 'gic', _("Guaranteed Investment Certificate (Canada)")
        HEALTH_REIMBURSEMENT_ARRANGEMENT = 'health reimbursement arrangement', _(
            "Tax-advantaged Health Reimbursement Arrangement (HRA) benefit plan (US)")
        HSA_NON_CASH = 'hsa non cash', _(
            "Non-cash tax-advantaged medical Health Savings Account (HSA) (US)")
        IRA = 'ira', _("Traditional Invididual Retirement Account (IRA) (US)")
        ISA = 'isa', _("Non-cash Individual Savings Account (ISA) (UK)")
        KEOGH = 'keogh', _("Keogh self-employed retirement plan (US)")
        LIF = 'lif', _("Life Income Fund (LIF) retirement account (Canada)")
        LIRA = 'lira', _("Locked-in Retirement Account (LIRA) (Canada)")
        LRIF = 'lrif', _("Locked-in Retirement Income Fund (LRIF) (Canada)")
        LRSP = 'lrsp', _("Locked-in Retirement Savings Plan (Canada)")
        MUTUAL_FUND = 'mutual fund', _("Mutual fund account")
        NON_TAXABLE_BROKERAGE_ACCOUNT = 'non-taxable brokerage account', _(
            "A non-taxable brokerage account that is not covered by a more specific subtype")
        PENSION = 'pension', _("Standard pension account")
        PRIF = 'prif', _(
            "Prescribed Registered Retirement Income Fund (Canada)")
        PROFIT_SHARING_PLAN = 'profit sharing plan', _(
            "Plan that gives employees share of company profits")
        QSHR = 'qshr', _("Qualifying share account")
        RDSP = 'rdsp', _("Registered Disability Savings Plan (RSDP) (Canada)")
        RESP = 'resp', _("Registered Education Savings Plan (Canada)")
        RETIREMENT = 'retirement', _(
            "Retirement account not covered by other subtypess")
        RLIF = 'rlif', _("Restricted Life Income Fund (RLIF) (Canada)")
        ROTH = 'roth', _("Roth IRA (US)")
        ROTH_401k = 'roth 401k', _("Employer-sponsored Roth 401(k) plan (US)")
        RRIF = 'rrif', _("Registered Retirement Income Fund (RRIF) (Canada)")
        RRSP = 'rrsp', _(
            "Registered Retirement Savings Plan (Canadian, similar to US 401(k))")
        SARSEP = 'sarsep', _(
            "Salary Reduction Simplified Employee Pension Plan (SARSEP), discontinued retirement plan (US)")
        SEP_IRA = 'sep ira', _(
            "Simplified Employee Pension IRA (SEP IRA), retirement plan for small businesses and self-employed (US)")
        SIMPLE_IRA = 'simple ira', _(
            "Savings Incentive Match Plan for Employees IRA, retirement plan for small businesses (US)")
        SIPP = 'sipp', _("Self-Invested Personal Pension (SIPP) (UK)")
        STOCK_PLAN = 'stock plan', _("Standard stock plan account")
        TFSA = 'tfsa', _(
            "Tax-Free Savings Account (TFSA), a retirement plan similar to a Roth IRA (Canada)")
        TRUST = 'trust', _(
            "Account representing funds or assets held by a trustee for the benefit of a beneficiary. Includes both revocable and irrevocable trusts.")
        UGMA = 'ugma', _(
            "\'Uniform Gift to Minors Act\' (brokerage account for minors, US)")
        UTMA = 'utma', _(
            "\'Uniform Transfers to Minors Act\' (brokerage account for minors, US)")
        VARIABLE_ANNUITY = 'variable annuity', _(
            "Tax-deferred capital accumulation annuity contract")
    subtype = models.CharField(
        max_length=36,
        choices=Subtype.choices
    )
