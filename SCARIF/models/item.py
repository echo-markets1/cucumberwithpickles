from django.db import models
from SCARIF.models import Link, Institution


class Item(models.Model):
    """ The plaid Item that connects a user to their account data
        with a given Institution. See
        https://plaid.com/docs/api/items/#item-get-response-item for reference.
    """
    # Case-sensitive unique identifier from plaid. Linking the same account at
    # the same institution twice will result in two Items with different
    # id values.
    id = models.CharField(primary_key=True, max_length=40)

    # Authorization token for accessing the data associated with this item
    access_token = models.CharField(max_length=128)

    # Connects an Echo user with their item
    user_link = models.ForeignKey(
        Link, on_delete=models.PROTECT, related_name="items")

    # The institution associated with this Item
    institution = models.ForeignKey(
        Institution, on_delete=models.PROTECT, related_name="institutions", null=True)
