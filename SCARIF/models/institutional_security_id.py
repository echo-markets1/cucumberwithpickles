from django.db import models
from SCARIF.models import Security, Institution


class InstitutionalSecurityId(models.Model):
    """
    Relates a Security to an institution via the institution_security_id
    See https://plaid.com/docs/api/products/#investments-holdings-get-response-institution-security-id
    for reference. Maintains uniqueness while minimizing database load.
    """
    # An identifier given to the security by the institution. Not pk because we
    # can't guarantee that multiple institutions don't use a common id for
    # different securities
    identifier = models.CharField(max_length=128)

    security = models.ForeignKey(
        Security, on_delete=models.PROTECT, related_name="institution_security_ids")

    institution = models.ForeignKey(
        Institution, on_delete=models.PROTECT, related_name="institution_security_ids")

    class Meta:
        unique_together = ('security', 'institution')
