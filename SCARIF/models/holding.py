from SCARIF.models import Account, Security
from django.db import models


class Holding(models.Model):
    """
    Represents a user's holding of a particular Security.
    See https://plaid.com/docs/api/products/#investments-holdings-get-response-holdings
    for reference
    """

    account = models.ForeignKey(
        Account, on_delete=models.PROTECT, related_name="holdings")

    # The Security this Holding relates to
    #
    # The currency code information is accessible via the security
    security = models.ForeignKey(
        Security, on_delete=models.PROTECT, related_name="holdings")

    # The last price given by the institution for this security
    #
    # Current most expensive stock is 6 figures, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    institution_price = models.DecimalField(max_digits=12, decimal_places=5)

    # The date at which institution_price was current
    #
    # The format may vary across institutions, so this date must be stored in string form
    institution_price_as_of = models.CharField(max_length=32, null=True)

    # The value of the holding, as reported by the institution
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    institution_value = models.DecimalField(max_digits=19, decimal_places=5)

    # The cost basis of the holding
    #
    # Current most expensive stock is 6 figures, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    cost_basis = models.DecimalField(
        null=True, max_digits=12, decimal_places=5)

    # The total quantity of the asset held, as reported by the financial institution.
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    quantity = models.DecimalField(max_digits=19, decimal_places=5)

    class Meta:
        unique_together = ('account', 'security')
