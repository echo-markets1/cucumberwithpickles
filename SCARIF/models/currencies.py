from django.db import models
from django.utils.translation import ugettext_lazy as _


class Currency(models.Model):
    """
    A unique identifier for the currency supporting transactions of the given security
    See https://www.iso.org/iso-4217-currency-codes.html for more info.
    Prevents us from recreating this info across securities
    """
    iso_code = models.CharField(
        null=True, max_length=3, unique=True)


# See https://plaid.com/docs/api/accounts/#currency-code-schema for list of supported codes
# List is accurate for API Version 2020-09-14
class UnofficialCurrencyCode(models.TextChoices):
    ADA = 'ADA', _("Cardano")
    BAT = 'BAT', _("Basic Attention Token")
    BCH = 'BCH', _("Bitcoin Cash")
    BNB = 'BNB', _("Binance Coin")
    BTC = 'BTC', _("Bitcoin")
    BTG = 'BTG', _("Bitcoin Gold")
    CNH = 'CNH', _("Chinese Yuan (offshore")
    DASH = 'DASH', _("Dash")
    DOGE = 'DOGE', _("Dogecoin")
    ETH = 'ETH', _("Ethereum")
    GBX = 'GBX', _("Pence sterling, i.e. British penny")
    LSK = 'LSK', _("Lisk")
    NEO = 'NEO', _("Neo")
    OMG = 'OMG', _("OmiseGO")
    QTUM = 'QTUM', _("Qtum")
    USDT = 'USDT', _("TehterUS")
    XLM = 'XLM', _("Stellar Lumen")
    XMR = 'XMR', _("Monero")
    XRP = 'XRP', _("Ripple")
    ZEC = 'ZEC', _("Zcash")
    ZRX = 'ZRX', _("Ox")
