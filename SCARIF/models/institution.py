from django.db import models
from SCARIF.core import institution_populator

class InstitutionManager(models.Manager):
    def get_or_create(self, **kwargs):
        (institution, created) = super().get_or_create(**kwargs)
        if created or institution.name==None:
            institution_populator.populate(institution)
        institution.refresh_from_db()
        return (institution, created)

class Institution(models.Model):
    """
    Represents a single institution which reports data on securities. Ex: Robinhood
    """
    objects = InstitutionManager()
    
    # Unique identifier for the institution, provided by plaid
    id = models.CharField(max_length=40, primary_key=True)

    # The official name of the institution
    name = models.CharField(max_length=128, null=True, blank=True, default=None)

    # The URL for the institution's website
    website_url = models.URLField(null=True, blank=True, default=None)

    # Hexadecimal representation of the primary color used by the institution
    primary_color = models.CharField(max_length=9, null=True, blank=True, default=None)
    
    # Base64 encoded representation of the institution's logo
    logo = models.TextField(null=True, blank=True, default=None)


