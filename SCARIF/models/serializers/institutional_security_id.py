from rest_framework import serializers
from SCARIF import models as scarif_models


class InstitutionalSecurityIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.InstitutionalSecurityId
        fields = '__all__'
