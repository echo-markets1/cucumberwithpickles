from SCARIF.models import serializers as scarif_serializers
from rest_framework import serializers
from SCARIF import models as scarif_models


class HoldingSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Holding
        fields = '__all__'
    security = scarif_serializers.SecuritySerializer()


class HoldingsDataSerializer(HoldingSerializer):
    """ Handles the de/serialization of holdings data from plaid """

    def to_internal_value(self, data):
        """ Map plaid data to the Holding model schema. """
        account = scarif_models.Account.objects.get(id=data["account_id"])
        security = scarif_models.Security.objects.get(id=data["security_id"])
        return {
            "account": account,
            "security": security,
            "institution_price": data["institution_price"],
            "institution_price_as_of": data["institution_price_as_of"],
            "institution_value": data["institution_value"],
            "cost_basis": data["cost_basis"],
            "quantity": data["quantity"]
        }

    def create(self, validated_data):
        return scarif_models.Holding.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Used to update a Holding. No other fields should be updated, and no
        other method should be used to update a Holding
        """
        instance.institution_price = validated_data.get(
            "institution_price", instance.institution_price)
        instance.institution_price_as_of = validated_data.get(
            "institution_price_as_of", instance.institution_price_as_of)
        instance.institution_value = validated_data.get(
            "institution_value", instance.institution_value)
        instance.cost_basis = validated_data.get(
            "cost_basis", instance.cost_basis)
        instance.quantity = validated_data.get("quantity", instance.quantity)
        instance.save()
        return instance
