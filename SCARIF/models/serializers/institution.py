from SCARIF import models as scarif_models
from SCARIF.models import serializers as scarif_serializers
from rest_framework import serializers


class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Institution
        fields = '__all__'
    institution_security_ids = scarif_serializers.InstitutionalSecurityIdSerializer(
        many=True)

class InstitutionDataSerializer(InstitutionSerializer):
    """ Handles the de/serialization of institution data from plaid """

    def to_internal_value(self, data):
        """ Map plaid data to the Institution model schema. """

        return {
            "id": data["institution_id"],
            "name": data["name"],
            "website_url": data["url"],
            "primary_color": data["primary_color"],
            "logo": data["logo"],
        }

    def create(self, validated_data):
        return scarif_models.Institution.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Used to update am Institution. No other fields should be updated, and no
        other method should be used to update a Institution
        """
        instance.name = validated_data.get("name", instance.name)
        instance.website_url = validated_data.get(
            "website_url", instance.website_url)
        instance.primary_color = validated_data.get(
            "primary_color", instance.primary_color)
        instance.logo = validated_data.get("logo", instance.logo)
        instance.save()
        return instance
