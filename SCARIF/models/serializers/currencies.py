from rest_framework import serializers
from SCARIF import models as scarif_models


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Currency
        fields = ['iso_code']
