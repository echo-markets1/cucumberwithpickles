from rest_framework import serializers
from SCARIF import models as scarif_models
from SCARIF.models import serializers as scarif_serializers


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Link
        fields = '__all__'
    items = scarif_serializers.ItemSerializer(many=True)
