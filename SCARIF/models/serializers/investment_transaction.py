from SCARIF.models import serializers as scarif_serializers
from rest_framework import serializers
from SCARIF import models as scarif_models
import datetime


class InvestmentTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.InvestmentTransaction
        fields = '__all__'
    security = scarif_serializers.SecuritySerializer()


class InvestmentTransactionsDataSerializer(InvestmentTransactionSerializer):
    """ Handles the de/serialization of investment_transactions data from plaid """

    def to_internal_value(self, data):
        """ Map plaid data to the InvestmentTransaction model schema. """
        account = scarif_models.Account.objects.get(id=data["account_id"])
        security = scarif_models.Security.objects.get(
            id=data["security_id"])
        date_posted = datetime.date.fromisoformat(
            data["date"])
        return {
            "id": data["investment_transaction_id"],
            "cancel_transaction_id": data["cancel_transaction_id"],
            "account": account,
            "security": security,
            "date_posted": date_posted,
            "name": data["name"],
            "quantity": data["quantity"],
            "amount": data["amount"],
            "price": data["price"],
            "fees": data["fees"],
            "type": data["type"],
            "subtype": data["subtype"]
        }

    def create(self, validated_data):
        return scarif_models.InvestmentTransaction.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Used to update an InvestmentTransaction. No other fields should be updated, and no
        other method should be used to update an InvestmentTransaction.
        The date_canceled will always be the date_posted from the
        investment_transactions data representing the cancelation.

        In other words, to update an InvestmentTransaction that is now canceled,
        we must pass in the new investment_transactions data that represents the
        cancelation of that transaction 
        """
        instance.date_canceled = validated_data.get(
            "date_posted", instance.date_canceled)
        instance.save()
        return instance
