from rest_framework import serializers
from SCARIF import models as scarif_models
from SCARIF.models import serializers as scarif_serializers


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Item
        fields = '__all__'
    accounts = scarif_serializers.AccountSerializer(many=True)
    institution = scarif_serializers.InstitutionSerializer()
