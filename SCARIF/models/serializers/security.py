from rest_framework import serializers
from SCARIF import models as scarif_models
from SCARIF.models import serializers as scarif_serializers


class SecuritySerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Security
        fields = '__all__'
    # scarif_serializers.CurrencySerializer()
    currency = serializers.SlugRelatedField(
        slug_field="iso_code", read_only=True)
    institutions = scarif_serializers.InstitutionSerializer(many=True)


class SecuritiesDataSerializer(SecuritySerializer):
    """ Handles the de/serialization of securities data from plaid """

    def to_internal_value(self, data):
        """ Map plaid data to the Security model schema. """
        (currency, _) = scarif_models.Currency.objects.get_or_create(
            iso_code=data["iso_currency_code"]
        )
        institutional_identifiers_data = {}
        institution_id = data.get("institution_id", None)

        # Although the institutional_identifiers represent a unique db table,
        # we recieve their data in the securities data from plaid. Therfore
        # it is the responsibility of the SecuritiesDataSerializer to handle
        # this data apropriately
        #
        # If no institution_id provide by the response, then there is no institutional
        # data associated with the security
        if institution_id is not None:
            # Although the institution is created on Item creation, and it *should* exist,
            # Webhooks are not account specific, and some situations like Sandbox
            # environments may create a situation where we are trying to fetch
            # an institution that has not yet been created. For this reason, prefer
            # get_or_create
            institution = scarif_models.Institution.objects.get_or_create(
                id=institution_id)[0]
            institutional_identifiers_data["identifier"] = data["institution_security_id"],
            institutional_identifiers_data["institution"] = institution
        ticker_symbol = data.get("ticker_symbol", None)
        if ticker_symbol is None:
            # Prefer blank over null for char fields
            ticker_symbol = ''
        return {
            "security_data": {
                "id": data["security_id"],
                "isin": data["isin"],
                "cusip": data["cusip"],
                "sedol": data["sedol"],
                "proxy_security_id": data["proxy_security_id"],
                "name": data["name"],
                "ticker_symbol": ticker_symbol,
                "is_cash_equivalent": data["is_cash_equivalent"],
                "type": data["type"],
                "close_price": data["close_price"],
                "close_price_as_of": data["close_price_as_of"],
                "currency": currency,
                "unofficial_currency_code": data["unofficial_currency_code"],
            },
            "institutional_identifiers_data": institutional_identifiers_data
        }

    def create(self, validated_data):
        security = scarif_models.Security.objects.create(
            **validated_data["security_data"])

        institutional_identifiers_data = validated_data["institutional_identifiers_data"]
        # If institutional_identifiers data is provided attempt to create them
        if institutional_identifiers_data:
            self._create_institutional_identifiers(
                institutional_identifiers_data, security)

        security.save()
        return security

    def update(self, instance, validated_data):
        """
        Used to update an Security. No other fields should be updated, and no
        other method should be used to update an Security
        """
        security_data = validated_data["security_data"]
        instance.close_price = security_data.get(
            "close_price", instance.close_price)
        instance.close_price_as_of = security_data.get(
            "close_price_as_of", instance.close_price_as_of)

        # A Security can have multiple institutional_identifiers, update them if
        # the data is provided.
        institutional_identifiers_data = validated_data["institutional_identifiers_data"]
        # If institutional_identifiers data is provided attempt to create them
        if institutional_identifiers_data:
            self._create_institutional_identifiers(
                institutional_identifiers_data, instance)
        instance.save()
        return instance

    def _create_institutional_identifiers(self, institutional_identifiers_data, security):
        # If the InstitutionalSecurityId is not already present in the db, create it.
        if not scarif_models.InstitutionalSecurityId.objects.filter(
            identifier=institutional_identifiers_data["identifier"],
            institution=institutional_identifiers_data["institution"],
            security=security
        ).exists():
            institutional_identifiers_data["security"] = security
            scarif_models.InstitutionalSecurityId.objects.create(
                **institutional_identifiers_data)
            print(f'INFO: Created Institutional Identifiers for Institution: ' +
                  f'{institutional_identifiers_data["institution"]} and ' +
                  f'Security: {security}')
