from SCARIF.models import serializers as scarif_serializers
from rest_framework import serializers
from SCARIF import models as scarif_models


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = scarif_models.Account
        fields = '__all__'
    investment_transactions = scarif_serializers.InvestmentTransactionSerializer(
        many=True)
    holdings = scarif_serializers.HoldingSerializer(many=True)
    balance_currency = scarif_serializers.CurrencySerializer()


class AccountsDataSerializer(AccountSerializer):
    """ 
    Handles the de/serialization of accounts data from plaid
    Plaid does not explicitly supply the item -> account relationships,
    so we must append the item to plaid accounts data
    """

    def to_internal_value(self, data):
        """ Map plaid data to the Account model schema. """
        (currency, created) = scarif_models.Currency.objects.get_or_create(
            iso_code=data["balances"]["iso_currency_code"]
        )
        return {
            "id": data["account_id"],
            "balance_available": data["balances"]["available"],
            "balance_current": data["balances"]["current"],
            "balance_limit": data["balances"]["limit"],
            "balance_currency": currency,
            "balance_unofficial_currency_code": data["balances"]["unofficial_currency_code"],
            "mask": data["mask"],
            "name": data["name"],
            "official_name": data["official_name"],
            "type": data["type"],
            "subtype": data["subtype"]
        }

    def create(self, validated_data):
        return scarif_models.Account.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Used to update an Account. No other fields should be updated, and no
        other method should be used to update an Account
        """
        instance.balance_available = validated_data.get(
            "balance_available", instance.balance_available)
        instance.balance_current = validated_data.get(
            "balance_current", instance.balance_current)
        instance.balance_limit = validated_data.get(
            "balance_limit", instance.balance_limit)
        instance.mask = validated_data.get("mask", instance.mask)
        instance.name = validated_data.get("name", instance.name)
        instance.official_name = validated_data.get(
            "official_name", instance.official_name)
        instance.save()
        return instance
