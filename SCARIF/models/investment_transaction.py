from SCARIF.models.security import Security
from SCARIF.models.account import Account
from django.db import models
from django.utils.translation import ugettext_lazy as _


class InvestmentTransaction(models.Model):
    """
    Represents a specific investment transaction made by a user in regards to a
    specific Security.

    See https://plaid.com/docs/api/products/#investmentstransactionsget for reference
    """
    # A unique, Plaid-specific identifier for the InvestmentTransaction, used in
    # their response to associate InvestmentTransactions with Securities
    id = models.CharField(max_length=40, primary_key=True)

    # When a transaction is being cancled, represents the id of the transaction to cancel.
    #
    # Prefer to storing as a OneToOne field with the canceled InvestmentTransaction
    # incase the cancellation transaction gets created before the original.
    #
    # This also indicates that the current transaction object representations
    # a cancellation of a transaction.
    cancel_transaction_id = models.CharField(max_length=40, null=True)

    account = models.ForeignKey(
        Account, on_delete=models.PROTECT, related_name="investment_transactions")

    # The Security this InvestmentTransaction relates to
    #
    # The currency code information is accessible via the security
    security = models.ForeignKey(
        Security, on_delete=models.PROTECT, related_name="transactions")

    # Date transaction was posted. If pending transaction, this refers to the date
    # the transaction posted, NOT neccessarily the date completed.
    #
    # Use https://docs.python.org/3.7/library/datetime.html#datetime.date.fromisoformat
    # to convert plaid response to Pythons datetime.Date object
    date_posted = models.DateField(
        auto_now=False, auto_created=False, auto_now_add=False)

    # Date transaction was cancelled. If null, the transaction has not been cancelled
    #
    # Use https://docs.python.org/3.7/library/datetime.html#datetime.date.fromisoformat
    # to convert plaid response to Pythons datetime.Date object
    date_canceled = models.DateField(
        auto_now=False, auto_created=False, auto_now_add=False, null=True)

    # The institution's description of the transaction.
    name = models.CharField(max_length=128, editable=False)

    # The number of units of the security involved in the transaction
    #
    # 1 Trillion has 13 digits, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    quantity = models.DecimalField(max_digits=19, decimal_places=5)

    # The complete value of the transaction.
    #   Positive values when cash is debited, e.g. purchases of stock;
    #   Negative values when cash is credited, e.g. sales of stock.
    #
    # Treatment remains the same for cash-only movements unassociated with securities.
    # 1 Billion has 10 digits. Since this represents money, only 2 decimal places
    # required.
    amount = models.DecimalField(max_digits=12, decimal_places=5)

    # The price of the security at which this transaction occurred.
    #
    # Current most expensive stock is 6 figures, some institutions list to 4 decimal
    # places, add 2 digits of buffer
    price = models.DecimalField(max_digits=12, decimal_places=5)

    # The combined value of all fees applied to this transaction
    fees = models.DecimalField(null=True, max_digits=12, decimal_places=5)

    class Type(models.TextChoices):
        BUY = 'buy', _("Buying an investment")
        SELL = 'sell', _("Selling an investment")
        CANCEL = 'cancel', _("A cancellation of a pending transaction")
        CASH = 'cash', _("Activity that modifies a cash position")
        FEE = 'fee', _("A fee on the account")
        TRANSFER = 'transfer', _(
            """Activity which modifies a position, but not through buy/sell
            activity e.g. options exercise, portfolio transfer""")
    type = models.CharField(
        max_length=8,
        choices=Type.choices
    )

    # Descriptive subtype of the InvestmentTransaction.type
    # See https://plaid.com/docs/api/accounts/#investment-transaction-subtypes-schema for reference
    # List is accurate for API Version 2020-09-14
    class Subtype(models.TextChoices):
        ACCOUNT_FEE = 'account_fee', _("Fees paid for account maintenance")
        ASSIGNMENT = 'assignment', _("Assignment of short option holding")
        BUY = 'buy', _("Purchase to open or increase a position")
        BUY_TO_COVER = 'buy to cover', _("Purchase to close a short position")
        CONTRIBUTION = 'contribution', _(
            "Inflow of assets into a tax-advantaged account")
        DEPOSIT = 'deposit', _("Inflow of cash into an account")
        DISTRIBUTION = 'distribution', _(
            "Outflow of assets from a tax-advantaged account")
        DIVIDEND = 'dividend', _("Inflow of cash from a dividend")
        DIVIDEND_REINVESTMENT = 'dividend reinvestment', _(
            "Purchase using proceeds from a cash dividend")
        EXERCISE = 'exercise', _("Exercise of an option or warrant contract")
        EXPIRE = 'expire', _("Expiration of an option or warrant contract")
        FUND_FEE = 'fund fee', _(
            "Fees paid for administration of a mutual fund or other pooled investment vehicle")
        INTEREST = 'interest', _("Inflow of cash from interest")
        INTEREST_RECEIVABLE = 'interest_receivable', _(
            "Inflow of cash from interest receivable")
        INTEREST_REINVESTMENT = 'interest_reinvestment', _(
            "Purchase using proceeds from a cash interest payment")
        LEGAL_FEE = 'legal fee', _("Fees paid for legal charges or services")
        LOAN_PAYMENT = 'loan payment', _(
            "Inflow of cash related to payment on a loan")
        LONG_TERM_CAPITAL_GAIN = 'long-term capital gain', _(
            "Long-term capital gain received as cash")
        LONG_TERM_CAPITAL_GAIN_REINVESTMENT = 'long-term capital gain reinvestment', _(
            "Purchase using long-term capital gain cash proceeds")
        MANAGEMENT_FEE = 'management fee', _(
            "Fees paid for investment management of a mutual fund or other pooled investment vehicle")
        MARGIN_EXPENSE = 'margin expense', _(
            "Fees paid for maintaining margin debt")
        MERGER = 'merger', _(
            "Stock exchanged at a pre-defined ratio as part of a merger between companies")
        MISCELLANEOUS_FEE = 'miscellaneous fee', _(
            "Fee associated with various account or holding actions")
        NON_QUALIFIED_DIVIDEND = 'non-qualified dividend', _(
            "Inflow of cash from a non-qualified dividend")
        NON_RESIDENT_TAX = 'non-resident tax', _(
            "Taxes paid on behalf of the investor for non-residency in investment jurisdiction")
        PENDING_CREDIT = 'pending credit', _("Pending inflow of cash")
        PENDING_DEBIT = 'pending debit', _("Pending outflow of cash")
        QUALIFIED_DIVIDEND = 'qualified dividend', _(
            "Inflow of cash from a qualified dividend")
        REBALANCE = 'rebalance', _(
            "Rebalancing transaction (buy or sell) with no net impact to value in the account")
        RETURN_OF_PRINCIPAL = 'return of principal', _(
            "Repayment of loan principal")
        SELL = 'sell', _("Sell to close or decrease an existing holding")
        SELL_SHORT = 'sell short', _("Sell to open a short position")
        SHORT_TERM_CAPITAL_GAIN = 'short-term capital gain', _(
            "Short-term capital gain received as cash")
        SHORT_TERM_CAPITAL_GAIN_REINVESTMENT = 'short-term capital gain reinvestment', _(
            "Purchase using short-term capital gain cash proceeds")
        SPIN_OFF = 'spin off', _(
            "Inflow of stock from spin-off transaction of an existing holding")
        SPLIT = 'split', _(
            "Inflow of stock from a forward split of an existing holding")
        STOCK_DISTRIBUTION = 'stock distribution', _(
            "Inflow of stock from a distribution")
        TAX = 'tax', _("Taxes paid on behalf of the investor")
        TAX_WITHHELD = 'tax withheld', _(
            "Taxes withheld on behalf of the customer")
        TRANSFER = 'transfer', _(
            "Movement of assets into or out of an account")
        TRANSFER_FEE = 'transfer fee', _(
            "Fees incurred for transfer of a holding or account")
        TRUST_FEE = 'trust fee', _(
            "Fees related to adminstration of a trust account")
        UNQUALIFIED_GAIN = 'unqualified gain', _(
            "Unqualified capital gain received as cash")
        WITHDRAWAL = 'withdrawal', _("Outflow of cash from an account")
    subtype = models.CharField(
        max_length=36,
        choices=Subtype.choices
    )

    class Meta:
        ordering = ['-date_posted']
