### Webhook Constants ###
WEBHOOK_TYPE = "webhook_type"
HOLDINGS_WEBHOOK = "HOLDINGS"
NEW_HOLDINGS = "new_holdings"
UPDATED_HOLDINGS = "updated_holdings"
INVESTMENTS_TRANSACTIONS_WEBHOOK = "INVESTMENTS_TRANSACTIONS"
NEW_INVESTMENT_TRANSACTIONS = "new_investments_transactions"
CANCELED_INVESTMENTS_TRANSACTIONS = "canceled_investments_transactions"
DEFAULT_UPDATE = "DEFAULT_UPDATE"
# This is what gets given to plaid to trigger when updates on an item is avaible
# Should always be prefixed by the rest of the url path
WEBHOOK_PATH = "handle-webhook"

### Client Response Constants ###
ACCOUNTS_DATA_KEY = "accounts"
SECURITIES_DATA_KEY = "securities"
HOLDINGS_DATA_KEY = "holdings"
INVESTMENT_TRANSACTIONS_DATA_KEY = "investment_transactions"
TOTAL_INVESTMENT_TRANSACTIONS_KEY = "total_investment_transactions"
INSTITUTION_DATA_KEY = "institution"
ITEM_DATA_KEY = "item"
