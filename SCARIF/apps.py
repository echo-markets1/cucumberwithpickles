from django.apps import AppConfig


class ScarifConfig(AppConfig):
    name = 'SCARIF'
