get_institution_by_id_response = {
    "institution": {
        "country_codes": [
            "US"
        ],
        "institution_id": "ins_109508",
        "name": "Houndstooth Bank",
        "products": [
            "auth",
            "balance",
            "identity",
            "transactions"
        ],
        "routing_numbers": [
            "110000000"
        ],
        "oauth": False,
        "status": {
            "item_logins": {
                "status": "HEALTHY",
                "last_status_change": "2019-02-15T15:53:00Z",
                "breakdown": {
                    "success": 0.9,
                    "error_plaid": 0.01,
                    "error_institution": 0.09
                }
            },
            "transactions_updates": {
                "status": "HEALTHY",
                "last_status_change": "2019-02-12T08:22:00Z",
                "breakdown": {
                    "success": 0.95,
                    "error_plaid": 0.02,
                    "error_institution": 0.03,
                    "refresh_interval": "NORMAL"
                }
            },
            "auth": {
                "status": "HEALTHY",
                "last_status_change": "2019-02-15T15:53:00Z",
                "breakdown": {
                    "success": 0.91,
                    "error_plaid": 0.01,
                    "error_institution": 0.08
                }
            },
            "balance": {
                "status": "HEALTHY",
                "last_status_change": "2019-02-15T15:53:00Z",
                "breakdown": {
                    "success": 0.89,
                    "error_plaid": 0.02,
                    "error_institution": 0.09
                }
            },
            "identity": {
                "status": "DEGRADED",
                "last_status_change": "2019-02-15T15:50:00Z",
                "breakdown": {
                    "success": 0.42,
                    "error_plaid": 0.08,
                    "error_institution": 0.5
                }
            },
            "investments_updates": {
                "status": "HEALTHY",
                "last_status_change": "2019-02-12T08:22:00Z",
                "breakdown": {
                    "success": 0.95,
                    "error_plaid": 0.02,
                    "error_institution": 0.03,
                    "refresh_interval": "NORMAL"
                }
            },
        },
        "primary_color": "#004966",
        "url": "https://plaid.com",
        "logo": "iVBORw0KGgoAAAANSUhEUgAAAJgAAACYCAMAAAAvHNATAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAIEaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4xNTI8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+MTUyPC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Ck8HWOIAAAAzUExURUdwTABgMgBhMwBhMgBfMgBjNwBgMQBfMgBfMwBnNQBfMgDIBQCZGABuKwCAIwCxDwC+CciFJbQAAAAKdFJOUwDXZkLpJsGljBO71ulrAAAE8ElEQVR42s2c0ZKDIAxFV6uACIH//9ptrdViaQshscnTTmeWOZNcQkDI31+jTWNvtFaq6wZrh65TSmvTj9PfD+2KpK40eRvUFe8nUPot0xOdPhfuYjpbbJ25nOQrM9hKGwy736ZeWZSpfmJ1lm0wNreN2jaaHkVisaBNJFgLGqnW+sGS2dDTpa3Oklp3ERZF2nheBstgQ7PTjGUy0xZGZdlMTdLC2B5OY5nNCOVCkml7gmlRsm+ZAlNnT7JukslVR3YmVxWZsqeaEjUfEXPT2NPNCOUqIrvYn9jXdXMafgM2TLImZPHUNPZnZk4SGDgXYpznGIKDVplRCcy7MCcWoU1mNJnVxfnVXEueJQmkD3PWoCGYFEu3j3muOZQs52/OATi55tkX/HvPpfwPXEUqy+pf83IVxTKn/5GZa56LxhgZHPaFq0hkry4b2blmQLlMs3MVqf/FZSNXXq0GO7isuar4zlUKllQZ0wlcpWB2Ikz6CVcMzsF1CjokWE9Xtz4QrpUX7EnBI8EUmfTd4iU45imPSheJ/Bulf2DysCJgwXb5U+2M/K2e3hEwmX9ZyikLRO/2ctplQxnrN5mm2U/hWEZ49+IxKB/REFSu163QIWLxFrIQjmCuYtCufU6GfBHh5iOYqxp2bM2uIV9DwHwEC3Xj9o2FhcuvO7BK/RDe6gMzdLLwea6lAErBKrnuCQMtsUwFFvf4PoNB9dhji8ReBRb9Ht/rXxEl/E1khk5g8PSz38gDYnCDryzgvfBTsFrhbxXGQCX8kOhuAwPM8AO2eM1tPXyiO1ij6nABmZCTMnwW2A4WkAoecZMS8oGEVHAOKbD7tDQ0ArsRJPG9gwE2FxnUgvR26U7BACuwZVHSJBnMvcT3BhZsA5iiEpjP/IQHUwiwr5kCne8TsI4okG5Gl/j5IrZjCWTpOd0HsKE9kJD9uQ1s+KMpDh3uyPWD1YLZbHHo86VZE9jQmlwhm3GdbQ1l1wjmshOicU5iZqX7skbO+CIsBVNt6QKyym8NJCbz+4/FDnK7lgPTTdOSR/m46iIeHObolb+AVReKLg2Zn+mVvxSKfQMYT85fS+vqzYhPQsai/GUzUr99SxwWOZS/bN/qN7yBO1Wsxz0KKzJgdJjCHKo47lSxHqr0yETmsw4DGrAecXDnPzks0HDdT4cHVCRzDnNE/lq/jWjMpHzJrZGMajsc7jGRTBcjUir7OE4fEZF8Vlh03hLbiPhkEw4OI/bVWr4iPnIlDgv0znpksdrPgvDIVn4PIbhAC3ZBfEgN25kAPKgiWVpNkkVlLGOS3v392l/giWTNvPTP6yEE2nXoMCerrjc8nZBvdyQDMZfCXAgJj3OcbWsUyNNFj7lCc1yNAkMWmxCXjiBdvoNnTGJV8nfPFTVPcj3ebNPFyWLdfDNhvdwFHEuTxf2cLjBhZW5P6kKJLV+92bAy903HwmTBx5R3WJHLIvnq891hJVeaPf3qc1y+J9QlcCDbNpYk/Ypr84Fqn/2tcq19aBC5pX9BPs0AgNOV/9vXUp+U/9P3ZV8CKfnBlNwnZnIf5Yl9xij34afcp7JyHxfLfY4t9wG72Cf/cpskyG0rIbcRh+DWJXKbvchtjyO3oZDcFkyCm1bJbfMltzGa4FZygpvvyW1XKLjBo+CWmIKbiApuuyq5Ua3k1r4bnMBmyPt0YGof/Q/0KNHgprixqgAAAABJRU5ErkJggg=="
    },
    "request_id": "m8MDnv9okwxFNBV"
}
