accounts_data = [
    {
        'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
        'balances': {
            'available': 6316.40404148,
            'current': 6316.40404148,
            'iso_currency_code': 'USD',
            'limit': None,
            'unofficial_currency_code': None
        },
        'mask': '5752',
        'name': 'Brokerage Account - 5752',
        'official_name': None,
        'subtype': 'brokerage',
        'type': 'investment'
    }, {
        "account_id": "blgvvBlXw3cq5GMPwqB6s6q4dLKB9WcVqGDGo",
        "balances": {
            "available": 100,
            "current": 110,
            "iso_currency_code": "USD",
            "limit": None,
            "unofficial_currency_code": None
        },
        "mask": "0000",
        "name": "Plaid Checking",
        "official_name": "Plaid Gold Standard 0% Interest Checking",
        "subtype": "checking",
        "type": "depository"
    }, {
        "account_id": "6PdjjRP6LmugpBy5NgQvUqpRXMWxzktg3rwrk",
        "balances": {
            "available": None,
            "current": 23631.9805,
            "iso_currency_code": "USD",
            "limit": None,
            "unofficial_currency_code": None
        },
        "mask": "6666",
        "name": "Plaid 401k",
        "official_name": None,
        "subtype": "401k",
        "type": "investment"
    }, {
        "account_id": "XMBvvyMGQ1UoLbKByoMqH3nXMj84ALSdE5B58",
        "balances": {
            "available": None,
            "current": 65262,
            "iso_currency_code": "USD",
            "limit": None,
            "unofficial_currency_code": None
        },
        "mask": "7777",
        "name": "Plaid Student Loan",
        "official_name": None,
        "subtype": "student",
        "type": "loan"
    }
]
