get_investment_transactions_response_1 = {
    'accounts': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'balances': {
                'available': 6316.40404148,
                'current': 6316.40404148,
                'iso_currency_code': 'USD',
                'limit': None,
                'unofficial_currency_code': None
            },
            'mask': '5752',
            'name': 'Brokerage Account - 5752',
            'official_name': None,
            'subtype': 'brokerage',
            'type': 'investment'
        }
    ],
    'investment_transactions': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-04-04',
            'fees': 0,
            'investment_transaction_id': '60xYwr34rp12rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-04-02',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx34xwiX0ynmexrzhEbr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-04-01',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX56nmex123Ebr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-04-01',
            'fees': 0,
            'investment_transaction_id': '60xY78R6rp12rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-03-21',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmexrz12br5Pd7dxF90L33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-03-10',
            'fees': 0,
            'investment_transaction_id': '12EAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL12n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }
    ],
    'item': {
        'available_products': [
            'auth',
            'balance',
            'transactions'
        ],
        'billed_products': ['investments'],
        'consent_expiration_time': None,
        'error': None,
        'institution_id': 'ins_54',
        'item_id': 'w6rXLKAwKasvxKE1qnDPuA9nXkvgNZsdOKN4K',
        'update_type': 'background',
        'webhook': 'https://echo-markets.herokuapp.com/handle_webhook/'
    },
    'request_id': 'HM2mpm83im6qK2K',
    'securities': [
        {
            'close_price': 6.52,
            'close_price_as_of': '2021-03-05',
            'cusip': '003881307',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Acacia Research Corporation',
            'proxy_security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'sedol': None,
            'ticker_symbol': 'ACTG',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 60.74,
            'close_price_as_of': '2021-03-05',
            'cusip': '458140100',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': 'US4581401001',
            'iso_currency_code': 'USD',
            'name': 'Intel Corporation',
            'proxy_security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'sedol': '2463247',
            'ticker_symbol': 'INTC',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 264.28,
            'close_price_as_of': '2021-03-05',
            'cusip': '30303M102',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Facebook, Inc.',
            'proxy_security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'sedol': None,
            'ticker_symbol': 'FB',
            'type': 'equity',
            'unofficial_currency_code': None
        }
    ],
    'total_investment_transactions': 6
}

get_investment_transactions_response_2 = {
    'accounts': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'balances': {
                'available': 6316.40404148,
                'current': 6316.40404148,
                'iso_currency_code': 'USD',
                'limit': None,
                'unofficial_currency_code': None
            },
            'mask': '5752',
            'name': 'Brokerage Account - 5752',
            'official_name': None,
            'subtype': 'brokerage',
            'type': 'investment'
        }
    ],
    'investment_transactions': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-03-04',
            'fees': 0,
            'investment_transaction_id': '60xYwrR6rph5rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-03-02',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmexrzhEbr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-03-01',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-03-01',
            'fees': 0,
            'investment_transaction_id': '60xYwrR6rp12rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-02-21',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmexrz12br5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-02-10',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL12n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }
    ],
    'item': {
        'available_products': [
            'auth',
            'balance',
            'transactions'
        ],
        'billed_products': ['investments'],
        'consent_expiration_time': None,
        'error': None,
        'institution_id': 'ins_54',
        'item_id': 'w6rXLKAwKasvxKE1qnDPuA9nXkvgNZsdOKN4K',
        'update_type': 'background',
        'webhook': 'https://echo-markets.herokuapp.com/handle_webhook/'
    },
    'request_id': 'HM2mpm83im6qK2K',
    'securities': [
        {
            'close_price': 6.52,
            'close_price_as_of': '2021-03-05',
            'cusip': '003881307',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Acacia Research Corporation',
            'proxy_security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'sedol': None,
            'ticker_symbol': 'ACTG',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 60.74,
            'close_price_as_of': '2021-03-05',
            'cusip': '458140100',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': 'US4581401001',
            'iso_currency_code': 'USD',
            'name': 'Intel Corporation',
            'proxy_security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'sedol': '2463247',
            'ticker_symbol': 'INTC',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 264.28,
            'close_price_as_of': '2021-03-05',
            'cusip': '30303M102',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Facebook, Inc.',
            'proxy_security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'sedol': None,
            'ticker_symbol': 'FB',
            'type': 'equity',
            'unofficial_currency_code': None
        }
    ],
    'total_investment_transactions': 6
}

investment_transactions_w_cancelation_response = {
    'accounts': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'balances': {
                'available': 6316.40404148,
                'current': 6316.40404148,
                'iso_currency_code': 'USD',
                'limit': None,
                'unofficial_currency_code': None
            },
            'mask': '5752',
            'name': 'Brokerage Account - 5752',
            'official_name': None,
            'subtype': 'brokerage',
            'type': 'investment'
        }
    ],
    'investment_transactions': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': '60xYwrR6rph5rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'date': '2021-03-05',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-03-04',
            'fees': 0,
            'investment_transaction_id': '60xYwrR6rph5rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-03-02',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmexrzhEbr5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-03-01',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmex12334r5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': 42,
            'cancel_transaction_id': None,
            'date': '2021-03-01',
            'fees': 0,
            'investment_transaction_id': '60xYwrR6rp12rxvZDj14sKoLJkvj9LC0Y3Lx1',
            'iso_currency_code': 'USD',
            'name': 'Buy',
            'price': 6,
            'quantity': 7,
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'subtype': 'buy',
            'type': 'buy',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -0.35,
            'cancel_transaction_id': None,
            'date': '2021-02-21',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmexrz12br5Pd7dxFxNL33n',
            'iso_currency_code': 'USD',
            'name': 'Dividend',
            'price': 0,
            'quantity': 0,
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'subtype': 'dividend',
            'type': 'cash',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': 'eZEAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL12n',
            'date': '2021-02-10',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX023mex123Ebr5Pd7dxFxNL12n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }, {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'amount': -478.31,
            'cancel_transaction_id': None,
            'date': '2021-02-08',
            'fees': 0,
            'investment_transaction_id': 'eZEAPx1jxwiX0ynmex123Ebr5Pd7dxFxNL12n',
            'iso_currency_code': 'USD',
            'name': 'Sell',
            'price': 72.69,
            'quantity': 6.58013482,
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'subtype': 'sell',
            'type': 'sell',
            'unofficial_currency_code': None
        }
    ],
    'item': {
        'available_products': [
            'auth',
            'balance',
            'transactions'
        ],
        'billed_products': ['investments'],
        'consent_expiration_time': None,
        'error': None,
        'institution_id': 'ins_54',
        'item_id': 'w6rXLKAwKasvxKE1qnDPuA9nXkvgNZsdOKN4K',
        'update_type': 'background',
        'webhook': 'https://echo-markets.herokuapp.com/handle_webhook/'
    },
    'request_id': 'HM2mpm83im6qK2K',
    'securities': [
        {
            'close_price': 6.52,
            'close_price_as_of': '2021-03-05',
            'cusip': '003881307',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Acacia Research Corporation',
            'proxy_security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'security_id': 'dRBp95pEwZfMX6mQQnZ6tYr08L09Xvs9PkRjq',
            'sedol': None,
            'ticker_symbol': 'ACTG',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 60.74,
            'close_price_as_of': '2021-03-05',
            'cusip': '458140100',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': 'US4581401001',
            'iso_currency_code': 'USD',
            'name': 'Intel Corporation',
            'proxy_security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'security_id': 'qJaMxDM3ydF4B0xbw6o9FpOw4Ywgkzta58X7m',
            'sedol': '2463247',
            'ticker_symbol': 'INTC',
            'type': 'equity',
            'unofficial_currency_code': None
        }, {
            'close_price': 264.28,
            'close_price_as_of': '2021-03-05',
            'cusip': '30303M102',
            'institution_id': None,
            'institution_security_id': None,
            'is_cash_equivalent': False,
            'isin': None,
            'iso_currency_code': 'USD',
            'name': 'Facebook, Inc.',
            'proxy_security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'security_id': 'JmN0JX0q5EcaQDORry7dCYzMgoMr6psg36DV5',
            'sedol': None,
            'ticker_symbol': 'FB',
            'type': 'equity',
            'unofficial_currency_code': None
        }
    ],
    'total_investment_transactions': 7
}

get_investment_transactions_response_empty = {
    'accounts': [
        {
            'account_id': '03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw',
            'balances': {
                'available': 6316.40404148,
                'current': 6316.40404148,
                'iso_currency_code': 'USD',
                'limit': None,
                'unofficial_currency_code': None
            },
            'mask': '5752',
            'name': 'Brokerage Account - 5752',
            'official_name': None,
            'subtype': 'brokerage',
            'type': 'investment'
        }
    ],
    'investment_transactions': [],
    'item': {
        'available_products': [
            'auth',
            'balance',
            'transactions'
        ],
        'billed_products': ['investments'],
        'consent_expiration_time': None,
        'error': None,
        'institution_id': 'ins_54',
        'item_id': 'w6rXLKAwKasvxKE1qnDPuA9nXkvgNZsdOKN4K',
        'update_type': 'background',
        'webhook': 'https://echo-markets.herokuapp.com/handle_webhook/'
    },
    'request_id': 'HM2mpm83im6qK2K',
    'securities': [],
    'total_investment_transactions': 0
}
