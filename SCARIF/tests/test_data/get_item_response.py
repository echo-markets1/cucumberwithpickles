get_item_response = {
    "item": {
        "available_products": [
            "balance",
            "auth"
        ],
        "billed_products": [
            "investments"
        ],
        "error": None,
        "institution_id": "ins_109508",
        "item_id": "Ed6bjNrDLJfGvZWwnkQlfxwoNz54B5C97ejBr",
        "update_type": "background",
        "webhook": "https://plaid.com/example/hook",
        "consent_expiration_time": None
    },
    "status": {
        "transactions": {
            "last_successful_update": "2019-02-15T15:52:39.000Z",
            "last_failed_update": "2019-01-22T04:32:00.000Z"
        },
        "last_webhook": {
            "sent_at": "2019-02-15T15:53:00.000Z",
            "code_sent": "DEFAULT_UPDATE"
        }
    },
    "request_id": "m8MDnv9okwxFNBV"
}
