import io
import sys
from unittest.mock import ANY, patch
from SCARIF.core.webhook_handler import handle
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.constants.plaid_constants import (
    CANCELED_INVESTMENTS_TRANSACTIONS, DEFAULT_UPDATE, HOLDINGS_WEBHOOK,
    INVESTMENTS_TRANSACTIONS_WEBHOOK, NEW_HOLDINGS, NEW_INVESTMENT_TRANSACTIONS,
    UPDATED_HOLDINGS, WEBHOOK_TYPE
)
from django.core.exceptions import ObjectDoesNotExist
from django.test.testcases import TestCase


class WebhookHandler(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.item = ItemFactory.create()
        cls.mock_investment_transactions_update_patcher = patch(
            'SCARIF.core.investments.investment_transactions_updater.update')
        cls.mock_holdings_update_patcher = patch(
            'SCARIF.core.investments.holdings_updater.update')
        cls.missed_creation_warning_prefix = "WARN: Created "
        cls.missed_update_warning_prefix = "WARN: Updated "

    def setUp(self):
        super().setUp()
        self.mock_investment_transactions_update = \
            self.mock_investment_transactions_update_patcher.start()
        self.mock_holdings_update = self.mock_holdings_update_patcher.start()

    def tearDown(self):
        super().tearDown()
        sys.stdout = sys.__stdout__
        self.mock_investment_transactions_update_patcher.stop()
        self.mock_holdings_update_patcher.stop()

    def test_handle_webhook_raise_object_does_not_exist_when_no_item_matching_webhook(self):
        item_id_not_in_db = "1234"
        with self.assertRaises(ObjectDoesNotExist):
            webhook = self._create_holdings_webhook_with(
                item_id=item_id_not_in_db,
                new_holdings=3,
                updated_holdings=5
            )
            handle(webhook)

    def test_handle_investment_transactions_webhook_with_expected_updates(self):
        # Arrange
        webhook_new_investments_transactions = 3
        webhook_canceled_investments_transactions = 5
        returned_new_investments_transactions = 3
        returned_canceled_investments_transactions = 5
        webhook = self._create_investment_transactions_webhook_with(
            item_id=self.item.id,
            new_investments_transactions=webhook_new_investments_transactions,
            canceled_investments_transactions=webhook_canceled_investments_transactions
        )
        self.mock_investment_transactions_update.return_value = (
            returned_new_investments_transactions, returned_canceled_investments_transactions)

        # Captured the system output (print statements) to verify _compare_updates
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

        # Act
        handle(webhook)

        # Assert
        self.mock_investment_transactions_update.assert_called_once_with(
            self.item,  # item
            ANY,  # start_date
            ANY  # end_date
        )
        self.assertNotIn(self.missed_creation_warning_prefix,
                         capturedOutput.getvalue())
        self.assertNotIn(self.missed_update_warning_prefix,
                         capturedOutput.getvalue())

    def test_handle_holdings_webhook_with_expected_updates(self):
        # Arrange
        webhook_new_holdings = 3
        webhook_updated_holdings = 5
        returned_new_holdings = 3
        returned_updated_holdings = 5
        webhook = self._create_holdings_webhook_with(
            item_id=self.item.id,
            new_holdings=webhook_new_holdings,
            updated_holdings=webhook_updated_holdings
        )
        self.mock_holdings_update.return_value = (
            returned_new_holdings, returned_updated_holdings)

        # Captured the system output (print statements) to verify _compare_updates
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

        # Act
        handle(webhook)
        # Assert
        self.mock_holdings_update.assert_called_once_with(self.item)
        self.assertNotIn(self.missed_creation_warning_prefix,
                         capturedOutput.getvalue())
        self.assertNotIn(self.missed_update_warning_prefix,
                         capturedOutput.getvalue())

    def test_handle_logs_unexpected_webhook(self):
        # Arrange
        webhook = {
            'webhook_type': 'SOME_OTHER_PRODUCT',
            'item_id': self.item.id
        }

        # Captured the system output (print statements) to verify _compare_updates
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

        # Act
        handle(webhook)
        # Assert
        self.assertIn(
            f'INFO: Recieved unexpected webhook of type: {webhook[WEBHOOK_TYPE]}',
            capturedOutput.getvalue()
        )

    def test_handle_emits_warning_when_missed_creation(self):
        # Arrange
        webhook_new_holdings = 3
        webhook_updated_holdings = 5
        returned_new_holdings = 2
        returned_updated_holdings = 5
        webhook = self._create_holdings_webhook_with(
            item_id=self.item.id,
            new_holdings=webhook_new_holdings,
            updated_holdings=webhook_updated_holdings
        )
        self.mock_holdings_update.return_value = (
            returned_new_holdings, returned_updated_holdings)

        # Captured the system output (print statements) to verify _compare_updates
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

        # Act
        handle(webhook)
        # Assert
        self.assertIn(self.missed_creation_warning_prefix,
                      capturedOutput.getvalue())
        self.assertNotIn(self.missed_update_warning_prefix,
                         capturedOutput.getvalue())

    def test_handle_emits_warning_when_missed_update(self):
        # Arrange
        webhook_new_holdings = 3
        webhook_updated_holdings = 5
        returned_new_holdings = 3
        returned_updated_holdings = 6
        webhook = self._create_holdings_webhook_with(
            item_id=self.item.id,
            new_holdings=webhook_new_holdings,
            updated_holdings=webhook_updated_holdings
        )
        self.mock_holdings_update.return_value = (
            returned_new_holdings, returned_updated_holdings)

        # Captured the system output (print statements) to verify _compare_updates
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput

        # Act
        handle(webhook)
        # Assert
        self.assertNotIn(self.missed_creation_warning_prefix,
                         capturedOutput.getvalue())
        self.assertIn(self.missed_update_warning_prefix,
                      capturedOutput.getvalue())

    def _create_holdings_webhook_with(self, item_id, new_holdings, updated_holdings):
        return {
            WEBHOOK_TYPE: HOLDINGS_WEBHOOK,
            "webhook_code": DEFAULT_UPDATE,
            "item_id": item_id,
            "error": None,
            NEW_HOLDINGS: new_holdings,
            UPDATED_HOLDINGS: updated_holdings
        }

    def _create_investment_transactions_webhook_with(self, item_id, new_investments_transactions, canceled_investments_transactions):
        return {
            WEBHOOK_TYPE: INVESTMENTS_TRANSACTIONS_WEBHOOK,
            "webhook_code": DEFAULT_UPDATE,
            "item_id": item_id,
            "error": None,
            NEW_INVESTMENT_TRANSACTIONS: new_investments_transactions,
            CANCELED_INVESTMENTS_TRANSACTIONS: canceled_investments_transactions
        }
