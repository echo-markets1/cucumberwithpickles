from SCARIF.models.link import Link
from SCARIF.core.transformers.item_data_transformer import transform
from SCARIF.models.institution import Institution
from SCARIF.tests.mixins.plaid_error_mock_mixin import PlaidErrorMockMixin
from SCARIF.tests.test_data.get_item_response import get_item_response
from SCARIF.tests.test_data.get_institution_by_id_response import get_institution_by_id_response
from unittest.mock import patch
from django.test.testcases import TestCase
import uuid


class ItemDataTransformerTest(TestCase, PlaidErrorMockMixin):
    @classmethod
    def setUpTestData(cls):
        cls.mock_get_item_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.get_item')
        cls.mock_get_institution_by_id_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.get_institution_by_id')
        cls.access_token = "access-sandbox-de3ce8ef-33f8-452c-a685-8671031fc0f6"
        cls.item_id = "M5eVJqLnv3tbzdngLDp9FL5OlDNxlNhlE55op"
        cls.user_id = uuid.uuid4()

    def setUp(self):
        super().setUp()
        self.mock_get_item = self.mock_get_item_patcher.start()
        self.mock_get_institution_by_id = self.mock_get_institution_by_id_patcher.start()

    def tearDown(self):
        super().tearDown()
        self.mock_get_item_patcher.stop()
        self.mock_get_institution_by_id_patcher.stop()

    def test_transform_creates_item_without_populated_institution_when_institution_population_fails(self):
        # Arrange
        self.mock_get_item.return_value = get_item_response
        self.mock_get_institution_by_id.side_effect = self.mock_plaid_error
        expected_institution_id = get_item_response["item"]["institution_id"]
        expected_success = False
        # Act
        (actual_success, actual_item) = transform(
            self.access_token, self.item_id, self.user_id)

        # Assert
        expected_institution = Institution.objects.get(
            id=expected_institution_id)
        self.assertEqual(expected_success, actual_success)
        self.assertIsNone(actual_item.institution.name)
        self.assertEqual(expected_institution, actual_item.institution)
        self._assert_creates_expected_item(
            actual_item, self.item_id, self.access_token, self.user_id)

    def test_transform_creates_item_with_no_institution_when_get_item_fails(self):
        # Arrange
        self.mock_get_item.side_effect = self.mock_plaid_error
        expected_success = False
        # Act
        (actual_success, actual_item) = transform(
            self.access_token, self.item_id, self.user_id)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertIsNone(actual_item.institution)
        self._assert_creates_expected_item(
            actual_item, self.item_id, self.access_token, self.user_id)

    def test_transform_creates_item_with_populated_institution_when_institution_population_succeeds(self):
        # Arrange
        self.mock_get_item.return_value = get_item_response
        self.mock_get_institution_by_id.return_value = get_institution_by_id_response
        expected_institution_id = get_item_response["item"]["institution_id"]
        expected_success = True
        # Act
        (actual_success, actual_item) = transform(
            self.access_token, self.item_id, self.user_id)

        # Assert
        expected_institution = Institution.objects.get(
            id=expected_institution_id)
        self.assertEqual(expected_success, actual_success)
        self.assertIsNotNone(actual_item.institution.name)
        self.assertEqual(expected_institution, actual_item.institution)
        self._assert_creates_expected_item(
            actual_item, self.item_id, self.access_token, self.user_id)

    def _assert_creates_expected_item(self, item, item_id, access_token, user_id):
        self.assertEqual(item.id, item_id)
        self.assertEqual(item.access_token, access_token)
        self.assertEqual(item.user_link,
                         Link.objects.get(user_id=user_id))
