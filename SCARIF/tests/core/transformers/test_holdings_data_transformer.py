
import decimal
from django.test.testcases import TestCase
from SCARIF.models.holding import Holding
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.constants.plaid_constants import ACCOUNTS_DATA_KEY, HOLDINGS_DATA_KEY, SECURITIES_DATA_KEY
from SCARIF.core.transformers.holdings_data_transformer import transform
from SCARIF.core.investments import investments_response_preprocessor
from SCARIF.tests.test_data.get_holdings_response import get_holdings_response


class HoldingsDataTransformerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up the db with the necessary accounts and securities data
        investments_response_preprocessor.preprocess(
            accounts_data=get_holdings_response[ACCOUNTS_DATA_KEY],
            item=ItemFactory.create(),
            securities_data=get_holdings_response[SECURITIES_DATA_KEY]
        )

    def test_transform_only_creates_when_all_holdings_new(self):
        # Arrange
        # Database empty, should result only in creates
        holdings_data = get_holdings_response[HOLDINGS_DATA_KEY]
        expected_creates = len(holdings_data)
        expected_updates = 0

        # Act
        (actual_creates, actual_updates) = transform(holdings_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_updates, actual_updates)
        self.assertEqual(expected_creates,
                         Holding.objects.all().count())

    def test_transform_creates_and_updates_when_existing_holdings(self):
        # Arrange
        holdings_data = get_holdings_response[HOLDINGS_DATA_KEY]
        expected_updates = 1
        expected_creates = len(holdings_data) - expected_updates

        # Set up the database with an existing holding
        initial_holdings_data = holdings_data[expected_updates]
        fields_to_update = {
            "institution_price": initial_holdings_data["institution_price"],
            "institution_price_as_of": initial_holdings_data["institution_price_as_of"],
            "institution_value": initial_holdings_data["institution_value"],
            "cost_basis": initial_holdings_data["cost_basis"],
            "quantity": initial_holdings_data["quantity"]
        }
        # The easiest way to prep the db with existing transactions is just
        # to call transform on a subset of the data we wish to test
        # Even though we are only transforming one holdings_data object, transform
        # expects a list. initial_holdings_data must be passed in a list.
        transform([initial_holdings_data])
        holding_to_update = Holding.objects.all().first()

        # Act
        (actual_creates, actual_updates) = transform(holdings_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertGreater(actual_updates, 0)
        self.assertEqual(expected_updates, actual_updates)
        self.assertEqual(actual_creates + actual_updates,
                         Holding.objects.all().count())
        self._assert_expected_updates(holding_to_update, fields_to_update)

    def test_transform_deletes_holdings_not_in_request(self):
        # Arrange
        holdings_data = get_holdings_response[HOLDINGS_DATA_KEY]

        # Set up the database with holdings
        # The easiest way to prep the db with existing transactions is just
        # to call transform on dataset we wish to test against
        transform(holdings_data)
        holding_to_delete = Holding.objects.all().last()
        self.assertTrue(Holding.objects.filter(
            pk=holding_to_delete.pk).exists())

        holdings_data_without_holding_to_delete = holdings_data[:-1]
        expected_deletes = 1
        expected_creates = 0  # Using the same dataset minus the item to delete
        expected_updates = len(holdings_data) - expected_deletes

        # Act
        (actual_creates, actual_updates) = transform(
            holdings_data_without_holding_to_delete)

        # Assert
        self.assertEqual(expected_creates, actual_creates)
        self.assertGreater(actual_updates, 0)
        self.assertEqual(expected_updates, actual_updates)
        self.assertFalse(Holding.objects.filter(
            pk=holding_to_delete.pk).exists())
        self.assertEqual(actual_creates + actual_updates,
                         Holding.objects.all().count())
        self.assertEqual(len(holdings_data) - expected_deletes,
                         Holding.objects.all().count())

    def _assert_expected_updates(self, holding_to_update, fields_to_update):
        holding_to_update.refresh_from_db()
        # Assert equal when rounded to nearest 1's place to account for
        # rounding issues.
        #
        # Additionally, NoneType can not convert to Decimal, so check to see
        # if nullable Decimal fields exist before comparing
        self.assertAlmostEqual(
            holding_to_update.institution_price,
            decimal.Decimal(fields_to_update["institution_price"]),
            0
        )
        self.assertEqual(
            holding_to_update.institution_price_as_of,
            fields_to_update["institution_price_as_of"]
        )
        self.assertAlmostEqual(
            holding_to_update.institution_value,
            decimal.Decimal(fields_to_update["institution_value"]),
            0
        )
        if holding_to_update.cost_basis:
            self.assertAlmostEqual(
                holding_to_update.cost_basis,
                decimal.Decimal(fields_to_update["cost_basis"]),
                0
            )
        else:
            self.assertEqual(
                holding_to_update.cost_basis,
                fields_to_update["cost_basis"],
            )
        self.assertAlmostEqual(
            holding_to_update.quantity,
            decimal.Decimal(fields_to_update["quantity"]),
            0
        )
