
from SCARIF.models.investment_transaction import InvestmentTransaction
from SCARIF.tests.factories.investment_transaction_factory import InvestmentTransactionFactory
from SCARIF.tests.factories.account_factory import AccountFactory
from SCARIF.constants.plaid_constants import (
    ACCOUNTS_DATA_KEY, INVESTMENT_TRANSACTIONS_DATA_KEY, SECURITIES_DATA_KEY
)
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.tests.test_data.get_investment_transactions_response import (
    get_investment_transactions_response_1,
    investment_transactions_w_cancelation_response
)
from SCARIF.core.transformers.investment_transactions_data_transformer import transform
from SCARIF.core.investments import investments_response_preprocessor
from django.test.testcases import TestCase


class InvestmentTransactionsDataTransformerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up the db with the necessary accounts and securities data
        investments_response_preprocessor.preprocess(
            accounts_data=get_investment_transactions_response_1[ACCOUNTS_DATA_KEY],
            item=ItemFactory.create(),
            securities_data=get_investment_transactions_response_1[SECURITIES_DATA_KEY]
        )
        cls.investment_transactions_data = get_investment_transactions_response_1[
            INVESTMENT_TRANSACTIONS_DATA_KEY]

    def test_transform_creates_new_investment_transactions(self):
        # Arrange
        # Database empty, should result only in creates
        expected_creates = len(self.investment_transactions_data)

        # Act
        (actual_creates, actual_cancels) = \
            transform(self.investment_transactions_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(actual_creates,
                         InvestmentTransaction.objects.all().count())

    def test_transform_only_creates_new_transactions(self):
        # Arrange
        # Setup the database as if we have already saved the least recent transaction
        # from the response
        num_investment_transactions = len(self.investment_transactions_data)
        last_index = num_investment_transactions - 1
        id_of_existing_transaction = self.investment_transactions_data[
            last_index]["investment_transaction_id"]

        account = AccountFactory.create()
        InvestmentTransactionFactory.create(
            account=account, id=id_of_existing_transaction)

        expected_creates = num_investment_transactions - 1
        investment_transactions_before = InvestmentTransaction.objects.all().count()
        # Act
        (actual_creates, actual_cancels) = \
            transform(self.investment_transactions_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(actual_creates + investment_transactions_before,
                         InvestmentTransaction.objects.all().count())


class InvestmentTransactionsDataTransformerWithCancelationsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up the db with the necessary accounts and securities data
        investments_response_preprocessor.preprocess(
            accounts_data=investment_transactions_w_cancelation_response[ACCOUNTS_DATA_KEY],
            item=ItemFactory.create(),
            securities_data=investment_transactions_w_cancelation_response[SECURITIES_DATA_KEY]
        )
        cls.investment_transactions_data = investment_transactions_w_cancelation_response[
            INVESTMENT_TRANSACTIONS_DATA_KEY]

    def test_transform_creates_and_cancels_investment_transactions_when_cancel_transaction_id(self):
        # Arrange
        def _count_expected_cancels(investment_transactions_data):
            return 1 if investment_transactions_data["cancel_transaction_id"] else 0

        # Count all the investment_transactions with a cancel_transaction_id
        expected_cancels = sum(map(
            _count_expected_cancels,
            self.investment_transactions_data
        ))
        expected_creates = len(self.investment_transactions_data)

        # Act
        (actual_creates, actual_cancels) = \
            transform(self.investment_transactions_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertGreater(actual_cancels, 0)
        self.assertEqual(expected_cancels, actual_cancels)
        self.assertEqual(actual_creates,
                         InvestmentTransaction.objects.all().count())
        self.assertEqual(actual_cancels,
                         InvestmentTransaction.objects.exclude(date_canceled=None).count())
