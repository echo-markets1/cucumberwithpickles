
from SCARIF.models import Security, InstitutionalSecurityId
from SCARIF.tests.factories.institution_factory import InstitutionFactory
from SCARIF.tests.factories.security_factory import SecurityFactory
from django.test.testcases import TestCase
from SCARIF.core.transformers.securities_data_transformer import transform
from SCARIF.tests.test_data.securities_data import securities_data, securities_data_with_institutional_ids
import decimal


class SecuritiesDataTransformerTest(TestCase):
    def test_transform_creates_new_securities(self):
        # Arrange
        # Database empty, should result only in creates
        expected_creates = len(securities_data)
        expected_updates = 0

        # Act
        (actual_creates, actual_updates) = transform(securities_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_updates, actual_updates)
        self.assertEqual(actual_creates, Security.objects.all().count())

    def test_transform_updates_existing_securities(self):
        # Arrange
        expected_updates = 1
        expected_creates = len(securities_data) - expected_updates

        # Set up the database with a set of securities
        existing_security_id = securities_data[0]["security_id"]
        security_to_update = SecurityFactory.create(id=existing_security_id)

        # Act
        (actual_creates, actual_updates) = transform(securities_data)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertGreater(actual_updates, 0)
        self.assertEqual(expected_updates, actual_updates)
        self.assertEqual(actual_creates,
                         Security.objects.all().count() - actual_updates)
        self._assert_expected_updates(
            security_to_update=security_to_update,
            expected_close_price=securities_data[0]["close_price"],
            expected_close_price_as_of=securities_data[0]["close_price_as_of"]
        )

    def test_transform_creates_institutional_identifiers(self):

        # Arrange
        # Count all the securities with an institutional_id
        def _count_expected_institutional_identifier_creates(security_data):
            return 1 if security_data["institution_id"] else 0
        expected_institutional_identifier_creates = sum(map(
            _count_expected_institutional_identifier_creates,
            securities_data_with_institutional_ids
        ))

        # Create the relevant Institution objects
        self._create_institutions_from_security_data(
            securities_data=securities_data_with_institutional_ids)

        num_institutional_identifier_before = InstitutionalSecurityId.objects.all().count()

        # Act
        # Create the InstitutionalSecurityIds
        transform(securities_data_with_institutional_ids)
        num_institutional_identifier_after = InstitutionalSecurityId.objects.all().count()
        actual_institutional_identifier_creates = num_institutional_identifier_after - \
            num_institutional_identifier_before

        # Assert
        # First confirm we created > 0 institutional_identifiers
        self.assertGreater(num_institutional_identifier_after, 0)
        self.assertEqual(expected_institutional_identifier_creates,
                         actual_institutional_identifier_creates)

    def test_transform_does_not_create_institutional_identifiers_when_no_institution_id(self):
        # Arrange
        # Database empty, should result only in creates
        expected_num_institutional_ids = InstitutionalSecurityId.objects.all().count()

        # Act
        transform(securities_data)

        # Assert
        actual_num_instituitional_ids = InstitutionalSecurityId.objects.all().count()
        self.assertEqual(expected_num_institutional_ids,
                         actual_num_instituitional_ids)

    def _create_institutions_from_security_data(self, securities_data):
        # Create an Institution object for each unique Insitution
        def _get_institutional_ids(security_data):
            return security_data["institution_id"]

        unique_institution_ids = set(
            map(_get_institutional_ids, securities_data))
        for id in unique_institution_ids:
            InstitutionFactory.create(id=id)

    def _assert_expected_updates(self, security_to_update, expected_close_price, expected_close_price_as_of):
        security_to_update.refresh_from_db()
        # Assert equal when rounded to nearest 1's place to account for
        # rounding issues
        self.assertAlmostEqual(
            security_to_update.close_price,
            decimal.Decimal(expected_close_price),
            0
        )
        self.assertEqual(
            security_to_update.close_price_as_of,
            expected_close_price_as_of
        )
