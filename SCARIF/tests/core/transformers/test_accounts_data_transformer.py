
import decimal
from django.test.testcases import TestCase
from SCARIF.core.transformers.accounts_data_transformer import transform
from SCARIF.models.account import Account
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.tests.factories.account_factory import AccountFactory
from SCARIF.tests.test_data.accounts_data import accounts_data


class AccountsDataTransformerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.item = ItemFactory.create()

    def __get_num_investment_accounts(self) -> int:
        num_investment_accounts = 0
        for account in accounts_data:
            if account['type'] == Account.Type.INVESTMENT:
                num_investment_accounts = num_investment_accounts + 1
        return num_investment_accounts

    def test_transform_creates_new_accounts(self):
        # Arrange
        # Database empty, should result only in creates
        expected_creates = self.__get_num_investment_accounts()
        expected_updates = 0

        # Act
        (actual_creates, actual_updates) = transform(
            accounts_data, self.item)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_updates, actual_updates)
        self.assertEqual(actual_creates, Account.objects.all().count())

    def test_transform_updates_existing_accounts(self):
        # Arrange
        expected_updates = 1
        expected_creates = self.__get_num_investment_accounts() - expected_updates
        # Set up the database with an existing account
        account_to_update_data = accounts_data[0]
        account_to_update_id = account_to_update_data["account_id"]
        account_to_update = AccountFactory.create(id=account_to_update_id)
        fields_to_update = {
            "balance_available": account_to_update_data["balances"]["available"],
            "balance_current": account_to_update_data["balances"]["current"],
            "balance_limit": account_to_update_data["balances"]["limit"],
            "mask": account_to_update_data["mask"],
            "name": account_to_update_data["name"],
            "official_name": account_to_update_data["official_name"],
        }

        # Act
        (actual_creates, actual_updates) = transform(
            accounts_data, self.item)

        # Assert
        self.assertGreater(actual_creates, 0)
        self.assertEqual(expected_creates, actual_creates)
        self.assertGreater(actual_updates, 0)
        self.assertEqual(expected_updates, actual_updates)
        self._assert_expected_updates(account_to_update, fields_to_update)

    def _assert_expected_updates(self, account_to_update, fields_to_update):
        account_to_update.refresh_from_db()
        # Assert equal when rounded to nearest 1's place to account for
        # rounding issues.
        #
        # Additionally, NoneType can not convert to Decimal, so check to see
        # if nullable Decimal fields exist before comparing
        if account_to_update.balance_available:
            self.assertAlmostEqual(
                account_to_update.balance_available,
                decimal.Decimal(fields_to_update["balance_available"]),
                0
            )
        else:
            self.assertEqual(
                account_to_update.balance_limit,
                fields_to_update["balance_limit"],
            )
        self.assertAlmostEqual(
            account_to_update.balance_current,
            decimal.Decimal(fields_to_update["balance_current"]),
            0
        )
        if account_to_update.balance_limit:
            self.assertAlmostEqual(
                account_to_update.balance_limit,
                decimal.Decimal(fields_to_update["balance_limit"]),
                0
            )
        else:
            self.assertEqual(
                account_to_update.balance_limit,
                fields_to_update["balance_limit"],
            )
        self.assertEqual(
            account_to_update.mask,
            fields_to_update["mask"]
        )
        self.assertEqual(
            account_to_update.name,
            fields_to_update["name"]
        )
        self.assertEqual(
            account_to_update.official_name,
            fields_to_update["official_name"]
        )
