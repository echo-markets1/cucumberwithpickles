import uuid
from SCARIF.core.token_exchange_flow import token_exchange_management
from SCARIF.tests.mixins.plaid_error_mock_mixin import PlaidErrorMockMixin
from SCARIF.tests.test_data.exchange_public_token_response import exchange_public_token_response
from SCARIF.tests.test_data.create_link_token_response import create_link_token_response
from unittest.mock import patch
from django.test.testcases import TestCase
from SCARIF.interfaces import plaid_client_interface


class SetAccessTokenCoreTest(TestCase, PlaidErrorMockMixin):
    @classmethod
    def setUpTestData(cls):
        cls.mock_exchange_public_token_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.exchange_public_token')
        cls.mock_transform_patcher = patch(
            'SCARIF.core.transformers.item_data_transformer.transform')
        # Stored as char field, not a UUID
        cls.public_token = str(uuid.uuid4())
        cls.user_id = uuid.uuid4()

    def setUp(self):
        super().setUp()
        self.mock_exchange_public_token = self.mock_exchange_public_token_patcher.start()
        self.mock_transform = self.mock_transform_patcher.start()

    def tearDown(self):
        super().tearDown()
        self.mock_exchange_public_token_patcher.stop()
        self.mock_transform_patcher.stop()

    def test_set_access_token_returns_success_when_public_token_exchange_succeeds(self):
        # Arrange
        self.mock_exchange_public_token.return_value = exchange_public_token_response
        expected_success = True
        expected_response = exchange_public_token_response

        # Act
        (actual_success, actual_response) = token_exchange_management.set_access_token(
            self.public_token, self.user_id)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)

        # Verify
        self.mock_transform.assert_called_once_with(
            access_token=exchange_public_token_response['access_token'],
            item_id=exchange_public_token_response['item_id'],
            user_id=self.user_id
        )
        self.mock_exchange_public_token.assert_called_once_with(
            self.public_token)

    def test_set_access_token_returns_error_when_public_token_exchange_fails(self):
        # Arrange
        self.mock_exchange_public_token.side_effect = self.mock_plaid_error
        expected_success = False
        expected_response = plaid_client_interface.format_plaid_error(
            self.mock_plaid_error)
        # Act
        (actual_success, actual_response) = token_exchange_management.set_access_token(
            self.public_token, self.user_id)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)
        self.mock_transform.assert_not_called()


class CreateLinkTokenCoreTest(TestCase, PlaidErrorMockMixin):
    @classmethod
    def setUpTestData(cls):
        cls.mock_create_link_token_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.create_link_token')
        cls.user_id = "some_user_id"

    def setUp(self):
        super().setUp()
        self.mock_create_link_token = self.mock_create_link_token_patcher.start()

    def tearDown(self):
        super().tearDown()
        self.mock_create_link_token_patcher.stop()

    def test_core_create_link_token_returns_success_when_client_create_link_token_succeeds(self):
        # Arrange
        self.mock_create_link_token.return_value = create_link_token_response
        expected_success = True
        expected_response = create_link_token_response

        # Act
        (actual_success, actual_response) = token_exchange_management.create_link_token(
            self.user_id)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)

    def test_core_create_link_token_returns_error_when_client_create_link_token_fails(self):
        # Arrange
        self.mock_create_link_token.side_effect = self.mock_plaid_error
        expected_success = False
        expected_response = plaid_client_interface.format_plaid_error(
            self.mock_plaid_error)
        # Act
        (actual_success, actual_response) = token_exchange_management.create_link_token(
            self.user_id)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)
