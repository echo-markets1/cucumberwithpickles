from SCARIF.core.institution_populator import populate
from SCARIF.constants.plaid_constants import INSTITUTION_DATA_KEY
from unittest.mock import patch
from django.test.testcases import TestCase
from SCARIF.tests.mixins.plaid_error_mock_mixin import PlaidErrorMockMixin
from SCARIF.models.institution import Institution
from SCARIF.tests.test_data.get_institution_by_id_response import get_institution_by_id_response
from SCARIF.interfaces import plaid_client_interface


class InstitutionPopulatorTest(TestCase, PlaidErrorMockMixin):
    @classmethod
    def setUpTestData(cls):
        cls.mock_get_institution_by_id_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.get_institution_by_id')
        cls.institution_id = get_institution_by_id_response[INSTITUTION_DATA_KEY]["institution_id"]

    def setUp(self):
        super().setUp()
        self.institution = Institution.objects.create(id=self.institution_id)
        self.mock_get_institution_by_id = self.mock_get_institution_by_id_patcher.start()

    def tearDown(self):
        super().tearDown()
        self.mock_get_institution_by_id_patcher.stop()

    def test_populate_updates_institution_data(self):
        # Arrange
        self.mock_get_institution_by_id.return_value = get_institution_by_id_response

        # Act
        populate(self.institution)

        # Assert
        self._assert_populated_institution_matches_response(
            self.institution, get_institution_by_id_response[INSTITUTION_DATA_KEY])

    def test_populate_returns_error_when_get_institution_by_id_fails(self):
        # Arrange
        self.mock_get_institution_by_id.side_effect = self.mock_plaid_error
        expected_success = False
        expected_response = plaid_client_interface.format_plaid_error(
            self.mock_plaid_error)
        # Act
        (actual_success, actual_response) = populate(
            self.institution)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)

    def test_populate_returns_success_when_get_institution_by_id_succeeds(self):
        # Arrange
        self.mock_get_institution_by_id.return_value = get_institution_by_id_response
        expected_success = True
        expected_response = get_institution_by_id_response
        # Act
        (actual_success, actual_response) = populate(
            self.institution)

        # Assert
        self.assertEqual(expected_success, actual_success)
        self.assertEqual(expected_response, actual_response)

    def _assert_populated_institution_matches_response(self, institution, institution_data):
        self.assertEqual(institution.id, institution_data["institution_id"])
        self.assertEqual(institution.name, institution_data["name"])
        self.assertEqual(institution.website_url, institution_data["url"])
        self.assertEqual(institution.primary_color,
                         institution_data["primary_color"])
        self.assertEqual(institution.logo, institution_data["logo"])
