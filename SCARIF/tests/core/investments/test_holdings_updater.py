
from SCARIF.models.holding import Holding
from SCARIF.models.security import Security
from SCARIF.models.account import Account
from SCARIF.constants.plaid_constants import HOLDINGS_DATA_KEY
from SCARIF.tests.factories.item_factory import ItemFactory
from django.test.testcases import TestCase
from SCARIF.core.investments.holdings_updater import update
from SCARIF.tests.test_data.get_holdings_response import get_holdings_response
from SCARIF.tests.mixins.plaid_error_mock_mixin import PlaidErrorMockMixin
from unittest.mock import patch


class HoldingsUpdaterTest(TestCase, PlaidErrorMockMixin):
    @classmethod
    def setUpTestData(cls):
        cls.item = ItemFactory.create()
        cls.mock_get_holdings_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.get_holdings')
        cls.mock_get_holdings = cls.mock_get_holdings_patcher.start()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.mock_get_holdings_patcher.stop()

    def test_update_creates_holdings_when_get_holdings_succeeds(self):
        # Arrange
        # Show the db is empty before update
        self._assert_db_empty()

        # Stub the plaid_client_interface.get_holdings response
        self.mock_get_holdings.return_value = get_holdings_response

        # Database empty, should result only in creates
        expected_creates = len(get_holdings_response[HOLDINGS_DATA_KEY])
        expected_updates = 0

        # Act
        (actual_creates, actual_updates) = update(self.item)

        # Assert
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_updates, actual_updates)
        # Confirm db was updated
        self._assert_db_has_accounts_securities_holdings()

    def test_update_does_not_create_holdings_when_get_holdings_fails(self):
        # Arrange
        # Show the db is empty before update
        self._assert_db_empty()

        # Fail the get_holdings call
        self.mock_get_holdings.side_effect = self.mock_plaid_error

        expected_updates = 0
        expected_creates = 0

        # Act
        (actual_creates, actual_updates) = update(self.item)

        # Assert
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_updates, actual_updates)
        # confirm db was not updated
        self._assert_db_empty()

    def _assert_db_has_accounts_securities_holdings(self):
        self.assertGreater(Account.objects.all().count(), 0)
        self.assertGreater(Security.objects.all().count(), 0)
        self.assertGreater(Holding.objects.all().count(), 0)

    def _assert_db_empty(self):
        self.assertEqual(Account.objects.all().count(), 0)
        self.assertEqual(Security.objects.all().count(), 0)
        self.assertEqual(Holding.objects.all().count(), 0)
