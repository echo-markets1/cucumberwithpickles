
from SCARIF.tests.factories.investment_transaction_factory import InvestmentTransactionFactory
from SCARIF.tests.factories.account_factory import AccountFactory
from SCARIF.constants.plaid_constants import INVESTMENT_TRANSACTIONS_DATA_KEY
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.tests.test_data.get_investment_transactions_response import (
    get_investment_transactions_response_1, get_investment_transactions_response_2,
    get_investment_transactions_response_empty
)
from SCARIF.core.investments.investment_transactions_updater import update
from django.test.testcases import TestCase
from unittest.mock import ANY, patch
import mock
import datetime
import plaid


class InvestmentTransactionsUpdaterTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.item = ItemFactory.create()
        cls.end_date = mock.create_autospec(datetime.date)
        cls.start_date = mock.create_autospec(datetime.date)
        cls.mock_get_investment_transactions_patcher = patch(
            'SCARIF.interfaces.plaid_client_interface.get_investment_transactions')
        cls.mock_get_investment_transactions = cls.mock_get_investment_transactions_patcher.start()

    def setUp(self):
        super().setUp()
        self.mock_get_investment_transactions = self.mock_get_investment_transactions_patcher.start()

    def tearDown(self):
        super().tearDown()
        self.mock_get_investment_transactions_patcher.stop()

    def test_update_only_retrieves_investment_transactions_for_time_period(self):
        # Arrange
        self.mock_get_investment_transactions.return_value = get_investment_transactions_response_1

        # Act
        update(self.item, self.start_date, self.end_date)

        # Assert

        # The objective is not to validate the plaid client, but rather that
        # we call plaid with the expected date range
        self.mock_get_investment_transactions.assert_called_with(
            access_token=self.item.access_token,
            start_date=self.start_date.isoformat(),
            end_date=self.end_date.isoformat(),
            count=ANY,
            offset=ANY
        )

    def test_update_stops_when_get_investment_transaction_raises_plaid_error(self):
        # Arrange
        expected_creates = 0
        expected_cancels = 0

        # Stub the plaid_client_interface.get_investment_transactions response to throw an error
        error = plaid.errors.PlaidError(
            message="error_message",
            type="TYPE",
            code="CODE",
            display_message=None
        )
        self.mock_get_investment_transactions.side_effect = error

        # Act
        (actual_creates, actual_cancels) = update(
            self.item, self.start_date, self.end_date)

        # Assert
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_cancels, actual_cancels)

        # Verify
        self.mock_get_investment_transactions.assert_called_once_with(
            access_token=self.item.access_token,
            start_date=self.start_date.isoformat(),
            end_date=self.end_date.isoformat(),
            count=ANY,
            offset=ANY
        )

    def test_update_keeps_calling_plaid_until_no_new_data(self):
        # Arrange

        # Sets different return values depending on input parameters https://stackoverflow.com/a/7665754
        def _side_effect(*args, **kwargs):
            if kwargs['offset'] == 0:
                return get_investment_transactions_response_1
            elif kwargs['offset'] == 500:
                return get_investment_transactions_response_2
            elif kwargs['offset'] == 1000:
                return get_investment_transactions_response_empty

        self.mock_get_investment_transactions.side_effect = _side_effect

        expected_creates = len(get_investment_transactions_response_1[INVESTMENT_TRANSACTIONS_DATA_KEY]) + \
            len(get_investment_transactions_response_2[INVESTMENT_TRANSACTIONS_DATA_KEY]) + \
            len(
                get_investment_transactions_response_empty[INVESTMENT_TRANSACTIONS_DATA_KEY])

        expected_cancels = 0
        expected_plaid_calls = 3
        # Act
        (actual_creates, actual_cancels) = update(
            self.item, self.start_date, self.end_date)

        # Assert
        self.assertEqual(expected_creates, actual_creates)
        self.assertEqual(expected_cancels, actual_cancels)
        # Confirms that we stop calling plaid once we get a response w no new data
        self.assertEqual(expected_plaid_calls,
                         self.mock_get_investment_transactions.call_count)

    def test_update_stops_calling_plaid_when_response_contains_old_data(self):
        # Arrange
        # Setup the database as if we have already saved the least recent transaction
        # from the response
        investment_transactions_data = get_investment_transactions_response_1[
            INVESTMENT_TRANSACTIONS_DATA_KEY]
        last_index = len(investment_transactions_data) - 1
        id_of_existing_transaction = investment_transactions_data[
            last_index]["investment_transaction_id"]

        account = AccountFactory.create(item=self.item)
        InvestmentTransactionFactory.create(
            account=account, id=id_of_existing_transaction)

        # Stub the plaid_client_interface.get_investment_transactions response
        self.mock_get_investment_transactions.return_value = get_investment_transactions_response_1
        expected_plaid_calls = 1

        # Act
        update(self.item, self.start_date, self.end_date)

        # Assert
        # Confirms that we stop calling plaid once we get a response w no new data
        self.assertEqual(expected_plaid_calls,
                         self.mock_get_investment_transactions.call_count)
