from SCARIF.models.currencies import Currency
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class CurrencyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Currency
        strategy = BUILD_STRATEGY
        django_get_or_create = ('iso_code',)

    iso_code = 'USD'
