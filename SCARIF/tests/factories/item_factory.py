from SCARIF.models.item import Item
from SCARIF.tests.factories.link_factory import LinkFactory
from SCARIF.tests.factories.institution_factory import InstitutionFactory
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class ItemFactory(factory.DjangoModelFactory):
    class Meta:
        model = Item
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.uuid4(cast_to=str)
    access_token = faker.uuid4(cast_to=str)
    user_link = factory.SubFactory(LinkFactory)
    institution = factory.SubFactory(InstitutionFactory)
