from SCARIF.tests.factories.currency_factory import CurrencyFactory
from SCARIF.tests.factories.item_factory import ItemFactory
from SCARIF.models.account import Account
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class AccountFactory(factory.DjangoModelFactory):
    class Meta:
        model = Account
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.uuid4(cast_to=str)
    item = factory.SubFactory(ItemFactory)
    balance_available = factory.fuzzy.FuzzyDecimal(0, 10000000, 5)
    balance_current = factory.fuzzy.FuzzyDecimal(0, 10000000, 5)
    balance_limit = None
    balance_currency = factory.SubFactory(CurrencyFactory)
    balance_unofficial_currency_code = None
    mask = '1234'
    name = faker.text(max_nb_chars=128)
    official_name = faker.text(max_nb_chars=128)
    type = 'investment'
    subtype = 'brokerage'
