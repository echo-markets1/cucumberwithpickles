from SCARIF.models.holding import Holding
from SCARIF.tests.factories.account_factory import AccountFactory
from SCARIF.tests.factories.security_factory import SecurityFactory
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class HoldingFactory(factory.DjangoModelFactory):
    class Meta:
        model = Holding
        strategy = BUILD_STRATEGY
        django_get_or_create = ('account', 'security',)

    account = factory.SubFactory(AccountFactory)
    security = factory.SubFactory(SecurityFactory)
    institution_price = factory.fuzzy.FuzzyDecimal(0, 4000, 5)
    institution_price_as_of = faker.date()
    institution_value = factory.fuzzy.FuzzyDecimal(0, 100000, 5)
    cost_basis = factory.fuzzy.FuzzyDecimal(0, 100000, 5)
    quantity = factory.fuzzy.FuzzyDecimal(0, 10000000, 5)
