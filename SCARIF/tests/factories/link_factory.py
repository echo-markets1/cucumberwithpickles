from SCARIF.models.link import Link
import factory
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class LinkFactory(factory.DjangoModelFactory):
    class Meta:
        model = Link
        strategy = BUILD_STRATEGY
        django_get_or_create = ('user_id',)

    user_id = faker.uuid4(cast_to=None)
