from SCARIF.tests.factories.account_factory import AccountFactory
from SCARIF.tests.factories.security_factory import SecurityFactory
from SCARIF.models.investment_transaction import InvestmentTransaction
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class InvestmentTransactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = InvestmentTransaction
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.uuid4(cast_to=str)
    cancel_transaction_id = faker.uuid4(cast_to=str)
    account = factory.SubFactory(AccountFactory)
    security = factory.SubFactory(SecurityFactory)
    date_posted = faker.date()
    date_canceled = faker.date()
    name = faker.text(max_nb_chars=128)
    quantity = factory.fuzzy.FuzzyDecimal(0, 100000, 5)
    amount = factory.fuzzy.FuzzyDecimal(0, 10000000, 5)
    price = factory.fuzzy.FuzzyDecimal(0, 1000, 5)
    fees = factory.fuzzy.FuzzyDecimal(0, 1000, 5)
    type = 'buy'
    subtype = 'buy'
