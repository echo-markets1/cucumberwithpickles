from SCARIF.tests.factories.currency_factory import CurrencyFactory
from SCARIF.models.security import Security
import factory
import factory.fuzzy
from factory import BUILD_STRATEGY
from faker import Factory
faker = Factory.create()


class SecurityFactory(factory.DjangoModelFactory):
    class Meta:
        model = Security
        strategy = BUILD_STRATEGY
        django_get_or_create = ('id',)

    id = faker.uuid4(cast_to=str)
    isin = faker.text(max_nb_chars=12)
    cusip = faker.text(max_nb_chars=9)
    sedol = faker.text(max_nb_chars=7)
    proxy_security_id = faker.uuid4(cast_to=str)
    name = faker.company()
    ticker_symbol = faker.text(max_nb_chars=24)
    is_cash_equivalent = faker.boolean()
    type = factory.fuzzy.FuzzyChoice(['cash', 'equity', 'etf', 'mutual_fund'])
    close_price = factory.fuzzy.FuzzyDecimal(0, 4000, 5)
    close_price_as_of = faker.date()
    currency = factory.SubFactory(CurrencyFactory)
    unofficial_currency_code = None
