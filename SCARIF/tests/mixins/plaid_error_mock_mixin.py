import plaid


class PlaidErrorMockMixin(object):
    """ Mixin to help mock raising errors from plaid """
    mock_plaid_error = plaid.errors.PlaidError(
        message="error_message",
        type="TYPE",
        code="CODE",
        display_message=None
    )
