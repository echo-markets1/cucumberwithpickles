from typing import Dict
import uuid
from SCARIF import models as scarif_models
from SCARIF.models.serializers import AccountSerializer, HoldingSerializer, InvestmentTransactionSerializer, LinkSerializer


# TODO handle DNE

def get_items(user_id: uuid.UUID = None) -> Dict:
    """
    Provides all items in a dictionary of user_id to list of user's items
    If user_id is not provided, this returns all links in the DB
    """
    if user_id is not None:
        links = scarif_models.Link.objects.filter(user_id=user_id)
    else:
        links = scarif_models.Link.objects.all()
    data = {}
    for link in links:
        link_data = LinkSerializer(link).data
        data[link_data['user_id']] = link_data['items']
    return data


def get_accounts_for_user_id(user_id: uuid.UUID) -> Dict:
    accounts = scarif_models.Account.objects.filter(
        item__user_link__user_id=user_id)
    return AccountSerializer(accounts, many=True).data


def get_holdings_for_user_id(user_id: uuid.UUID) -> Dict:
    holdings = scarif_models.Holding.objects.filter(
        account__item__user_link__user_id=user_id)
    return HoldingSerializer(holdings, many=True).data


def get_investment_transactions_for_user_id(user_id: uuid.UUID) -> Dict:
    investment_transactions = scarif_models.InvestmentTransaction.objects.filter(
        account__item__user_link__user_id=user_id)
    return InvestmentTransactionSerializer(investment_transactions, many=True).data
