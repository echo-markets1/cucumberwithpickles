from typing import Dict
import uuid
import plaid
from django.conf import settings

"""
All interactions with the Plaid Client should be centralized here.
Allows us to have a singleton client, and allows visibility as to all
ways we are interacting with the client. Also standardizes and simplifies
our interactions with it.

See settings.PLAID_SUPPORTED_PRODUCTS for a list of supported plaid endpoints
"""
# TODO reconfigure for prod [https://app.clickup.com/t/pezw8n]
client = plaid.Client(
    client_id=settings.PLAID_CLIENT_ID,
    secret=settings.PLAID_SECRET_KEYS[settings.PLAID_ENV],
    environment=settings.PLAID_ENV
)


def get_item(access_token: str) -> Dict:
    return client.Item.get(access_token)


def exchange_public_token(public_token: str) -> Dict:
    return client.Item.public_token.exchange(public_token)


def get_holdings(access_token: str) -> Dict:
    return client.Holdings.get(access_token)


def get_investment_transactions(access_token: str, start_date: str, end_date: str, count: int = 100, offset: int = 0) -> Dict:
    return client.InvestmentTransactions.get(
        access_token,
        start_date=start_date,
        end_date=end_date,
        count=count,
        offset=offset
    )


def get_institution_by_id(institution_id: str) -> Dict:
    return client.Institutions.get_by_id(
        institution_id=institution_id,
        country_codes=settings.PLAID_COUNTRY_CODES,
        _options={
            'include_optional_metadata': True
        }
    )


def create_link_token(user_id: uuid.UUID) -> Dict:
    return client.LinkToken.create(
        {
            'user': {
                # This should correspond to a unique id for the current user.
                'client_user_id': str(user_id),
            },
            'client_name': "Echo Markets",
            'products': settings.PLAID_SUPPORTED_PRODUCTS,
            'country_codes': settings.PLAID_COUNTRY_CODES,
            'language': "en",
            # TODO reconfigure for prod [https://app.clickup.com/t/pezw8n]
            # 'webhook': settings.domain + PLAID_WEBHOOK_PATH
            # temporary webhook until cucumber deployment:
            'webhook': 'https://echo-markets.herokuapp.com/handle_webhook/'
            # or use a request bin:
            # 'webhook': 'https://enr4axvvq1ig1pe.m.pipedream.net'
        }
    )


def fire_sandbox_webhook():
    return client.Sandbox.item.fire_webhook('access-sandbox-8cf38c4c-e278-476d-93ca-2c401e563766', 'DEFAULT_UPDATE')


def format_plaid_error(e: Dict) -> Dict:
    return {
        'error': {
            'display_message': e.display_message,
            'error_code': e.code,
            'error_type': e.type,
            'error_message': e.message
        }
    }
