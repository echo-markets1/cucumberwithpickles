from typing import List
from rest_framework.views import APIView
from rest_framework.response import Response

from SCARIF.core.token_exchange_flow import token_exchange_management
from SCARIF.core import webhook_handler
from SCARIF.models import Link, Item, Institution
from SCARIF.interfaces import plaid_client_interface


class FireWebhook(APIView):
    """
    This is a helper view to allow us to programmatically fire webhook for testing purposes

    Fire a POST request in the following format to update a single account
    request:
        - item_id (str): The id of the Plaid item for the account being updated
        - access_token (str): The access_token of the Plaid item for the account being updated

    Make a GET request (by visiting the api endpoint) to updated all items for an account.

    The POST is useful when there are issues with the items or links. Otherwise,
    the GET should be sufficient. You will know if the get request was successful
    if you see "Successfully updated <int> account(s)" in the response.
    """

    def post(self, request, format=None):
        def _get_or_create_dev_brokerage_account(user_id, item_id, access_token) -> Item:
            """
            Helper code to set up real data for experiementation
            """
            def _create_item(user_id) -> Item:
                link = Link.objects.get_or_create(user_id=user_id)
                return Item.objects.get_or_create(
                    id=item_id,
                    access_token=access_token,
                    user_link=link[0]
                )[0]

            def _set_item_institution(item: Item):
                """ Calls to initialize/retrieve Institution data may fail. This should
                    not cause us to fail the Item creation
                """
                try:
                    institution_id = plaid_client_interface.get_item(
                        access_token=item.access_token
                    )["item"]["institution_id"]
                    item.institution = Institution.objects.get_or_create(id=institution_id)[
                        0]
                    item.save()
                except Exception as e:
                    # Institution retrieval error should not fail the item creation process
                    # Log the error and continue as normal
                    print(f'ERROR: Exception occurred while handling retrieving '
                          #   f'Institution data for institution_id: {institution_id}'
                          f' Error: {e}'
                          )
                    pass
            item = _create_item(user_id)
            _set_item_institution(item)
            return item

        ### MAIN FUNCTION CODE ###
        item = _get_or_create_dev_brokerage_account(
            request.user.id, request.data["item_id"], request.data["access_token"])
        holdings_webhook = {
            "webhook_type": "HOLDINGS",
            "webhook_code": "DEFAULT_UPDATE",
            "item_id": item.id,
            "error": None,
            "new_holdings": 20,
            "updated_holdings": 0
        }
        investment_transactions_webhook = {
            "webhook_type": "INVESTMENTS_TRANSACTIONS",
            "webhook_code": "DEFAULT_UPDATE",
            "item_id": item.id,
            "error": None,
            "new_investments_transactions": 1,
            "canceled_investments_transactions": 0
        }
        token_exchange_management.fire_webhook(investment_transactions_webhook)
        (success, data) = token_exchange_management.fire_webhook(
            holdings_webhook)
        if success:
            return Response(data)
        else:
            return Response(data, status=400)

    def get(self, request, format=None):
        def _get_all_dev_brokerage_account_items(user_id) -> List[Item]:
            """
            Helper code to set up real data for experiementation. Assumes the Item
            instances and Link instance have already been created.
            """
            link = Link.objects.get(user_id=user_id)
            return Item.objects.filter(user_link=link)

        ### MAIN FUNCTION CODE ###
        items = _get_all_dev_brokerage_account_items(request.user.id)
        for item in items:
            holdings_webhook = {
                "webhook_type": "HOLDINGS",
                "webhook_code": "DEFAULT_UPDATE",
                "item_id": item.id,
                "error": None,
                "new_holdings": 20,
                "updated_holdings": 0
            }
            investment_transactions_webhook = {
                "webhook_type": "INVESTMENTS_TRANSACTIONS",
                "webhook_code": "DEFAULT_UPDATE",
                "item_id": item.id,
                "error": None,
                "new_investments_transactions": 1,
                "canceled_investments_transactions": 0
            }
            token_exchange_management.fire_webhook(
                investment_transactions_webhook)
            (success, data) = token_exchange_management.fire_webhook(
                holdings_webhook)
            if not success:
                return Response(data, status=400)
        return Response(f'Successfully updated {items.count()} account(s)')


class HandleWebhook(APIView):
    """
    This handles webhooks triggered by Plaid when info is availible for update. 
    It should trigger the correct core logic to update our copy of Plaid's data
    """
    permission_classes = []
    authentication_classes = []

    def post(self, request, format=None):
        print("test webhook recieved (POST) ******************")
        print(request.data)
        webhook_handler.handle(request.data)
        # PRODUCE EVENT: Webhook recieved for item_id, include access token
        return Response()
