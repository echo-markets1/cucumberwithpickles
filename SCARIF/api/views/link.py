from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.conf import settings

from SCARIF.core.token_exchange_flow import token_exchange_management


class CreateLinkToken(APIView):
    """
    The first step in linking a user's account. Provides a link_token that initializes the Plaid frontend module
    """

    def post(self, request, format=None):
        (success, data) = token_exchange_management.create_link_token(request.user.id)
        if success:
            return Response(data=data, content_type="application/json", status=status.HTTP_200_OK)
        else:
            return Response(data=data, status=400)


class SetAccessToken(APIView):
    """
    Exchanges the public_token recieved by the Plaid frontend module with the secret access_token
    """

    def post(self, request, format=None):
        (success, data) = token_exchange_management.set_access_token(
            request.data['public_token'], request.user.id)
        if success:
            return Response(data=data, content_type="application/json", status=status.HTTP_200_OK)
        else:
            return Response(data, status=400)
