from django.urls import path

from SCARIF.api.views import link as link_views
from SCARIF.api.views import webhook as webhook_views
from SCARIF.constants.plaid_constants import WEBHOOK_PATH


urlpatterns = [

    path('plaid-link-management/create-link-token',
         link_views.CreateLinkToken.as_view(), name='create-link-token'),
    path('plaid-link-management/set-access-token',
         link_views.SetAccessToken.as_view(), name='get-access-token'),

    path(WEBHOOK_PATH, webhook_views.HandleWebhook.as_view(), name='handle-webhook'),
    path('fire-webhook', webhook_views.FireWebhook.as_view(), name='fire-webhook'),

]
