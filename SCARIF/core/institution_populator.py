import plaid
from typing import Dict, Tuple
from SCARIF.constants.plaid_constants import INSTITUTION_DATA_KEY
from SCARIF.interfaces import plaid_client_interface


def populate(institution) -> Tuple[bool, Dict]:
    """
    Populate the db with institutional data from plaid

    Parameters:
        - institution (Institution): The institution instance to populate
    """
    institution_response = {}
    try:
        institution_response = plaid_client_interface.get_institution_by_id(
            institution.id)
    except plaid.errors.PlaidError as e:
        # PRODUCE EVENT: Institution fields population failed
        return (False, plaid_client_interface.format_plaid_error(e))

    # Importing inline to avoid circular imports
    from SCARIF.models.serializers import InstitutionDataSerializer
    serializer = InstitutionDataSerializer(
        institution, institution_response[INSTITUTION_DATA_KEY])
    serializer.is_valid()
    serializer.save()

    print(f'INFO: Successfully populated institutional data for ' +
          f'institution_id: {institution.id}'
          )
    # PRODUCE EVENT: Institution fields populated
    return (True, institution_response)
