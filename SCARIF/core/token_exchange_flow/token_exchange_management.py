from typing import Dict, Tuple
import uuid
from SCARIF.core import webhook_handler
from SCARIF.core.transformers import item_data_transformer
from SCARIF.interfaces import plaid_client_interface
import plaid

# TODO: Create Item deletion workflow [https://app.clickup.com/t/pezdwq]


def create_link_token(user_id: uuid.UUID) -> Tuple[bool, Dict]:
    response = None
    try:
        response = plaid_client_interface.create_link_token(user_id)
    except plaid.errors.PlaidError as e:
        return _handle_plaid_error(e)
    # PRODUCE EVENT: Link Token Created
    return (True, response)


def set_access_token(public_token, user_id: uuid.UUID) -> Tuple[bool, Dict]:
    exchange_response = None
    try:
        exchange_response = plaid_client_interface.exchange_public_token(
            public_token)
    except plaid.errors.PlaidError as e:
        return _handle_plaid_error(e)

    item_data_transformer.transform(
        access_token=exchange_response['access_token'],
        item_id=exchange_response['item_id'],
        user_id=user_id
    )
    # TODO: Cue asynchronous call to load users investments data [https://app.clickup.com/t/pezbqf]
    # PRODUCE EVENT: Access token set for item_id
    return (True, exchange_response)


def fire_webhook(webhook):
    ### For Sandbox testing. Not Production Code ###
    try:
        webhook_handler.handle(webhook)
        plaid_client_interface.fire_sandbox_webhook()
    except plaid.errors.PlaidError as e:
        _handle_plaid_error(e)
    print("DEFAULT_UPDATE webhook fired")
    return (True, "Success")


def _handle_plaid_error(error: Dict) -> Tuple[bool, Dict]:
    return (False, plaid_client_interface.format_plaid_error(error))
