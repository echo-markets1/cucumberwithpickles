from datetime import date
from typing import Tuple
from SCARIF.core.investments import investments_response_preprocessor
from SCARIF.core.transformers import investment_transactions_data_transformer
from SCARIF.constants.plaid_constants import ACCOUNTS_DATA_KEY, INVESTMENT_TRANSACTIONS_DATA_KEY, SECURITIES_DATA_KEY, TOTAL_INVESTMENT_TRANSACTIONS_KEY
from SCARIF.interfaces import plaid_client_interface
import plaid

from SCARIF.models import Item


def update(item: Item, start_date: date, end_date: date) -> Tuple[int, int]:
    """
    For a given Item, retrieves and updates all investment_transactions
    between start_date and end_date. Keeps increasing the offset and calling
    plaid until there are no more new investment_transactions to store.
    Transform the response into our internal representations. 

    item = SCARIF.models.item.Item
    start_date = datetime.date
    end_date = datetime.date

    Return: tuple with the number InvestmentTransaction created and updated
    """

    offset = 0
    count = 500
    has_more_investments = True
    total_created = 0
    total_canceled = 0
    num_api_calls_with_response = 0
    while(has_more_investments):
        # Keep pulling investment_transactions for the time period until they
        # have all been collected
        investment_transactions_response = {}
        try:
            investment_transactions_response = plaid_client_interface.get_investment_transactions(
                access_token=item.access_token,
                start_date=start_date.isoformat(),
                end_date=end_date.isoformat(),
                count=count,
                offset=offset
            )
        except plaid.errors.PlaidError as e:
            print(f'WARN: Recieved an error in response to "get_investment_transactions" ' +
                  f'for item.id: {item.id}. Error: {plaid_client_interface.format_plaid_error(e)}')
            return (total_created, total_canceled)

        print(f'INFO: Retrieved investment transactions for item_id: {item.id} ' +
              f'request_id: {investment_transactions_response["request_id"]}')

        total_investment_transactions_returned = investment_transactions_response.get(
            TOTAL_INVESTMENT_TRANSACTIONS_KEY,
            0
        )
        if total_investment_transactions_returned > 0:
            num_api_calls_with_response += 1
            investments_response_preprocessor.preprocess(
                accounts_data=investment_transactions_response[ACCOUNTS_DATA_KEY],
                item=item,
                securities_data=investment_transactions_response[SECURITIES_DATA_KEY]
            )
            (num_created, num_canceled) = investment_transactions_data_transformer.transform(
                investment_transactions_response[INVESTMENT_TRANSACTIONS_DATA_KEY]
            )
            total_created = total_created + num_created
            total_canceled = total_canceled + num_canceled
            if num_created < total_investment_transactions_returned:
                # If we created less investment_transactions than provided by the
                # response, it means we have proccessed all of the
                # new investment_transactions
                has_more_investments = False
            else:
                offset = offset + count
        else:
            # If the response was empty, there are no more investment_transactions
            # to process
            has_more_investments = False

    print(f'INFO: Finished updating InvestmentTransactions. Created: ' +
          f'{total_created}. Canceled: {total_canceled}. Number of ' +
          f'get_investment_transactions calls: {num_api_calls_with_response} '
          )
    return (total_created, total_canceled)
