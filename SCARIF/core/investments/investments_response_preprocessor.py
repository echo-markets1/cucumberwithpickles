from typing import List
from SCARIF.core.transformers import accounts_data_transformer, securities_data_transformer
from SCARIF.models import Item


def preprocess(accounts_data: List, item: Item, securities_data: List):
    """
    Preprocesses data returned from calls to plaid's Investments endpoints:

    https://plaid.com/docs/api/products/#investments

    or interntally as:

        - plaid_client_interface.get_holdings
        - plaid_client_interface.get_investment_transactions

    This must always run before holdings or investment_transactions data
    can be updated
    """
    # Update and create any account data returned from plaid.
    accounts_data_transformer.transform(
        accounts_data=accounts_data,
        item=item
    )

    # Update and create any security data returned from plaid.
    securities_data_transformer.transform(securities_data)
