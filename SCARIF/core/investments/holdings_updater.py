
from typing import Tuple
from SCARIF.constants.plaid_constants import ACCOUNTS_DATA_KEY, HOLDINGS_DATA_KEY, SECURITIES_DATA_KEY
from SCARIF.core.investments import investments_response_preprocessor
from SCARIF.core.transformers import holdings_data_transformer
from SCARIF.interfaces import plaid_client_interface
import plaid

from SCARIF.models import Item


def update(item: Item) -> Tuple[int, int]:
    """
    Call plaid to retrieve updated Holdings info for a given Item. Transform
    the response into our internal representations. Return a tuple with the
    number Holdings created and updated

    item = SCARIF.models.item.Item

    Return: tuple with the number InvestmentTransaction created and updated
    """
    num_creates = 0
    num_updates = 0
    holdings_response = {}
    try:
        holdings_response = plaid_client_interface.get_holdings(
            item.access_token)
    except plaid.errors.PlaidError as e:
        print(f'WARN: Recieved an error in response to "get_holdings" ' +
              f'for item.id: {item.id}. Error: {plaid_client_interface.format_plaid_error(e)}')
        # Return 0 creates and 0 updates
        return (num_creates, num_updates)

    print(f'INFO: Retrieved holdings for item_id: {item.id} ' +
          f'request_id: {holdings_response["request_id"]}')

    # First, update and create any account and securities data returned from plaid.
    # The holdings response is dependent on this data being stored to the db
    investments_response_preprocessor.preprocess(
        accounts_data=holdings_response[ACCOUNTS_DATA_KEY],
        item=item,
        securities_data=holdings_response[SECURITIES_DATA_KEY]
    )

    # Then, update the db with holdings returned from plaid. Return the results
    # so we can compare with the number of holdings we expected to update
    (num_creates, num_updates) = holdings_data_transformer.transform(
        holdings_response[HOLDINGS_DATA_KEY])
    print(
        f'Finished updating Holdings. Created: {num_creates}. Updated {num_updates}')
    return (num_creates, num_updates)
