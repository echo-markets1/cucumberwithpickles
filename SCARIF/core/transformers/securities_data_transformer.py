
from typing import Dict, List, Tuple
from django.core.exceptions import ObjectDoesNotExist
from SCARIF.models import Security


def transform(securities_data: List) -> Tuple[int, int]:
    """
    Transforms securities data from plaid to create new Security objects
    and update any existing Security. Return a tuple with the number of
    Security created and updated
    """
    ### Private inner functions ###
    def _create_security(security_data: Dict):
        serializer = SecuritiesDataSerializer(data=security_data)
        serializer.is_valid()
        return serializer.save()

    def _update_security(security: Security, security_data: Dict):
        serializer = SecuritiesDataSerializer(security, data=security_data)
        serializer.is_valid()
        return serializer.save()

    ### Main logic ###
    num_updates = 0
    num_creates = 0

    # Importing inline to avoid circular imports
    from SCARIF.models.serializers import SecuritiesDataSerializer
    for security_data in securities_data:
        try:
            security = Security.objects.get(id=security_data["security_id"])
            # Update the Security if it already exists in the db
            security = _update_security(security, security_data)
            num_updates += 1
        except ObjectDoesNotExist:
            # Create the security object if it does not already exist
            security = _create_security(security_data)
            num_creates += 1

    print(
        f'INFO: Securities created: {num_creates}. Securities updated: {num_updates}')

    return (num_creates, num_updates)
