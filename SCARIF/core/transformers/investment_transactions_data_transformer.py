from typing import Dict, List
from SCARIF.models.investment_transaction import InvestmentTransaction
from django.core.exceptions import ObjectDoesNotExist


def transform(investment_transactions_data: List):
    """
    Transforms investment_transactions data from plaid to create new
    InvestmentTransactions and update any canceled InvestmentTransaction.
    Return a tuple with the number InvestmentTransactions created and canceled
    """
    ### Private inner functions ###
    def _cancel_investment_transaction(investment_transaction_data: Dict):
        """
        Cancel the transaction with id == cancel_transaction_id. 
        Return True when successful, False when Exception occurs
        """
        try:
            canceled_transaction = InvestmentTransaction.objects.get(
                id=cancel_transaction_id)
            serializer = InvestmentTransactionsDataSerializer(
                canceled_transaction, data=investment_transaction_data)
            serializer.is_valid()
            serializer.save()
            return True
        except ObjectDoesNotExist:
            print(f'WARN: Failure to cancel investment_transaction. No ' +
                  f'investment_transaction with id: {cancel_transaction_id} ' +
                  f'was found'
                  )
            return False

    def _create_investment_transaction(investment_transaction_data: Dict):
        serializer = InvestmentTransactionsDataSerializer(
            data=investment_transaction_data)
        serializer.is_valid()
        serializer.save()

    ### Main logic ###
    num_creates = 0
    num_cancels = 0

    # Importing inline to avoid circular imports
    from SCARIF.models.serializers import InvestmentTransactionsDataSerializer
    # Reverse the order, this way we can process the investments in the
    # order they occurred. This will allow us to properly cancel past
    # transactions
    for investment_transaction_data in reversed(investment_transactions_data):
        if not InvestmentTransaction.objects.filter(id=investment_transaction_data["investment_transaction_id"]).exists():
            _create_investment_transaction(investment_transaction_data)
            num_creates += 1

            cancel_transaction_id = investment_transaction_data["cancel_transaction_id"]
            if cancel_transaction_id:
                cancel_success = _cancel_investment_transaction(
                    investment_transaction_data)
                if cancel_success:
                    num_cancels += 1

    print(
        f'INFO: InvestmentTransactions created: {num_creates}. ' +
        f'InvestmentTransactions canceled: {num_cancels}')
    return (num_creates, num_cancels)
