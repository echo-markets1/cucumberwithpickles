from typing import Dict, List, Tuple
from SCARIF.models.holding import Holding
from django.core.exceptions import ObjectDoesNotExist


def transform(holdings_data: List) -> Tuple[int, int]:
    """
    Transforms holdings data from plaid to create new Holding objects
    and update any existing Holdings. Return a tuple with the number of
    Holdings created and updated
    """
    ### Private inner functions ###
    def _create_holding(holding_data: Dict):
        serializer = HoldingsDataSerializer(data=holding_data)
        serializer.is_valid()
        serializer.save()

    def _update_holding(holding: Holding, holding_data: Dict):
        serializer = HoldingsDataSerializer(holding, data=holding_data)
        serializer.is_valid()
        serializer.save()

    def _delete_holdings_not_updated(updated_holdings: Dict):
        """
        Holdings that are not updated imply that they are no longer held in the account,
        and should be deleted.

        Params:
            - updated_holdings (Dict): A mapping of account_id to related security_ids
                whose holdings were updated
        """
        for account_id, updated_security_ids in updated_holdings.items():
            outdated_holdings = Holding.objects.filter(
                account__id=account_id).exclude(security__id__in=updated_security_ids)
            (num_deleted, deleted_entries) = outdated_holdings.delete()
            print(
                f"INFO: {num_deleted} holdings were deleted from account {account_id}. ")

    ### Main logic ###
    num_creates = 0
    num_updates = 0
    updated_holdings = {
        # account_id: [security_ids]
    }
    from SCARIF.models.serializers.holding import HoldingsDataSerializer
    for holding_data in holdings_data:
        account_id = holding_data["account_id"]
        security_id = holding_data["security_id"]
        try:
            holding = Holding.objects.get(
                security__id=security_id, account__id=account_id)
            _update_holding(holding, holding_data)
            num_updates += 1
        except ObjectDoesNotExist:
            _create_holding(holding_data)
            num_creates += 1
        if not (account_id in updated_holdings):
            updated_holdings[account_id] = []
        updated_holdings[account_id].append(security_id)

    _delete_holdings_not_updated(updated_holdings)

    print(
        f'INFO: Holdings created: {num_creates}. Holdings updated: {num_updates}')
    return (num_creates, num_updates)
