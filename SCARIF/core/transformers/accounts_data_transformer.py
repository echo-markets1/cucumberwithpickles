
from typing import Dict, List, Tuple
from django.core.exceptions import ObjectDoesNotExist
from SCARIF.models import Account, Item


def transform(accounts_data: List, item: Item) -> Tuple[int, int]:
    """
    Transforms securities data from plaid to create new Account objects
    and update any existing Account. Return a tuple with the number of
    Account created and updated
    """
    ### Private inner functions ###
    def _create_account(account_data: Dict, item: Item):
        serializer = AccountsDataSerializer(data=account_data)
        serializer.is_valid()
        # Plaid does not explicitly supply the item -> account relationships,
        # so we must append the item to plaid accounts data
        serializer.save(item=item)

    def _update_account(account: Account, account_data: Dict):
        serializer = AccountsDataSerializer(account, data=account_data)
        serializer.is_valid()
        serializer.save()

    ### Main logic ###
    num_updates = 0
    num_creates = 0

    # Importing inline to avoid circular imports
    from SCARIF.models.serializers import AccountsDataSerializer
    for account_data in accounts_data:
        # We only care about Investment accounts while 'investments' is the only
        # supported plaid endpoint
        if account_data["type"] == Account.Type.INVESTMENT:
            try:
                account = Account.objects.get(id=account_data["account_id"])
                # Update the account if it already exists in the db
                _update_account(account, account_data)
                num_updates += 1
            except ObjectDoesNotExist:
                # Create the Account object if it does not already exist
                _create_account(account_data, item)
                num_creates += 1

    print(
        f'INFO: Accounts created: {num_creates}. Accounts updated: {num_updates}')
    return (num_creates, num_updates)
