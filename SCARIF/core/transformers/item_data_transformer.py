import uuid
from SCARIF.models.institution import Institution
from SCARIF.models.item import Item
from SCARIF.models.link import Link
from SCARIF.interfaces import plaid_client_interface
import plaid


def transform(access_token: str, item_id: str, user_id: uuid.UUID):
    """ Transforms an access_token, item_id, and user_id into an Item """

    def _build_item_essentials(access_token: str, item_id: str, user_id: uuid.UUID):
        """ Builds the essential Item information, incase institution workflow fails """
        (link, created) = Link.objects.get_or_create(user_id=user_id)
        return Item(
            id=item_id,
            access_token=access_token,
            user_link=link
        )

    item = _build_item_essentials(access_token, item_id, user_id)
    # Calls to initialize/retrieve Institution data may fail. This should
    # not cause us to fail the Item creation
    institution_assignment_success = False
    institution_id = None
    try:
        institution_id = plaid_client_interface.get_item(
            access_token)["item"]["institution_id"]
        (item.institution, created) = Institution.objects.get_or_create(
            id=institution_id)
        if item.institution.name:
            # Check to see that the institution was populated
            institution_assignment_success = True
    except plaid.errors.PlaidError as e:
        # Institution retrieval error should not fail the item creation process
        # Log the error and continue as normal
        if institution_id:
            print(f'ERROR: Exception occurred while retrieving Institution data ' +
                  f'for institution_id: {institution_id} ' +
                  f'Error: {e}'
                  )
        else:
            print(f'ERROR: Exception occurred while retrieving Institution data. ' +
                  f'Could not retrieve Item from plaid for item_id: {item_id} ' +
                  f'Error: {e}'
                  )
        pass
    item.save()
    # Return the Item and whether or not institution creation was successful
    return (institution_assignment_success, item)
