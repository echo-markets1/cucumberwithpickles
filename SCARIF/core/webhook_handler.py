# https://plaid.com/docs/api/webhooks/#investments-webhooks
from typing import Dict, Tuple
from SCARIF.constants.plaid_constants import (
    CANCELED_INVESTMENTS_TRANSACTIONS, HOLDINGS_WEBHOOK,
    INVESTMENTS_TRANSACTIONS_WEBHOOK, NEW_HOLDINGS,
    NEW_INVESTMENT_TRANSACTIONS, UPDATED_HOLDINGS, WEBHOOK_TYPE
)
from django.core.exceptions import ObjectDoesNotExist
from SCARIF.core.investments import holdings_updater, investment_transactions_updater
from SCARIF.models.item import Item
import datetime

# TODO: Validate webhooks originate from Plaid [https://app.clickup.com/t/pezedp]


def handle(webhook: Dict):
    """
    Routes a webhook from plaid to the correct handling logic. Compares
    the expected updates provided by the webhook to the updates we
    performed to help detect data loss.
    """

    ### Private inner functions ##
    def _get_item_from_webhook(item_id: str) -> Item:
        try:
            return Item.objects.get(id=item_id)
        except ObjectDoesNotExist as e:
            print(f'ERROR: Exception occurred while handling HOLDINGS webhook. Unable ' +
                  f'to update new transactions. Could not find Item for id: {item_id}. {e}'
                  )
            raise e

    def _compare_updates(expected_num_created: int, expected_num_updated: int,
                         actual_num_created: int, actual_num_updated: int, webhook_type: str):
        missed_creation_warning = (f'WARN: Created {actual_num_created} ' +
                                   f'{webhook_type}. Expected to create {expected_num_created}.'
                                   )
        missed_updated_warning = (f'WARN: Updated {actual_num_updated} ' +
                                  f'{webhook_type}. Expected to update {expected_num_updated}.'
                                  )
        if expected_num_created != actual_num_created:
            print(missed_creation_warning)
        if expected_num_updated != actual_num_updated:
            print(missed_updated_warning)

    ### Main logic ###
    item = _get_item_from_webhook(webhook["item_id"])
    webhook_type = webhook[WEBHOOK_TYPE]
    print(f'INFO: Received {webhook_type} webhook with data: {webhook} ')
    if webhook_type == INVESTMENTS_TRANSACTIONS_WEBHOOK:
        new_investments_transactions = webhook[NEW_INVESTMENT_TRANSACTIONS]
        canceled_investments_transactions = webhook[CANCELED_INVESTMENTS_TRANSACTIONS]
        if new_investments_transactions + canceled_investments_transactions > 0:
            (actual_num_created,
             actual_num_updated) = _handle_investment_transactions_webhook(item)
            _compare_updates(
                expected_num_created=new_investments_transactions,
                expected_num_updated=canceled_investments_transactions,
                actual_num_created=actual_num_created,
                actual_num_updated=actual_num_updated,
                webhook_type=webhook_type
            )

    elif webhook_type == HOLDINGS_WEBHOOK:
        new_holdings = webhook[NEW_HOLDINGS]
        updated_holdings = webhook[UPDATED_HOLDINGS]
        if new_holdings + updated_holdings > 0:
            (actual_num_created, actual_num_updated) = _handle_holdings_webhook(item)
            _compare_updates(
                expected_num_created=new_holdings,
                expected_num_updated=updated_holdings,
                actual_num_created=actual_num_created,
                actual_num_updated=actual_num_updated,
                webhook_type=webhook_type
            )
    else:
        print(f'INFO: Recieved unexpected webhook of type: {webhook_type}')

    print(f'INFO: Handled {webhook_type} webhook with data: {webhook}')


def _handle_investment_transactions_webhook(item: Item) -> Tuple[int, int]:
    end_date = datetime.date.today()
    # Max market closure is 4 days, + 1 day for flexibility
    five_days = datetime.timedelta(days=500)
    start_date = end_date - five_days
    return investment_transactions_updater.update(item, start_date, end_date)


def _handle_holdings_webhook(item: Item) -> Tuple[int, int]:
    return holdings_updater.update(item)
