from django.contrib import admin

from SCARIF.models import Holding, Security, InvestmentTransaction, Link, Item, Account, InstitutionalSecurityId, Institution
from SCARIF.models.currencies import Currency

# Register your models here.

admin.site.register(Holding)
admin.site.register(Security)
admin.site.register(InvestmentTransaction)
admin.site.register(Account)
admin.site.register(Item)
admin.site.register(Link)
admin.site.register(Currency)
admin.site.register(InstitutionalSecurityId)
admin.site.register(Institution)
