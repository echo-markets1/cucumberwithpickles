# Getting Started

To learn more about the cucumberwithpickles project, the technologies involved,
and the services involved, see the
[project overview](https://share-docs.clickup.com/2268607/p/h/257dz-1947/09967e12532d5a9) on
ClickUp.

# Development

To learn more about using and developing for the cucumberwithpickles project,
see the [development docs](https://share-docs.clickup.com/2268607/p/h/257dz-1927/accc093e3137b18)
on ClickUp
